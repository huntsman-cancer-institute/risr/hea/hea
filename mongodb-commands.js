// HEA_REGISTRY
db.components.createIndex({owner: 1, name: 1}, {name: "idx_components_owner_and_name", unique: true, partialFilterExpression: {$and: [{owner: {$exists: true}}, {owner: {$type: "string"}}, {name: {$exists: true}}, {name: {$type: "string"}}]}});
db.components.createIndex({name: 1}, {name: "index_components_name"});
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-registry", description: "Registry of HEA services, HEA web clients, and other web sites of interest", display_name: "HEA Registry Service", base_url: "http://heaserver-registry:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.registry.Component", base_path: "components"}, {type: "heaobject.registry.Resource", resource_type_name: "heaobject.registry.Property", base_path: "properties"}]});


// HEA_ORGANIZATION
db.organizations.createIndex({name: 1}, {name: "idx_organizations_name", unique: true, partialFilterExpression: {$and: [{name: {$exists: true}}, {name: {$type: "string"}}]}});
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-organizations", description: "Repository of organizations", display_name: "HEA ORGANIZATION Service", base_url: "http://heaserver-organizations:8087", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.organization.Organization", base_path: "organizations"}]});
db.organizations.insertOne({type: "heaobject.organization.Organization", name: "SYSTEM_LAB", display_name: "System Lab", description: "System Lab for Test", created: ISODate(), owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: [ "VIEWER", "EDITOR" ] } ] });
db.organizations.insertOne({type: "heaobject.organization.Organization", name: "BRAD_LAB", display_name: "Brad Lab", description: "Brad Lab", created: ISODate(), owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: [ "VIEWER" ] } ] });
db.organizations.insertOne({type: "heaobject.organization.Organization", name: "TEST_LONG_NAME_LAB", display_name: "Test Longer Name Lab with a Very Long First Name and a Very Long Last Name", description: "Test Longer Name Lab ", created: ISODate(), owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: [ "VIEWER" ] } ] });

// HEA_FOLDERS
db.folders.createIndex({owner: 1, name: 1}, {name: "idx_folders_owner_and_name", unique: true, partialFilterExpression: {$and: [{owner: {$exists: true}}, {owner: {$type: "string"}}, {name: {$exists: true}}, {name: {$type: "string"}}]}});
db.folders_items.createIndex({owner: 1, name: 1}, {name: "idx_folders_children_owner_and_name", unique: true, partialFilterExpression: {$and: [{owner: {$exists: true}}, {owner: {$type: "string"}}, {name: {$exists: true}}, {name: {$type: "string"}}]}});
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-folders", description: "Repository of folders", display_name: "HEA Folder Service", base_url: "http://heaserver-folders:8086", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.folder.Folder", base_path: "folders"}, {type: "heaobject.registry.Resource", resource_type_name: "heaobject.folder.Item", base_path: "items"}]});

// HEA_DATA_ADAPTERS
db.dataadapters.createIndex({owner: 1, name: 1}, {name: "idx_data_adapters_owner_and_name", unique: true, partialFilterExpression: {$and: [{owner: {$exists: true}}, {owner: {$type: "string"}}, {name: {$exists: true}}, {name: {$type: "string"}}]}});
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-data-adapters", description: "Repository of data adapters", display_name: "HEA Data Adapter Service", base_url: "http://heaserver-data-adapters:8082", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.dataadapter.DataAdapter", base_path: "dataadapters"}]});

// HEA_PEOPLE
// No index because we get people directly from Keycloak.
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-people", description: "Repository of people", display_name: "HEA People Service", base_url: "http://heaserver-people:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.person.Person", base_path: "people"}]});

// HEA_KEYCHAIN
db.credentials.createIndex({owner: 1, name: 1}, {name: "idx_credentials_owner_and_name", unique: true, partialFilterExpression: {$and: [{owner: {$exists: true}}, {owner: {$type: "string"}}, {name: {$exists: true}}, {name: {$type: "string"}}]}});
db.credentials.createIndex({name: 1}, {name: "index_credentials_name"});
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-keychain", description: "Repository of user credentials", display_name: "HEA Keychain Service", base_url: "http://heaserver-keychain:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.keychain.Credentials", base_path: "credentials"}]});

// HEA_FILESYSTEMS
db.filesystems.createIndex({name: 1, type: 1}, {name: "idx_filesystems_name_and_type", unique: true, partialFilterExpression: {$and: [{name: {$exists: true}}, {name: {$type: "string"}}, {type: {$exists: true}}, {type: {$type: "string"}}]}});
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-filesystems", description: "Collection of file systems", display_name: "HEA Volumes Service (File Systems)", base_url: "http://heaserver-volumes:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.volume.FileSystem", base_path: "filesystems"}]});
db.filesystems.insertOne({type: "heaobject.volume.AWSFileSystem", name: "DEFAULT_FILE_SYSTEM", display_name: "AWS", description: "Amazon Web Services", created: ISODate(), owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: [ "VIEWER" ] } ] });
db.filesystems.insertOne({type: "heaobject.volume.MongoDBFileSystem", name: "DEFAULT_FILE_SYSTEM", display_name: "System", description: "System object storage", created: ISODate(), owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: [ "VIEWER" ] } ] });

// HEA_VOLUMES
db.volumes.createIndex({owner: 1, name: 1}, {name: "idx_volumes_owner_and_name", unique: true, partialFilterExpression: {$and: [{owner: {$exists: true}}, {owner: {$type: "string"}}, {name: {$exists: true}}, {name: {$type: "string"}}]}});
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-volumes", description: "Collection of volumes", display_name: "HEA Volumes Service (Volumes)", base_url: "http://heaserver-volumes:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.volume.Volume", base_path: "volumes"}]});
db.volumes.insertOne({display_name: "System", name: "SYSTEM", created: new ISODate(), owner: 'system|none', type: "heaobject.volume.Volume", file_system_name: "DEFAULT_FILE_SYSTEM", file_system_type: "heaobject.volume.MongoDBFileSystem"})

// HEA_ACCOUNTS
// No index because we get accounts directly from AWS.
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-accounts", description: "Collection of user accounts", display_name: "HEA Accounts Service", base_url: "http://heaserver-accounts:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.account.AWSAccount", file_system_type: "heaobject.volume.AWSFileSystem", base_path: "volumes"}]});

// HEA_BUCKETS
// No index because we get buckets directly from AWS.
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-buckets", description: "Collection of user buckets", display_name: "HEA Buckets Service", base_url: "http://heaserver-buckets:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.bucket.AWSBucket", file_system_type: "heaobject.volume.AWSFileSystem", base_path: "volumes"}, {type: "heaobject.registry.Resource", resource_type_name: "heaobject.folder.AWSS3BucketItem", file_system_type: "heaobject.volume.AWSFileSystem", base_path: "volumes"}]});

// HEA_FOLDERS_AWS_S3
// No index because we get buckets directly from AWS.
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-folders-aws-s3", description: "Repository of folders from AWS S3 buckets", display_name: "HEA AWS S3 Bucket Folder Service", base_url: "http://heaserver-folders-aws-s3:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.folder.AWSS3Folder", base_path: "volumes", file_system_type: "heaobject.volume.AWSFileSystem"}, {type: "heaobject.registry.Resource", resource_type_name: "heaobject.project.AWSS3Project", base_path: "volumes", file_system_type: "heaobject.volume.AWSFileSystem"}, {type: "heaobject.registry.Resource", resource_type_name: "heaobject.folder.AWSS3Item", base_path: "volumes", file_system_type: "heaobject.volume.AWSFileSystem"}]});
db.awss3foldersmetadata.createIndex({bucket_id: 1, encoded_key: 1}, {name: "idx_awss3foldersmetadata_bucket_and_key", unique: true});    

// HEA_FILES_AWS_S3
// No index because we get buckets directly from AWS.
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-files-aws-s3", description: "Files from AWS S3 buckets", display_name: "HEA AWS S3 Bucket Folder Service", base_url: "http://heaserver-files-aws-s3:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.data.AWSS3FileObject", base_path: "volumes", file_system_type: "heaobject.volume.AWSFileSystem"}]});

// HEA_ACTIVITY
// Desktop object actions
db.desktopobjectactions.createIndex({owner: 1, name: 1}, {name: "idx_desktop_object_actions_owner_and_name", unique: true, partialFilterExpression: {$and: [{owner: {$exists: true}}, {owner: {$type: "string"}}, {name: {$exists: true}}, {name: {$type: "string"}}]}});
db.desktopobjectactions.createIndex({status_updated: -1}, {name: "idx_desktop_object_actions_status_updated"});
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-activity", description: "Repository for tracking activity in HEA", display_name: "HEA Activity Service", base_url: "http://heaserver-activity:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.activity.DesktopObjectAction", base_path: "desktopobjectactions"}]});

// HEA_STORAGE
// No index because we get storage directly from AWS.
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-storage", description: "Collection of AWS account storage", display_name: "HEA Storage Service", base_url: "http://heaserver-storage:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.storage.AWSStorage", file_system_type: "heaobject.volume.AWSFileSystem", base_path: "volumes"}]});

// HEA_TRASH_AWS_S3
// No index because we get storage directly from AWS.
db.components.insertOne({type: "heaobject.registry.Component", created: new ISODate(), name: "heaserver-trash-aws-s3", description: "Trash for AWS S3 storage", display_name: "HEA AWS S3 Trash Service", base_url: "http://heaserver-trash-aws-s3:8080", owner: 'system|none', shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: ["VIEWER"]}], resources: [{type: "heaobject.registry.Resource", resource_type_name: "heaobject.trash.AWSS3FolderFileTrashItem", file_system_type: "heaobject.volume.AWSFileSystem", base_path: "volumes/{id}/awss3trash"}]});
