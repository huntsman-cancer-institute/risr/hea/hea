"""Populates the HEA database, using the MongoDB commands in the mongodb-commands-*.js files in the root of the hea
directory. It creates all collections and indexes used by HEA. It also creates heaserver-registry entries for all HEA 
microservices. The database is versioned, and the current version is stored in the .current_data_version file in the
hea directory. 

To add custom database objects that are specific to your HEA instance, create a file called custom-mongodb-commands.js 
in the root of the hea directory prior to running this script for the first time, and add the commands for inserting 
and updating your custom objects there. This file is in the hea project's .gitignore. You can also create 
custom-mongodb-commands-<version>.js files to update your custom objects and add new objects alongside a new release of
the HEA database, where <version> is the version of the hea database to which the custom MongoDB commands apply. Your 
custom-mongodb-commands-<version>.js files will be processed after the packaged database updates for that version. 

The --reset option deletes all collections and indexes, including custom collections and objects, before 
re-creating the database including running the commands in your custom files. YOU WILL LOSE ALL DATA! Create a 
custom-mongodb-reset.js file in the root of hea directory to delete custom objects during a reset.

The command files, including your custom command files, contain mongodb commands, one per line. Quotes must be ' not ".

The script uses the .env file in the hea directory to get the MongoDB connection information. See the README.md file
for the required environment variables.
"""
import os
import os.path
import argparse
import sys
from string import whitespace
from typing import Optional

"""
The command files are processed in order of the keys in the COMMAND_FILES dictionary. The keys are the target version of
the HEA data model. The values are the names of the command files to execute for that version. The command files should
be named mongodb-commands-<version>.js where <version> is the target version of the HEA data model. The command files
should be in the root of the hea directory. Any custom mongodb command file not in the command files dictionary will be
ignored.

The mongodb commands are executed like <mongodb> "<command>" where <mongodb> is the path to the mongo shell or the 
docker command to execute the mongo shell in the mongo container, and <command> is the mongodb command to execute. The 
script escapes " on linux and windows for legacy command files, but only single quotes should be used going forward.
"""
COMMAND_FILES = { # target version -> upgrade file, in version order.
    '1.0.0b1': 'mongodb-commands.js',
    '1.0.0b2': 'mongodb-commands-1.0.0b2.js',
    '1.0.0b3': 'mongodb-commands-1.0.0b3.js',
    '1.0.0b4': 'mongodb-commands-1.0.0b4.js',
    '1.0.0b5': 'mongodb-commands-1.0.0b5.js',
    '1.0.0b6': 'mongodb-commands-1.0.0b6.js',
    '1.0.0b7': 'mongodb-commands-1.0.0b7.js',
    '1.0.0b8': 'mongodb-commands-1.0.0b8.js',
    '1.0.0b9': 'mongodb-commands-1.0.0b9.js',
    '1.0.0b10': 'mongodb-commands-1.0.0b10.js',
    '1.0.0b11': 'mongodb-commands-1.0.0b11.js',
    '1.0.0b12': 'mongodb-commands-1.0.0b12.js',
    '1.0.0b13': 'mongodb-commands-1.0.0b13.js',
    '1.0.0b14': 'mongodb-commands-1.0.0b14.js',
    '1.0.0b15': 'mongodb-commands-1.0.0b15.js',
    '1.0.0b16': 'mongodb-commands-1.0.0b16.js',
    '1.0.0': 'mongodb-commands-1.0.0.js',
    '1.0.1': 'mongodb-commands-1.0.1.js'
    # Add next version here.
}

###############################################################################
# END CONFIGURATION.
###############################################################################

CURRENT_VERSION_FILE = '.current_data_version'

def get_env_data_as_dict(path: str) -> dict:
    with open(path, 'r') as f:
        return dict((k, v.strip("'\" ")) for k, v in (tuple(line.strip().split('=')) for line in f.readlines()
                    if not line.startswith('#') and not line.startswith(tuple(w for w in whitespace))))


def execute_commands(filename):
    with open(filename, 'r') as commands:
        for line in commands.readlines():
            line = line.strip()
            if len(line) == 0 or line.startswith('//'):
                continue
            if os.name == 'nt':
                line = line.replace('"', '\\"')  # Assume we use single quotes around anything that needs escaping.
            else:
                line = line.replace('"', '\\"')  # Assume we use single quotes around anything that needs escaping.
            command = f'{cmd} "{line}"'
            # print(f'Executing command: {command}')  # Uncomment for debugging only (will reveal sensitive data).
            if status := os.system(command) != 0:
                exit(status)

def update_current_version_file(version: str):
    with open(CURRENT_VERSION_FILE, 'w') as current_version_fd:
        current_version_fd.writelines([version])


def read_current_version_file() -> Optional[str]:
    if os.path.exists(CURRENT_VERSION_FILE):
        with open(CURRENT_VERSION_FILE, 'r') as current_version_fd:
            return current_version_fd.readline().strip()
    else:
        return None

def run_upgrades_from(version: str):
    do_upgrade = (version is None)
    for _version, command_file in COMMAND_FILES.items():
        if do_upgrade:
            pre_custom_command_file = f'pre-custom-{command_file}'
            if os.path.exists(pre_custom_command_file):
                print('Executing commands from pre-custom command file...')
                execute_commands(pre_custom_command_file)
            else:
                print(f'No pre-custom command file. Put your own mongodb commands, one per line, in {pre_custom_command_file}.')
            execute_commands(command_file)
            custom_command_file = f'custom-{command_file}'
            if os.path.exists(custom_command_file):
                print('Executing commands from custom command file...')
                execute_commands(custom_command_file)
            else:
                print(f'No custom command file. Put your own mongodb commands, one per line, in {custom_command_file}.')
            update_current_version_file(_version)
        elif _version == version:
            do_upgrade = True


dot_env = get_env_data_as_dict('.env')
print(dot_env)

if 'MONGO_HOSTNAME' in dot_env:
    cmd = f"mongosh \"{dot_env.get('MONGO_HEA_DATABASE', 'hea')}\" --host \"{dot_env['MONGO_HOSTNAME']}\" -u \"{dot_env['MONGO_HEA_USERNAME']}\" -p \"{dot_env['MONGO_HEA_PASSWORD']}\" --tls --eval 'disableTelemetry()' --eval "
else:
    cmd = f"docker exec -it mongo mongosh \"{dot_env.get('MONGO_HEA_DATABASE', 'hea')}\" -u \"{dot_env['MONGO_HEA_USERNAME']}\" -p \"{dot_env['MONGO_HEA_PASSWORD']}\" --eval 'disableTelemetry()' --eval "

desc = __doc__

parser = argparse.ArgumentParser(description=desc)
parser.add_argument('-r', '--reset', action='store_true', help='drop the contents of the HEA database and re-populate it')
args = parser.parse_args()

if 'MONGO_HOSTNAME' in dot_env and  args.reset:
    drop = input("You are trying to DROP THE DATABASE. Are you sure you want to continue? This action is irreversible (yes/no): ").strip().lower() in ["yes", "y"]
    if not drop:
        sys.exit()

if args.reset:
    print('Dropping the contents of the HEA database...')
    execute_commands('mongodb-reset.js')

print('Creating HEA collections and default objects...')
version = read_current_version_file()
run_upgrades_from(version)

print('Done!')
