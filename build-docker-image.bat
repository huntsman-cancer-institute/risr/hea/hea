@echo off

set usage=Usage: build-docker-image.bat project-name version
set helptext=build-docker-image.bat - Builds a docker image.
set usageoptions=Options:
set usageoptionsprojnm=    package-name: the project name.
set usageoptionsprojvn=    version: the project version.

if "%~1"=="-h" goto HELP
if "%~1"=="--help" goto HELP
if "%~1"=="" goto ERR
if "%~2"=="" goto ERR
goto MAIN

:ERR
echo %usage%
exit /b 1

:HELP
echo %helptext%
echo.
echo %usage%
echo %usageoptions%
echo %usageoptionsprojnm%
echo %usageoptionsprojvn%
exit

:MAIN
echo Working...
setlocal enabledelayedexpansion

echo Building docker image for %1 version %2...

set olddir = %~dp0
cd %1 >nul
docker build --no-cache --build-arg PROJECT_VERSION="%2" -t "registry.gitlab.com/huntsman-cancer-institute/risr/hea/%1:%2" -t "registry.gitlab.com/huntsman-cancer-institute/risr/hea/%1:latest" .
if not "%errorlevel%"=="0" (set retval=1) else set retval=0
if "%retval%"=="0" (echo Done) else echo Building docker image failed

:CLEANUP
cd %olddir% >nul
exit /b %retval%
