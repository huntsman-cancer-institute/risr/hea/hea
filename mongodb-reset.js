// Drop all collections in the HEA database.

db.getCollectionNames().forEach(function(c) { if (c.indexOf("system.") == -1) db.getCollection(c).dropIndexes(); db.getCollection(c).drop(); })