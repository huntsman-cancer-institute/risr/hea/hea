#!/bin/sh
set -e

cat > .hea-config.cfg <<EOF
[DEFAULT]
Registry=${HEASERVER_REGISTRY_URL:-http://heaserver-registry:8080}
MessageBrokerEnabled=${HEA_MESSAGE_BROKER_ENABLED:-true}

[MessageBroker]
Hostname = ${RABBITMQ_HOSTNAME:-rabbitmq}
Port = 5672
Username = ${RABBITMQ_USERNAME:-guest}
Password = ${RABBITMQ_PASSWORD:-guest}
PublishQueuePersistencePath = ${HEA_MESSAGE_BROKER_QUEUE_PATH}

[MongoDB]
ConnectionString=mongodb://${MONGO_HEA_USERNAME}:${MONGO_HEA_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT:-27017}/${MONGO_HEA_DATABASE}?authMechanism=DEFAULT&authSource=${MONGO_HEA_AUTH_SOURCE:-admin}&tls=${MONGO_USE_TLS:-false}
EOF

cat > .hea-logging-config.cfg <<EOF
[loggers]
keys=root

[handlers]
keys=defaulthand

[formatters]
keys=defaultform

[logger_root]
level=INFO
handlers=defaulthand

[handler_defaulthand]
class=StreamHandler
level=NOTSET
formatter=defaultform
args=(sys.stdout,)

[formatter_defaultform]
format=%(levelname)s:%(name)s:%(message)s
EOF

exec heaserver-folders-aws-s3 -f .hea-config.cfg -b ${HEASERVER_FOLDERS_AWS_S3_URL} -l .hea-logging-config.cfg



