db.volumes.updateOne({name: 'SYSTEM'}, [{$set: {shares: [{type: 'heaobject.root.ShareImpl', user: 'system|all', permissions: ['VIEWER']}]}}, {$set: {owner: 'system|none'}}, {$set: {modified: new ISODate()}}]);

//Add groups endpoint to people service registry entry.
db.components.updateOne({name: 'heaserver-people'}, {$push: {resources: {type: "heaobject.registry.Resource", resource_type_name: "heaobject.person.Group", base_path: "groups", file_system_name: 'DEFAULT_FILE_SYSTEM', file_system_type: 'heaobject.volume.KeycloakFileSystem', resource_collection_type_display_name: 'Groups', collection_accessor_users: [], creator_users: [], default_shares: []}}, $set: {modified: new ISODate()}});
db.components.updateOne({name: 'heaserver-people'}, {$set: {"resources.$[i].file_system_type": "heaobject.volume.KeycloakFileSystem", modified: new ISODate()}}, {arrayFilters: [{"i.base_path": {$in: ["roles", "groups", "people"]}}]});
db.components.updateOne({name: 'heaserver-accounts'}, {$push: {resources: {type: "heaobject.registry.Resource", resource_type_name: "heaobject.account.AccountView", base_path: "accounts", file_system_name: 'DEFAULT_FILE_SYSTEM', file_system_type: 'heaobject.volume.MemoryFileSystem', resource_collection_type_display_name: 'Accounts', collection_accessor_users: [], creator_users: [], default_shares: []}}, $set: {modified: new ISODate()}});
db.components.updateOne({name: 'heaserver-keychain'}, {$push: {resources: {type: "heaobject.registry.Resource", resource_type_name: "heaobject.keychain.AWSCredentials", base_path: "credentials", file_system_name: 'DEFAULT_FILE_SYSTEM', file_system_type: 'heaobject.volume.MongoDBFileSystem', resource_collection_type_display_name: 'AWS Credentials', collection_accessor_users: [], creator_users: [], default_shares: []}}, $set: {modified: new ISODate()}});

//Set the new credential_type_name on Volume objects.
db.volumes.updateMany({file_system_type: 'heaobject.volume.AWSFileSystem'}, {$set: {credential_type_name: 'heaobject.keychain.AWSCredentials', modified: new ISODate()}});

//Fix longstanding wrong base_path for AWSAccount objects.
db.components.updateOne({name: 'heaserver-accounts'}, {$set: {"resources.$[i].base_path": "awsaccounts", modified: new ISODate()}}, {arrayFilters: [{"i.resource_type_name": "heaobject.account.AWSAccount"}]});

//Set creator_users to system|credentialsmanager for AWSCredentials and Volumes.
db.components.updateOne({name: 'heaserver-keychain'}, {$push: {"resources.$[i].creator_users": 'system|credentialsmanager'}, $set: {modified: new ISODate()}}, {arrayFilters: [{"i.resource_type_name": "heaobject.keychain.AWSCredentials"}]});
db.components.updateOne({name: 'heaserver-volumes'}, {$push: {"resources.$[i].creator_users": 'system|credentialsmanager'}, $set: {modified: new ISODate()}}, {arrayFilters: [{"i.resource_type_name": "heaobject.volume.Volume"}]});

//Replace role_arn with role.
db.credentials.updateMany({type: 'heaobject.keychain.AWSCredentials'}, {$unset: {'role': ''}, $set: {modified: new ISODate()}});
db.credentials.updateMany({type: 'heaobject.keychain.AWSCredentials'}, {$rename: {'role_arn': 'role'}, $set: {modified: new ISODate()}});

//Change permissions for temporary credentials
db.credentials.updateMany({type: "heaobject.keychain.AWSCredentials", temporary: true}, [{$set: {shares: [{type: 'heaobject.root.ShareImpl', user: '$owner', permissions: ['VIEWER']}]}}, {$set: {owner: 'system|credentialsmanager'}}, {$set: {modified: new ISODate()}}]);

//Schema change for volumes
db.volumes.updateMany({file_system_type: "heaobject.volume.AWSFileSystem"}, [{$set: {account_id: {$concat: ["$account.actual_object_type_name", '^', "$account.actual_object_id"]}}}, {$unset: "account"}, {$set: {modified: new ISODate()}}]);

//Schema change for organizations
db.organizations.updateMany({}, [{$set: {account_ids: {$map: {input: "$aws_account_ids", as: "aws_account_id", in: {$concat: ["heaobject.account.AWSAccount", '^', "$$aws_account_id"]}}}}}, {$unset: "aws_account_ids"}, {$set: {modified: new ISODate()}}]);

//Changes permissions for volumes associated with temporary credentials
temp_credential_ids = db.credentials.distinct("_id", {type: "heaobject.keychain.AWSCredentials", temporary: true}).map(id => id.toString()); db.volumes.updateMany({credential_id: {$in: temp_credential_ids}}, [{$set: {shares: [{type: 'heaobject.root.ShareImpl', user: '$owner', permissions: ['VIEWER']}]}}, {$set: {owner: 'system|credentialsmanager'}}, {$set: {modified: new ISODate()}}]);

//Schema change for credential, adding managed field
db.credentials.updateMany({}, { $set: { managed: false } });

// Adding property
db.properties.insertOne({type: "heaobject.registry.Property", name: "AWS_ADMIN_ROLE", display_name: "AWS ADMIN ROLE", value:"OrganizationAccountAccessRole", created: ISODate(), owner: 'system|none' , shares: [{type: "heaobject.root.ShareImpl", user: 'system|all', permissions: [ "VIEWER" ] } ] });