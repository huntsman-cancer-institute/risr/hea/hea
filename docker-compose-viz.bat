@echo off

set usage=Usage: docker-compose-viz.bat
set helptext=docker-compose-viz.bat - Writes to docker-compose.png a diagram of docker image dependencies in the docker-compose.yml file.
set addlusagetext=    Must be run from this directory, and Docker must be running.

if "%~1"=="-h" goto HELP
if "%~1"=="--help" goto HELP
goto MAIN

:HELP
echo %helptext%
echo.
echo %usage%
echo %addlusagetext%
exit

:MAIN
docker run --rm -it --name dcv -v %cd%:/input pmsipilot/docker-compose-viz render -m image docker-compose.yml