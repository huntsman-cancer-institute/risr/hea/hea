#!/bin/sh
set -e

cat > .hea-config.cfg <<EOF
[MongoDB]
ConnectionString=mongodb://${MONGO_HEA_USERNAME}:${MONGO_HEA_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT:-27017}/${MONGO_HEA_DATABASE}?authMechanism=DEFAULT&authSource=${MONGO_HEA_AUTH_SOURCE:-admin}&tls=${MONGO_USE_TLS:-false}&appname=heaserver-registry&compressors=zlib
EOF

cat > .hea-logging-config.cfg <<EOF
[loggers]
keys=root

[handlers]
keys=defaulthand

[formatters]
keys=defaultform

[logger_root]
level=INFO
handlers=defaulthand

[handler_defaulthand]
class=StreamHandler
level=NOTSET
formatter=defaultform
args=(sys.stdout,)

[formatter_defaultform]
format=%(levelname)s:%(name)s:%(message)s
EOF

exec heaserver-registry -f .hea-config.cfg -b ${HEASERVER_REGISTRY_URL:-http://localhost:8080} -l .hea-logging-config.cfg


