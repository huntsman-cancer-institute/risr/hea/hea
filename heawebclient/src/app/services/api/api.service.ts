import { Injectable } from '@angular/core';
import {HttpClient, HttpBackend, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {EMPTY, Observable, from, of, throwError, timer} from 'rxjs';
import { environment } from 'src/environments/environment';
import {map, expand, takeLast, mergeMap} from 'rxjs/operators';
import {HEAObject} from '../../models/heaobject.model';
import {ICJReadObject, ICJWriteObject} from '../../models/cj.model';
import { KeycloakService } from 'keycloak-angular';
import { SKIP_ERROR_HANDLING_INTERCEPTOR } from 'src/app/components/util/interceptor/error-handler.interceptor';
import { DialogsService } from 'src/app/components/util/dialog/dialogs.service';
import { UtilService } from '../util.service';


/**
 * Supports making REST API calls.
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public static COLLECTION_PLUS_JSON_MIME_TYPE = 'application/vnd.collection+json';
  public static NVPJSON_MIME_TYPE = 'application/json';
  public static X_WWW_FORM_ENCODED_MIME_TYPE = 'application/x-www-form-urlencoded';
  private static ABSOLUTE_URL_REGEX_PATTERN = /^https?:\/\//i;

  private apiUrl = environment.apiURL ? environment.apiURL : '';
  private wsUrl = environment.wsUrl ? environment.wsUrl : '';
  private httpManualErrorHandler: HttpClient;

  constructor(private http: HttpClient, private handler: HttpBackend, private dialogService: DialogsService,
    private keycloak: KeycloakService, private utilService: UtilService) {
    this.httpManualErrorHandler = new HttpClient(handler);
  }

  /**
   * Returns a Collection+JSON document observable from the given URL, optionally with query parameters.
   *
   * @param url the resource to get.
   * @param options HTTP query parameters, and handleErrorsManually (true or false on whether to handle recoverable
   * errors manually).
   * @return an observable of an array of ICJObjects.
   */
  getCollectionPlusJson(url: string | URL, options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}, handleErrorsManually?: boolean}): Observable<ICJReadObject[]> {
    let headers = new HttpHeaders({Accept: ApiService.COLLECTION_PLUS_JSON_MIME_TYPE, Prefer: 'respond-async'});
    if (options?.handleErrorsManually) {
      console.debug('Skipping auto error handling');
      headers = headers.append(SKIP_ERROR_HANDLING_INTERCEPTOR, "yes");
    }
    const urlToSend = this.urlToSend(url.toString());
    return this.handleAsync<string>(this.http.get(urlToSend, {headers: headers, params: options?.params, observe: 'response', responseType: 'text'})).pipe(map(resp => {
      return resp.body ? JSON.parse(resp.body) : (resp as any).error;
    }));
  }

  /**
   * Returns whether the resource at the given URL exists.
   *
   * @param url the resource to check.
   * @param options HTTP query parameters, and handleErrorsManually (true or false on whether to handle recoverable
   * errors manually).
   * @returns an HTTPResponse with status 200 if the resource exists, an HTTPResponse with status 404 if it does not,
   * or an error status code if something went wrong.
   */
  head(url: string | URL, options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}, handleErrorsManually?: boolean}) {
    let headers = new HttpHeaders({Prefer: 'respond-async'});
    if (options?.handleErrorsManually) {
      console.debug('Skipping auto error handling');
      headers = headers.append(SKIP_ERROR_HANDLING_INTERCEPTOR, "yes");
    }
    const urlToSend = this.urlToSend(url.toString());
    return this.handleAsync<string>(this.http.head(urlToSend, {headers: headers, params: options?.params, observe: 'response', responseType: 'text'})).pipe(map(resp => {
      return resp.body ? JSON.parse(resp.body) : (resp as any).error;
    }));
  }

  /**
   * Returns a HEAObject observable from the given URL, optionally with query parameters.
   *
   * @param url the resource to get.
   * @return an observable of an array of HEAObjects.
   */
  getJson(url: string): Observable<HEAObject[]> {
    return this.http.get<HEAObject[]>(this.urlToSend(url), {headers: new HttpHeaders({Accept: ApiService.NVPJSON_MIME_TYPE})});
  }

  getText(url: string): Observable<HttpResponse<string>> {
    return this.http.get(this.urlToSend(url), {responseType: 'text', observe: 'response'});
  }

  /**
   * Gets an HEAObject's content.
   *
   * @param url the URL of the object to get.
   * @return an observable of a response.
   */
  getContent(url: string): Observable<Response> {
    return from(this.keycloak.getToken()).pipe(mergeMap(token => {
      return from(fetch(url, {headers: {Authorization: `Bearer ${token}`}})).pipe(mergeMap(err => {
        if (err.status >= 400) {
          return from(err.text()).pipe(mergeMap(msg => {
            this.dialogService.error(`${this.utilService.removePeriod(msg)} (status ${err.status}).`);
            return throwError(err);
          }));
        } else {
          return of(err);
        }
      })).pipe(map(data => {
        return data;
      }));
    }));
  }

  /**
   * Updates a HEAObject from a Collection+JSON template.
   *
   * @param url the resource to update.
   * @param body the Collection+JSON template with the updated HEA object.
   * @return an observable of a HTTP response. If successful, the response will have status code 204 (No Content).
   */
  updateCollectionPlusJson(url: string, body: ICJWriteObject,
                           options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}}): Observable<HttpResponse<null>> {
    return this.handleAsync(this.http.put(this.urlToSend(url), body,
      {headers: {'Content-Type': ApiService.COLLECTION_PLUS_JSON_MIME_TYPE, Prefer: 'respond-async'},
       observe: 'response',
       responseType: 'text',
       params: options?.params}));
  }

  /**
   * Updates a HEAObject.
   *
   * @param url the resource to update.
   * @param body the HEAObject to update.
   * @return an observable of a HTTPResonse. If successful, the response will have status code 204 (No Content).
   */
  updateJson(url: string, body: HEAObject): Observable<HttpResponse<null>> {
    return this.handleAsync(this.http.put(this.urlToSend(url), body,
      {headers: {'Content-Type': ApiService.NVPJSON_MIME_TYPE}, observe: 'response', responseType: 'text'}));
  }

  putMultipart(url: string, body: FormData, options?: {params?: HttpParams | {[param: string]: string | string[]}})
    : Observable<HttpResponse<null>> {
    // browser needs to figure this not us in the header for multipart
    // const headers: HttpHeaders = new HttpHeaders().set('Content-Type', undefined)
    //                                               .set('Content-Disposition', `attachment; filename="${body.name}"`);
    //return this.handleAsync<null>(this.http.put(this.urlToSend(url), body));
    return this.handleAsync(this.http.put(this.urlToSend(url), body,
      {observe: 'response', responseType: 'text', params: options?.params }));
  }

  /**
   * Creates a new HEAObject on the server.
   *
   * @param url the endpoint.
   * @param body the Collection+JSON template with the HEAObject's properties.
   * @return an observable of a HTTPResponse. If successful, the response will have a Location header with the URL of
   * the created resource and a 201 (Created) status code. The response will have no content.
   */
  createCollectionPlusJson(url: string, body: ICJWriteObject,
                           options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}}): Observable<HttpResponse<string>> {
    return this.handleAsync(this.http.post(this.urlToSend(url), body,
      {headers: {'Content-Type': ApiService.COLLECTION_PLUS_JSON_MIME_TYPE}, observe: 'response', responseType: 'text', params: options?.params}));
  }

  /**
   * Deletes a HEAObject from the server.
   *
   * @param url the URL of the resource to delete.
   * @return an observable of a HTTPResonse. If successful, the response will have status code 204 (No Content).
   */
  delete(url: string, options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}, handleErrorsManually?: boolean}): Observable<HttpResponse<null>> {
    let headers = new HttpHeaders({'Prefer': 'respond-async'});
    if (options?.handleErrorsManually) {
      console.debug('Skipping auto error handling');
      headers = headers.append(SKIP_ERROR_HANDLING_INTERCEPTOR, "yes");
    }
    return this.handleAsync(this.http.delete(this.urlToSend(url), {observe: 'response', responseType: 'text', headers: headers, params: options?.params}));
  }

  options(url: string): Observable<HttpResponse<ArrayBuffer>> {
    return this.httpManualErrorHandler.options(this.urlToSend(url), {observe: 'response', responseType: 'arraybuffer'});
  }

  allowedMethods(url: string): Observable<string[]> {
    return this.options(url).pipe(map(resp => {
      const allowedMethods = resp.headers.get('Allow');
      return allowedMethods.split(/\s*,\s*/).map(method => method.toUpperCase());
    }));
  }

  isMethodAllowed(url: string, method: string): Observable<boolean> {
    return this.allowedMethods(url).pipe(map(resp => {
      return resp.indexOf(method.toUpperCase()) > -1
    }));
  }

  /**
   * Creates a new HEAObject on the server.
   *
   * @param url the endpoint.
   * @param params the properties of the HEA object to create as HTTP parameters.
   * @return an observable of a HTTPResponse. If successful, the response will have a Location header with the URL of
   * the created resource and a 201 (Created) status code. The response will have no content.
   */
  createXWWWFormEncoded(url: string, params: HttpParams): Observable<HttpResponse<null>> {
    return this.handleAsync(this.http.post<null>(this.urlToSend(url), params.toString(),
      {headers: {'Content-Type': ApiService.X_WWW_FORM_ENCODED_MIME_TYPE}, observe: 'response'}));
  }

  /**
   * Creates a new HEAObject on the server.
   *
   * @param url the endpoint.
   * @param body the HEAObject to create.
   * @return an observable of a HTTPResponse. If successful, the response will have a Location header with the URL of
   * the created resource and a 201 (Created) status code. The response will have no content.
   */
  createJson(url: string, body: HEAObject): Observable<HttpResponse<string>> {
    return this.handleAsync(this.http.post(this.urlToSend(url), body,
      {headers: {'Content-Type': ApiService.NVPJSON_MIME_TYPE}, observe: 'response', responseType: 'text'}));
  }

  /**
   * Updates a HEAObject.
   *
   * @param url the resource to update.
   * @param params the HEA object's properties as HTTP parameters.
   * @return an observable of a HTTP response. If successful, the response will have status code 204 (No Content).
   */
  updateXWWWFormEncoded(url: string, params: HttpParams): Observable<HttpResponse<null>> {
    return this.handleAsync(this.http.put<null>(this.urlToSend(url), params.toString(),
      {headers: {'Content-Type': ApiService.X_WWW_FORM_ENCODED_MIME_TYPE}, observe: 'response'}));
  }

  getWebSocket(url: string): Observable<WebSocket> {
    return from(this.keycloak.getToken()).pipe(map(token => {
      const _url = this.wsUrlToSend(url);
      if (_url.indexOf('?') >= 0) {
        return new WebSocket(_url + '&access_token=' + token);
      } else {
        return new WebSocket(_url + '?access_token=' + token);
      }
    }));

  }

  /**
   * Prepends the provided URL with the value of this object's apiURL field, unless the provided URL is an absolute,
   * URL, in which case it returns it verbatim.
   *
   * @param url the URL.
   * @return the updated URL.
   * @private
   */
  private urlToSend(url: string): string {
    return ApiService.urlToSend(url, this.apiUrl);
  }

  private wsUrlToSend(wsUrl: string): string {
    return ApiService.urlToSend(wsUrl, this.wsUrl);
  }

  private handleAsync<T>(obx: Observable<HttpResponse<any>>): Observable<HttpResponse<T>> {
    const delaySeconds = ApiService.delaySeconds();
    return obx.pipe(expand<HttpResponse<T>, HttpResponse<T>>(resp => {
      console.debug('in handleAsync', resp);
      if (resp.status === 202) {
        return timer(delaySeconds()).pipe(mergeMap(_ => {
          return this.http.get(resp.url, {observe: 'response', responseType: 'text'})
        }));
      } else {
        return EMPTY
      }
    }), takeLast(1));
  }

  private static urlToSend(url: string, baseUrl?: string): string {
    if (ApiService.isAbsoluteUrl(url)) {
      return url;
    } else if (!baseUrl) {
      return url;
    } else if (baseUrl[baseUrl.length - 1] === '/' || url[0] === '/') {
      return baseUrl + url;
    } else {
      return baseUrl + "/" + url
    }
  }

  private static isAbsoluteUrl(url: string): boolean {
    return ApiService.ABSOLUTE_URL_REGEX_PATTERN.test(url);
  }

  private static delaySeconds(): () => number {
    let _delaySeconds = 0;
    return () => {
      _delaySeconds = Math.min(_delaySeconds + 250, 5000)
      return _delaySeconds;
    }
  }
}
