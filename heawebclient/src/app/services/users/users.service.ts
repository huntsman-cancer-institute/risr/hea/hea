import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { CjService } from '../cj/cj.service';
import { Person } from 'src/app/models/heaobject.model';
import { map, mergeMap, take } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { HEAObjectContainer } from 'src/app/models/heaobject-container.model';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private api: ApiService,
    private cjService: CjService,
    private auth: AuthService) {
    }

  /**
   * Returns all users known to the system.
   *
   * @returns an observable of HEA object container projects for each user.
   */
  getAllUsers(): Observable<HEAObjectContainer<Person>[]> {
    return this.api.getCollectionPlusJson('/people/').pipe(map(result => {
      const heaObjectContainers = this.cjService.asHEAObjectContainers<Person>(result);
      if (heaObjectContainers) {
        return heaObjectContainers;
      } else {
        return [];
      }
    }, this));
  }

  /**
   * Returns the currently logged in user.
   *
   * @returns the current user.
   */
  getCurrentUser(): Observable<HEAObjectContainer<Person>> {
    return this.auth.userProfile.pipe(map(profile => {
      return profile.id;
    }))
    .pipe(mergeMap(userid => this.api.getCollectionPlusJson(`/people/${userid}`)))
    .pipe(map(result => {
      const heaObjectContainers = this.cjService.asHEAObjectContainers<Person>(result);
      if (heaObjectContainers) {
        return heaObjectContainers[0];
      } else {
        return null;
      }
    }, this));
  }


}
