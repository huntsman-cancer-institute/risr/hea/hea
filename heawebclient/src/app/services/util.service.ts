import {Injectable} from '@angular/core';
import {CurrencyPipe, DecimalPipe} from '@angular/common';
import * as _ from 'lodash-es';


export enum actionType {
  UPLOAD = 'Upload',
  DOWNLOAD = 'Download',
  CREATE = 'Create',
  DELETE = 'Delete',
}

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(private cp: CurrencyPipe, private dp: DecimalPipe) {
  }

  public static readonly REL_OPENER_DEFAULT: string = 'hea-opener hea-default';
  public static readonly REL_AWS_OPENER: string = 'hea-opener hea-context-aws';
  public static readonly REL_AWS_OPENER_DEFAULT: string = 'hea-opener hea-context-aws hea-default';
  public static readonly REL_ACTUAL: string = 'hea-actual';
  public static readonly REL_OPENER_CHOICES: string = 'hea-opener-choices';
  public static readonly REL_AWS_CONTEXT = 'hea-context-aws';
  public static readonly REL_SELF = 'self';
  public static readonly MIME_TYPE_AWS_STORAGE: string = 'application/x.awss3storage';
  public static readonly MIME_TYPE_AWS_BUCKET: string = 'application/x.awsbucket';
  public static readonly MIME_TYPE_AWS_FOLDER: string = 'application/x.awsfolder';
  public static readonly MIME_TYPE_AWS_ACCOUNT: string = 'application/x.awsaccount';
  public static readonly MIME_TYPE_AWS_ITEM: string = 'application/x.item';
  public static readonly MIME_TYPE_ZIP: string = 'application/zip'
  public static readonly AWS_FOLDER_TYPE: string = 'heaobject.folder.AWSS3Folder';
  public static readonly AWS_FILE_TYPE: string = 'heaobject.data.AWSS3FileObject';
  public static readonly MIME_TYPE_AWS_FILE: string = 'application/octet-stream';
  public static readonly REGEX_WHITESPACE = /\s+/;
  public readonly BUSY_ICON: string = 'fas fa-sync fa-spin';

  /**
   * Create a true copy of the original object. Described at
   * https://javascript.plainenglish.io/deep-clone-an-object-and-preserve-its-type-with-typescript-d488c35e5574.
   *
   * @param original the object to copy.
   * @return a true copy of the original object.
   */
  public static deepCopy<T>(original: T): T {
    return _.cloneDeep(original);
  }

  sortNumber(valueA, valueB, nodeA?: any, nodeB?: any, isInverted?: boolean) {
    valueA = +valueA;
    valueB = +valueB;
    return valueA > valueB ? 1 : valueA === valueB ? 0 : -1;
  }

  sortDate(obj1: any, obj2: any, dateField?: string) {
    if (obj1[dateField] && obj2[dateField]) {
      if (obj1[dateField] instanceof Date && obj2[dateField] instanceof Date) {
        return obj1.getTime() - obj2.getTime();
      }
      const dateVal1 = new Date(obj1[dateField]).getTime();
      const dateVal2 = new Date(obj2[dateField]).getTime();

      return (dateVal1 === dateVal2) ? 0 : (dateVal1 > dateVal2) ? 1 : -1;
    } else {
      if (obj1 && obj2) {
        if (obj1 instanceof Date && obj2 instanceof Date) {
          return obj1.getTime() - obj2.getTime();
        }
        const dateVal1 = new Date(obj1).getTime();
        const dateVal2 = new Date(obj2).getTime();
        return (dateVal1 === dateVal2) ? 0 : (dateVal1 > dateVal2) ? 1 : -1;
      }

    }
    return 0;
  }

  formatCurrency(val: any, currencyType = 'USD', display: any = 'symbol', digitInfo: string = '1.2-2') {
    return this.cp.transform(val, currencyType, display, digitInfo);
  }

  formatNumber(val: any, digitInfo = '1.2-2', locale = 'en-US') {
    return this.dp.transform(val, digitInfo, locale);
  }

  formatSize(val: string | number, sizeUnit?: string, determineUnit = false): string {
    /* assume all values in bytes */
    let calSize: number;
    const optimalIndex = Math.trunc(Math.log10(+val) / 3);
    const sizeUnitArray = ['B', 'KB', 'MB', 'GB', 'TB'];

    if (!sizeUnit) {
      sizeUnit = '';
    }

    let i = sizeUnitArray.indexOf(sizeUnit.toUpperCase());
    i = i === -1 ? 0 : i;
    calSize = (+val) / Math.pow(1000, i);
    if (calSize === 0) {
      return '' + 0.00;
    }

    if (determineUnit) {
      return '' + ((+val) / Math.pow(1000, optimalIndex)).toFixed(2) + ' ' + sizeUnitArray[optimalIndex];
    }

    return '' + calSize.toFixed(2);
  }

  /**
   * Returns an ISO datetime string representation of the provided Date that is compatible with the datetime-local
   * HTML5 tag. Courtesy of https://webreflection.medium.com/using-the-input-datetime-local-9503e7efdce. The string
   * omits milliseconds and timezone information because they are not supported by datetime-local, so you cannot
   * pass the created string into the Date() constructor and expect to get an identical Date back.
   *
   * Setting the value of the datetime-local tag: input.value = toDatetimeLocal(new Date(ISOString));
   * Getting the value of the datetime-local tag: newValue = new Date(input.value).toISOString();
   *
   * @param date a Date object (required).
   * @return an ISO datetime string.
   */
  toDatetimeLocal(date: Date): string {
    function ten(i: number): string {
      return (i < 10 ? '0' : '') + i;
    }
    const YYYY = date.getFullYear();
    const MM = ten(date.getMonth() + 1);
    const DD = ten(date.getDate());
    const HH = ten(date.getHours());  /* Local time */
    const II = ten(date.getMinutes());
    const SS = ten(date.getSeconds());

    return `${YYYY}-${MM}-${DD}T${HH}:${II}:${SS}`;
  }

  /**
   * Removes a period from the end of a string.
   * @param str the string to check.
   * @returns a copy of the string without the period, if a period was found.
   */
  removePeriod(str: string): string {
    if (str) {
      return str.replace(/\.$/, '');
    } else {
      return null;
    }
  }
}
