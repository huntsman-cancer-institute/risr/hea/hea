import { Injectable } from '@angular/core';
import {ApiService} from '../api/api.service';
import {CjService} from '../cj/cj.service';
import {MatLegacyDialog as MatDialog, MatLegacyDialogConfig as MatDialogConfig} from '@angular/material/legacy-dialog';
import {map, take} from 'rxjs/operators';
import { HEAObjectContainer } from 'src/app/models/heaobject-container.model';
import { DesktopObject } from 'src/app/models/heaobject.model';
import { DialogWrapInterpretedContentsComponent } from 'src/app/components/util/interpreted-contents/dialog-wrap-interpreted-contents.component';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DialogOpenerService {

  constructor(private apiService: ApiService,
              private cjService: CjService,
              private dialog: MatDialog) {
  }

  openModalWithHEAObjectContainer(heaObjectContainer: HEAObjectContainer<DesktopObject>, action: string, afterClosed?: (result: any) => any) {
    console.log('Opening modal with', heaObjectContainer);
    let method: string;
    switch (action) {
      case 'Edit':
        method = 'PUT';
        break;
      case 'New':
        method = 'POST';
        break;
      default:
        throw new Error(`Invalid method ${method}`);
    }

    if (heaObjectContainer) {
      const config = new MatDialogConfig<any>();
      config.panelClass = 'dynamic-standard-dialog-container';
      config.autoFocus = false;
      config.disableClose = true;
      config.data = {
        useOwnHeader: true,
        data: heaObjectContainer,
        href: heaObjectContainer.href,
        method: method
      };
      config.width = "50%";
      config.height = "75%";
      const dialogRef = this.dialog.open(DialogWrapInterpretedContentsComponent, config);
      dialogRef.afterClosed().pipe(take(1)).subscribe(result2 => {
        if (afterClosed) {
          afterClosed(result2);
        }
      });
    }
  }

  openModal(href: string, action: string, afterClosed?: (result: any) => any, options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}}): void {
    let method: string;
    switch (action) {
      case 'Edit':
        method = 'PUT';
        break;
      case 'New':
        method = 'POST';
        break;
      default:
        throw new Error(`Invalid method ${method}`);
    }

    this.apiService.getCollectionPlusJson(href, {params: options?.params}).pipe(map(result => {
      const heaObjectContainers = this.cjService.asHEAObjectContainers(result);
      if (heaObjectContainers) {
        return heaObjectContainers[0];
      } else {
        return null;
      }
    }, this)).subscribe(result => {
      if (result) {
        const config = new MatDialogConfig<any>();
        config.panelClass = 'dynamic-standard-dialog-container';
        config.autoFocus = false;
        config.disableClose = true;
        config.data = {
          useOwnHeader: true,
          data: result,
          href: href,
          method: method
        };
        config.width = "50%";
        config.height = "75%";
        const dialogRef = this.dialog.open(DialogWrapInterpretedContentsComponent, config);
        dialogRef.afterClosed().pipe(take(1)).subscribe(result2 => {
          if (afterClosed) {
            afterClosed(result2);
          }
        });
      }
    });
  }
}
