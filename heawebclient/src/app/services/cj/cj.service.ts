import {Injectable} from '@angular/core';
import {UtilService} from '../util.service';
import {ApiService} from '../api/api.service';
import {IDesktopObject, registeredClasses, Item} from '../../models/heaobject.model';
import {HEAObjectContainer, IHEAObjectContainer, urlToUniqueId} from '../../models/heaobject-container.model';
import {from, Observable, of, throwError} from 'rxjs';
import {catchError, concatMap, first, map, mergeMap} from 'rxjs/operators';
import {HttpErrorResponse, HttpParams} from '@angular/common/http';
import {ICJItem, ICJLink, ICJReadObject} from '../../models/cj.model';
import * as _ from 'lodash-es';
import { KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class CjService {

  private static findLinkWithAllRels(linkList: ICJLink[], relAttribute: string): ICJLink {
    const relArr = CjService.relStringToArray(relAttribute);
    const result = linkList.find(link => {
      const linkRelSet = new Set(CjService.relStringToArray(link.rel));
      return relArr.every(relItem => linkRelSet.has(relItem));
    });
    return result;
  }

  private static relStringToArray(rel: string): string[] {
    return rel.toLowerCase().split(UtilService.REGEX_WHITESPACE);
  }

  constructor(private api: ApiService, private keycloak: KeycloakService) {
  }

  /**
   * Finds the first link matching the provided rel values.
   *
   * @param linkList an array of links.
   * @param rel a string of rel values separated by spaces.
   * @returns a link or undefined.
   */
  findLink(linkList: ICJLink[], rel: string): ICJLink {
    //const rels = rel.split(/ +/)
    return linkList.find(link => link.rel.toLowerCase().includes(rel));
  }

  /**
   * Finds all links matching the provided rel values.
   *
   * @param linkList an array of links.
   * @param rel a string of rel values separated by spaces.
   * @returns an array of links.
   */
  findAllLinks(linkList: ICJLink[], rel: string): ICJLink[] {
    return linkList.filter(link => link.rel.toLowerCase().includes(rel));
  }

  /**
   * Parses a Collection+JSON document into a HEA desktop object and links. Assumes the Collection+JSON document was
   * just retrieved from a GET request.
   *
   * @param cjResults the Collection+JSON document.
   * @returns an array of EAObjectContainer objects.
   */
  asHEAObjectContainers<H extends IDesktopObject>(cjResults: ICJReadObject[]): HEAObjectContainer<H>[] {
    const objContainers: HEAObjectContainer<H>[] = [];

    function extractType(item: ICJItem): string | null {
      let type: string = null;
      for (const datum of item.data || []) {
        if (!datum.section && datum.name === 'type') {
          if (Array.isArray(datum.value)) {
            throw Error('type attribute must be a string but was an array');
          }
          type = datum.value;
        }
      }
      return type;
    }

    function getInstance(type: string, item: ICJItem): H {
      console.debug('Getting instance', type, item);
      const Cls = registeredClasses[type];
      console.debug('Got', Cls);
      const obj = new Cls();
      for (const datum of item.data || []) {
        if (datum.section) {
          if (datum.index != null) {
            if (!obj[datum.section]) {
              obj[datum.section] = [];
            }
            if (obj[datum.section].length <= datum.index) {
              obj[datum.section][datum.index] = {}
            }
            obj[datum.section][datum.index][datum.name] = datum.value;
          } else {
            if (!obj[datum.section]) {
              obj[datum.section] = {};
            }
            obj[datum.section][datum.name] = datum.value;
          }

        } else {
          obj[datum.name] = datum.value;
        }
      }
      return obj;
    }
    if (cjResults) {
      for (const cjResult of cjResults) {
        if (cjResult && cjResult.collection && cjResult.collection.items) {
          for (const [item, permissions] of _.zip(cjResult.collection.items, cjResult.collection.permissions)) {
            const type = extractType(item);
            const obj = type ? getInstance(type, item) : null;
            const objContainer = new HEAObjectContainer<H>();
            objContainer.href = item.href ? item.href : cjResult.collection.href;
            objContainer.heaObject = obj;
            if (cjResult.collection.template && cjResult.collection.template.data) {
              objContainer.templatePrompt = cjResult.collection.template.prompt;
              objContainer.template = _.cloneDeep(cjResult.collection.template.data);
            }
            if (cjResult.collection.queries) {
              objContainer.queries = _.cloneDeep(cjResult.collection.queries);
            }
            if (item.links) {
              objContainer.links = _.cloneDeep(item.links.map(link => {
                const linkCopy = _.cloneDeep(link);
                linkCopy.href = decodeURIComponent(linkCopy.href);
                return linkCopy;
              }));
            }
            objContainer.uniqueId = this.getUniqueId(objContainer);
            objContainer.permissions = permissions ? [...permissions] : [];
            objContainers.push(objContainer);
          }
        }
      }
    }
    return objContainers;
  }

  /**
   * GET the opener choices at the provided URL and then GET the link with the provided link relation values.
   *
   * @param linkUrl the opener choices URL (required).
   * @param rel the rel attributes to search (required).
   * @param options HTTP query parameters, and handleErrorsManually (true or false on whether to handle recoverable
   * errors manually).
   */
  getOpenerAndGo<H extends IDesktopObject>(linkUrl: string, rel: string, options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}, handleErrorsManually?: boolean}): Observable<HEAObjectContainer<H>[]> {
    const context = this.extractContext(rel);
    return this.getOpenerLinks(linkUrl, options).pipe(
      concatMap(links => {
        let link = CjService.findLinkWithAllRels(links, rel);
        if (!link && context) {
          const relArr = CjService.relStringToArray(rel);
          const relWithoutContext = relArr.filter(item => !item.startsWith('hea-context-'));
          link = CjService.findLinkWithAllRels(links, relWithoutContext.join(' '));
        }
        if (link) {
          return this.getResource<H>(link.href, options);
        } else {
          return of([]);
        }
      })
    );
  }

  getDefaultOpener(obj: IHEAObjectContainer<IDesktopObject> | string, context?: string, options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}}): Observable<ICJLink> {
    let openerChoicesLink;
    if (typeof obj === 'string') {
      openerChoicesLink = obj;
    } else {
      const link = CjService.findLinkWithAllRels(obj.links, UtilService.REL_OPENER_CHOICES)
      if (link) {
        openerChoicesLink = link.href;
      } else {
        return of(null);
      }
    }
    let rel;
    if (context) {
      rel = `${context} ${UtilService.REL_OPENER_DEFAULT}`;
    } else {
      rel = UtilService.REL_OPENER_DEFAULT;
    }
    const context_ = this.extractContext(rel);
    return this.getOpenerLinks(openerChoicesLink, options).pipe(map(links => {
        let link = CjService.findLinkWithAllRels(links, rel);
        if (!link && context_) {
          const relArr = CjService.relStringToArray(rel);
          const relWithoutContext = relArr.filter(item => !item.startsWith('hea-context-'));
          link = CjService.findLinkWithAllRels(links, relWithoutContext.join(' '));
        }
        return link;
      })
    );
  }

  /**
   * GET the provided HEA object's opener choices link and then GET the default opener, optionally providing a context.
   *
   * @param obj the HEAObjectContainer, or an HEA object's opener choices link (required).
   * @param context optional context link relation value.
   * @param options HTTP query parameters, and handleErrorsManually (true or false).
   */
  getDefaultOpenerAndGo<H extends IDesktopObject>(obj: IHEAObjectContainer<H> | string,
                                                  context?: string, options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}, handleErrorsManually?: boolean}): Observable<HEAObjectContainer<H>[]> {
    let openerChoicesLink;
    if (typeof obj === 'string') {
      openerChoicesLink = obj;
    } else {
      openerChoicesLink = CjService.findLinkWithAllRels(obj.links, UtilService.REL_OPENER_CHOICES).href;
    }
    if (context) {
      return this.getOpenerAndGo<H>(openerChoicesLink, `${context} ${UtilService.REL_OPENER_DEFAULT}`, options);
    } else {
      return this.getOpenerAndGo<H>(openerChoicesLink, UtilService.REL_OPENER_DEFAULT, options);
    }
  }

  getOpenerLinks<H extends IDesktopObject>(linkUrl: string, options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}}): Observable<ICJLink[]> {
    // provide it with the opener and it will open the content for you
    return this.api.getCollectionPlusJson(linkUrl, options).pipe(catchError((err: HttpErrorResponse) => {
        if (err.status === 300) {
          // this is not an error
          return of(JSON.parse(err.error));
        }
        return throwError(err);
      }), map(result => {
        const heaObjectContainers = this.asHEAObjectContainers<H>(result);
        const links: ICJLink[] = [];
        if (result && result.length > 0 && result[0].collection?.links) {
          links.push(...result[0].collection?.links);
        }
        if (heaObjectContainers && heaObjectContainers.length > 0 && heaObjectContainers[0].links) {
          links.push(...heaObjectContainers[0].links);
        }
        return links;
      }, this)
    );
  }

  /**
   * Gets an HEAObject resource, optionally passing in query parameters.
   *
   * @param url the URL of the resource (required).
   * @param options HTTP query parameters, and handleErrorsManually (true or false).
   */
  getResource<H extends IDesktopObject>(url: string | URL, options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}, handleErrorsManually?: boolean}): Observable<HEAObjectContainer<H>[]> {
    return this.api.getCollectionPlusJson(url, options).pipe(map(result => {
      const heaObjectContainers = this.asHEAObjectContainers<H>(result);
      if (heaObjectContainers) {
        return heaObjectContainers;
      } else {
        return null;
      }
    }, this));
  }

  /**
   * Extracts a context link relation value from a string of link relation values. Context link relation values begin
   * with hea-context-.
   *
   * @param rel a string of link relation values.
   */
  extractContext(rel: string): string {
    const relArr = CjService.relStringToArray(rel);
    for (const relArrItem of relArr) {
      if (relArrItem.startsWith('hea-context-')) {
        return relArrItem;
      }
    }
    return null;
  }

  /**
   * Gets an item's actual object in a HEAObjectContainer.
   *
   * @param item the item as a HEAObjectContainer.
   * @returns the actual object in a HEAObjectContainer. Behavior is undefined if the passed in object is not a
   * HEAObjectContainer with an Item
   */
  getActualObject<H extends IDesktopObject>(item: IHEAObjectContainer<Item>) {
    const objUrl = this.findLink(item.links, UtilService.REL_ACTUAL).href;
    return this.api.getCollectionPlusJson(objUrl).pipe(first(), map(resp => (this.asHEAObjectContainers<H>(resp)[0])));
  }

  /**
   * Gets an item's actual object from a HEAObjectContainer.
   *
   * @param item any desktop object container.
   * @param options when getting the actual object, whether to request retrieval of the data and links or only the
   * links. The response is not guaranteed to omit the data; this is a performance hint to reduce the overhead of
   * retrieving data unnecessarily. The params property accepts any query parameters. Specifying the data query
   * parameter here overrides the value of the data property.
   * @return if item is an Item, this method will get the item's actual object and return an Observable of it. If item
   * is another kind of DesktopObject, this method will return an Observable of item.
   */
  getActualObjectOrSelf<H extends IDesktopObject>(
    item: IHEAObjectContainer<IDesktopObject>,
    options?: {data?: boolean, params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}, handleErrorsManually?: boolean}): Observable<HEAObjectContainer<IDesktopObject>> {
    const actualURL = this.findLink(item.links, UtilService.REL_ACTUAL)?.href;
    let httpParams = options?.data ? new HttpParams().set('data', options?.data) : new HttpParams();
    if (options?.params) {
      if (options.params instanceof HttpParams) {
        for (const key of options.params.keys()) {
          for (const val of options.params.getAll(key)) {
            httpParams = httpParams.append(key, val)
          }
        }
      } else {
        httpParams = httpParams.appendAll(options.params)
      }
    }
    if (actualURL) {
      return this.api.getCollectionPlusJson(actualURL, {params: httpParams, handleErrorsManually: options?.handleErrorsManually})
        .pipe(first(), map(resp => (this.asHEAObjectContainers<H>(resp)[0])));
    } else {
      const link = this.findLink(item.links, 'self');
      if (!link || link.href === item.href) {
        return of(item);
      } else {
        return this.getResource(link.href, {params: httpParams}).pipe(map(self => {
          return self[0];
        }));
      }
    }
  }

  /**
   * Gets a desktop object's URL.
   *
   * @param item any desktop object container.
   * @returns if the object is a view, the actual object's URL will be returned. Otherwise, it will return the URL from
   * the object's self link, if found, otherwise the object's URL.
   */
  getActualUrlOrSelfUrl(item: IHEAObjectContainer<IDesktopObject>): string {
    const actual = this.findLink(item.links, UtilService.REL_ACTUAL);
    if (actual) {
      return actual.href;
    } else {
      const link = this.findLink(item.links, UtilService.REL_SELF);
      if (link) {
        return link.href;
      } else {
        item.href;
      }
    }
  }

  /**
   * Gets a desktop object's content URL.
   *
   * @param item a desktop object container.
   * @param mode optional mode (open or download)
   * @param addAccessToken whether to include the user's access token.
   * @returns the URL string, or null if the object does not have an opener.
   */
  getDesktopObjectContentUrl(item: IHEAObjectContainer<IDesktopObject>, mode?: 'open' | 'download', addAccessToken?: boolean): Observable<string> {
    return from(this.keycloak.getToken()).pipe(mergeMap(token => {
      return this.getActualObjectOrSelf(item).pipe(mergeMap(actual => {
        return this.getDefaultOpener(actual, UtilService.REL_AWS_CONTEXT).pipe(first()).pipe(map(link => {
          if (link) {
            let url = link.href;
            if (mode) {
              url = `${url}?mode=${mode}`;
            }
            if (addAccessToken) {
              url = `${url}&access_token=${token}`; //needed for if the browser ends up opening the link in a new tab.
            }
            return url;
          }
          return null;
        }));
      }));
    }))
  }

  /**
   * Returns an unique id for the given container, if possible.
   *
   * @param container the HEAObjectContainer (required).
   * @returns an unique id string, or null if a unique id is not in the data returned from the server.
   */
  getUniqueId(container: IHEAObjectContainer<IDesktopObject>): string {
    let href = this.findLink(container.links, "hea-actual")?.href;
    if (!href) {
      href = this.findLink(container.links, "self")?.href;
    }
    return href ? urlToUniqueId(href) : null;
  }

  /**
   * Converts an object's properties to serialized HTML form X-www-form-urlencoded parameters or query parameters.
   *
   * @param objParams an object with properties representing HTML form parameters or query parameters.
   * @returns an HttpParams object representing the serialized parameters.
   * @private
   */
  private objParamsToHttpParams(objParams: object): HttpParams {
    let hParams = objParams ? new HttpParams() : null;
    if (hParams) {
      for (const p in objParams) {
        if (objParams[p]) {
          hParams = hParams.set(p, objParams[p]);
        }
      }
    }
    return hParams;
  }
}
