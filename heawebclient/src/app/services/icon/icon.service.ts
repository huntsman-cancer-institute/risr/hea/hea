import { Injectable } from '@angular/core';
import { ICJLink } from 'src/app/models/cj.model';
import { CjService } from '../cj/cj.service';
import { DataObject, HEAObject, IView } from 'src/app/models/heaobject.model';
import { IconOption } from 'src/app/models/icon.model';

@Injectable({
  providedIn: 'root'
})
export class IconService {

  private fontLinkIcons = new Map<string, string>([
    ["hea-creator-choices", "hci fa-creates"],
    ["hea-generate-clipboard-icon", "bi bi-file-earmark-plus"],
    ["hea-retrieve-clipboard-icon", "bi bi-arrow-counterclockwise"],
    ["hea-archive", "bi bi-archive"],
    ["hea-unarchive", "hci  fa-un-archive-regular"],
    ["hea-icon-duplicator", "bi bi-copy"],
    ["hea-icon-mover", "bi bi-arrows-move"],
    ["hea-opener-choices", "bi bi-box-arrow-up-right"],
    ["hea-icon-for-clipboard", "bi-link-45deg"],
    ["hea-trash", "bi bi-trash3"],
    ["hea-downloader", "bi bi-download"],
    ["hea-uploader", "bi bi-upload"],
    ["hea-versions", "bi bi-collection"],
    ["application/x.settingsobject", "bi bi-tools"],
    ["application/x.collection", "bi bi-folder-fill"]
  ]);

  private heaObjectIcons = new Map<string, string>([
    ["heaobject.account.AWSAccount", "hci fa-sub-account"],
    ["heaobject.bucket.AWSBucket", "hci fa-bucket"],
    ["heaobject.project.AWSS3Project", "hci fa-projects"],
    ["heaobject.folder.AWSS3Folder", "bi bi-folder-fill"],
    ["application/csv", "bi bi-file-earmark-spreadsheet-fill"],
    ["application/doc", "bi bi-file-earmark-word-fill"],
    ["application/html", "bi bi-file-earmark-code-fill"],
    ["application/pdf", "bi bi-file-earmark-pdf-fill"],
    ["application/pptx", "bi bi-file-earmark-ppt-fill"],
    ["application/txt", "bi bi-file-earmark-text-fill"],
    ["application/xlsx", "bi bi-file-earmark-excel-fill"],
    ["application/zip", "bi bi-file-earmark-zip-fill"],
    ["heaobject.person.Person", "bi bi-file-person-fill"],
    ["heaobject.organization.Organization", "bi bi-building"],
    ["heaobject.keychain.AWSCredentials", "fab fa-aws"],
    ["heaobject.registry.Collection", "bi bi-folder-fill"],
    ["heaobject.settings.SettingsObject", "bi bi-tools"],
    ["heaobject.volume.Volume", "fa-solid fa-hard-drive"]
  ]);

  private defaultIcon = "bi bi-file-earmark-fill";

  private optionToFontClass = new Map<IconOption, string>();

  constructor(private cj: CjService) {
    this.optionToFontClass[IconOption.LARGE] = "fa-lg";
  }

  getFontIconForLink(link: ICJLink): string {
    if (link) {
      for (const rel of link.rel.split(' ')) {
        if (this.fontLinkIcons.has(rel)) {
          return this.fontLinkIcons.get(rel);
        }
      }
    }
    return this.defaultIcon;
  }

  getIconForLink(link: ICJLink, options?: IconOption[]): string {
    let fontIcon = this.getFontIconForLink(link);
    const class_ = this.iconOptionsToClasses(options);
    return this.fontIconToHTMLTag(fontIcon, class_);
  }

  getFontIconForRel(rel: string): string {
    if (this.fontLinkIcons.has(rel)) {
      return this.fontLinkIcons.get(rel);
    } else {
      return this.defaultIcon;
    }
  }

  getIconForRel(rel: string, options?: IconOption[]): string {
    const fontIcon = this.getFontIconForRel(rel);
    const class_ = this.iconOptionsToClasses(options);
    return this.fontIconToHTMLTag(fontIcon, class_);
  }

  getFontIconForHEAObject(heaObject: HEAObject): string {
    const dataObject = heaObject as DataObject;
    const view = heaObject as IView;
    if (dataObject.mime_type && this.heaObjectIcons.has(dataObject.mime_type)) {
      return this.heaObjectIcons.get(dataObject.mime_type);
    } else if (view.actual_object_type_name && this.heaObjectIcons.has(view.actual_object_type_name)) {
      return this.heaObjectIcons.get(view.actual_object_type_name);
    } else if (this.heaObjectIcons.has(heaObject.type)) {
      return this.heaObjectIcons.get(heaObject.type);
    } else {
      return this.defaultIcon;
    }
  }

  getIconForHEAObject(heaObject: HEAObject, options?: IconOption[]): string {
    const fontIcon = this.getFontIconForHEAObject(heaObject);
    const class_ = this.iconOptionsToClasses(options);
    return this.fontIconToHTMLTag(fontIcon, class_);
  }

  private fontIconToHTMLTag(fontIcon?: string, class_?: string): string {
    if (!fontIcon) {
      fontIcon = this.defaultIcon;
    }
    return `<i class="${fontIcon} ${class_} fa-fw fa-lg" aria-hidden="true"></i>`
  }

  private iconOptionsToClasses(options?: IconOption[]): string | null {
    let class_: string = null;
    if (options) {
      class_ = options.map(o => this.optionToFontClass[o]).join(' ');
    }
    return class_
  }
}


