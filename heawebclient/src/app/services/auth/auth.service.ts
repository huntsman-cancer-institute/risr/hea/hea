import { Injectable, Inject } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import {KeycloakProfile} from 'keycloak-js';
import { APP_BASE_HREF } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public logged: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public loggedIn = false;
  public userProfileSubject = new BehaviorSubject<KeycloakProfile>(null);
  public userProfile = this.userProfileSubject.asObservable();

  constructor(private router: Router, private keycloak: KeycloakService,
    @Inject(APP_BASE_HREF) private app_base_ref: string) {
    this.keycloak.isLoggedIn().then(loggedIn => {
      this.loggedIn =  loggedIn;
      this.logged.next(loggedIn);

      if (loggedIn) {
        this.keycloak.loadUserProfile(false).then(value => {
          this.userProfileSubject.next(value);
        });
      }
    });
  }

  async login(redirectPath?: string) {
    // Redirect to root if not specified
    if (! redirectPath) {
      redirectPath = new URL(this.app_base_ref, window.location.origin).href;
    }

    this.keycloak.login({
      redirectUri: redirectPath
    }).then(() => {
      this.logged.next(true);
      this.loggedIn = true;
      this.keycloak.loadUserProfile(false).then(value => {
        this.userProfileSubject.next(value);
      });
    });
  }

  logout() {
    this.keycloak.logout().then(() => {
      this.logged.next(false);
      this.loggedIn = false;
      this.keycloak.loadUserProfile(false).then(value => {
        this.userProfileSubject.next(value);
      });
    });
  }

  isAuthenticated(): Observable<boolean> {
    return this.logged.asObservable();
  }

}
