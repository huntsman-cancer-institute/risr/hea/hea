import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {ApiService} from '../api/api.service';
import {HEAObjectContainer} from '../../models/heaobject-container.model';
import {Organization} from '../../models/heaobject.model';
import {map, take} from 'rxjs/operators';
import {ICJWriteObject} from '../../models/cj.model';
import {CjService} from '../cj/cj.service';
import { HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrgService {

  selectedOrgValueNoCache = new Subject<HEAObjectContainer<Organization>>();

  private selectedOrg = new Subject<HEAObjectContainer<Organization>>();
  private authorizedOrganizations = new BehaviorSubject<HEAObjectContainer<Organization>[]>(undefined);
  private _selectedOrgValue: any;

  get selectedOrgValue(): HEAObjectContainer<Organization> {
    return this._selectedOrgValue;
  }

  set selectedOrgValue(value: HEAObjectContainer<Organization>) {
    this._selectedOrgValue = value;
  }


  constructor(private api: ApiService, private cjService: CjService) {
  }

  getSelectedOrg() {
    return this.selectedOrg;
  }

  updateSelectedOrg(organization: HEAObjectContainer<Organization>, excludeCachedData: boolean = false): void {
    console.debug('updating org selection', organization, excludeCachedData);
    this.selectedOrgValue = organization;
    if (!excludeCachedData){
      this.selectedOrg.next(organization); // this subject will invoke nav tree to reload which isn't always wanted
    }
    this.selectedOrgValueNoCache.next(organization);
  }


  getAuthorizedOrganizations(): BehaviorSubject<HEAObjectContainer<Organization>[]> {
    return this.authorizedOrganizations;
  }

  setAuthorizedOrganizations(orgDataList: HEAObjectContainer<Organization>[]): void {
    this.authorizedOrganizations.next(orgDataList);
  }

  getAll(): Observable<HEAObjectContainer<Organization>[]> {
    return this.api.getCollectionPlusJson(`/organizations/?sort=asc`).pipe(map(result => {
      const heaObjectContainers = this.cjService.asHEAObjectContainers<Organization>(result);
      if (heaObjectContainers) {
        this.setAuthorizedOrganizations(heaObjectContainers);
        return heaObjectContainers;
      } else {
        this.setAuthorizedOrganizations([]);
        return [];
      }
    }, this));
  }

  create(id: string, template: ICJWriteObject): Observable<HttpResponse<string>> {
    console.log(`creating organization using template: ${template}`);
    return this.api.createCollectionPlusJson(`/organizations/`, template);
  }

  refresh() {
    this.getAll().pipe(take(1)).subscribe(obx => {
      if (this.selectedOrgValue) {
        for (const o of obx) {
          if (o.heaObject.id === this.selectedOrgValue.heaObject.id) {
            this.updateSelectedOrg(o, false);
            return;
          }
        }
      }
    });
  }
}
