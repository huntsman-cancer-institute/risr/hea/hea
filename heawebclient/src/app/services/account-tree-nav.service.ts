import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {UtilService} from "./util.service";
import {catchError, concatMap} from 'rxjs/operators';
import {HEAObjectContainer, IHEAObjectContainer} from "../models/heaobject-container.model";
import {DesktopObject, Item, Organization} from "../models/heaobject.model";
import {OrgService} from "./org/org.service";
import {CjService} from './cj/cj.service';
import {IStorageSummary} from '../models/storage.model';
import { ITreeNode } from '@circlon/angular-tree-component/lib/defs/api';

@Injectable({
  providedIn: 'root'
})
export class AccountTreeNavService {
  prevSelectedOrg: IHEAObjectContainer<Organization>;
  treeDataLoading = new BehaviorSubject<boolean>(false);
  treeData = new BehaviorSubject<ITreeNode[]>(null);
  selectedAwsAccount = new BehaviorSubject<any>(undefined);
  accountsStorage = new BehaviorSubject<IStorageSummary[]>([]);
  expandedTreeNodeIDs = new Set<string>();
  readonly maxLevel = 2;

  constructor(private orgService: OrgService,
              private cjService: CjService) {
  }

  emitStorageSummary(storageSummary: IStorageSummary) {
    const existingData = [...this.accountsStorage.getValue()];
    const data = existingData.find(accInfo => accInfo.accountId === storageSummary.accountId);
    if (data) {
      existingData.splice(existingData.indexOf(data), 1);
    }
    existingData.push(storageSummary);
    this.accountsStorage.next(existingData);
  }

  getStorageObservable(): Observable<IStorageSummary[]> {
    return this.accountsStorage.asObservable();
  }

  toTreeNodes(containers: IHEAObjectContainer<Item>[], hasChildren: boolean = false): ITreeNode[] {
    const nodes: ITreeNode[] = [];
    for (const c of containers){
      nodes.push(this.createNode(c, hasChildren));
    }
    return nodes;
  }

  invokeTreeDataSource() {
    return this.orgService.getSelectedOrg().pipe(
      concatMap(orgResp => {
        console.debug('got organization', orgResp);
        if (orgResp) {
          this.prevSelectedOrg = orgResp;
          this.treeDataLoading.next(true);
          return this.cjService.getDefaultOpenerAndGo(orgResp, UtilService.REL_AWS_CONTEXT).pipe(catchError(this.reportError<HEAObjectContainer<DesktopObject>>));
        } else {
          return of(null as HEAObjectContainer<DesktopObject>[]);
        }
      })
    );
  }

  reportError<T>(err) {
    console.error(err);
    this.treeDataLoading.next(false);
    return of([] as T[]);
  }

  private createNode(container: IHEAObjectContainer<Item>, hasChildren: boolean): ITreeNode {
    // making deep copy need to modify object for tree to consume
    const node = JSON.parse(JSON.stringify(container));
    node.id = container.uniqueId;
    node.display_name = container.heaObject.display_name;
    node.hasChildren = hasChildren; // this flag controls whether the getChildren callback gets called
    return node;
  }

}
