import { BrowserModule } from '@angular/platform-browser';
import {NgModule, APP_INITIALIZER} from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { MatLegacyListModule as MatListModule } from '@angular/material/legacy-list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatLegacyMenuModule as MatMenuModule } from '@angular/material/legacy-menu';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatLegacyDialogModule as MatDialogModule } from '@angular/material/legacy-dialog';
import { MatLegacyInputModule as MatInputModule } from '@angular/material/legacy-input';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { RenameObjectDialogComponent } from './components/object-explorer/rename-object/rename-object-dialog.component';
import { environment } from '../environments/environment';
import { DateModule, InlineModule, SelectModule } from '@huntsman-cancer-institute/input';
import { NavigationModule, NavigationService } from '@huntsman-cancer-institute/navigation';
import { MatLegacySelectModule as MatSelectModule } from '@angular/material/legacy-select';
import { NotificationModule, NotificationService } from '@huntsman-cancer-institute/notification';
import { UtilModule } from './components/util/util.module';
import { CurrencyPipe, DecimalPipe, PlatformLocation } from '@angular/common';
import { MatLegacyTooltipModule as MatTooltipModule } from '@angular/material/legacy-tooltip';
import {MiscModule} from '@huntsman-cancer-institute/misc';
import {ErrorHandlerInterceptor} from './components/util/interceptor/error-handler.interceptor';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {DialogsModule} from './components/util/dialog/dialogs.module';
import {ChartsModule} from 'ng2-charts';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatExpansionModule} from '@angular/material/expansion';
import {LicenseManager} from 'ag-grid-enterprise';
import { APP_BASE_HREF } from '@angular/common';
import { TreeModule } from '@circlon/angular-tree-component';


LicenseManager.setLicenseKey('Using_this_{AG_Grid}_Enterprise_key_{AG-061412}_in_excess_of_the_licence_granted_is_not_permitted___Please_report_misuse_to_legal@ag-grid.com___For_help_with_changing_this_key_please_contact_info@ag-grid.com___{University_of_Utah_-_Huntsman_Cancer_Institute__Research_Informatics}_is_granted_a_{Single_Application}_Developer_License_for_the_application_{Core_Browser}_only_for_{5}_Front-End_JavaScript_developers___All_Front-End_JavaScript_developers_working_on_{Core_Browser}_need_to_be_licensed___{Core_Browser}_has_not_been_granted_a_Deployment_License_Add-on___This_key_works_with_{AG_Grid}_Enterprise_versions_released_before_{17_August_2025}____[v3]_[01]_MTc1NTM4NTIwMDAwMA==51621811f3421b697b711367c6267157');

export function getBaseHref(platformLocation: PlatformLocation): string {
  return platformLocation.getBaseHrefFromDOM();
  //console.debug('baseHref is', baseHref);
  //return new URL(baseHref, window.location.origin).href;
}

function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
      keycloak.init({
        config: {
          url: environment.keycloakURL,
          realm: environment.keycloakRealm,
          clientId: environment.keycloakClientId,
        },
      loadUserProfileAtStartUp: true,
      initOptions: {
        checkLoginIframe: true
      },
      enableBearerInterceptor: true,
      bearerExcludedUrls: []
    });
}

@NgModule({
    declarations: [
        AppComponent,
        RenameObjectDialogComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        NgbModule,
        KeycloakAngularModule,
        MatListModule,
        MatIconModule,
        MatToolbarModule,
        FlexLayoutModule,
        MatGridListModule,
        MatMenuModule,
        MatDialogModule,
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        MatMomentDateModule,
        DateModule,
        InlineModule,
        MatSelectModule,
        NavigationModule.forRoot(),
        NotificationModule.forRoot({ popupRemoveDelay: 5000 }),
        SelectModule,
        UtilModule,
        ChartsModule,
        MatTooltipModule,
        MiscModule,
        DialogsModule,
        MatSidenavModule,
        MatExpansionModule,
        TreeModule
    ],
    providers: [
        NotificationService,
        NavigationService,
        DecimalPipe,
        CurrencyPipe,
        {
            provide: APP_INITIALIZER,
            useFactory: initializeKeycloak,
            multi: true,
            deps: [KeycloakService],
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
        },
        {
            provide: APP_BASE_HREF,
            useFactory: getBaseHref,
            deps: [PlatformLocation]
        }
    ],
    exports: [],
    bootstrap: [AppComponent]
})

export class AppModule { }
