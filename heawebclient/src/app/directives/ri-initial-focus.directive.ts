import {Directive, ElementRef} from "@angular/core";

@Directive({
  selector: "[riInitialFocus]"
})
export class RIInitialFocusDirective {
  constructor(private el: ElementRef) { }

  focusSelector: string = `a[href]:not([disabled]):not([tabindex="-1"]),
                           input:not([disabled]):not([tabindex="-1"]),
                           select:not([disabled]):not([tabindex="-1"]),
                           textarea:not([disabled]):not([tabindex="-1"]),
                           button:not([disabled]):not([tabindex="-1"]),
                           *[tabindex]:not([disabled]):not([tabindex="-1"])
                           `;

  timer: any;

  ngAfterViewInit(): void {
    // If the element with this directive is focusable, focus it. If not, find
    // the first focusable child element and focus that.
    let firstFocusable: HTMLElement;

    if (this.el.nativeElement.matches(this.focusSelector)) {
      firstFocusable = this.el.nativeElement;
    } else {
      firstFocusable = this.el.nativeElement.querySelector(this.focusSelector);
    }

    this.focusElement(firstFocusable);
  }

  focusElement(focusEl: HTMLElement): void {
    if (focusEl) {
      // Adding the following attributes should prevent a "flicker" effect where focus
      // is set after the page loads then quickly changed

      // The standard HTML autofocus attribute will set the focus when a page initially
      // loads but may not refocus after navigation in a single page app
      focusEl.setAttribute("autofocus", "");

      // This attribute is used by angular mat-dialog to request an initial focus
      // it will be ignored otherwise
      focusEl.setAttribute("cdkFocusInitial", "");

      // Force focus on the element
      // some components can't be focused until after AfterContentInit
      // this timeout is the easiest way to ensure that
      this.timer = setTimeout(() => {
        focusEl.focus();
      }, 0);
    }
  }

  ngOnDestroy() {
    clearTimeout(this.timer);
  }
}
