import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import {MatLegacySnackBar as MatSnackBar} from '@angular/material/legacy-snack-bar';
import {MatLegacyTooltip as MatTooltip} from "@angular/material/legacy-tooltip";

@Directive({
  selector: '[tooltipToClip]',
})
export class TooltipExtractorDirective {
  constructor(private el: ElementRef, private snackbar: MatSnackBar ) {}
  private _tooltip : MatTooltip;
  @Input() defaultColor: string;

  @Input('tooltipToClip') set tooltipToClip(val: any){
    this._tooltip = val;
  }

  @HostListener('contextmenu', ['$event']) onRightClick(event: any) {
    event.preventDefault();
    if (this._tooltip && this._tooltip._isTooltipVisible()){
      navigator.clipboard.writeText(this._tooltip.message);
      if (this.snackbar){
        this.snackbar.open('Copied tooltip text to clipboard', 'Copied', {
          duration: 3000
        });
      }
    }
  }

}
