import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {MatLegacyTooltipModule as MatTooltipModule} from '@angular/material/legacy-tooltip';
import {MatLegacySnackBarModule as MatSnackBarModule} from '@angular/material/legacy-snack-bar';
import {TooltipExtractorDirective} from './tooltip-extractor.directive';


@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    MatTooltipModule,
    MatSnackBarModule
  ],

  declarations: [
    TooltipExtractorDirective
  ],
  providers: [
  ],
  exports: [
    TooltipExtractorDirective,
    MatTooltipModule
  ]
})

export class TooltipExtractorModule { }
