import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth/auth.guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { KeycloakBearerInterceptor } from 'keycloak-angular';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {
    path: 'home',
    canActivate: [AuthGuard],
    loadChildren: () => import('./components/heaobject-explorer/heaobject-explorer.module').then(m => m.HeaobjectExplorerModule)
  },
  {
    path: 'settings',
    canActivate: [AuthGuard],
    loadChildren: () => import('./components/object-explorer/views/icon-view/icon-view.module').then(m => m.IconViewModule)
  },
  { path: 'object-explorer',
    canActivate: [AuthGuard],
    loadChildren: () => import('./components/object-explorer/object-explorer.module').then(m => m.ObjectExplorerModule)
  },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: KeycloakBearerInterceptor,
      multi: true
    }
  ]
})
export class AppRoutingModule { }
