import {Component, HostBinding, OnDestroy, OnInit, ViewChild,} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {RowNode} from 'ag-grid-community';
import 'ag-grid-enterprise';
import {catchError, mergeMap, take} from 'rxjs/operators';
import {
  Action,
  Card,
  CREATE_ACTION,
  DELETE_ACTION,
  DOWNLOAD_ACTION,
  DynamicClipboardActionWithCJLink,
  DynamicStandardActionWithCJLink,
  OpenActionWithCJLink,
  PREVIEW_ACTION,
  PROPERTIES_ACTION,
  REFRESH_SELECTED_ACTION,
  REFRESH_SELECTED_PURGE_ACTION,
  TrashActionWithCJLink,
  UPLOAD_ACTION,
  VersionsActionWithCJLink
} from 'src/app/models/object-explorer-api.model';
import {UtilService} from '../../services/util.service';
import {HEAObjectContainer, IHEAObjectContainer, uniqueIdToURL} from '../../models/heaobject-container.model';
import {MatLegacyDialog as MatDialog, MatLegacyDialogConfig as MatDialogConfig} from '@angular/material/legacy-dialog';
import {DeleteCardComponent} from './cards/delete-card.component';
import {ICJLink} from '../../models/cj.model';
import {CjService} from '../../services/cj/cj.service';
import {DataObject, DesktopObject, IDesktopObject} from '../../models/heaobject.model';
import {TrashComponent} from './trash/trash-dialog.component';
import {IconService} from 'src/app/services/icon/icon.service';
import {AbstractViewComponent} from './views/abstract-view/abstract-view.component';
import {TreeGridViewComponent} from './views/tree-grid-view/tree-grid-view.component';
import {IconViewComponent} from './views/icon-view/icon-view.component';
import {of, Subscription} from 'rxjs';
import {OrgService} from 'src/app/services/org/org.service';


@Component({
  selector: 'cb-object-explorer',
  templateUrl: './object-explorer.component.html',
  styles: [`

    .bottom-left {
      left: 0;
      bottom: 0;
    }

    .grid-gradient div.ag-center-cols-viewport {
      background: linear-gradient( 0deg, var(--white-lightest) 30%, var(--bluewarmvividfade-lighter) 100% ) !important;
    }

    .highlight {
      color: #FFCB00;
    }

    .flex-basis-100 {
      flex-basis: 100%;
    }

    .icon-color {
      color: var(--bluewarm-lightest);
    }

    .refresh-color {
      color: var(--pure-teal);
      border-color: var(--bluewarm-lightest);
    }

    .text-align-center {
      text-align: center;
    }

  `]
})
export class ObjectExplorerComponent implements OnInit, OnDestroy {
  @HostBinding('class') classList = 'outlet-column';

  static readonly DEFAULT_TITLE = 'Object Explorer';

  showUploadCard = false;
  showDownloadCard = false;
  showCard = false;
  chosenCard: Card = null;
  chosenAction: Action = null;
  dynamicStandardChosenLink: ICJLink;
  dynamicClipboardChosenLink: ICJLink;

  serverSideStoreType: any = true;
  mostRecentActualDesktopObject: IHEAObjectContainer<DesktopObject>;
  mostRecentSelectedDesktopObject: IHEAObjectContainer<DesktopObject>;
  uploadSelectedDesktopObject: IHEAObjectContainer<DesktopObject>;
  dynamicStandardSelectedDesktopObject: IHEAObjectContainer<DesktopObject>;
  dynamicClipboardSelectedDesktopObject: IHEAObjectContainer<DesktopObject>;
  versionsSelectedDesktopObject: IHEAObjectContainer<DesktopObject>;
  dynamicStandardFormData: Map<string, any>;
  selectedDesktopObjectDataPath: string[];
  selectedObjects: IHEAObjectContainer<DesktopObject>[];
  currentMimeType = 'text/plain';
  actualObjects: IHEAObjectContainer<DesktopObject>[];
  objectSelection = 'multiple';
  suppressSelection = false;
  title = ObjectExplorerComponent.DEFAULT_TITLE;
  currentView: new (...args: any[]) => AbstractViewComponent = TreeGridViewComponent;
  initialExpansion: string[];
  roots: IHEAObjectContainer<IDesktopObject>[];
  addSelectedDesktopObjectDataPath: string[];
  dynamicStandardSelectedDesktopObjectDataPath: string[];
  dynamicClipboardSelectedDesktopObjectDataPath: string[];
  uploadSelectedDesktopObjectDataPath: string[];
  versionsSelectedDesktopObjectDataPath: string[];

  @ViewChild('treeGridView') set treeGridView(content: TreeGridViewComponent) {
    if (content) {
      this.view = content;
    }
  }

  @ViewChild('iconView') set iconView(content: IconViewComponent) {
    if (content) {
      this.view = content;
    }
  }

  private _collapsed = false;
  private dynamicClipboardFormData: Map<string, any>;
  private rootsSub: Subscription;
  private rootsSub2: Subscription;
  private paramMapSub: Subscription;
  private view: AbstractViewComponent;
  public previewUrl: string;


  constructor(private router: Router,
              private dialog: MatDialog,
              private cjService: CjService,
              public iconService: IconService,
              private route: ActivatedRoute,
              private orgService: OrgService) {
  }

  ngOnInit(): void {
    // TODO move this route change out of the object explorer, and find a better solution to a query parameter
    // change (removal of the initial expansion from the URL) not being detected as a route change.
    this.rootsSub = this.orgService.getSelectedOrg()
    .subscribe(org => {
      console.debug('object explorer picked up organization change', org);
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate(['object-explorer', 'collections', 'default']).then(() => {});
      });
    });
    this.paramMapSub = this.route.paramMap.subscribe(paramMap => {
      const collectionId = paramMap.get('collectionId');
      console.debug('Opening object explorer for collection', collectionId);
      if (collectionId) {
        if (collectionId === 'default') {
          if (this.orgService.selectedOrgValue) {
            this.cjService.getDefaultOpenerAndGo(this.orgService.selectedOrgValue, UtilService.REL_AWS_CONTEXT)
            .pipe(catchError(this.reportError<HEAObjectContainer<DesktopObject>>))
            .subscribe(resp => {
              this.currentView = TreeGridViewComponent;
              this.roots = resp;
            });
          } else {
            this.roots = [];
          }
        } else {
          console.log('in default');
          this.rootsSub2?.unsubscribe();
          this.rootsSub2 = this.cjService.getResource(uniqueIdToURL(collectionId)).pipe(take(1)).pipe(mergeMap(resp => {
            const container = resp[0];
            const httpParams = this.view.getParentPathParams(container);
            this.title = container.heaObject.display_name;
            return this.cjService.getDefaultOpenerAndGo<DesktopObject>(container, null, {params: httpParams}).pipe(catchError(this.reportError<HEAObjectContainer<DesktopObject>>))
          })).subscribe(roots => {
            console.debug('Setting roots', roots);
            this.currentView = IconViewComponent;
            this.roots = roots;
          });
        }
      }
      this.route.queryParamMap.pipe(take(1)).subscribe(params => {
        console.debug('got query params', params);
        this.initialExpansion = params.getAll('initialExpansion');
      });
    });
  }

  ngOnDestroy(): void {
    this.rootsSub?.unsubscribe();
    this.rootsSub2?.unsubscribe();
    this.paramMapSub?.unsubscribe();
  }

  get TreeGridViewComponent() {
    return TreeGridViewComponent;
  }

  get IconViewComponent() {
    return IconViewComponent;
  }

  get collapsed(): boolean {
    return this._collapsed;
  }

  get Card() {
    return Card;
  }

  getIconForLink(link: ICJLink): string {
    return this.iconService.getFontIconForLink(link);
  }

  onClickSearch(): void {
    console.log('Search clicked!');
  }

  collapseStatusChanged(status: boolean) {
    this._collapsed = status;
  }

  goHome(): void {
    this.router.navigate(['/home'], {queryParamsHandling: 'merge'});
  }

  actionChosen(action: Action) {
    console.debug('Received action', action);
    this.chosenAction = action;
    switch(action) {
      case PROPERTIES_ACTION:
        this.onClickProperties();
        break;
      case DOWNLOAD_ACTION:
        this.onClickDownload();
        break;
      case REFRESH_SELECTED_PURGE_ACTION:
        this.refresh(this.selectedObjects, null, true);
        break;
      case REFRESH_SELECTED_ACTION:
        this.refresh(this.selectedObjects);
        break;
      case UPLOAD_ACTION:
        this.onClickUpload();
        break;
      case DELETE_ACTION:
        this.onClickDelete();
        break;
      case CREATE_ACTION:
        this.onClickAdd();
        break;
      case PREVIEW_ACTION:
        this.onClickPreview();
        break;

      default:
        if (action instanceof DynamicStandardActionWithCJLink) {
          this.onClickDynamicStandard(action.param)
        } else if (action instanceof DynamicClipboardActionWithCJLink) {
          this.onClickDynamicClipboard(action.param);
        } else if (action instanceof OpenActionWithCJLink) {
          this.onClickOpen(action.param);
        } else if (action instanceof TrashActionWithCJLink) {
          this.onClickTrash(action.param);
        } else if (action instanceof VersionsActionWithCJLink) {
          this.onClickVersions(action.param);
        } else {
          throw new Error(`Unexpected action ${action}`);
        }
    }
  }

  openCurrentlySelected() {
    for (const obj of this.selectedObjects) {
      this.view.openContainerObject(obj);
    }
  }

  onClickOpen(link?: ICJLink): void {
    this.openCurrentlySelected(); // should only be called when the link rel shows that we're opening a folder or a folder-like object.
  }

  onClickDownload(): void {
    this.closeOpenedCard();
    this._collapsed = true;
    this.chosenCard = Card.DOWNLOAD;
  }
  private onClickPreview() {
    this.closeOpenedCard();
    this._collapsed = true;
    this.chosenCard = Card.PREVIEW;
  }

  onClickUpload(): void {

    let openCard = this.chosenCard;
    this.uploadSelectedDesktopObject = this.mostRecentSelectedDesktopObject;
    this.uploadSelectedDesktopObjectDataPath = this.view.getParentPath(this.uploadSelectedDesktopObject);

    this.closeOpenedCard();

    // unless the card was already open...
    if ( openCard !== Card.UPLOAD ) {
      if (this.objectSelection !== 'single') {
        this.objectSelection = 'single';
      }

      setTimeout(() => {
        this.suppressSelection = false;
        this.showUploadCard = true;
        this.showDownloadCard = false;
        this.showCard = true;

        this.chosenCard = Card.UPLOAD;
        this._collapsed = true;
      });
    }
  }

  closeUploadCard(event: boolean): void {
    if (this.chosenCard === Card.UPLOAD) {
      this.chosenCard = null;
      this.chosenAction = null;
      this._collapsed = false;
      this.objectSelection = 'multiple';
      this.refresh(event);
    }
  }

  closeAddCard(event: boolean): void {
    if (this.chosenCard === Card.ADD) {
      this.chosenCard = null;
      this.chosenAction = null;
      this._collapsed = false;
      if (event) {
        this.refresh(event);
      }
    }
  }

  closeDynamicStandardCard(event: string | boolean | IHEAObjectContainer<IDesktopObject>): void {
    if (this.chosenCard === Card.DYNAMIC_STANDARD) {
      this.chosenCard = null;
      this.chosenAction = null;
      this._collapsed = false;
      if (event) {
        this.refresh(this.selectedObjects, this.dynamicStandardSelectedDesktopObject);
      }
    }
  }

  closeDynamicClipboardCard(event: string | boolean | IHEAObjectContainer<IDesktopObject>): void {
    if (this.chosenCard === Card.DYNAMIC_CLIPBOARD) {
      this.chosenCard = null;
      this.chosenAction = null;
      this._collapsed = false;
      if (event) {
        this.refresh(event);
        this.refresh(this.dynamicClipboardSelectedDesktopObject);
      }
    }
  }

  closeDownloadCard() {
    if (this.chosenCard === Card.DOWNLOAD) {
      this.chosenCard = null;
      this.chosenAction = null;
      this._collapsed = false;
    }
  }
  closePreviewCard(refresh: boolean) {
    if (this.chosenCard === Card.PREVIEW) {
      this.chosenCard = null;
      this.chosenAction = null;
      this._collapsed = false;
      if (refresh) {
        this.refresh('parentRefresh');
      }
    }
  }

  closeVersionsCard(event: string | boolean) {
    if (this.chosenCard === Card.VERSIONS) {
      this.chosenCard = null;
      this.chosenAction = null;
      this._collapsed = false;
      if (event) {
        this.refresh('parentRefresh');
      }
    }
  }

  closeDeleteCard(event: string | boolean | URL | IHEAObjectContainer<IDesktopObject> | IHEAObjectContainer<IDesktopObject>[]) {
    if (this.chosenCard === Card.DELETE) {
      this.chosenCard = null;
      this.chosenAction = null;
      console.debug('Requesting refresh', event);
      if (typeof event === 'boolean') {
        this.refresh('parentRefresh');
      } else if (typeof event === 'string') {
        this.refresh(event);
      } else if (event instanceof URL) {
        this.refresh(null, event);
      } else if (Array.isArray(event)) {
        this.refresh(null, event);
      } else {
        this.refresh(null, [event]);
      }
    }
  }

  closePropertiesCard(refresh: boolean) {
    if (this.chosenCard === Card.PROPERTIES) {
      this.chosenCard = null;
      this.chosenAction = null;
      this._collapsed = false;
      if (refresh) {
        this.refresh('parentRefresh');
      }
    }
  }

  onClickProperties(): void {
    this.closeOpenedCard();
    this.chosenCard = Card.PROPERTIES;
    this._collapsed = true;
  }

  onClickAdd(): void {
    this.closeOpenedCard();
    this.chosenCard = Card.ADD;
    this.addSelectedDesktopObjectDataPath = this.view.getParentPath(this.mostRecentSelectedDesktopObject);
    this._collapsed = true;
  }

  onClickDynamicStandard(link: ICJLink): void {
    this.closeOpenedCard();
    this.chosenCard = Card.DYNAMIC_STANDARD;
    this.dynamicStandardSelectedDesktopObject = this.mostRecentSelectedDesktopObject;
    this.dynamicStandardSelectedDesktopObjectDataPath = this.view.getParentPath(this.dynamicStandardSelectedDesktopObject);
    this.dynamicStandardChosenLink = link;
    this._collapsed = true;
  }

  onClickDynamicClipboard(link: ICJLink): void {
    this.closeOpenedCard();
    this.chosenCard = Card.DYNAMIC_CLIPBOARD;
    this.dynamicClipboardSelectedDesktopObject = this.mostRecentSelectedDesktopObject;
    this.dynamicClipboardSelectedDesktopObjectDataPath = this.view.getParentPath(this.dynamicClipboardSelectedDesktopObject);
    this.dynamicClipboardChosenLink = link;
    this._collapsed = true;
  }

  onClickVersions(link: ICJLink): void {
    this.closeOpenedCard();
    this.chosenCard = Card.VERSIONS;
    this.versionsSelectedDesktopObject = this.mostRecentSelectedDesktopObject;
    this.versionsSelectedDesktopObjectDataPath = this.view.getParentPath(this.versionsSelectedDesktopObject);
    this._collapsed = true;
  }

  onClickDelete(): void {
    this.closeOpenedCard();
    this.chosenCard = Card.DELETE;
    this._collapsed = true;

    const config: MatDialogConfig = new MatDialogConfig<DeleteCardComponent>();
    config.panelClass = 'delete-dialog-container';
    config.autoFocus = false;
    config.disableClose = true;
    config.data = {
      subject: this.selectedObjects,
      view: this.view
    };

    const dialogRef = this.dialog.open(DeleteCardComponent, config);
    dialogRef.afterClosed().pipe(take(1)).subscribe(res => {
      this.closeDeleteCard(res);
    });
  }

  onClickTrash(link: ICJLink): void {
    this.closeOpenedCard();
    this.chosenCard = Card.TRASH;
    this._collapsed = true;

    const config: MatDialogConfig = new MatDialogConfig<TrashComponent>();
    config.panelClass = 'delete-dialog-container';
    config.autoFocus = false;
    config.disableClose = true;
    config.height = '80vh';
    config.width = '80vw';
    config.data = {
      useOwnHeader: true,
      trashLink: link
    };

    const dialogRef = this.dialog.open(TrashComponent, config);
    dialogRef.afterClosed().pipe(take(1)).subscribe(res => {
      this.refresh(res ? [] : false);
    });
  }

  isCurrentSelectionOpenable(): boolean {
    for (const rowNode of this.selectedObjects) {
      const link_ = this.cjService.findLink(rowNode.links, UtilService.REL_OPENER_CHOICES);
      if (link_) {
        return true;
      }
    }
    return false;
  }

  onClickTags(): void {
  }

  onClickOptions(): void {
  }

  onObjectSelection(objs: IHEAObjectContainer<IDesktopObject>[]) {
    if (!objs) {
      objs = [];
    }
    console.debug('selected objects', objs);
    this.selectedObjects = objs;
    this.mostRecentSelectedDesktopObject = objs.length > 0 ? objs[objs.length  - 1] : null;
    this.mostRecentActualDesktopObject = null;
    this.actualObjects = [];
    if (objs.length > 0) {
      const actualObjs: IHEAObjectContainer<IDesktopObject>[] = [];
      let count = 0;
      for (let i = 0; i < this.selectedObjects.length; i++) {
        const options = {params: this.view.getParentPathParams(this.selectedObjects[i]), handleErrorsManually: true};
        const objToCheck = this.selectedObjects[i];
        console.debug('Checking ancestor', objToCheck);
        this.cjService.getActualObjectOrSelf(objToCheck, options)
        .pipe(catchError(err => {
          console.error('Error getting actual object', err);
          this.refresh('parentRefresh');
          return of(null as IHEAObjectContainer<IDesktopObject>);
        }))
        .pipe(take(1)).subscribe(obj => {
          if (obj) {
            actualObjs[i] = obj;
            count++;
            if (count === this.selectedObjects.length) {
              console.debug('actual objects', actualObjs);
              this.actualObjects = actualObjs;
              this.mostRecentActualDesktopObject = actualObjs[actualObjs.length - 1];
              this.dynamicStandardFormData = new Map<string, any>();
              this.dynamicStandardFormData.set('target', this.mostRecentActualDesktopObject.href);
              this.dynamicClipboardFormData = new Map<string, any>();
              this.dynamicClipboardFormData.set('target', this.mostRecentActualDesktopObject.href);
            }
          }
        });
      }
    } else {
      this.dynamicStandardFormData = new Map<string, any>();
      this.dynamicClipboardFormData = new Map<string, any>();
    }
  }

  /**
   * Refresh the specified objects in this view.
   *
   * @param event objects to refresh, or URLs of objects to refresh, true to refresh all objects, false to do nothing,
   * or 'refreshParent' to refresh the parent of the currently selected objects.
   * @param parentOf objects whose parents to refreshed.
   * @param purge whether to remove and reload the objects to be refreshed, or refresh in place.
   */
  private refresh(event: string | boolean | URL | IHEAObjectContainer<IDesktopObject> | IHEAObjectContainer<IDesktopObject>[],
    parentOf: URL | IHEAObjectContainer<IDesktopObject> | IHEAObjectContainer<IDesktopObject>[] = null, purge = false) {
    if (this.view) {
      console.debug('Sending refresh request', event, parentOf, purge, 'to view', this.view);
      this.view.refresh(event, parentOf, purge);
    }
  }

  private closeOpenedCard(): void {
    switch (this.chosenCard) {
      case Card.DOWNLOAD:
        this.closeDownloadCard();
        break;
      case Card.UPLOAD:
        this.closeUploadCard(false);
        break;
      case Card.ADD:
        this.closeAddCard(false);
        break;
      case Card.DYNAMIC_STANDARD:
        this.closeDynamicStandardCard(false);
        break;
      case Card.DYNAMIC_CLIPBOARD:
        this.closeDynamicClipboardCard(false);
        break;
      case Card.VERSIONS:
        this.closeVersionsCard(false);
        break;
      case Card.DELETE:
        this.closeDeleteCard(false);
        break;
      case Card.PROPERTIES:
        this.closePropertiesCard(false);
        break;
      case Card.PREVIEW:
        this.closePreviewCard(true);
        break;
      default : // do nothing
    }
  }

  private reportError<T>(err) {
    console.error(err);
    return of([] as T[]);
  }
}


