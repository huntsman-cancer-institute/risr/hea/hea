import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { ICJLink } from 'src/app/models/cj.model';
import { HEAObjectContainer, IHEAObjectContainer, isContainerType } from 'src/app/models/heaobject-container.model';
import { IDesktopObject } from 'src/app/models/heaobject.model';
import { ActionStates } from 'src/app/models/object-explorer-api.model';
import { OpenActionWithCJLink } from 'src/app/models/object-explorer-api.model';
import { VersionsActionWithCJLink } from 'src/app/models/object-explorer-api.model';
import { TrashActionWithCJLink } from 'src/app/models/object-explorer-api.model';
import { DynamicClipboardActionWithCJLink } from 'src/app/models/object-explorer-api.model';
import { DynamicStandardActionWithCJLink } from 'src/app/models/object-explorer-api.model';
import { CREATE_ACTION } from 'src/app/models/object-explorer-api.model';
import { REFRESH_SELECTED_PURGE_ACTION } from 'src/app/models/object-explorer-api.model';
import { PROPERTIES_ACTION } from 'src/app/models/object-explorer-api.model';
import { DOWNLOAD_ACTION } from 'src/app/models/object-explorer-api.model';
import { UPLOAD_ACTION } from 'src/app/models/object-explorer-api.model';
import { DELETE_ACTION } from 'src/app/models/object-explorer-api.model';
import { PREVIEW_ACTION } from 'src/app/models/object-explorer-api.model';
import { Action } from 'src/app/models/object-explorer-api.model';
import { CjService } from 'src/app/services/cj/cj.service';
import { IconService } from 'src/app/services/icon/icon.service';
import {Router} from "@angular/router";
import {forkJoin, Observable} from "rxjs";
import {first} from "rxjs/operators";
import {HttpParams} from "@angular/common/http";

@Component({
  selector: 'cb-object-explorer-header',
  templateUrl: './object-explorer-header.component.html',
  styleUrls: ['./object-explorer-header.component.css']
})
export class ObjectExplorerHeaderComponent {
  static readonly DEFAULT_TITLE = 'Object Explorer';
  contextMenuLinks: ICJLink[] = [];
  @Output() newActionEvent = new EventEmitter<Action>();
  @Output() collapseEvent = new EventEmitter<boolean>();
  public search: string;
  public searchLinks: ICJLink[] = [];

  private _title = ObjectExplorerHeaderComponent.DEFAULT_TITLE;
  private _selectedRows: HEAObjectContainer<IDesktopObject>[];
  private _menuOpen = false;
  private _chosenAction: Action;
  private _collapsed: boolean;
  private _roots: IHEAObjectContainer<IDesktopObject>[] = [];


  constructor(private cjService: CjService, private iconService: IconService, private router: Router) {}

  get UPLOAD_ACTION() {
    return UPLOAD_ACTION;
  }

  get DOWNLOAD_ACTION() {
    return DOWNLOAD_ACTION;
  }

  get PREVIEW_ACTION() {
    return PREVIEW_ACTION;
  }

  get DELETE_ACTION() {
    return DELETE_ACTION;
  }

  get PROPERTIES_ACTION() {
    return PROPERTIES_ACTION;
  }

  get roots(): IHEAObjectContainer<IDesktopObject>[] {
    return this._roots;
  }

  @Input() set roots(roots: IHEAObjectContainer<IDesktopObject>[]) {
    this._roots = roots;
    if (!roots) {
      return;
    }
    for (const root  of roots) {
      const link = this.cjService.findLink(root.links, 'hea-search');
      if (link){
        this.searchLinks.push(link);
      }
    }
    console.log(this.searchLinks);
  }
  /**
   * The title to display for the current object explorer view.
   */
  get title() {
    return this._title;
  }

  @Input() set title(title: string) {
    this._title = title ? title : ObjectExplorerHeaderComponent.DEFAULT_TITLE;
  }

  @Input() set actionUpdated(action: Action) {
    this._chosenAction = action;
  }

  @Input() set collapsed(status: boolean) {
    this._collapsed = status;
  }

  get collapsed(): boolean {
    return this._collapsed;
  }

  get isMenuOpen(): boolean {
    return this._menuOpen;
  }


  /**
   * Gets the currently selected objects in the current object explorer view.
   *
   * @returns an array of RowNodes.
   */
  @Input('selectedObjects') set selectedRows(rows: HEAObjectContainer<IDesktopObject>[]) {
    this._selectedRows = rows;
    if (rows && rows.length === 1) {
      this.contextMenuLinks = this.cjService.findAllLinks(rows[0].links, 'hea-context-menu');
    } else {
      this.contextMenuLinks = [];
    }
  }

  get selectedRows(): HEAObjectContainer<IDesktopObject>[] {
    return this._selectedRows;
  }

  get chosenAction(): Action {
    return this._chosenAction;
  }

  chooseAction(action: Action) {
    this._chosenAction = action;
    console.debug('Emitting action', action);
    this.newActionEvent.emit(action);
  }

  @ViewChild('iconCorral', { static: true, read: ElementRef }) iconCorralDiv: ElementRef<HTMLElement>;

  get menuData(): object {
    return {
      menuWidth: this.iconCorralDiv.nativeElement.clientWidth
    };
  }

  /**
   * Gets current states for on-screen representations of an action.
   *
   * @param action the action of interest.
   * @returns the current highlight, disabled, and activated states for on-screen representations of this action.
   */
  getActionStates(action: Action): ActionStates {
    return {
      highlight: this.isActionHighlighted(action),
      disabled: this.isActionDisabled(action),
      activated: this.isActionActivated(action)
    }
  }
onGoHome(): void {
  this.router.navigate(['/home'], {queryParamsHandling: 'merge'});
}

  onClickCollapse() {
    this._collapsed = true;
    this.collapseEvent.emit(true);
  }

  onClickExpand() {
    this._collapsed = false;
    this.collapseEvent.emit(false);
  }

  onClickProperties() {
    this.chooseAction(PROPERTIES_ACTION);
  }

  onClickDownload() {
    this.chooseAction(DOWNLOAD_ACTION);
  }

  onClickPreview() {
    this.chooseAction(PREVIEW_ACTION);
  }

  onClickUpload() {
    this.chooseAction(UPLOAD_ACTION);
  }

  onClickDelete() {
    this.chooseAction(DELETE_ACTION);
  }

  onClickRefresh() {
    this.chooseAction(REFRESH_SELECTED_PURGE_ACTION);
  }

  onClickAdd() {
    this.chooseAction(CREATE_ACTION);
  }

  onClickContextButton(link: ICJLink) {
    if (link && link.rel) {
      const contextButtonRels = link.rel.split(/ +/);
      if (contextButtonRels.indexOf('hea-dynamic-standard') >= 0) {
        this.onClickDynamicStandard(link);
      } else if (contextButtonRels.indexOf('hea-dynamic-clipboard') >= 0) {
        this.onClickDynamicClipboard(link);
      } else if (contextButtonRels.indexOf('hea-creator-choices') >= 0) {
        this.onClickAdd();
      } else if (contextButtonRels.indexOf('hea-trash') >= 0) {
        this.onClickTrash(link);
      } else if (contextButtonRels.indexOf('hea-versions') >= 0) {
        this.onClickVersions(link);
      } else if (contextButtonRels.indexOf('hea-opener-choices') >= 0) {
        this.onClickOpen(link);
      }
    }
  }

  onClickDynamicStandard(link: ICJLink) {
    this.chooseAction(new DynamicStandardActionWithCJLink(link));
  }

  onClickDynamicClipboard(link: ICJLink) {
    this.chooseAction(new DynamicClipboardActionWithCJLink(link));
  }

  onClickTrash(link: ICJLink) {
    this.chooseAction(new TrashActionWithCJLink(link));
  }

  onClickVersions(link: ICJLink) {
    this.chooseAction(new VersionsActionWithCJLink(link));
  }

  onClickOpen(link: ICJLink) {
    this.chooseAction(new OpenActionWithCJLink(link));
  }

  menuItemsExist(): boolean {
    return this.contextMenuLinks && this.contextMenuLinks.length > 0;
  }

  onClickMenu(event: MouseEvent) {
    if (this._selectedRows && this._selectedRows.length > 0) {
      this._menuOpen = true;
    }
  }

  onCloseMenu(event: CloseEvent): void {
    this._menuOpen = false;
  }

  onSearchClicked(event: any) {
    const searchObserve: Observable<HEAObjectContainer<IDesktopObject>[]>[] = [];
    for (let i = 0;  i <  this.searchLinks.length;  i++) {
      const l = this.searchLinks[i];
      const r = this.roots[i];
      let params = new HttpParams();
      for (const q of  r.queries){
        for (let d of q.data) {
          if (d.name === 'text'){
            params = params.set(d.name, this.search);
          }else {
            params = params.set(d.name, typeof d.value === 'string' ? d.value : '');
          }
        }

      }
      searchObserve.push(this.cjService.getResource(l.href, {params }));
    }
    forkJoin(searchObserve).pipe(first()).subscribe(resp => {
      console.log(resp);
    });

  }

  getIconForLink(link: ICJLink): string {
    return this.iconService.getFontIconForLink(link);
  }

  getIconForObject(obj: IHEAObjectContainer<IDesktopObject>) {
    return this.iconService.getIconForHEAObject(obj.heaObject);
  }

  private isActionDisabled(action: Action): boolean {
    let result = !this.selectedRows || this.selectedRows.length === 0 || this._chosenAction?.actionName() === action.actionName();
    if (!result) {
      result = action !== UPLOAD_ACTION && this.selectedRows.length > 1;
    }
    if (!result && action === UPLOAD_ACTION) {
      result = this.selectedRows.some(obj => !isContainerType(obj));
    }
    if (!result && (action === DOWNLOAD_ACTION || action === PREVIEW_ACTION)) {
      result = this.selectedRows.some(obj => !this.cjService.findLink(obj.links, 'hea-opener-choices'));
    }
    if (!result && action === DELETE_ACTION) {
      result = this.selectedRows.every(obj => obj.isDeleteForbidden());
    }
    return result;
  }

  private isActionHighlighted(action: Action): boolean {
    return this._chosenAction ? this._chosenAction?.actionName() === action.actionName() : false;
  }

  private isActionActivated(action: Action): boolean {
    return this._chosenAction ? this._chosenAction?.actionName() === action.actionName() : false;
  }
}
