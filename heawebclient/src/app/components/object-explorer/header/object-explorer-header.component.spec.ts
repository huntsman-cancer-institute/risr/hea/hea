import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectExplorerHeaderComponent } from './object-explorer-header.component';

describe('ObjectExplorerHeaderComponent', () => {
  let component: ObjectExplorerHeaderComponent;
  let fixture: ComponentFixture<ObjectExplorerHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObjectExplorerHeaderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ObjectExplorerHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
