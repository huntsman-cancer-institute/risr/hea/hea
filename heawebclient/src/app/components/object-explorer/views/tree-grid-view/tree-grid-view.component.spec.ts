import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeGridViewComponent } from './tree-grid-view.component';

describe('TreeGridViewComponent', () => {
  let component: TreeGridViewComponent;
  let fixture: ComponentFixture<TreeGridViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreeGridViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TreeGridViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
