import { Component, Input } from '@angular/core';
import { CellValueChangedEvent, ColDef, FirstDataRenderedEvent, GetRowIdParams,
  GridApi, GridReadyEvent, GridSizeChangedEvent, IRowNode,
  IServerSideDatasource, IServerSideGetRowsParams, RowDoubleClickedEvent,
  RowGroupOpenedEvent, RowNode, SelectionChangedEvent, StoreRefreshedEvent } from 'ag-grid-community';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, mergeMap, take } from 'rxjs/operators';
import { TextAndTooltipRenderer } from 'src/app/components/util/grid-renderers/text-and-tooltip.renderer';
import { HEAObjectContainer, IHEAObjectContainer, isActualObjectContainerType, isContainerType, toUniqueId, urlToUniqueId } from 'src/app/models/heaobject-container.model';
import { DesktopObject, IDesktopObject, Item } from 'src/app/models/heaobject.model';
import { IconService } from 'src/app/services/icon/icon.service';
import { UtilService } from 'src/app/services/util.service';
import { AbstractViewComponent } from '../abstract-view/abstract-view.component';
import { CjService } from 'src/app/services/cj/cj.service';

interface IRefreshEvent {
  route: string[],
  purge: boolean
}

@Component({
  selector: 'cb-tree-grid-view',
  templateUrl: './tree-grid-view.component.html',
  styleUrls: ['./tree-grid-view.component.css']
})
export class TreeGridViewComponent extends AbstractViewComponent {
  autoGroupColumnDef: ColDef = {
    headerName: 'NAME',
    field: 'heaObject.display_name',
    cellRendererParams: {
      suppressCount: true,
      innerRenderer: this.getGroupRenderer()
    },
    minWidth: 400
  };

  defaultColDef: ColDef = {
    resizable: true,
    sortable: false,
    enableCellChangeFlash: false
  };

  rowModelType: 'clientSide' | 'infinite' | 'viewport' | 'serverSide' = 'serverSide';
  gridApi: GridApi<IHEAObjectContainer<IDesktopObject>>;
  selectedRows: IRowNode[];

  @Input('objectSelection') rowSelection:  'single' | 'multiple' = 'multiple';
  @Input() set suppressSelection(suppressSelection: boolean) {
    if (this.gridApi) {
      this.gridApi.setGridOption('suppressRowClickSelection', suppressSelection);
    }
    this._suppressSelection = suppressSelection;
  }

  get suppressSelection() {
    return this._suppressSelection;
  }

  private _suppressSelection: boolean;
  private gridColumnDefs: ColDef[] = [];
  private refreshing = false;
  private refreshQueue: IRefreshEvent[] = []
  private initialExpandQueue: string[] = [];
  private rowSelectedSubject: BehaviorSubject<IRowNode<IHEAObjectContainer<DesktopObject>>[]> = new BehaviorSubject<IRowNode[]>([]);


  constructor(private iconService: IconService,
    private cjService: CjService) {
      super();
  }

  ngOnInit(): void {
    console.debug('Initializing tree grid view');
    super.ngOnInit();
    this.gridColumnDefs = this.getGridColumnDefs();
  }

  isRowSelectable(params: RowNode): boolean {
    return true;
  }

  ngOnDestroy(): void {
    console.debug('Destroying tree grid view');
    super.ngOnDestroy();
  }

  getRowId(params: GetRowIdParams<IHEAObjectContainer<IDesktopObject>>) {
    return params.data.uniqueId;
  }

  getServerSideGroupKey(container: IHEAObjectContainer<IDesktopObject>) {
    // specify which group key to use
    return container.uniqueId;
  }

  isServerSideGroup(dataItem: IHEAObjectContainer<Item>): boolean {
    // indicate if node is a group
    return isActualObjectContainerType(dataItem) || dataItem.heaObject.mime_type === UtilService.MIME_TYPE_AWS_ACCOUNT;
  }

  onCellValueChanged(event: CellValueChangedEvent) {
    // make changes as needed

    if (this.gridApi) {
      this.gridApi.redrawRows();
      this.gridApi.sizeColumnsToFit();
    }
  }

  onFirstDataRendered(event: FirstDataRenderedEvent) {
    console.debug('onFirstDataRendered', event);
    this.initialExpandQueue = [...this.initialExpansion];
    if (this.initialExpandQueue.length > 0) {
      setTimeout(() => {
        console.debug('unique id', this.initialExpandQueue[0]);
        const rowNode = event.api.getRowNode(this.initialExpandQueue.shift());
        event.api.setRowNodeExpanded(rowNode, true);
      });
    }
  }

  refresh(event: string | boolean | URL | IHEAObjectContainer<IDesktopObject> | IHEAObjectContainer<IDesktopObject>[],
    parentOf: URL | IHEAObjectContainer<IDesktopObject> | IHEAObjectContainer<IDesktopObject>[], purge: boolean) {
    console.debug('Tree grid received refresh request for', event, parentOf);
    const rowNodesToRefresh: IRowNode<IHEAObjectContainer<IDesktopObject>>[] = []
    if (Array.isArray(event)) {
      if (this.gridApi) {
        const rows = event.map(val => this.gridApi.getRowNode(val.uniqueId)).filter(row => !!row);
        if (rows.length > 0) {
          for (let row of rows) {
            rowNodesToRefresh.push(row);
          }
        }
      }
    } else if (typeof event === 'string' || typeof event === 'boolean') {
      this.refreshObjectTree(event, purge);
      return;
    } else if (event instanceof URL) {
      if (this.gridApi) {
        const rowNode = this.gridApi.getRowNode(urlToUniqueId(event));
        if (rowNode) {
          rowNodesToRefresh.push(rowNode);
        }
      }
    } else if (event) {
      if (this.gridApi) {
        const rowNode = this.gridApi.getRowNode(event.uniqueId)
        if (rowNode) {
          rowNodesToRefresh.push(rowNode);
        }
      }
    }
    if (Array.isArray(parentOf)) {
      if (this.gridApi) {
        const rows = parentOf.map(val => this.gridApi.getRowNode(toUniqueId(val))?.parent).filter(val => !!val)
        if (rows.length > 0) {
          for (let row of rows) {
            rowNodesToRefresh.push(row);
          }
        }
      }
    } else if (parentOf) {
      const rowNode = this.gridApi?.getRowNode(toUniqueId(parentOf))?.parent;
      if (rowNode) {
        rowNodesToRefresh.push(rowNode);
      }
    }
    this.refreshObjectTree(rowNodesToRefresh, purge);
  }

  onGridReady(event: GridReadyEvent): void {
    console.debug('onGridReady', event);
    event.api.setGridOption('columnDefs', this.gridColumnDefs);
    event.api.setGridOption('suppressRowClickSelection', this._suppressSelection);
    this.gridApi = event.api;
    this.gridApi.sizeColumnsToFit();

    this.gridApi.updateGridOptions({
      'serverSideDatasource': this.createServerSideDatasource(),
      "alwaysShowHorizontalScroll": false,
      "autoGroupColumnDef": this.autoGroupColumnDef,
      "isRowSelectable": this.isRowSelectable,
      "defaultColDef": this.defaultColDef,
      "getServerSideGroupKey": this.getServerSideGroupKey,
      "headerHeight": 22,
      "isServerSideGroup": this.isServerSideGroup,
      "rowHeight": 30,
      "rowClass": "custom-class-normal",
      "suppressCellFocus": true,
      "rowSelection": this.rowSelection,
      "treeData": true,
      "onRowDoubleClicked": this.onRowDoubleClicked.bind(this),
      "onSelectionChanged": this.onSelectionChanged.bind(this)
    });
  }

  onRowDoubleClicked(event: RowDoubleClickedEvent): void {
    for (const obj of this.selectedObjects) {
      if (!isContainerType(obj)) {
        this.openContainerObject(obj);
      }
    }
  }

  onGridSizeChanged(event: GridSizeChangedEvent): void {
    event.api.sizeColumnsToFit();
  }

  onSelectionChanged(event: SelectionChangedEvent): void {
    console.debug('Tree grid emitting selection changed', event, this.selectedObjects);
    this.selectedRows = event.api.getSelectedNodes() ? event.api.getSelectedNodes() : [];
    this.selectedObjects = new Set(this.selectedRows.map(row => row.data));
    this.objectsSelected.emit([...this.selectedObjects]);
    this.rowSelectedSubject.next([...this.selectedRows]);
  }

  initiateGridRefresh(path?: string[], purge = false) {
    this.refreshing = true;
    if (this.gridApi) {
      console.debug('refreshing', path);
      this.gridApi.refreshServerSide({route: path ? path : [], purge: purge});
    }
  }

  getModel(flatRepresentation: boolean = false): any[] {
    const flatRowData: any[] = [];
    if (!flatRepresentation) {
      return JSON.parse(JSON.stringify(this.roots));
    } else {
      this.gridApi.forEachNode(node => {
        const data = JSON.parse(JSON.stringify(node.data));
        data.path = node.getRoute();
        flatRowData.push(data);
      });
    }
    return flatRowData;
  }

  rowGroupOpened(event: RowGroupOpenedEvent) {
    if (event.expanded) {
      if (this.initialExpandQueue && this.initialExpandQueue.length > 1) {
        const obj = this.initialExpandQueue.shift();
        // This code is needed because rowGroupOpened might be fired before the row node object is available.
        let trynum = 0;
        const myLoop = () => {
          setTimeout(() => {
            if (!event.api.isDestroyed()) {
              const rowNode = event.api.getRowNode(obj);
              console.debug('rowNode', rowNode);
              if (rowNode) {
                event.api.setRowNodeExpanded(rowNode, true);
              } else {
                if (trynum++ < 100) {
                  myLoop();
                }

              }
            }
          }, 100);
        }
        myLoop();
      }
    }
  }

  /**
   * Opens a desktop object. If it's a container, it will expand the row, and if not, it will attempt to open the
   * object with its default opener. If it has no opener, this method will do nothing.
   *
   * @param obj the desktop object to open.
   */
  openContainerObject(obj: IHEAObjectContainer<IDesktopObject>) {
    console.debug('opening object', obj);
    if (this.gridApi) {
      const rowNode = this.gridApi.getRowNode(obj.uniqueId);
      if (rowNode && rowNode.isExpandable()) {
        this.gridApi.setRowNodeExpanded(rowNode, true);
      } else {
        this.cjService.getDesktopObjectContentUrl(obj, 'open', true).pipe(take(1)).subscribe(href => {
          if (href) {
            const tab = window.open('', '_blank');
            console.debug('Get content URL', href);
            tab.location = href;
          }
        });
      }
    }
  }

  onStoreRefreshed(event: StoreRefreshedEvent) {
    console.debug('storeRefreshed', event);
    if (this.refreshQueue.length > 0) {
      const popped = this.refreshQueue.pop();
      this.initiateGridRefresh(popped.route, popped.purge);
    } else {
      this.refreshing = false;
    }
  }

  getParentPath(obj: IHEAObjectContainer<IDesktopObject>): string[] {
    const path: string[] = [];
    let parent: IRowNode<IHEAObjectContainer<IDesktopObject>> = this.gridApi.getRowNode(obj.uniqueId);
    if (parent) {
      do {
        path.push(parent.data.uniqueId);
        parent = parent.parent;
      } while (parent.rowIndex !== null);
    }
    return path.reverse();
  }

  private getGroupRenderer() {
    const self = this;
    // tslint:disable-next-line:no-empty
    function GroupRenderer() {
    }

    GroupRenderer.prototype.init = function(params) {
      const tempDiv = document.createElement('div');
      const heaObject = params.data.heaObject;
      const heaObjectSize = heaObject.human_readable_size ? heaObject.human_readable_size : '--';
      const heaObjectType = heaObject.type_display_name;
      const currentIcon = self.iconService.getIconForHEAObject(heaObject);
      const tooltip = `Name: ${heaObject.display_name}\nType: ${heaObjectType}\nSize: ${heaObjectSize}`
      if (currentIcon) {
        tempDiv.innerHTML = '<span title="' + tooltip + '">' +
          currentIcon +
          '<span style=\'padding-left: 0.5em\' >' + params.value + '</span>'
          + '</span>';
      } else {
        tempDiv.innerHTML = '<span title="' + tooltip + '">' + params.value + '</span>';
      }
      this.eGui = tempDiv.firstChild;
    };

    GroupRenderer.prototype.getGui = function() {
      return this.eGui;
    };

    return GroupRenderer;
  }

  private getGridColumnDefs() {
    const tempColumnDefs: ColDef[] = [];

    tempColumnDefs.push({
      headerName: 'SOURCE',
      field: 'heaObject.source',
      suppressSizeToFit: true,
      editable: false,
      width: 250,
      cellRenderer: TextAndTooltipRenderer,
      cellRendererParams: {
        tooltipField: 'heaObject.source_detail',
      }
    });
    tempColumnDefs.push({
      headerName: 'MODIFIED',
      field: 'heaObject.modified',
      suppressSizeToFit: true,
      editable: false,
      cellRenderer: params => {
        if (params.value === null || params.value === undefined) {
          return '';
        } else {
          return new Date(params.value).toLocaleString()
        }
      },
      comparator: (valueA, valueB, nodeA: RowNode, nodeB: RowNode, isInverted) => {
        const valueADate = new Date(valueA);
        const valueBDate = new Date(valueB);
        if (valueADate === valueBDate) return 0;
        return (valueADate > valueBDate) ? 1 : -1;
      },
      width: 200
    });
    tempColumnDefs.push({
      headerName: 'TYPE',
      field: 'heaObject.type_display_name',
      suppressSizeToFit: true,
      editable: false,
      width: 200
    });
    tempColumnDefs.push({
      headerName: 'SIZE',
      field: 'heaObject',
      suppressSizeToFit: true,
      editable: false,
      type: 'rightAligned',
      cellRenderer: params => {
        if (params.value === null || params.value === undefined) {
          return '';
        } else {
          return params.value.human_readable_size
        }
      },
      comparator: (valueA, valueB, nodeA: RowNode, nodeB: RowNode, isInverted) => {
        const valueASize = valueA.size;
        const valueBSize = valueB.size;
        if (valueASize === valueBSize) return 0;
        return valueASize - valueBSize;
      },
      width: 150
    });

    return tempColumnDefs;
  }

  private refreshObjectTree(event: string | boolean | IRowNode<IHEAObjectContainer<IDesktopObject>> | IRowNode<IHEAObjectContainer<IDesktopObject>>[], purge = false): void {
    console.debug('refreshObjectTree', event, purge);
    const refreshRows: Set<IRowNode<IHEAObjectContainer<IDesktopObject>>> = new Set();
    if (event === 'parentRefresh') {
      const selectedRows = this.rowSelectedSubject.value;
      for (const r of selectedRows) {
        if (r.parent) {
          refreshRows.add(r.parent);
        }
      }
      this.gridApi?.deselectAll();
      this.rowSelectedSubject.next([]);
    } else if (event instanceof RowNode) {
      refreshRows.add(event);
    } else if (Array.isArray(event)) {
      for (const e of event) {
        if (e instanceof RowNode) {
          refreshRows.add(e);
        }
      }
    } else if (event) {
      for (const r of this.rowSelectedSubject.value) {
        refreshRows.add(r)
      }
      if (refreshRows.size === 0) {
        refreshRows.add(null);
      }
    } else {
      return;
    }

    const refreshRowsSorted = [];
    for (const row of refreshRows) {
      refreshRowsSorted.push(row);
    }
    refreshRowsSorted.sort((a, b) => {
      if (a.rowIndex < b.rowIndex) {
        return -1;
      } else if (a.rowIndex > b.rowIndex) {
        return 1;
      } else {
        return 0;
      }
    })

    for (const row of refreshRowsSorted) {
      console.debug('refreshRowSorted', row);
      let refRowRoute = row?.getRoute();
      console.debug('refRowRoute', refRowRoute);
      if (row && !refRowRoute) {
        refRowRoute = row.parent.getRoute();
      }
      if (purge) {
        this.initiateGridRefresh(refRowRoute, true);
      } else {
        this.refreshQueue.push({route: refRowRoute, purge: false});
      }
    }

    while (this.refreshQueue.length > 0) {
      this.initiateGridRefresh(this.refreshQueue.pop().route, false);
    }
  }

  private reportError<T>(err) {
    console.error(err);
    return of([] as T[]);
  }

  private getChildren(obj: IHEAObjectContainer<IDesktopObject>): Observable<HEAObjectContainer<DesktopObject>[]> {
    const httpParams = this.getParentPathParams(obj);
    const obx = this.cjService.getActualObjectOrSelf(obj, {data: false, params: httpParams,
                                                           handleErrorsManually: this.refreshing});
    return obx.pipe(mergeMap(actual => {
      return this.cjService.getDefaultOpenerAndGo(actual, UtilService.REL_AWS_CONTEXT,
        {handleErrorsManually: this.refreshing, params: httpParams});
    })).pipe(catchError(this.reportError<IHEAObjectContainer<IDesktopObject>>));
  }

  private createServerSideDatasource(): IServerSideDatasource {
    const dataSource: IServerSideDatasource = {
      getRows: (params: IServerSideGetRowsParams<IHEAObjectContainer<IDesktopObject>>) => {
        if (this.roots && params.parentNode.level === -1) {
          params.success({rowData: this.roots});
        } else if (params.parentNode.data) {
          this.getChildren(params.parentNode.data)
          .pipe(take(1)).subscribe(resp => {
            const result = {rowData: resp, rowCount: resp.length};
            setTimeout(() => {
              this.gridApi.sizeColumnsToFit();
            });
            params.success(result);
          });
        }
        if (this.refreshQueue.length === 0) {
          this.refreshing = false;
        }

      },
    };
    return dataSource;
  }
}
