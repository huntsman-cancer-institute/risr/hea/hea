import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { HEAObjectContainer, IHEAObjectContainer } from 'src/app/models/heaobject-container.model';
import { IDesktopObject } from 'src/app/models/heaobject.model';

@Component({
  template: ''
})
export abstract class AbstractViewComponent implements OnInit, OnDestroy {

  @Input() set roots(roots: IHEAObjectContainer<IDesktopObject>[]) {
    console.debug('got roots', roots);
    this._roots = roots;
    this.rootsBS.next(roots);
  }

  get roots(): IHEAObjectContainer<IDesktopObject>[] {
    return this._roots;
  }
  @Input() set initialExpansion(initialExpansion: string[]) {
    this._initialExpansion = initialExpansion;
  }

  get initialExpansion(): string[] {
    return this._initialExpansion;
  }

  @Input() title: string;

  selectedObjects: Set<IHEAObjectContainer<IDesktopObject>>;
  @Output('selectedObjects') objectsSelected = new EventEmitter<IHEAObjectContainer<IDesktopObject>[]>(false);

  private rootsBS = new BehaviorSubject<HEAObjectContainer<IDesktopObject>[]>(null);
  private _roots: IHEAObjectContainer<IDesktopObject>[];
  private _initialExpansion: string[];
  private rootsSub: Subscription;

  ngOnInit(): void {
    this.rootsSub = this.rootsBS.subscribe(roots => {
      this.refresh(true, null, true);
    });
  }

  ngOnDestroy(): void {
    this.rootsSub?.unsubscribe();
  }

  /**
   * Refresh the specified objects in this view.
   *
   * @param event objects to refresh, URLs of objects to refresh, true to refresh all objects, false to do nothing, or
   * 'refreshParent' to refresh the parent of the currently selected objects.
   * @param parentOf objects whose parents to refreshed.
   * @param purge whether to remove and reload the objects to be refreshed, or refresh in place.
   */
  abstract refresh(event: string | boolean | URL | HEAObjectContainer<IDesktopObject> | HEAObjectContainer<IDesktopObject>[],
                   parentOf: URL | HEAObjectContainer<IDesktopObject> | HEAObjectContainer<IDesktopObject>[], purge: boolean);
  abstract openContainerObject(obj: IHEAObjectContainer<IDesktopObject>);

  abstract getParentPath(obj: IHEAObjectContainer<IDesktopObject>): string[];

  getParentPathParams(obj: IHEAObjectContainer<IDesktopObject>): HttpParams {
    return new HttpParams().appendAll({path: this.getParentPath(obj)});
  }

  clearSelection() {
    console.debug('clearing selection');
    if (this.selectedObjects && this.selectedObjects.size > 0) {
      this.selectedObjects = new Set<IHEAObjectContainer<IDesktopObject>>();
    }
  }

  clearSelectionAndEmit() {
    console.debug('clearing selection and emitting change');
    if (this.selectedObjects && this.selectedObjects.size > 0) {
      this.selectedObjects = new Set<IHEAObjectContainer<IDesktopObject>>();
      this.objectsSelected.emit(null);
    }
  }

}
