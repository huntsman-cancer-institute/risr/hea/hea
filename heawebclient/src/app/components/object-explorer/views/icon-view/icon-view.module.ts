import {NgModule} from '@angular/core';
import {IconViewComponent} from './icon-view.component';
import {OrgService} from '../../../../services/org/org.service';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NavigationModule} from '@huntsman-cancer-institute/navigation';
import {GridModule} from '@angular/flex-layout';
import {AuthGuard} from '../../../../services/auth/auth.guard';
import {MatLegacyTooltipModule as MatTooltipModule} from '@angular/material/legacy-tooltip';
import {TooltipExtractorModule} from '../../../../directives/tooltip-extractor.module';
import {MatLegacyInputModule as MatInputModule} from '@angular/material/legacy-input';
import {MatLegacyFormFieldModule as MatFormFieldModule} from '@angular/material/legacy-form-field';
import {UtilModule} from "../../../util/util.module";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '', component: IconViewComponent, canActivate: [AuthGuard]
      }
    ]),
    CommonModule,
    FormsModule,
    NgbModule,
    GridModule,
    NavigationModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    TooltipExtractorModule,
    UtilModule
  ],
  providers: [
    OrgService,
  ],
  declarations: [],
  exports: []
})

export class IconViewModule {}
