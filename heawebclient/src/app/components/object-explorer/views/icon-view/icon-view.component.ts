import {Component, OnDestroy, OnInit} from '@angular/core';
import { take } from 'rxjs/operators';
import { HEAObjectContainer, IHEAObjectContainer, isContainerType } from 'src/app/models/heaobject-container.model';
import { IDesktopObject } from 'src/app/models/heaobject.model';
import { IconService } from 'src/app/services/icon/icon.service';
import { IconOption } from 'src/app/models/icon.model';
import { CjService } from 'src/app/services/cj/cj.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { DialogOpenerService } from 'src/app/services/dialog-opener/dialog-opener.service';
import { UtilService } from 'src/app/services/util.service';
import { AbstractViewComponent } from '../abstract-view/abstract-view.component';
import { HttpParams } from '@angular/common/http';


@Component({
  selector: 'cb-icon-view',
  template: `
      <div class="d-flex flex-column m-3">
        <div class="d-flex flex-row mb-3" style="font-size: 1.25rem">
          <button [title]="backToDisplayName()" class="me-2 hea-nav" [disabled]="backDisabled" (click)="onClickBack()"><i class="fas fa-arrow-left"></i></button>
          <button [title]="forwardToDisplayName()" class="me-2 hea-nav" [disabled]="forwardDisabled" (click)="onClickForward()"><i class="fas fa-arrow-right"></i></button>
        </div>
        <div class="d-flex flex-row mb-3" style="font-size: 2rem;">
          <span class="me-2" *ngIf="parentDesktopObjectContainer" [innerHTML]="icons.getIconForHEAObject(parentDesktopObjectContainer.heaObject, [iconSize])"></span>
          <span class="font-weight-bold" *ngIf="parentDesktopObjectContainer" >{{parentDesktopObjectContainer.heaObject.display_name}}</span>
        </div>
        <div class="d-flex flex-row flex-wrap">
          <loading-overlay [busy]="dataLoading" [icon]="utilService.BUSY_ICON"></loading-overlay>
          <button type="button" data-bs-toggle="button" [title]="obj.heaObject.display_name" (click)="onClickObject(obj, $event)" (dblclick)="onDoubleClickObject(obj)"
                *ngFor="let obj of objList" style="min-width: 5em; margin: 1em; display: block" class="hea-object btn btn-primary" [ngClass]="{'active': isActive(obj)}" aria-pressed="false">
            <span style="display: block" style="color: #3d7a99" class="font-lg" [innerHTML]="icons.getIconForHEAObject(obj.heaObject, [iconSize])"></span>
            <span style="display: block" class="font-sm">{{obj.heaObject.display_name}}</span>
          </button>
        </div>
      </div>
  `,
  styles: [`
    button.hea-nav {
      border: none;
      background: none;
    }

    button.hea-object:not(.active) {
      background: none !important;
      color: black !important;
      border-color: transparent !important
    }

    button.hea-object:not(.active):hover {
      background: #98c7e07a !important;
    }

    button.hea-object.active {
    }
  `]
})
export class IconViewComponent extends AbstractViewComponent implements OnInit, OnDestroy {
  parentDesktopObjectContainer: IHEAObjectContainer<IDesktopObject>;
  objList: IHEAObjectContainer<IDesktopObject>[] = [];
  iconSize: IconOption;
  breadcrumbs: IHEAObjectContainer<IDesktopObject>[] = [];
  dataLoading = [true];

  private subs: Subscription[] = [];
  private desktopObjectContainerBS = new BehaviorSubject<IHEAObjectContainer<IDesktopObject>[]>(null);
  private highWaterMark = 0;
  private level = 0;
  private singleClick: boolean;

  constructor(public icons: IconService,
              public utilService: UtilService,
              private cj: CjService,
              private dialog: DialogOpenerService) {
    super();
    this.iconSize = IconOption.LARGE;
  }

  ngOnInit() {
    console.debug('Initializing icon view');
    super.ngOnInit();
    this.selectedObjects = new Set();

    this.subs.push(this.desktopObjectContainerBS.subscribe(resp => {
      console.debug('rendering', resp);
      if (resp) {
        this.objList = resp;
      }
      this.dataLoading = [false];
    }));
  }

  ngOnDestroy(): void {
    console.debug('Destroying icon view');
    super.ngOnDestroy();
    this.subs.map(sub => sub.unsubscribe());
  }

  onClickObject(obj: HEAObjectContainer<IDesktopObject>, event: PointerEvent) {
    this.singleClick = true;
    setTimeout(() => {
      if (this.singleClick) {
        console.debug('single click');
        if (this.selectedObjects.has(obj)) {
          if (!event.ctrlKey) {
            this.clearSelection();
          } else {
            this.selectedObjects.delete(obj);
          }
        } else {
          if (!event.ctrlKey) {
            this.clearSelection();
          }
          this.selectedObjects.add(obj);
        }
        console.debug('emitting', this.selectedObjects);
        this.objectsSelected.emit([...this.selectedObjects]);
      }
    }, 250)


  }

  onDoubleClickObject(obj: HEAObjectContainer<IDesktopObject>) {
    this.singleClick = false;
    console.debug('double click', obj);
    this.cj.getActualObjectOrSelf(obj, {data: false}).pipe(take(1)).subscribe(obj => {
      const containerType = isContainerType(obj)
      if (containerType) {
        this.level++;
        this.openContainerObject(obj);
      } else {
        this.dialog.openModal(obj.href, 'Edit', (result) => {
          if (result) {
            this.refresh(obj, this.parentDesktopObjectContainer, false);
          }
        }, {params: this.getParentPathParams(obj)});
      }
    });
  }

  onClickBack() {
    console.debug('history onClickBack', this.parentDesktopObjectContainer, this.breadcrumbs);
    this.level--;
    this.openContainerObject(this.backTo());
  }

  onClickForward() {
    console.debug('history onClickForward', this.parentDesktopObjectContainer, this.breadcrumbs);
    this.level++;
    this.openContainerObject(this.forwardTo());
  }

  get roots() {
    return super.roots;
  }

  set roots(roots: IHEAObjectContainer<IDesktopObject>[]) {
    this.parentDesktopObjectContainer = null;
    this.breadcrumbs = [];
    this.objList = [];
    this.level = 0;
    super.roots = roots;
  }

  refresh(event: string | boolean | HEAObjectContainer<IDesktopObject> | HEAObjectContainer<IDesktopObject>[],
    parentOf: HEAObjectContainer<IDesktopObject> | HEAObjectContainer<IDesktopObject>[],
    purge: boolean) {
    this.dataLoading = [true];
    this.openContainerObject(this.parentDesktopObjectContainer);
  }

  openContainerObject(obj: IHEAObjectContainer<IDesktopObject>) {
    console.debug('opening', obj);
    this.dataLoading = [true];
    this.clearSelectionAndEmit();
    this.objList = null;
    this.parentDesktopObjectContainer = obj;
    if (obj) {
      if (obj !== this.breadcrumbs[this.breadcrumbs.length - 1]) {
        if (this.level > this.highWaterMark) {
          this.breadcrumbs.length = this.highWaterMark;
        }
        this.breadcrumbs.push(obj);
      }
      let httpParams = new HttpParams();
      for (const pathPart of this.breadcrumbs) {
        httpParams = httpParams.append('path', pathPart.uniqueId);
      }
      this.cj.getDefaultOpenerAndGo(obj, null, {params: httpParams}).pipe(take(1)).subscribe(resp => {
        this.desktopObjectContainerBS.next(resp);
        this.highWaterMark = this.level;
      });
    } else {
      this.desktopObjectContainerBS.next(this.roots);
      this.highWaterMark = this.level;
    }
  }

  backToDisplayName() {
    if (this.backDisabled) {
      return 'Back';
    } else {
      const bt = this.backTo();
      if (bt) {
        return `Back to ${bt.heaObject.display_name}`
      } else if (this.title) {
        return `Back to ${this.title}`
      } else {
        return 'Back';
      }
    }
  }

  forwardToDisplayName() {
    const ft = this.forwardTo();
    return ft ? `Forward to ${ft.heaObject.display_name}` : 'Forward';
  }

  get backDisabled() {
    return this.level === 0;
  }

  get forwardDisabled() {
    return this.level === this.breadcrumbs.length;
  }

  isActive(obj: IHEAObjectContainer<IDesktopObject>) {
    return this.selectedObjects && this.selectedObjects.has(obj);
  }

  getParentPath(obj: IHEAObjectContainer<IDesktopObject>): string[] {
    const path: string[] = []
    for (const obj of this.breadcrumbs) {
      path.push(obj.uniqueId);
    }
    return path.reverse();
  }

  private backTo() {
    return this.breadcrumbs[this.breadcrumbs.length - 2] ? this.breadcrumbs[this.breadcrumbs.length - 2] : null
  }

  private forwardTo() {
    return this.parentDesktopObjectContainer ? this.breadcrumbs[this.breadcrumbs.indexOf(this.parentDesktopObjectContainer)] : this.breadcrumbs[0];
  }
}
