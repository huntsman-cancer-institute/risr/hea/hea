import {RouterModule, Routes} from "@angular/router";
import {ObjectExplorerComponent} from "./object-explorer.component";

const ROUTES: Routes = [
  { path: 'collections/:collectionId', component: ObjectExplorerComponent }
];

export const OBJECT_EXPLORER_ROUTING = RouterModule.forChild(ROUTES);
