import {Component, ElementRef, HostListener, Inject, OnInit, ViewChild} from '@angular/core';
import {LegacyDialogPosition as DialogPosition, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef} from '@angular/material/legacy-dialog';
import { UtilService } from 'src/app/services/util.service';
import { DialogsService } from '../../util/dialog/dialogs.service';
import { ApiService } from 'src/app/services/api/api.service';
import { CjService } from 'src/app/services/cj/cj.service';
import {AgGridDynamicComponent} from '../../util/interpreted-contents/dynamic-components/ag-grid-dynamic.component';
import { HEAObjectContainer } from 'src/app/models/heaobject-container.model';
import { IconService } from 'src/app/services/icon/icon.service';
import { IconOption } from 'src/app/models/icon.model';
import {map, take} from 'rxjs/operators';
import {forkJoin} from "rxjs";


@Component({
  selector: 'cb-trash',
  template: `
    <div #topmostLeftmost class="cb-card cb-card-no-margin d-flex flex-column">
      <loading-overlay [busy]="dataLoading" [icon]="this.utilService.BUSY_ICON"></loading-overlay>
      <div class="cb-card-header d-flex flex-row" (mousedown)="onMouseDownHeader($event)">
        <div class="padded-left-right" [innerHTML]="icon">
        </div>
        <div class="flex-grow-1 padded-left-right">
          TRASH
        </div>
        <div class="me-1" (click)="close()" title="Close">
          <i class="bi bi-x-lg" style="font-size: 1.1rem">
          </i>
        </div>
      </div>
      <div class="d-flex flex-column cb-card-body flex-grow-1 overflow-auto">
        <div class="font-small">Contains objects that were not permanently deleted</div>
        <div class="flex-grow-1">
          <ag-grid-dynamic #agGridDynamic [showAddRemove]="false"
                           [mode]="'file'"
                           [columnDefs]="gridColumnDefs"
                           [rowSelection]="'multiple'"
                           [data]="rowDataForGrid"
                           [paramName]="'object'"
                           [readOnly]="true"
                           (rowSelected)=onRowSelected($event)>
          </ag-grid-dynamic>
        </div>
        <div class="padded">
          <div>Selected Objects: {{ rowData.length }}  </div>
          <div>Size: {{ dataSize }} </div>
        </div>
      </div>
      <div class="d-flex flex-column full-width">
        <div class="d-flex flex-row full-width justify-content-end padded-bottom padded-left padded-left-right">
          <div class="flex-grow-1">
          </div>
          <div class="d-flex flex-row">
            <div class="padded-top-left-bottom">
              <button (click)="close()" mat-button class="grey-1g">
                CANCEL
              </button>
            </div>
            <div class="padded-top-left-bottom">
              <button [disabled]="!(!!rowSelected)" (click)="delete($event)" mat-button class="delete-1g">
                DELETE
              </button>
            </div>
            <div class="padded-top-left-bottom">
              <button [disabled]="!(!!rowSelected)" (click)="restore($event)" mat-button class="primary-1g">
                RESTORE
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`

    .cb-card {
      margin: 0.5em;
      background-color: #EAF9FF;
      box-shadow: 0 4px 8px #92969e;
      border: 0.1px solid #707070;
      border-radius: 10px;
      color: #5a6366;
      opacity: 1;
      min-height: 24em;
      width: 80vw;
      height: 80vh;
    }


    .cb-card-body {
      padding: 0 0.3em 0 0.3em;
      min-height: 10em;
    }

    .padded-top-left-bottom {
      padding: 0.3em 0 0.3em 0.3em;
    }


  `]
})
export class TrashComponent implements OnInit {

  // TODO: This is called by the original generated component of OriginalHeaobjectExplorerComponent.
  //  Need to be refactored for adding new objects in ObjectExplorerComponent.

  folderName: string;
  refresh = false;
  rowData: any[] = [];
  gridColumnDefs: any[];
  dataLoading: boolean[] = [false];
  @ViewChild('agGridDynamic') agGridDynamic: AgGridDynamicComponent;
  @ViewChild('topmostLeftmost', {static: false}) private topmostLeftmost: ElementRef;

  private initialOffsetX: any;
  private initialOffsetY: any;
  private movingDialog = false;

  rowDataForGrid: any[];
  rowSelected?: HEAObjectContainer<any>;

  constructor(private dialogRef: MatDialogRef<TrashComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    public utilService: UtilService,
    private dialogsService: DialogsService,
    private apiService: ApiService,
    private cjService: CjService,
    private iconService: IconService) {

  }

  ngOnInit(): void {
    this.dataLoading = [true];
    this.gridColumnDefs = this.getGridColumnDefs();
    this.cjService.getResource(this.data.trashLink.href).pipe(take(1)).subscribe( result => {
      this.rowData = result;
      this.rowDataForGrid = result;
      this.dataLoading = [false];
    }, err => {
      this.dataLoading = [false];
    });
  }

  public close() {
    this.clear();
    this.dialogRef.close(this.refresh);
  }

  public clear() {
    this.rowData.length = 0;
    this.rowDataForGrid = [];
    this.agGridDynamic.clear();
  }

  public get dataSize(): string {
    let size = 0;
    for (const data of this.rowData) {
      size += data.heaObject.size;
    }
    return this.utilService.formatSize('' + size, 'GB', true);
  }

  public get icon(): string {
    return this.iconService.getIconForRel('hea-trash', [IconOption.LARGE]);
  }

  private getGridColumnDefs(): any[] {
    const tempColumnDefs: any[] = [];

    tempColumnDefs.push({
      headerName: 'NAME',
      field: 'heaObject.display_name',
      editable: false,
      width: 300,
      resizable: true,
      sortable: true
    });
    tempColumnDefs.push({
      headerName: 'ORIGIN',
      field: 'heaObject.human_readable_original_location',
      editable: false,
      width: 200,
      maxWidth: 250,
      resizeable: true,
      sortable: true,
    });
    tempColumnDefs.push({
      headerName: 'VERSION_ID',
      field: 'heaObject.version',
      editable: false,
      width: 200,
      maxWidth: 200,
      resizeable: true,
      sortable: true,
    });
    tempColumnDefs.push({
      headerName: 'STORAGE',
      field: 'heaObject.storage_class',
      editable: false,
      width: 200,
      maxWidth: 150,
      resizeable: true,
      sortable: true,
    });
    tempColumnDefs.push({
      headerName: 'CREATED',
      field: 'heaObject.created',
      editable: false,
      width: 200,
      maxWidth: 160,
      resizeable: true,
      sortable: true,
      cellRenderer: params => {
        if (params.value === null || params.value === undefined) {
          return '--';
        } else {
          return new Date(params.value).toLocaleString()
        }
      }
    });
    tempColumnDefs.push({
      headerName: 'DELETED',
      field: 'heaObject.deleted',
      editable: false,
      width: 200,
      maxWidth: 160,
      resizeable: true,
      sortable: true,
      cellRenderer: params => {
        if (params.value === null || params.value === undefined) {
          return '--';
        } else {
          return new Date(params.value).toLocaleString()
        }
      }
    });
    tempColumnDefs.push({
      headerName: 'TYPE',
      field: 'heaObject.actual_object_type_name',
      editable: false,
      width: 200,
      maxWidth: 220,
      resizeable: true,
      sortable: true,
    });
    tempColumnDefs.push({
      headerName: 'SIZE',
      field: 'heaObject.human_readable_size',
      editable: false,
      width: 100,
      maxWidth: 100,
      resizeable: true,
      sortable: true
    });

    return tempColumnDefs;
  }

  onRowSelected(event: any) {
    if (event.node.isSelected()) {
      console.debug('Selected row', event);
      this.rowSelected = event.data;
    }

  }

  delete(event: PointerEvent) {
    this.dialogsService.confirm("The selected objects will be permanently deleted. Are you sure?").pipe(take(1)).subscribe(result => {
      if (result) {
        let deleteResults: any[] = [];

        this.dataLoading = [true];
        for (let node of this.agGridDynamic.selectedNodes) {
          if (node && node.data && node.data.links) {
            deleteResults.push(this.cjService.getResource(this.cjService.findLink(node.data.links, 'hea-trash-delete-confirmer').href).pipe(take(1)).pipe(map((result: any) => {
              // console.log("deleted");
            })));
          }
        }

        forkJoin(deleteResults).subscribe(resp => {
          this.dataLoading = [false];
          this.close();
        }, (err: any) => {
          console.log(err);
          this.dataLoading = [false];
          this.close();
        });
      }
    });
  }

  restore(event: PointerEvent) {
    let restoreResults: any[] = [];

    this.dataLoading = [true];
    for (let node of this.agGridDynamic.selectedNodes) {
      if (node && node.data && node.data.links) {
        restoreResults.push(this.cjService.getResource(this.cjService.findLink(node.data.links, 'hea-trash-restore-confirmer').href).pipe(take(1)).pipe(map((result: any) => {
          // console.log("restored");
        })));
      }
    }

    forkJoin(restoreResults).subscribe(resp => {
      this.refresh = true;
      this.dataLoading = [false];
      this.close();
    }, (err: any) => {
      console.log(err);
      this.refresh = true;
      this.dataLoading = [false];
      this.close();
    });
  }

  public onMouseDownHeader(event: MouseEvent) {
    if (!event) {
      return;
    }
    //hack until we upgrade to angular 14 and can directly get the cdk-overlay-pane and query its position.
    let cdkOverlayPane = this.topmostLeftmost.nativeElement.parentNode;
    while (true) {
      if (cdkOverlayPane.className.indexOf('cdk-overlay-pane') >= 0) {
        break;
      } else {
        cdkOverlayPane = cdkOverlayPane.parentNode;
      }
    }
    const clientRect = cdkOverlayPane.getBoundingClientRect();
    this.initialOffsetX = event.clientX - clientRect.left;
    this.initialOffsetY = event.clientY - clientRect.top;

    this.movingDialog = true;
  }

  @HostListener('window:mousemove', ['$event'])
  public onMouseMove(event: MouseEvent) {
    if (!event) {
      return;
    }

    if (this.movingDialog) {
      const positionX = event.clientX - this.initialOffsetX;
      const positionY = event.clientY - this.initialOffsetY;

      const newDialogPosition: DialogPosition = {
        left:   '' + positionX + 'px',
        top:    '' + positionY + 'px',
      };

      this.dialogRef.updatePosition(newDialogPosition);
    }
  }

  @HostListener('window:mouseup', ['$event'])
  public onMouseUp() {
    this.movingDialog = false;
  }
}
