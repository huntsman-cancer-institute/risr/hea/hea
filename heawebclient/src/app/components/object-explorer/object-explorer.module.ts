import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule} from '@angular/common/http';
import { MatLegacyListModule as MatListModule } from '@angular/material/legacy-list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatLegacyMenuModule as MatMenuModule } from '@angular/material/legacy-menu';
import { MatLegacyDialogModule as MatDialogModule } from '@angular/material/legacy-dialog';
import { MatLegacyInputModule as MatInputModule } from '@angular/material/legacy-input';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { MatLegacyTooltipModule as MatTooltipModule } from '@angular/material/legacy-tooltip';
import {ObjectExplorerComponent} from './object-explorer.component';
import {OBJECT_EXPLORER_ROUTING} from './object-explorer.routes';
import {AgGridEditorModule} from '../util/grid-editors/ag-grid-editor.module';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {UtilModule} from '../util/util.module';
import {UploadFileCardComponent} from './cards/upload-file-card.component';
import {DownloadCardComponent} from './cards/download-card.component';
import {MatLegacyProgressBarModule as MatProgressBarModule} from '@angular/material/legacy-progress-bar';
import {MatLegacySnackBarModule as MatSnackBarModule} from '@angular/material/legacy-snack-bar';
import {DeleteCardComponent} from './cards/delete-card.component';
import {SelectModule} from '@huntsman-cancer-institute/input';
import {MiscModule} from '@huntsman-cancer-institute/misc';
import {CreateCardComponent} from './cards/create-card.component';
import {DynamicStandardCardComponent} from './cards/dynamic-standard-card.component';
import { DynamicClipboardCardComponent } from './cards/dynamic-clipboard-card.component';
import {TrashComponent} from './trash/trash-dialog.component';
import { VersionsCardComponent } from './cards/versions-card.component';
import { ObjectExplorerHeaderComponent } from './header/object-explorer-header.component';
import { TreeGridViewComponent } from './views/tree-grid-view/tree-grid-view.component';
import { IconViewComponent } from './views/icon-view/icon-view.component';
import {FilePreviewCardComponent} from './cards/file-preview-card.component';


@NgModule({
    imports: [
        OBJECT_EXPLORER_ROUTING,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatButtonModule,
        MatDialogModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatSnackBarModule,
        MatListModule,
        MatMenuModule,
        MatToolbarModule,
        MatTooltipModule,
        MatProgressBarModule,
        MatMomentDateModule,
        MatCardModule,
        AgGridEditorModule,
        AgGridModule,
        SelectModule,
        MiscModule,
        UtilModule
    ],
    declarations: [
        ObjectExplorerComponent,
        DeleteCardComponent,
        UploadFileCardComponent,
        DownloadCardComponent,
        CreateCardComponent,
        DynamicStandardCardComponent,
        DynamicClipboardCardComponent,
        TrashComponent,
        VersionsCardComponent,
        ObjectExplorerHeaderComponent,
        TreeGridViewComponent,
        IconViewComponent,
        FilePreviewCardComponent
    ],
    exports: [DeleteCardComponent],
    providers: []
}) export class ObjectExplorerModule { }
