import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cb-rename-heaobject',
  template: `
      <h1 mat-dialog-title>Rename Element</h1>

      <mat-dialog-content>
          <mat-form-field class="example-full-width">
              <input matInput placeholder="Folder Name" [(ngModel)]="folderName" />
          </mat-form-field>
      </mat-dialog-content>

      <mat-dialog-actions>
          <button mat-raised-button mat-dialog-close>Cancel</button>
          <button mat-raised-button [mat-dialog-close]="folderName" color="primary">
              OK
          </button>
      </mat-dialog-actions>
  `,
  styles: []
})
export class RenameObjectDialogComponent implements OnInit {

  // TODO: This is called by the original generated component of OriginalHeaobjectExplorerComponent.
  //  Need to be refactored for editing object in ObjectExplorerComponent.

  folderName: string;
  constructor() { }

  ngOnInit(): void {
  }

}
