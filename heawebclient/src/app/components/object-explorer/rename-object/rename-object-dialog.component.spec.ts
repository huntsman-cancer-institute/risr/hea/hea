import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RenameObjectDialogComponent } from './rename-object-dialog.component';

describe('RenameHEAObjectDialogComponent', () => {
  let component: RenameObjectDialogComponent;
  let fixture: ComponentFixture<RenameObjectDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RenameObjectDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenameObjectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
