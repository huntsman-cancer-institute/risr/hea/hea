import {Component, ViewChild} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import streamSaver from 'streamsaver';
import {ColDef, RowNode} from 'ag-grid-community';
import {UtilService} from '../../../services/util.service';
import {first, takeUntil} from 'rxjs/operators';
import {ApiService} from '../../../services/api/api.service';
import {AgGridDynamicComponent} from '../../util/interpreted-contents/dynamic-components/ag-grid-dynamic.component';
import {CjService} from '../../../services/cj/cj.service';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { AbstractCardComponent } from '../../util/cards/abstract-card.component';

@Component({
  selector: 'cb-download-card',
  template: `
    <div class="cb-card cb-card-no-margin d-flex flex-column">
      <loading-overlay [busy]="dataLoading" [icon]="busyIcon"></loading-overlay>
      <div class="cb-card-header d-flex flex-row">
        <div class="padded-left-right">
          <i class="bi bi-download" style="font-size: 1.2rem">
          </i>
        </div>
        <div class="flex-grow-1">
          DOWNLOAD
        </div>
        <div class="me-1" (click)="close()" title="Close">
          <i class="bi bi-x-lg" style="font-size: 1.1rem">
          </i>
        </div>
      </div>
      <div class="d-flex flex-column cb-card-body inner-container-scroll overflow-auto">
        <div class="flex-grow-1">
          <ag-grid-dynamic #agGridDynamic [showAddRemove]="false" [title]="'FOLDERS & FILES'"
                           [mode]="'file'"
                           [infoTooltip]="infoMessage"
                           [columnDefs]="gridColumnDefs"
                           [data]="subject"
                           [paramName]="'object'"
                           [readOnly]="true">
          </ag-grid-dynamic>
        </div>
        <div class="padded">
          <div>Selected Files: {{this.subject.length}}</div>
          <div>Size: {{dataSize}}</div>
        </div>
      </div>
      <div class="d-flex flex-column">
        <div class="d-flex flex-row padded-bottom padded-left padded-left-right">
          <div class="flex-grow-1">
          </div>
          <div class="padded-top-left-bottom">
            <button (click)="close()" mat-button class="grey-1g">
              CANCEL
            </button>
          </div>
          <div class="padded-top-left-bottom">
            <button (click)="download()" mat-button class="secondary-1g">
              DOWNLOAD
            </button>
          </div>
        </div>
      </div>
    </div>


  `,
  styles: [`

    .cb-card {
      margin: 0.5em;
      background-color: #EAF9FF;
      box-shadow: 0 4px 8px #92969e;
      border: 0.1px solid #707070;
      border-radius: 10px;
      color: #5a6366;
      opacity: 1;
      width: 40em;
    }

    .cb-card-body {
      padding: 0 0.3em 0 0.3em;
      min-height: 10em;
    }

    .padded-top-left-bottom {
      padding: 0.3em 0 0.3em 0.3em;
    }

  `]
})
export class DownloadCardComponent extends AbstractCardComponent{

  progressValue: number;
  gridColumnDefs: any[];
  defaultColDef: ColDef = {resizable: true, sortable: true};
  infoMessage = 'Use the left grid to select and deselect objects. Data transfer fees apply when you download';
  @ViewChild('agGridDynamic') agGridDynamic: AgGridDynamicComponent;

  private _isCollapsed = false;
  private downloadSubscription: Subscription = new Subscription();
  private saveSubscriptions: Subscription[] = [];
  private _saveCounter: number = 0;

  private get saveCounter(): number {
    return this._saveCounter;
  }

  private set saveCounter(value: number) {
    this._saveCounter = value;
    if (this._saveCounter >= this.saveSubscriptions.length) {
      for (let saveSubscription of this.saveSubscriptions) {
        saveSubscription.unsubscribe();
      }

      this.saveSubscriptions = [];
    }
  }

  constructor(private utilService: UtilService,
              private apiService: ApiService, private cjService: CjService,
              private snackBar: MatSnackBar) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.gridColumnDefs = this.getGridColumnDefs();
    this.dataLoading = [false];
  }

  public get isCollapsed(): boolean {
    return this._isCollapsed;
  }
  public get dataSize(): string {
    let size = 0;
    for (const data of this.subject) {
      size += data.heaObject['size'] || 0;
    }
    return this.utilService.formatSize('' + size, 'GB', true);
  }

  public getRowNodeID(dataItem: any) {
    return dataItem.heaObject.id;
  }

  public clickCollapse(): void {
    this._isCollapsed = true;
  }

  public clickExpand(): void {
    this._isCollapsed = false;
  }

  public download(): void {

    for (let saveSubscription of this.saveSubscriptions) {
      saveSubscription.unsubscribe();
    }
    this.saveSubscriptions = [];
    this.saveCounter = 0;
    this.dataLoading = [true];

    for (let row of this.subject) {
      this.cjService.getActualObjectOrSelf(row).pipe(first()).subscribe(temp => {
        let openerLink: any = this.cjService.findLink(temp.links, UtilService.REL_OPENER_CHOICES).href;
        const stopSubject = new Subject();
        this.cjService.getOpenerLinks(openerLink).pipe(takeUntil(stopSubject)).subscribe(links => {
          // Try hea-default-downloader, then hea-downloader, and then the default opener.
          let cjLink = this.cjService.findLink(links, 'hea-default-downloader')
          if (!cjLink) {
            cjLink = this.cjService.findLink(links, 'hea-downloader')
          }
          if (!cjLink) {
            cjLink = this.cjService.findLink(links, UtilService.REL_OPENER_DEFAULT)
          }

          this.saveSubscriptions.push(this.apiService.getContent(cjLink.href).subscribe(event => {
            if (event) {
              const contentDisp = event.headers.get('Content-Disposition');
              // always 9 chars because 'filename='
              const fileName = contentDisp ? contentDisp.slice(contentDisp.lastIndexOf('filename') + 9) : 'CORE Browser Download';
              const contentLength = parseInt(event.headers['Content-Length'])
              streamSaver.mitm = './assets/downloads/mitm.html';
              const fileStream = streamSaver.createWriteStream(fileName, {size: contentLength});
              const readableStream = event.body;
              if (readableStream.pipeTo) {
                return readableStream.pipeTo(fileStream);
              }
              const writer = fileStream.getWriter();
              const reader = readableStream.getReader();
              const pump = () => reader.read()
                  .then(res => res.done
                      ? writer.close()
                      : writer.write(res.value).then(pump));
              pump();
              this.saveCounter++;
            }
            stopSubject.next();
            stopSubject.complete();
          }));
        });

        this.dataLoading = [false];
        this.close();
      });
    }

    this.snackBar.open('You can view your file\'s download progress in your browser.',
      null, {horizontalPosition: 'center', verticalPosition: 'top', duration: 4000});
  }

  public close(): void {
    this.clear();
    super.close();
  }

  public clear() {
    this.subject.length = 0;
    this.agGridDynamic.clear();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    if (this.downloadSubscription){
      this.downloadSubscription.unsubscribe();
    }
  }

  private getGridColumnDefs(): any[] {
    const tempColumnDefs: any[] = [];

    tempColumnDefs.push({
      headerName: 'Object',
      field: 'heaObject.display_name',
      editable: false,
      width: 300,
      resizable: true,
      sortable: true
    });
    tempColumnDefs.push({
      headerName: 'Storage Class',
      field: 'heaObject.storage_class',
      editable: false,
      width: 200,
      resizeable: true,
      sortable: true,
    });
    tempColumnDefs.push({
      headerName: 'Size',
      field: 'heaObject.human_readable_size',
      editable: false,
      width: 100,
      resizeable: true,
      sortable: true,
      comparator: (valueA, valueB, nodeA: RowNode, nodeB: RowNode, isInverted) => {
        return this.utilService.sortNumber(nodeA.data.heaObject.size, nodeB.data.heaObject.size);
      }
    });

    return tempColumnDefs;
  }
}
