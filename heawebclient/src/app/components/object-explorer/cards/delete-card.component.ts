import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription, from, of, throwError} from 'rxjs';
import {RowNode} from 'ag-grid-community';
import {UtilService} from '../../../services/util.service';
import {ApiService} from '../../../services/api/api.service';
import {MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef} from '@angular/material/legacy-dialog';
import {AgGridDynamicComponent} from '../../util/interpreted-contents/dynamic-components/ag-grid-dynamic.component';
import {CjService} from '../../../services/cj/cj.service';
import { HEAObjectContainer, IHEAObjectContainer, uniqueIdToURL } from 'src/app/models/heaobject-container.model';
import { DesktopObject, IDesktopObject } from 'src/app/models/heaobject.model';
import { catchError, concatMap, map, mergeMap, take, toArray } from 'rxjs/operators';
import { AbstractViewComponent } from '../views/abstract-view/abstract-view.component';
import { error } from 'console';
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { ICJReadObject } from 'src/app/models/cj.model';


@Component({
  selector: 'cb-delete-card',
  template: `
    <div class="cb-card d-flex flex-column">
      <loading-overlay [busy]="dataLoading" [icon]="this.utilService.BUSY_ICON"></loading-overlay>
      <div class="cb-card-header d-flex flex-row">
        <div class="">
          <i class="bi bi-exclamation-triangle-fill" style="font-size: 1.2rem; color: var(--vermilion-darker);">
          </i>
        </div>
        <div class="flex-grow-1 padded-left-right">
          DELETE
        </div>
        <div class="me-1" (click)="close()" title="Close">
          <i class="bi bi-x-lg" style="font-size: 1.1rem">
          </i>
        </div>
      </div>
      <div class="d-flex flex-column cb-card-body inner-container-scroll overflow-auto">
        <div class="font-small">
          <text-display *ngIf="notEmptyBucket && notEmptyBucket !== ''" [description]="unEmptyDescription"
                        [isList]="true" style="color: var(--vermilion-dark);">
          </text-display>
          <text-display *ngIf="!notEmptyBucket || notEmptyBucket === ''" [description]="description"
                        [isList]="true" style="color: var(--vermilion-dark);">
          </text-display>
        </div>
        <div class="flex-grow-1">
          <ag-grid-dynamic #agGridDynamic [showAddRemove]="false" [title]="'SELECTED OBJECTS'"
                           [mode]="'file'"
                           [infoTooltip]="infoMessage"
                           [columnDefs]="gridColumnDefs"
                           [data]="rowDataForGrid"
                           [paramName]="'object'"
                           [readOnly]="true">
          </ag-grid-dynamic>
        </div>
        <div class="padded">
          <div>Selected Objects: {{this.rowData.length}}</div>
          <div>Size: {{dataSize}}</div>
        </div>
        <div *ngIf="!notEmptyBucket || notEmptyBucket === ''" class="padded">
          <div class="flex-grow-1"  style="color: var(--vermilion-darker)">CONFIRM
          </div>
          <div class="border">
            <div class="flex-grow-1 padded-left">To confirm deletion, type <b>delete</b> in the text input field:
            </div>
            <div class="flex-grow-1 padded">
              <input [(ngModel)]="deleteConfirm" type="text" class="full-width full-height"/>
            </div>
          </div>
        </div>
      </div>
      <div class="d-flex flex-column full-width">
        <div class="d-flex flex-row padded-right">
          <div class="flex-grow-1">
          </div>
          <div class="padded-top-left-bottom">
            <button (click)="close()" mat-button class="grey-1g">
              CANCEL
            </button>
          </div>
          <div [hidden]="notEmptyBucket && notEmptyBucket != ''" class="padded-top-left-bottom">
            <button [disabled]="!deleteConfirmed" (click)="delete()" mat-button class="delete-1g">
              DELETE
            </button>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`

    .cb-card {
      margin: 0.5em;
      background-color: #EAF9FF;
      box-shadow: 0 4px 8px #92969e;
      border: 0.1px solid #707070;
      border-radius: 10px;
      color: #5a6366;
      opacity: 1;
      min-height: 24em;
      width: 42em;
      height: 32em;
    }

    .cb-card-body {
      padding: 0 0.3em 0 0.3em;
      min-height: 10em;
    }

    .padded-top-left-bottom {
      padding: 0.3em 0 0.3em 0.3em;
    }


  `]
})
export class DeleteCardComponent implements OnInit, OnDestroy {
  deleteConfirm: string = '';
  notEmptyBucket: string = '';

  progressValue: number;
  rowData: HEAObjectContainer<IDesktopObject>[] = [];
  gridColumnDefs: any[];
  infoMessage = 'Use the left grid to select and deselect objects. Data transer fees apply when you download';
  @ViewChild('agGridDynamic') agGridDynamic: AgGridDynamicComponent;
  rowDataForGrid: any[];
  unEmptyDescription: string[] = ['The selected bucket is not empty. Buckets must be empty before they can be deleted.'];
  description: string[] = [
    'If a folder is selected for deletion, all objects in the folder will be deleted, and any new objects added while the delete action is in progress might also be deleted.',
    'If an object is selected for deletion, any new objects with the same name that are uploaded before the delete action is completed will also be deleted.',
    'Deletions cannot be undone',
  ];
  dataLoading: boolean[] = [false];

  private _isCollapsed = false;
  private deleteSubscription: Subscription = new Subscription();
  private subject: IHEAObjectContainer<IDesktopObject>[];
  private view: AbstractViewComponent;

  constructor(private dialogRef: MatDialogRef<DeleteCardComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              public utilService: UtilService,
              private apiService: ApiService,
              private cjService: CjService) {
    if (this.data) {
      this.subject = this.data.subject;
      this.view = this.data.view;
    }
  }

  ngOnInit(): void {
    this.gridColumnDefs = this.getGridColumnDefs();
    this.notEmptyBucket = '';
    this.rowData = this.subject.filter(val => val && val.heaObject);
    this.rowDataForGrid = this.rowData;
  }

  get isCollapsed(): boolean {
    return this._isCollapsed;
  }
  get deleteConfirmed() {
    return this.deleteConfirm && this.deleteConfirm.toUpperCase() === 'DELETE';
  }

  get dataSize(): string {
    let size = 0;
    for (const data of this.rowData) {
      if ('size' in data.heaObject) {
        size += data.heaObject['size'] as number;
      }
    }
    return this.utilService.formatSize('' + size, 'GB', true);
  }

  getRowNodeID(dataItem: any) {
    return dataItem.heaObject.id;
  }

  clickCollapse(): void {
    this._isCollapsed = true;
  }

  clickExpand(): void {
    this._isCollapsed = false;
  }

  /**
   * Deletes the selected objects 'delete' has been entered into the confirmation field. Also refreshes any parent
   * objects of the deleted objects that no longer exist as a result of the deletion (for example, AWS S3 objects,
   * relational database objects where a delete cascades to a parent object, etc.).
   */
  delete(): void {
    if (this.deleteConfirm && this.deleteConfirm.toUpperCase() === 'DELETE') {
      this.dataLoading = [true];
      from(this.rowData)
        .pipe(
          mergeMap(item => {
            const params = this.view.getParentPathParams(item);
            return this.cjService.getActualObjectOrSelf(item, {params: params}).pipe(map(actual => {
              return {actual: actual, href: actual.href, path: params.getAll('path')};
            }));
          }))
        .pipe(
          mergeMap(resp => {
            return this.apiService.delete(resp.href).pipe(map(resp2 => {
              return {actual: resp.actual, response: resp2, path: resp.path}
            })); // Passes the path query params from above.
          }), take(this.rowData.length))
        .subscribe(res => {
          console.debug('Successfully deleted the files', res);
          const pathParts = res.path?.slice()?.reverse() || [];
          from(pathParts)
          .pipe(mergeMap(pathPart => {
            // Use inner observable so that errors don't put the outer observable in an error state.
            return this.apiService.head(uniqueIdToURL(pathPart), {handleErrorsManually: true})
              .pipe(catchError((err: HttpErrorResponse, caught) => {
                console.debug('Parent of ', res.actual, 'is gone', err);
                return of(err.url);
              }));
          }))
          .pipe(toArray())
          .pipe(take(1))
          .subscribe(res => {
            // URLs are parent objects that are gone (see catchError above).
            console.debug('Delete parents response:', res)
            this.dataLoading = [false];
            const urlStringsToRefresh = res.filter((val): val is string => typeof val === 'string')
            const urlsToRefresh = urlStringsToRefresh.map(url => new URL(url));
            console.debug('Requesting refresh of', urlsToRefresh, 'and', this.rowData);
            const objectsToRefresh: (URL | HEAObjectContainer<IDesktopObject>)[] = [];
            objectsToRefresh.push(...urlsToRefresh);
            objectsToRefresh.push(...this.rowData);
            this.dialogRef.close(objectsToRefresh);
          });

        },
        err => {
          console.error('Error deleting files', err);
          this.dataLoading = [false];
        })
    }
  }

  close() {
    this.clear();
    this.dialogRef.close(false);
  }

  clear() {
    this.rowData.length = 0;
    this.rowDataForGrid = [];
    this.agGridDynamic.clear();
  }

  ngOnDestroy() {
    if (this.deleteSubscription) {
      this.deleteSubscription.unsubscribe();
    }
  }

  private getGridColumnDefs(): any[] {
    const tempColumnDefs: any[] = [];

    tempColumnDefs.push({
      headerName: 'Object',
      field: 'heaObject.display_name',
      editable: false,
      width: 300,
      resizable: true,
      sortable: true
    });
    tempColumnDefs.push({
      headerName: 'Storage Class',
      field: 'heaObject.storage_class',
      editable: false,
      width: 200,
      resizeable: true,
      sortable: true,
    });
    tempColumnDefs.push({
      headerName: 'Size',
      field: 'heaObject.human_readable_size',
      editable: false,
      width: 100,
      resizeable: true,
      sortable: true,
      comparator: (valueA, valueB, nodeA: RowNode, nodeB: RowNode, isInverted: boolean) => {
        return this.utilService.sortNumber(nodeA.data.heaObject.size, nodeB.data.heaObject.size, null, null, isInverted);
      }
    });

    return tempColumnDefs;
  }
}
