import { Component, EventEmitter, Input, OnDestroy, Output, ViewChild} from "@angular/core";
import {forkJoin, of} from "rxjs";
import {UntypedFormBuilder, UntypedFormGroup} from "@angular/forms";
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import {ApiService} from "../../../services/api/api.service";

import {concatMap, first, map, take} from "rxjs/operators";
import {DialogsService, DialogType} from "../../util/dialog/dialogs.service";
import {CjService} from "../../../services/cj/cj.service";
import {IDesktopObject, Item} from "../../../models/heaobject.model";
import {HEAObjectContainer, IHEAObjectContainer, urlToUniqueId} from "../../../models/heaobject-container.model";
import {ICJData, ICJOption} from "../../../models/cj.model";
import {MatLegacySnackBar as MatSnackBar} from "@angular/material/legacy-snack-bar";
import {SelectDynamicComponent} from "../../util/interpreted-contents/dynamic-components/select-dynamic.component";
import { AbstractCardComponent } from "../../util/cards/abstract-card.component";

@Component({
  selector: "upload-file-card",
  template: `

    <div class="cb-card cb-card-no-margin d-flex flex-column h-100">
      <div class="cb-card-header d-flex flex-row">
        <div class="padded-left-right">
          <i class="bi bi-upload" style="font-size: 1.2rem">
          </i>
        </div>
        <div class="flex-grow-1">
          UPLOAD
        </div>
        <div class="me-1" (click)="close()" title="Close">
          <i class="bi bi-x-lg" style="font-size: 1.1rem">
          </i>
        </div>
      </div>
      <div class="d-flex flex-column cb-card-body flex-grow inner-container-scroll overflow-auto">
        <div class="font-small">
          <text-display [description]="description">
          </text-display>
        </div>
        <div>
          <aws-location-select [value]="destination">
          </aws-location-select>
        </div>
        <div class="padded-top">
          <select-dynamic #select
            [title]="'STORAGE'" [options]="storageClassList"
            [containerFormGroup]="form"
            [paramName]="this.scParamName"
            [valueField]="'value'"
            [required]="true">
            </select-dynamic>
        </div>
        <div class="padded-top">
          <upload-files-grid [title]="'FOLDERS & FILES'" [rowData]="rowData">
          </upload-files-grid>
        </div>
        <div class="d-flex flex-row">
          <div class="flex-grow-1">
          </div>
          <div class="padded-top-left-bottom">
            <button mat-button class="grey-1g" (click)="close()">
              CANCEL
            </button>
          </div>
          <!--<div class="padded-top-left-bottom">-->
          <!--<button mat-button class="normal-button">-->
          <!--CLI-->
          <!--</button>-->
          <!--</div>-->
          <div class="padded-top-left-bottom">
            <button [disabled]="!(!!this.destination)" mat-button class="secondary-1g" (click)="saveFiles()">
              UPLOAD
            </button>
          </div>
        </div>
      </div>
    </div>


  `,
  styles: [`

    .cb-card {
      margin: 0.5em;
      /*background-color: #EAF9FF;*/
      background-image: linear-gradient(#EAF9FF, #FFFFFF);
      box-shadow: 0 4px 8px #92969e;
      border: 0.1px solid #707070;
      border-radius: 10px;
      color: #5a6366;
      opacity: 1;
      width: 40em;
    }

    .cb-card-body {
      padding: 0 0.3em 0 0.3em;
    }

    .padded-top-left-bottom {
      padding: 0.3em 0 0.3em 0.3em;
    }

    .font-small {
      font-size: small;
    }

  `]
})
export class UploadFileCardComponent extends AbstractCardComponent {

  @ViewChild('select', {static: true}) storageSelect: SelectDynamicComponent;

  private metadataURL: string;

  private metadataTemplate: ICJData[];

  folderObject: HEAObjectContainer<Item>;

  form: UntypedFormGroup;
  rowData: any[] = [];
  destination: string = '';
  storageClassList: ICJOption[] = [];
  valueStorageClass: string;
  scParamName: string = 'storage_class';

  description: string = "If you upload an object with a name that already exists, another version of the object "
    + "will be created instead of replacing the existing object";


  public constructor( private formBuilder: UntypedFormBuilder,
                      private apiService: ApiService,
                      private dialogService: DialogsService,
                      private cjService: CjService,
                      private http: HttpClient,
                      private snackbar: MatSnackBar) {
    super();
    this.form = this.formBuilder.group({});
  }

  public saveFiles(): void {
    let fileRequests = [];
    const tempVal = this.form.get(this.scParamName).value;
    this.valueStorageClass = Array.isArray(tempVal) && tempVal.length ? tempVal[0] : tempVal;

    this.dialogService.confirm("Note, this operation will override existing files if not versioned. Do you want to continue?", "Upload")
      .pipe(first()).subscribe((action: boolean) => {
        if(action){
          this.refresh = true;
          for (let rowData of this.rowData) {
            let file: string = rowData.file.name;
            if (rowData.filePath && Array.isArray(rowData.filePath) && rowData.filePath.length > 1) {
             file = (rowData.filePath as string[]).join('/');
            }
            if ( rowData && rowData.file && rowData.file.name) {
              let fileMetadata: any = {
                name: urlToUniqueId(unescape(encodeURIComponent(file))),
                type: this.metadataTemplate.find(t => (t.name === 'target_type')).value,
              };
              fileMetadata[this.scParamName] = this.valueStorageClass;

              fileRequests.push(this.apiService.createJson(this.metadataURL, fileMetadata).pipe(map((resp: HttpResponse<string>) => {
                const location: string =  resp.headers.get('location');
                if (!location){
                  throw Error('Location not found');
                }
                const result: [string, any] = [location, rowData];
                return result;
              })).pipe(concatMap(tuple => {
                const formData = new FormData();
                this.snackbar.open('Initiated upload, progress can be viewed on the home dashboard',
                  null, {horizontalPosition: 'center', verticalPosition: 'top', duration: 4000});
                let queryParams = new HttpParams();
                if (this.valueStorageClass){
                  queryParams = new HttpParams().set(this.scParamName, this.valueStorageClass );
                }
                formData.append('files',  tuple[1].file, tuple[1].file.name);
                return this.apiService.putMultipart(tuple[0], formData, {params: queryParams});
              }), map(resp => {
                return resp;
              })));
            }
          }

          forkJoin(fileRequests).subscribe(resp => {
            console.log(resp);
            this.close();
          }, (err: any) => {
            console.log(err);
          });
        }
      });
  }

  protected processSubject(): void {
    const options = this.dataPath ? {params: {path: this.dataPath}} : {};
    this.cjService.getActualObjectOrSelf(this.subject[0], options).pipe(concatMap((actualData: HEAObjectContainer<IDesktopObject>) => {
      this.folderObject = actualData;
      this.metadataURL = this.cjService.findLink(actualData.links, 'hea-uploader')?.href;
      if (!this.metadataURL) {
        this.dialogService.alert(`You can\'t upload to ${actualData.heaObject.display_name}.`, DialogType.ERROR);
        return of(null);
      }
      return this.cjService.getResource(this.metadataURL, options);
    })).pipe(take(1)).subscribe((data: HEAObjectContainer<IDesktopObject>[]) => {
      if (data && data.length > 0) {
        this.metadataTemplate = data[0].template;
        const sc = this.metadataTemplate.find(t => (t.name === this.scParamName));
        setTimeout(() => {
          this.storageSelect.setValue(sc.value);
        });
        this.storageClassList = sc.options as ICJOption[];
        this.destination = data[0].heaObject.display_name;
      }
    });
  }

}

