import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
  OnInit
} from '@angular/core';
import {BehaviorSubject, Subscription} from 'rxjs';
import {CjService} from '../../../services/cj/cj.service';
import {HEAObjectContainer} from '../../../models/heaobject-container.model';
import {DesktopObject} from '../../../models/heaobject.model';
import {InterpretedContentsComponent} from '../../util/interpreted-contents/interpreted-contents.component';
import {ICJData, ICJLink} from '../../../models/cj.model';
import { IconService } from 'src/app/services/icon/icon.service';
import { ApiService } from 'src/app/services/api/api.service';
import { IconOption } from 'src/app/models/icon.model';
import { take } from 'rxjs/operators';
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { AbstractCardComponent } from '../../util/cards/abstract-card.component';


@Component({
  selector: 'cb-dynamic-standard-card',
  template: `

      <div class="cb-card cb-card-no-margin d-flex flex-column">
          <loading-overlay [busy]="dataLoading" [icon]="busyIcon"></loading-overlay>
          <div class="cb-card-header d-flex flex-row">
              <div class="" [innerHTML]="icon"></div>
              <div class="flex-grow-1 padded-left-right">
                <ng-container *ngIf="title">
                  {{this.subject && this.subject.length > 0 ? this.subject[0].heaObject.display_name + ' ' : ''}}{{this.title}}
                </ng-container>
                <ng-container *ngIf="link">
                  {{this.link.prompt + ' '}}{{this.subject && this.subject.length > 0 ? this.subject[0].heaObject.display_name : ''}}
                </ng-container>
              </div>
              <div class="me-1" (click)="close()" title="Close">
                  <i class="bi bi-x-lg" style="font-size: 1.1rem">
                  </i>
              </div>
          </div>
          <div class="d-flex flex-column cb-card-body h-100 flex-grow-1">
            <div class="inner-container-scroll overflow-auto">
              <interpreted-contents #interpretedContentsComponent (onReady)="onReady($event)" [template]="this.template" [formData]="formData" (valid)="onValidityChange($event)"></interpreted-contents>
            </div>
            <div class="d-flex flex-row">
                <div class="flex-grow-1">
                </div>
                <div class="padded-top-left-bottom">
                    <button mat-button class="grey-1g" (click)="close()">
                        {{actionButtonName ? 'CANCEL' : 'CLOSE'}}
                    </button>
                </div>
                <div *ngIf="actionButtonName" class="padded-top-left-bottom">
                    <button mat-button class="secondary-1g" (click)="save()" [disabled]="!valid">
                        {{actionButtonName}}
                    </button>
                </div>
            </div>
          </div>
      </div>


  `,
  styles: [`

    .cb-card {
      margin: 0.5em;
      background-color: #EAF9FF;
      box-shadow: 0 4px 8px #92969e;
      border: 0.1px solid #707070;
      border-radius: 10px;
      color: #5a6366;
      opacity: 1;
      width: 40em;
    }

    .cb-card-body {
      padding: 0 0.3em 0 0.3em;
      min-height: 33em;
    }

    .padded-top-left-bottom {
      padding: 0.3em 0 0.3em 0.3em;
    }


    .font-small {
      font-size: small;
    }

    .mat-form-field-appearance-outline.mat-focused .mat-form-field-outline-thick {
        color: red;
    }
    .background-color-white {
        background-color: white;
    }
    .border {
      border-radius: 6px;
    }

  `]
})
export class DynamicStandardCardComponent extends AbstractCardComponent {

  selectedObject: HEAObjectContainer<DesktopObject> = null;
  @ViewChild('interpretedContentsComponent') interpretedContentsRef: InterpretedContentsComponent;
  selectedType: any;
  creatorObjContainer: HEAObjectContainer<DesktopObject>;
  icon: string;
  title: string;
  actionButtonName: string;

  private _link: ICJLink;
  private _formData: Map<string, any>;
  private _valid: boolean;
  private _method: 'POST' | 'PUT' = 'POST';
  private _data: any;
  private _template: ICJData[];
  private linkBS: BehaviorSubject<ICJLink> = new BehaviorSubject(null);
  private linkBSSub: Subscription;

  get template(): ICJData[] {
    return this._template;
  }

  public constructor(private cjService: CjService,
                     private iconService: IconService,
                     private apiService: ApiService) {
    super();
    this.dataLoading = [true];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.linkBSSub = this.linkBS.subscribe(link => {
      if (link) {
        this.icon = this.iconService.getIconForLink(this.link, [IconOption.LARGE]);
        const httpParams = this.dataPath ? new HttpParams().appendAll({'path': this.dataPath}) : new HttpParams();
        this.cjService.getResource(this.link.href, {params: httpParams}).pipe(take(1)).subscribe(resource => {
          this.creatorObjContainer = resource[0];
          this._template = this.creatorObjContainer.getTemplate(this.method);
          this.configureButtons(this.link.href);
        });
      }
    });
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    if (this.linkBSSub) {
      this.linkBSSub.unsubscribe();
    }
    if (this.linkBS) {
      this.linkBS.unsubscribe();
    }
  }

  @Input() public set method(method: 'POST' | 'PUT') {
    this._method = method ? method : 'POST';
  }

  public get method() {
    return this._method;
  }

  @Input('link') set link(link: ICJLink) {
    this._link = link;
    if (this.linkBS) {
      this.linkBS.next(link);
    }
  }

  get link(): ICJLink {
    return this._link;
  }

  @Input() set data(value: HEAObjectContainer<DesktopObject>) {
    this._data = {inHEAObjectContainer: value};
  }

  get data(): HEAObjectContainer<DesktopObject> {
    return this._data ? this._data.inHEAObjectContainer : null;
  }

  @Input('formData') set formData(formData: Map<string, any>) {
    this._formData = formData;
  }

  get formData(): Map<string, any> {
    return this._formData;
  }

  get valid(): boolean {
    return this._valid;
  }

  onValidityChange(event: boolean) {
    setTimeout(() => {this._valid = event}, 0);
  }

  save(): void {
    if (!this.valid) {
      return;
    }
    this.dataLoading = [true];
    this.interpretedContentsRef.update(this.method, this.creatorObjContainer.href, this.dataPath ? {params: {'path': this.dataPath}} : {}).pipe(take(1)).subscribe(resp => {
        this.refresh = true;
      }, err => {
        this.dataLoading = [false];
        this.close();
      }, () => {
        this.dataLoading = [false];
        this.close();
      });
  }

  onReady(event: InterpretedContentsComponent) {
    this.dataLoading = [false];
  }

  protected processSubject(): void {
    console.debug('Got subject', this.subject);
    if (this.subject && !this.link) {
      this.dataLoading = [true];
      this.creatorObjContainer = this.subject[0];
      this.icon = this.iconService.getIconForHEAObject(this.creatorObjContainer.heaObject, [IconOption.LARGE]);
      this._template = this.creatorObjContainer.getTemplate(this.method);
      this.title = 'Properties';
      this.configureButtons(this.creatorObjContainer.href);
    } else {
      this.icon = null;
      this.creatorObjContainer = null;
      this._template = null;
      this.configureButtons(null);
    }
  }

  private configureButtons(href: string) {
    console.debug('Configuring buttons for', href, this.creatorObjContainer);
    if (!this.creatorObjContainer) {
      this.actionButtonName = null;
      this.dataLoading = [false];
    } else if (!this.creatorObjContainer.isFormActionForbidden(this.method)) {
      const actionButtonCreator = this._link && this._link.prompt ?  () => (this._link.prompt.toUpperCase())
        : () => {
        switch (this.method) {
          case 'POST':
            return 'CREATE';
          case 'PUT':
            return 'UPDATE';
          default:
            return 'SAVE';
        }
      };
      this.apiService.isMethodAllowed(href, this.method).pipe(take(1)).subscribe(resp => {
        if (resp) {
          this.actionButtonName = actionButtonCreator();
        }
        this.dataLoading = [false];
      }, (error: HttpErrorResponse) => {
        if (error.status === 405) {
          this.actionButtonName = actionButtonCreator();
        }
        this.dataLoading = [false];
      });
    } else {
      this.dataLoading = [false];
    }
  }
}

