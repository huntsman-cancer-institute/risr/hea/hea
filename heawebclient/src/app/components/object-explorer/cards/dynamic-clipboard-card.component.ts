import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  AfterViewInit,
  ViewChild
} from '@angular/core';
import {Subscription} from 'rxjs';
import {UtilService} from '../../../services/util.service';
import {CjService} from '../../../services/cj/cj.service';
import {HEAObjectContainer} from '../../../models/heaobject-container.model';
import {DesktopObject} from '../../../models/heaobject.model';
import {InterpretedContentsComponent} from '../../util/interpreted-contents/interpreted-contents.component';
import {ICJData, ICJLink} from '../../../models/cj.model';
import { IconService } from 'src/app/services/icon/icon.service';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { take } from 'rxjs/operators';
import { IconOption } from 'src/app/models/icon.model';
import { HttpParams } from '@angular/common/http';
import { AbstractCardComponent } from '../../util/cards/abstract-card.component';


@Component({
  selector: 'cb-dynamic-clipboard-card',
  template: `

      <div class="cb-card cb-card-no-margin d-flex flex-column">
          <loading-overlay [busy]="dataLoading" [icon]="busyIcon"></loading-overlay>
          <div class="cb-card-header d-flex flex-row">
              <div class="" [innerHTML]="iconService.getIconForLink(link, [iconSize])">
              </div>
              <div class="flex-grow-1 padded-left-right">
                  {{this.link.prompt}} {{this.subject && this.subject.length > 0 ? this.subject[0].heaObject.display_name : 'OBJECT'}}
              </div>
              <div class="me-1" (click)="close()" title="Close">
                  <i class="bi bi-x-lg" style="font-size: 1.1rem">
                  </i>
              </div>
          </div>
          <div class="d-flex flex-column cb-card-body h-100 flex-grow-1">
            <div class="inner-container-scroll overflow-auto">
              <interpreted-contents #interpretedContentsComponent [template]="this.template" [formData]="formData"></interpreted-contents>
              <div class="d-flex flex-row">
                <div class="flex-grow-1">
                </div>
                <div *ngIf="hasFormFields()" class="padded-top-left-bottom">
                    <button mat-button class="primary-1g" (click)="save()">
                        {{this.link.prompt.toUpperCase()}}
                    </button>
                </div>
              </div>
              <div id="clipboardDataForm" class="d-flex flex-row">
                <div class="d-flex flex-column container">
                  <textarea name="clipboardData"
                    [value]="(!mimeType || mimeType?.startsWith('text/')) ? this.data : 'Data to copy'"
                    readonly
                    cdkTextareaAutosize
                    cdkAutosizeMinRows="3"
                    cdkAutosizeMaxRows="15"
                    class="font-sm"></textarea>
                </div>
              </div>
              <div class="d-flex flex-row">
                <div class="flex-grow-1">
                </div>
                <div class="padded-top-left-bottom">
                    <button mat-button class="secondary-1g" [disabled]="!mimeType" (click)="copyToClipboard()">
                        COPY TO CLIPBOARD
                    </button>
                </div>
              </div>
            </div>
            <div class="d-flex flex-row">
              <div class="flex-grow-1">
              </div>
              <div class="padded-top-left-bottom">
                  <button mat-button class="grey-1g" (click)="close()">
                      CLOSE
                  </button>
              </div>
            </div>
          </div>
      </div>


  `,
  styles: [`

    .cb-card {
      margin: 0.5em;
      background-color: #EAF9FF;
      box-shadow: 0 4px 8px #92969e;
      border: 0.1px solid #707070;
      border-radius: 10px;
      color: #5a6366;
      opacity: 1;
      width: 40em;
    }


    .cb-card-body {
      padding: 0 0.3em 0 0.3em;
      min-height: 33em;
    }

    .padded-top-left-bottom {
      padding: 0.3em 0 0.3em 0.3em;
    }


    .font-small {
      font-size: small;
    }

    .mat-form-field-appearance-outline.mat-focused .mat-form-field-outline-thick {
        color: red;
    }
    .background-color-white {
        background-color: white;
    }
    .border {
      border-radius: 6px;
    }

    textarea:read-only {
      border: none
    }

    #clipboardDataForm {
      margin-top: 2em
    }

  `]
})
export class DynamicClipboardCardComponent extends AbstractCardComponent implements AfterViewInit {

  @ViewChild('interpretedContentsComponent') interpretedContentsRef: InterpretedContentsComponent;
  selectedType: any;
  @Input() link: ICJLink;
  creatorObjContainer: HEAObjectContainer<DesktopObject>;
  iconSize = IconOption.LARGE;
  mimeType: string;
  data: any = '';

  get template(): ICJData[] {
    return this._template;
  }

  private _formData: Map<string, any>;
  private rowSelectedSubscription: Subscription;
  private _template: ICJData[];

  public constructor(private utilService: UtilService,
                     private cjService: CjService,
                     public iconService: IconService,
                     private snackBar: MatSnackBar) {
    super();
  }

  ngAfterViewInit() {
    const httpParams = this.dataPath ? new HttpParams().appendAll({'path': this.dataPath}) : new HttpParams();
    this.cjService.getResource(this.link.href, {params: httpParams}).pipe(take(1)).subscribe(resource => {
      this.creatorObjContainer = resource[0];
      this._template = resource[0].getTemplate('GET');
      this.dataLoading = [false];
      if (!this.hasFormFields()) {
        this.save();
      }
    });
  }

  @Input('formData') set formData(formData: Map<string, any>) {
    this._formData = formData;
  }

  get formData(): Map<string, any> {
    return this._formData;
  }

  hasFormFields() {
    return !!(this.creatorObjContainer?.template?.length > 0);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    if (this.rowSelectedSubscription != null) {
      this.rowSelectedSubscription.unsubscribe();
    }
  }

  isValid() {
  //  TODO: validation check including required and name rules checks
    return true;
  }

  save() {
    if (!this.isValid()) {
      return;
    }
    this.dataLoading = [true];
    console.debug('Saving clipboard data for', this.creatorObjContainer);
    const httpParams = this.dataPath ? new HttpParams().appendAll({'path': this.dataPath}) : new HttpParams();
    this.interpretedContentsRef.update('POST', this.creatorObjContainer.href, {params: httpParams}).pipe(take(1)).subscribe(resp => {
      const clipboardData = JSON.parse(resp.body);
      this.mimeType = clipboardData[0]['mime_type']?.split(';')[0] || "text/plain";
      this.data = clipboardData[0]['data'];
    }, err => {
      this.dataLoading = [false];
    }, () => {
      this.dataLoading = [false];
    });
  }

  copyToClipboard() {
    const promise = async () => {
      return new Blob([this.data || ''], {type: this.mimeType})
    };
    // @ts-ignore
    navigator.clipboard.write([new ClipboardItem({[this.mimeType]: promise()})]).then(result => {
      this.snackBar.open('Copied data to clipboard', 'Copied', {
        duration: 5000
      });
    }, err => {
      this.snackBar.open(`Copy to clipboard failed: ${err}`, 'Copy failed', {
        duration: 5000
      });
    });
  }
}

// When we upgrade to TypeScript 4.4, everything below can be deleted.
interface ClipboardItem {
  readonly types: string[];
  readonly presentationStyle: "unspecified" | "inline" | "attachment";
  getType(): Promise<Blob>;
}

interface ClipboardItemData {
  [mimeType: string]: Blob | string | Promise<Blob | string>;
}

declare var ClipboardItem: {
  prototype: ClipboardItem;
  new (itemData: ClipboardItemData): ClipboardItem;
};
