import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  OnInit
} from '@angular/core';
import {RowNode} from 'ag-grid-community';
import {CjService} from '../../../services/cj/cj.service';
import {HEAObjectContainer} from '../../../models/heaobject-container.model';
import {DesktopObject} from '../../../models/heaobject.model';
import {ICJLink} from '../../../models/cj.model';
import { DialogsService } from '../../util/dialog/dialogs.service';
import { ApiService } from 'src/app/services/api/api.service';
import { IconService } from 'src/app/services/icon/icon.service';
import { IconOption } from 'src/app/models/icon.model';
import { take } from 'rxjs/operators';
import { AbstractCardComponent } from '../../util/cards/abstract-card.component';


@Component({
  selector: 'cb-versions-card',
  template: `

      <div class="cb-card cb-card-no-margin d-flex flex-column">
          <loading-overlay [busy]="dataLoading" [icon]="busyIcon"></loading-overlay>
          <div class="cb-card-header d-flex flex-row">
              <div class="" [innerHTML]="icon">
              </div>
              <div class="flex-grow-1 padded-left-right">
                  {{this.subject && this.subject.length > 0 ? this.subject[0].heaObject.display_name + ' ' : ''}}Versions
              </div>
              <div class="me-1" (click)="close()" title="Close">
                  <i class="bi bi-x-lg" style="font-size: 1.1rem">
                  </i>
              </div>
          </div>
          <div class="d-flex flex-column cb-card-body inner-container-scroll overflow-auto">
            <div>
              <ag-grid-dynamic #agGridDynamic [showAddRemove]="false"
                          [mode]="'file'"
                          [columnDefs]="gridColumnDefs"
                          [data]="rowDataForGrid"
                          [paramName]="'object'"
                          [readOnly]="true"
                          (rowSelected)=onRowSelected($event)>
              </ag-grid-dynamic>
            </div>
            <div class="d-flex flex-row">
                <div class="flex-grow-1">
                </div>
                <div class="padded-top-left-bottom">
                    <button mat-button class="grey-1g" (click)="close()">
                        CANCEL
                    </button>
                </div>
                <div class="padded-top-left-bottom">
                    <button [disabled]="readOnly || !(!!rowSelected)" mat-button class="delete-1g" (click)="delete($event)">
                        DELETE
                    </button>
                </div>
                <div class="padded-top-left-bottom">
                    <button [disabled]="readOnly || !(rowSelected && rowSelected.rowIndex > 0)" mat-button class="primary-1g" (click)="makeCurrent($event)">
                        MAKE CURRENT
                    </button>
                </div>
            </div>
          </div>
      </div>


  `,
  styles: [`

    .cb-card {
      margin: 0.5em;
      background-color: #EAF9FF;
      box-shadow: 0 4px 8px #92969e;
      border: 0.1px solid #707070;
      border-radius: 10px;
      color: #5a6366;
      opacity: 1;
      width: 40em;
      height: 32em;
    }


    .cb-card-body {
      padding: 0 0.3em 0 0.3em;
      min-height: 33em;
    }

    .padded-top-left-bottom {
      padding: 0.3em 0 0.3em 0.3em;
    }


    .font-small {
      font-size: small;
    }

    .mat-form-field-appearance-outline.mat-focused .mat-form-field-outline-thick {
        color: red;
    }
    .background-color-white {
        background-color: white;
    }
    .border {
      border-radius: 6px;
    }

  `]
})
export class VersionsCardComponent extends AbstractCardComponent {
  selectedObject: HEAObjectContainer<DesktopObject> = null;
  selectedType: any;
  gridColumnDefs: any[];
  rowSelected?: RowNode;
  rowDataForGrid?: any[];
  makeCurrentLink?: ICJLink
  private selfLink?: ICJLink;

  public constructor(private dialogsService: DialogsService,
                     private cjService: CjService,
                     private apiService: ApiService,
                     private iconService: IconService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.gridColumnDefs = this.getGridColumnDefs();
  }

  get icon(): string {
    return this.iconService.getIconForRel('hea-versions', [IconOption.LARGE]);
  }

  get readOnly(): boolean {
    return this.subject[0].isPutForbidden()
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  makeCurrent(event: PointerEvent): void {
    if (this.makeCurrentLink) {
      this.dataLoading = [true];
      this.apiService.getText(this.makeCurrentLink.href).pipe(take(1)).subscribe(result => {
        this.refresh = true;
        this.dataLoading = [false];
        this.close();
      }, error => {
        this.dataLoading = [false];
      });
    }
  }

  delete(event: PointerEvent): void {

    this.dialogsService.confirm("The selected version will be permanently deleted. Are you sure?").pipe(take(1)).subscribe(result => {
      if (result) {
        this.dataLoading = [true];
        this.apiService.delete(this.selfLink.href).pipe(take(1)).subscribe(result => {
          this.refresh = true;
          this.dataLoading = [false];
          this.close();
        }, error => {
          this.dataLoading = [false];
        });
      }
    });
  }

  onRowSelected(event: any) {
    if (event.node.selected) {
      this.rowSelected = event.node;
      this.makeCurrentLink = this.cjService.findLink(this.rowSelected.data.links, "hea-current-version-maker");
      this.selfLink = this.cjService.findLink(this.rowSelected.data.links, "self");
    }
  }

  protected processSubject(): void {
    const versionsLink = this.cjService.findLink(this.subject[0].links, 'hea-versions');
    this.rowSelected = null;
    if (versionsLink) {
      this.dataLoading = [true];
      this.cjService.getResource(versionsLink.href, this.dataPath ? {params: {path: this.dataPath}} : {}).pipe(take(1)).subscribe(result => {
        this.rowDataForGrid = result;
        this.dataLoading = [false];
      });
    } else {
      this.rowDataForGrid = null;
      this.dataLoading = [false];
    }
  }

  private getGridColumnDefs(): any[] {
    const tempColumnDefs: any[] = [];

    tempColumnDefs.push({
      headerName: 'NAME',
      field: 'heaObject.display_name',
      editable: false,
      width: 300,
      resizable: true,
      sortable: true
    });
    tempColumnDefs.push({
      headerName: 'LAST MODIFIED',
      field: 'heaObject.modified',
      editable: false,
      width: 300,
      resizable: true,
      sortable: true,
      cellRenderer: params => {
        if (params.value === null || params.value === undefined) {
          return '--';
        } else {
          return new Date(params.value).toLocaleString()
        }
      }
    });

    return tempColumnDefs;
  }
}

