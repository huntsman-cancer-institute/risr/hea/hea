import {Component} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {DataObject, DesktopObject} from '../../../models/heaobject.model';
import {IconOption} from '../../../models/icon.model';
import {IconService} from '../../../services/icon/icon.service';
import {take} from 'rxjs/operators';
import {CjService} from '../../../services/cj/cj.service';
import {AbstractCardComponent} from '../../util/cards/abstract-card.component';
import {HEAObjectContainer} from '../../../models/heaobject-container.model';

@Component({
  selector: 'file-preview-card',
  template: `
    <div class="cb-card cb-card-no-margin d-flex flex-column">
      <div class="cb-card-header d-flex flex-row">
        <div class="" [innerHTML]="icon"></div>
        <div class="flex-grow-1 padded-left-right">
          <ng-container *ngIf="title">
            {{creatorObjContainer ? creatorObjContainer.heaObject.display_name + ' ' : ''}}{{this.title}}
          </ng-container>
        </div>
        <div class="me-1" (click)="close()" title="Close">
          <i class="bi bi-x-lg" style="font-size: 1.1rem">
          </i>
      </div>
      </div>
      <loading-overlay [busy]="dataLoading" [icon]="busyIcon"></loading-overlay>
      <div class="flex-grow-1 .cb-card-body padded-top-left-bottom">
        <div *ngIf="!isValid && !dataLoading[0]">
          <p>{{previewMsg}}</p>
        </div>
        <ng-template #blank>
          <p></p>
        </ng-template>
        <iframe name="preview-frame" [hidden]="!isValid || dataLoading[0]" (load)="onLoad()" [src]="urlSafe" width="100%" height="100%"></iframe>
      </div>
    </div>
  `,
  styles: [`

    .cb-card {
      margin: 0.5em;
      background-color: #EAF9FF;
      box-shadow: 0 4px 8px #92969e;
      border: 0.1px solid #707070;
      border-radius: 10px;
      color: #5a6366;
      opacity: 1;
      width: 40em;
    }

    .cb-card-body {
      padding: 0 0.3em 0 0.3em;
      min-height: 10em;
    }

    .padded-top-left-bottom {
      padding: 1em 0 1em 1em;
    }

  `]
})
export class FilePreviewCardComponent extends AbstractCardComponent {

  private iframeUrl = '';
  private _isCollapsed = false;
  urlSafe: SafeResourceUrl;
  isValid: boolean;
  icon: string;
  title = 'Preview';
  previewMsg: string;
  private readonly PLAIN_TEXT_MIME_TYPE = 'text/';
  creatorObjContainer: HEAObjectContainer<DesktopObject>;

  commonMimeTypes: { display_name: string; extension: string; type: string }[] = [
    { display_name: 'PDF', extension: '.pdf', type: 'application/pdf' },
    { display_name: 'Text', extension: '.txt', type: 'text/plain' },
    { display_name: 'HTML', extension: '.html', type: 'text/html' },
    { display_name: 'CSS', extension: '.css', type: 'text/css' },
    { display_name: 'CSV', extension: '.csv', type: 'text/csv' },
    { display_name: 'SH', extension: '.sh', type: 'text/x-sh' },
    { display_name: 'JS', extension: '.js', type: 'application/javascript' },
    { display_name: 'JSON', extension: '.json', type: 'application/json' },
    { display_name: 'XML', extension: '.xml', type: 'application/xml' },
    { display_name: 'JPEG', extension: '.jpg', type: 'image/jpeg' },
    { display_name: 'JPEG', extension: '.jpeg', type: 'image/jpeg' },
    { display_name: 'PNG', extension: '.png', type: 'image/png' },
    { display_name: 'GIF', extension: '.gif', type: 'image/gif' },
    { display_name: 'SV', extension: '.svg', type: 'image/svg+xml' },
    { display_name: 'WEBP', extension: '.webp', type: 'image/webp' },
    { display_name: 'MP3', extension: '.mp3', type: 'audio/mpeg' },
    { display_name: 'WAV', extension: '.wav', type: 'audio/wav' },
    { display_name: 'MP4', extension: '.mp4', type: 'video/mp4' }
  ];

  constructor(private cjService: CjService,
              public sanitizer: DomSanitizer,
              private iconService: IconService) {
    super();
  }

  processSubject(): void {
    this.dataLoading = [true];
    this.creatorObjContainer = this.subject[0];
    if (!this.creatorObjContainer) {
      this.resetToBlank();
    } else {
      this.dataLoading = [true];
      this.previewMsg = 'NO PREVIEW AVAILABLE';
      this.icon = this.iconService.getIconForHEAObject(this.creatorObjContainer.heaObject, [IconOption.LARGE]);
      this.title = 'Preview';
      this.cjService.getDesktopObjectContentUrl(this.creatorObjContainer, 'open', true).pipe(take(1)).subscribe(href => {
        const obj = (this.creatorObjContainer.heaObject as DataObject);
        if (href && obj && this.checkMimeType(this.commonMimeTypes, obj.mime_type)) {
          this.isValid = true;
          this.iframeUrl = href;
          this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.iframeUrl);
          console.log('Subject URL: ' + href);
        } else {
          this.isValid = false;
          this.dataLoading = [false];
        }
      });
    }
  }

  private resetToBlank() {
    this.isValid = false;
    this.dataLoading = [false];
    this.iframeUrl = 'about:blank';
    this.previewMsg = '';
    this.icon = null;
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.iframeUrl);
  }

  onLoad() {
    console.log(this.dataLoading);
    this.dataLoading = [false];
  }

  public get isCollapsed(): boolean {
    return this._isCollapsed;
  }

  checkMimeType(mimeList, mimeType) {
    if (!mimeType) {
      return false;
    }
    if (mimeType.toLowerCase().includes(this.PLAIN_TEXT_MIME_TYPE)) {
      return true;
    }
    const selectedMimeType = mimeList.find((obj) => {
      return mimeType === obj?.type;
    });
    return !!selectedMimeType;
  }
}
