import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  AfterViewInit,
  ViewChild,
  OnInit
} from '@angular/core';
import {Subject} from 'rxjs';
import {CjService} from '../../../services/cj/cj.service';
import {HEAObjectContainer, IHEAObjectContainer} from '../../../models/heaobject-container.model';
import {DesktopObject, IDesktopObject} from '../../../models/heaobject.model';
import {InterpretedContentsComponent} from '../../util/interpreted-contents/interpreted-contents.component';
import { IconService } from 'src/app/services/icon/icon.service';
import { IconOption } from 'src/app/models/icon.model';
import { take, takeUntil } from 'rxjs/operators';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { ICJData } from 'src/app/models/cj.model';
import { AbstractCardComponent } from '../../util/cards/abstract-card.component';


@Component({
  selector: 'cb-create-card',
  template: `
      <div class="cb-card cb-card-no-margin d-flex flex-column">
          <loading-overlay [busy]="dataLoading" [icon]="busyIcon"></loading-overlay>
          <div class="cb-card-header d-flex flex-row">
              <div class="" [innerHTML]="icon" style="font-size: 1.2rem">
              </div>
              <div class="flex-grow-1">
                  New Object in {{this.subject[0].heaObject.display_name}}
              </div>
              <div class="me-1" (click)="close()" title="Close">
                  <i class="bi bi-x-lg" style="font-size: 1.1rem;">
                  </i>
              </div>
          </div>
          <div class="d-flex flex-column cb-card-body inner-container-scroll overflow-auto">
              <div>
                  <select-dynamic [title]="'Type'" [options]="dataType"
                                  [valueField]="'value'" [required]="true"
                                  [containerFormGroup]="formGroup"
                                  paramName="typeSelector"
                                  (optionSelected)="onDataTypeSelected($event)">
                  </select-dynamic>
              </div>
            <div>
              <interpreted-contents #interpretedContentsComponent [template]="template"></interpreted-contents>
            </div>
              <div class="d-flex flex-column w-100">
                  <text-display *ngIf="footerText.length == 0" style="color: red; font-weight: bold;" [description]="reviewDes">
                  </text-display>
                  <text-display *ngIf="footerText.length == 0 && isFolder" [description]="nameRulesDes">
                  </text-display>
                  <text-display *ngIf="footerText.length == 0 && !isFolder" [description]="bucketNameRulesDes">
                  </text-display>
                  <text-display *ngIf="footerText.length > 0" [description]="footerText" >
                  </text-display>
              </div>
              <div class="flex-grow-1"></div>
              <div class="d-flex flex-row">
                  <div class="flex-grow-1">
                  </div>
                  <div class="padded-top-left-bottom">
                      <button mat-button class="grey-1g" (click)="close()">
                          CANCEL
                      </button>
                  </div>
                  <div class="padded-top-left-bottom">
                      <button mat-button class="secondary-1g" (click)="save()">
                          CREATE
                      </button>
                  </div>
              </div>
          </div>
      </div>

  `,
  styles: [`

    .cb-card {
      margin: 0.5em;
      background-color: #EAF9FF;
      box-shadow: 0 4px 8px #92969e;
      border: 0.1px solid #707070;
      border-radius: 10px;
      color: #5a6366;
      opacity: 1;
      width: 40em;
      height: 32em;
    }

    .cb-card-body {
      padding: 0 0.3em 0 0.3em;
      min-height: 33em;
    }

    .padded-top-left-bottom {
      padding: 0.3em 0 0.3em 0.3em;
    }

    .cancel-button {
      color: #727b80;
      background-color: white;
      border: solid #727b80 2px;
      padding: 0.5em;
      line-height: 1em;
    }

    .normal-button {
      color: white;
      background-image: linear-gradient(#46c0fd, #0490d7);
      border: solid #005580 2px;
      padding: 0.5em;
      line-height: 1em;
    }

    .font-small {
      font-size: small;
    }

    .mat-form-field-appearance-outline.mat-focused .mat-form-field-outline-thick {
        color: red;
    }
    .background-color-white {
        background-color: white;
    }
    .border {
      border-radius: 6px;
    }

  `]
})
export class CreateCardComponent extends AbstractCardComponent implements AfterViewInit {

  dataType: any[];
  @ViewChild('interpretedContentsComponent') interpretedContentsRef: InterpretedContentsComponent;
  selectedType: any;
  isFolder = false;

  selectedAccount: HEAObjectContainer<DesktopObject>;
  accountList: any[];
  selectedBucket;
  selectedFolder;
  selectedRegion: any;
  reviewDes = '\nReview These Requirements Prior to Saving';
  nameRulesDes: string[] = [
    'AWS: ' + '<a href="https://docs.aws.amazon.com/AmazonS3/latest/userguide/object-keys.html" target="_blank">' +
    'https://docs.aws.amazon.com/AmazonS3/latest/userguide/object-keys.html</a>\n\n'
  ];
  bucketNameRulesDes: string[] = [
    'AWS: ' + '<a href="https://docs.aws.amazon.com/AmazonS3/latest/userguide/bucketnamingrules.html" target="_blank">' +
    'https://docs.aws.amazon.com/AmazonS3/latest/userguide/bucketnamingrules.html</a>',
    'HCI RISR: All buckets created in this application will begin with "cb" followed by the first initial and beginning four characters of the owner\'s last name.',
    '&nbsp; &nbsp; <b>Naming Standard:</b> cb-firstinitialLastname-IRB#(if available)-StudyName',
    '&nbsp; &nbsp; <b>Example:</b> cb-jsmith-99999-cancergenetics\n\n'
  ];

  creatorObjContainer: HEAObjectContainer<DesktopObject>;
  formGroup: UntypedFormGroup;
  footerText: string | string[] = [];

  get template(): ICJData[] {
    return this._template;
  }

  private _template: ICJData[];


  public constructor(private cjService: CjService,
                     private iconService: IconService,
                     private formBuilder: UntypedFormBuilder) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.formGroup = this.formBuilder.group({});
    this.dataLoading = [true];
  }

  ngAfterViewInit() {
  }

  get icon(): string {
    return this.iconService.getIconForRel('hea-creator-choices', [IconOption.LARGE])
  }

  onDataTypeSelected(event: string) {
    console.debug('Changing selected type to', event);
    if (this.dataType) {
      for (const d of this.dataType) {
        if (d.value === event) {
          this.selectedType = d;
        }
      }
    }
    this.getTypeCreatorForm();
  }

  isValid(): boolean {
  //  TODO: validation check including required and name rules checks
    return true;
  }

  save(): void {
    if (!this.isValid()) {
      return;
    }
    this.dataLoading = [true];
    this.interpretedContentsRef.update('POST', this.creatorObjContainer.href).pipe(take(1)).subscribe(resp => {
        this.refresh = true;
      }, err => {
        this.dataLoading = [false];
        this.close();
      }, () => {
        this.dataLoading = [false];
        this.close();
      });
  }

  protected processSubject(): void {
    if (!this.subject || this.subject.length === 0) {
      this.selectedAccount = null;
      this.selectedBucket = null;
      this.selectedFolder = null;
      this.dataLoading = [false];
    } else {
      this.cjService.getActualObjectOrSelf(this.subject[0], this.dataPath ? {params: {'path': this.dataPath}} : {}).pipe(take(1)).subscribe(actual => {
        const creatorLinks = this.cjService.findLink(actual.links, 'hea-creator-choices');
        const stopSubject = new Subject();
        this.cjService.getOpenerLinks(creatorLinks.href, this.dataPath ? {params: {'path': this.dataPath}} : {}).pipe(takeUntil(stopSubject)).subscribe(links => {
          const _dataType = [];
          for (const link of links) {
            _dataType.push({
              id: link.href,
              value: link.href,
              text: link.prompt
            });
          }
          console.debug('Setting data type to', _dataType);
          this.dataType = _dataType;
          this.selectedType = this.dataType && this.dataType.length > 0 ? this.dataType[0] : null;
          setTimeout(() => {
            this.formGroup.get('typeSelector').setValue(this.dataType && this.dataType.length > 0 ? [this.dataType[0].value] : [] );
          });
          this.getTypeCreatorForm();
          stopSubject.next();
          stopSubject.complete();
          this.dataLoading = [false];
        });
      });
    }
  }

  private getTypeCreatorForm() {
    if (this.selectedType) {
      this.cjService.getResource(this.selectedType.value, this.dataPath ? {params: {'path': this.dataPath}} : {}).pipe(take(1)).subscribe(creator => {
        if (creator && creator.length > 0) {
          this.creatorObjContainer = creator[0];
          this._template = this.creatorObjContainer.getTemplate('GET');
          const ft = this._template.find(t => (t.name === 'footer_text'));
          this.footerText =  ft ? ft.value : '' ;
        }
      });
    }
  }
}

