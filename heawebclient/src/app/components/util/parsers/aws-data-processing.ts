import { AWSS3Storage } from 'src/app/models/heaobject.model';
import {StorageClass} from '../../../models/aws-s3-object.model';


export class AwsDataProcessing {
    /* Function to roll up storage classes data */
    static processStorageClassData(storageSummaries: AWSS3Storage[]): StorageClass[] {
      const storageDataList: StorageClass[] = [];
      console.debug('storageSummaries is', storageSummaries);
      for (const storage of storageSummaries) {
        storageDataList.push(this.parseStorageClass(storage));
      }
      return storageDataList;
    }

    /* Function to roll up storage class data */
    static parseStorageClass(obj: AWSS3Storage): StorageClass {
      console.debug('obj is', obj);
      const storage: StorageClass = new StorageClass();
      storage.id = obj.id ? obj.id : '';
      storage.name = obj.name ? obj.name : '';
      storage.displayName = obj.display_name ? obj.display_name : '';
      storage.description = obj.description ? obj.description : '';
      storage.arn = obj.arn ? obj.arn : '';
      storage.created = obj.created ? obj.created : '';
      storage.modified = obj.modified ? obj.modified : '';
      storage.derivedBy = obj.derived_by ? obj.derived_by : '';
      storage.mimeType = obj.mime_type ? obj.mime_type : '';
      storage.minStorageDuration = obj.object_min_duration ? obj.object_min_duration.toLocaleString() : '';
      storage.objectCount = obj.object_count ? obj.object_count : 0;
      storage.objectInitModified = obj.object_earliest_created ? obj.object_earliest_created.toLocaleString() : '';
      storage.objectLastModified = obj.object_last_modified ? obj.object_last_modified.toLocaleString() : '';
      storage.owner = obj.owner ? obj.owner : '';
      storage.source = obj.source ? obj.source : '';
      storage.storageBytes = obj.size ? obj.size : 0;
      storage.storageClass = obj.name ? obj.name : '';
      storage.version = obj.version ? obj.version : '';
      storage.maxDurationDays = obj.object_max_duration ? Math.round(obj.object_max_duration / (60 * 60 * 24)) : (obj.object_max_duration === 0 ? 0 : null);
      storage.minDurationDays = obj.object_min_duration ? Math.round(obj.object_min_duration / (60 * 60 * 24)) : (obj.object_min_duration === 0 ? 0 : null);
      storage.avgDurationDays = obj.object_average_duration ? Math.round(obj.object_average_duration / (60 * 60 * 24)) : (obj.object_average_duration === 0 ? 0 : null);
      return storage;
    }

}
