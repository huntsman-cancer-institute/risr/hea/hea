import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayFilter'
})
export class ArrayFilterPipe implements PipeTransform {
  transform(items: any[], fieldName: string, operator: string, value: any): any[] {
    if (!items) {return []; }
    if (!fieldName) {return items; }

    const fields = fieldName.split('.');
    return items.filter(item => {
      if (item) {
        let i = item;
        for (const field of fields) {
          i = i[field];
          if (i === undefined) {
            return items;
          }
        }
        switch (operator) {
          case '===': return i === value;
          case '>=': return i >= value;
          case '<=': return i <= value;
          case '!==': return i !== value;
          default: throw new Error(`Invalid operator ${operator}`);
        }
      } else {
        return false;
      }
    });
  }
}
