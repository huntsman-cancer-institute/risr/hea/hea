import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  Input,
  isDevMode,
  Renderer2,
  SimpleChange,
  TemplateRef,
  ViewRef
} from "@angular/core";
import {of, Subject, Subscription} from "rxjs";
import {delay} from "rxjs/operators";

@Component({
  selector: 'loading-overlay',
  template: `

    <div class="h-100 w-100 loading-overlay {{ busy && busy?.length === 1 && busy[0] === true  ? 'busy' : '' }}" [class.mxAuto]="mxAuto" [class.myAuto]="myAuto">
      <div class="h-100 w-100 d-flex flex-column">
        <div class="flex-grow-2">
        </div>
        <div class="d-flex flex-row">
          <div class="flex-grow-1">
          </div>
          <div [style.display]="showIcon ? 'flex' : 'none'">
            <i *ngIf="showFAIcon" class="{{icon}} {{iconSize}}"></i>
            <img *ngIf="!showFAIcon" src="./assets/COREbrowserLoadingSpinnerHighrez.svg" style="width: 10em;" alt=""/>
          </div>
          <div class="text" [style.display]="text ? 'flex' : 'none'">
            {{ text }}
          </div>
          <div class="flex-grow-1">
          </div>
        </div>
        <div class="flex-grow-3">
          <ng-container [ngTemplateOutlet]="template" [ngTemplateOutletContext]="config"></ng-container>
        </div>
      </div>
    </div>

  `,
  styles: [`

    .loading-overlay {
      display: none;
      background-color: rgba(150,150,150,.5);
      position: absolute;
      z-index: 500;
      color: #fff;
      padding: 1rem;
    }

    .loading-overlay.no-background {
      background-color: transparent;
    }

    .loading-overlay.busy {
      display: block;
    }

    .loading-overlay div {
      display: flex;
      height: fit-content;
    }

    .loading-overlay .text {
      display: flex;
      font-size: 2.5rem;
      align-items: flex-end;
      margin-left: 10px;
    }

    .flex-grow-2 {
      flex-grow: 2;
    }
    .flex-grow-3 {
      flex-grow: 3;
    }

  `]
})
export class LoadingOverlayComponent {
  @Input() busy: boolean[] = [false];

  @Input() busySubjects: Subject<boolean>[] = [];
  @Input() getBusySubjects: () => Subject<boolean>[];
  busySubscriptions: Subscription[] = [];
  busyCount: number = 0;

  @Input() parentSelector: string;
  @Input() rootClass: string;
  @Input() icon: string = "hci fa-core fa-spin";
  @Input() iconSize: string = "fa-4x";
  @Input() showIcon: boolean = true;
  @Input() showFAIcon: boolean = false;
  @Input() mxAuto: boolean = false;
  @Input() myAuto: boolean = false;
  @Input() text: string;
  @Input() template: TemplateRef<any>;
  @Input() config: any = {};

  constructor(private elementRef: ElementRef,
              private renderer: Renderer2,
              private changeDetectorRef: ChangeDetectorRef) {
    this.renderer.addClass(this.elementRef.nativeElement, "loading-overlay");
  }

  ngOnInit() {
    if (this.rootClass) {
      let classes: string[] = this.rootClass.split(" ");
      for (let className of classes) {
        this.renderer.addClass(this.elementRef.nativeElement, className);
      }
    }
    if (this.mxAuto) {
      this.renderer.addClass(this.elementRef.nativeElement, "mx-auto");
    }
    if (this.myAuto) {
      this.renderer.addClass(this.elementRef.nativeElement, "my-auto");
    }
  }

  ngAfterViewInit(): void {
    if (this.getBusySubjects) {
      this.busySubjects = this.getBusySubjects();
      this.listenToBusySubjects();
    } else if (this.busySubjects) {
      this.listenToBusySubjects();
    }

    this.resize(true);
  }

  /**
   * Listens to changes to the inputs and changes to the busy arrays.
   *
   * @param {{[p: string]: SimpleChange}} changes
   */
  ngOnChanges(changes: {[propName: string]: SimpleChange}) {
    if (changes["busy"]) {
      this.renderer.removeClass(this.elementRef.nativeElement, "busy");
      if (this.busy) {
        for (let o of this.busy) {
          if (o) {
            this.renderer.addClass(this.elementRef.nativeElement, "busy");
            this.resize();
          }
        }
      }
    } else if (changes["busySubjects"]) {
      this.listenToBusySubjects();
    }
  }

  ngOnDestroy(): void {
    if (this.busySubscriptions) {
      for (let subscription of this.busySubscriptions) {
        subscription.unsubscribe();
      }
    }
  }

  /**
   * If there is a subject boolean, this will listen to those subjects.  There is a counter that is zero.  When subjects
   * are busy the counter increments.  When the subject becomes not busy, the counter decrements.  When the counter arrives
   * back at zero, the overall busy is disabled.
   */
  private listenToBusySubjects(): void {
    if (isDevMode()) {
      console.debug("BusyComponent.listenToBusySubjects: " + ((this.busySubjects) ? this.busySubjects.length : "undefined"));
    }

    if (!this.busySubjects) {
      return;
    }

    if (this.busySubscriptions) {
      for (let subscription of this.busySubscriptions) {
        subscription.unsubscribe();
      }
    }
    this.busySubscriptions = [];
    this.busyCount = 0;

    // Iterate over subjects.  When a subject is busy, increment a counter, when not busy, decrement.  When all subjects
    // are not busy, they overall we are not busy.
    for (let subject of this.busySubjects) {
      let subscription: Subscription = subject.subscribe((busy: boolean) => {
        if (busy !== undefined) {
          if (busy) {
            this.busyCount++;
          } else if (!busy && this.busyCount > 0) {
            this.busyCount--;
          }

          if (isDevMode()) {
            console.debug("BusyComponent.busySubjects: " + busy + ", " + this.busyCount);
          }

          this.renderer.removeClass(this.elementRef.nativeElement, "busy");

          if (this.busyCount > 0) {
            this.renderer.addClass(this.elementRef.nativeElement, "busy");
          }

          if (!(<ViewRef>this.changeDetectorRef).destroyed) {
            this.changeDetectorRef.detectChanges();
          }
        }

        this.resize();
      });
      this.busySubscriptions.push(subscription);
    }
  }

  /**
   * Listens to the window resize and adjusts its size.
   *
   * @param event
   */
  @HostListener("window:resize", ["$event"])
  private windowResize(event) {
    this.resize();
  }

  private resize(isDelay?: boolean): void {
    this.resetSize();

    if (isDelay) {
      of(undefined).pipe(delay(100)).subscribe(() => {
        this.resetSize();
      });
    }
  }

  /**
   * Looks at the size of the parent of this component and makes its height and width match the parent.
   */
  private resetSize(): void {
    let parent: HTMLElement;

    if (this.parentSelector) {
      parent = this.elementRef.nativeElement.closest(this.parentSelector);
    } else {
      parent = this.elementRef.nativeElement.parentElement;
      while (parent.offsetWidth === 0 && parent.parentElement) {
        parent = parent.parentElement;
      }
    }

    if (parent) {
      if ((parent.style.position === undefined || parent.style.position === "") && !parent.classList.contains("dropdown-menu")) {
        this.renderer.setStyle(parent, "position", "relative");
        this.renderer.setStyle(this.elementRef.nativeElement, "left", "0px");
        this.renderer.setStyle(this.elementRef.nativeElement, "top", "0px");
        this.renderer.setStyle(this.elementRef.nativeElement, "right", "0px");
        this.renderer.setStyle(this.elementRef.nativeElement, "bottom", "0px");
      } else if (parent.style.position === "relative") {
        this.renderer.setStyle(this.elementRef.nativeElement, "left", "0px");
        this.renderer.setStyle(this.elementRef.nativeElement, "top", "0px");
        this.renderer.setStyle(this.elementRef.nativeElement, "right", "0px");
        this.renderer.setStyle(this.elementRef.nativeElement, "bottom", "0px");
      } else if (parent.offsetWidth === 0 || parent.offsetHeight === 0) {
        this.renderer.setStyle(this.elementRef.nativeElement, "width", parent.style.width);
        this.renderer.setStyle(this.elementRef.nativeElement, "height", parent.style.height);
      } else {
        this.renderer.setStyle(this.elementRef.nativeElement, "width", parent.offsetWidth + "px");
        this.renderer.setStyle(this.elementRef.nativeElement, "height", parent.offsetHeight + "px");
      }
    }
  }

  /**
   * Finds the margin in terms of a number by stripping off px, rem, or em.
   *
   * @param {string} margin
   * @returns {number}
   */
  private getMargin(margin: string): number {
    return +margin.replace("px", "").replace("rem", "").replace("em", "");
  }

}
