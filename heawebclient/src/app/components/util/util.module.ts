import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatLegacyDialogModule as MatDialogModule} from "@angular/material/legacy-dialog";
import {MatLegacyButtonModule as MatButtonModule} from "@angular/material/legacy-button";
import {DynamicModule} from "ng-dynamic-component";
import {CustomSearchComponent} from './custom-search-dropdown/custom-search.component';
import {OrgGroupFilterPipe} from './custom-search-dropdown/org-group-filter.pipe';
import {RIInitialFocusDirective} from '../../directives/ri-initial-focus.directive';
import {InterpretedContentsComponent} from "./interpreted-contents/interpreted-contents.component";
import {DialogWrapInterpretedContentsComponent} from "./interpreted-contents/dialog-wrap-interpreted-contents.component";
import {AgGridDynamicComponent} from "./interpreted-contents/dynamic-components/ag-grid-dynamic.component";
import {FieldsetDynamicComponent} from './interpreted-contents/dynamic-components/fieldset.component';
import {SelectDynamicComponent} from "./interpreted-contents/dynamic-components/select-dynamic.component";
import {TextAreaDynamicComponent} from "./interpreted-contents/dynamic-components/text-area-dynamic.component";
import {AgGridModule} from "ag-grid-angular";
import {AgGridRendererModule} from "./grid-renderers/ag-grid-renderer.module";
import {AgGridEditorModule} from "./grid-editors/ag-grid-editor.module";
import {MatLegacyInputModule as MatInputModule} from "@angular/material/legacy-input";
import {SelectModule} from "@huntsman-cancer-institute/input";
import {MiscModule} from '@huntsman-cancer-institute/misc';
import {AwsLocationSelectComponent} from "./interpreted-contents/dynamic-components/aws-location-select.component";
import {UploadFilesGridComponent} from "./interpreted-contents/dynamic-components/upload-files-grid.component";
import {MatLegacyTooltipModule as MatTooltipModule} from "@angular/material/legacy-tooltip";
import {TextDisplayComponent} from "./interpreted-contents/dynamic-components/text-display.component";
import {InputDynamicComponent} from "./interpreted-contents/dynamic-components/input-dynamic.component";
import {ArrayFilterPipe} from './pipes/arrayFilter.pipe';
import {LoadingOverlayComponent} from "./loading-overlay.component";
import { AsyncCellRenderer } from './grid-renderers/async-cell-renderer.component';
import {CbSelectComponent} from "./select/cb-select.component";
@NgModule({
  imports: [
    NgbModule,
    FormsModule,
    CommonModule,
    DynamicModule,
    MatDialogModule,
    MatButtonModule,
    MatTooltipModule,
    AgGridModule,
    AgGridRendererModule,
    AgGridEditorModule,
    MatInputModule,
    ReactiveFormsModule,
    SelectModule,
    MiscModule,
    CbSelectComponent
  ],
    declarations: [
        CustomSearchComponent,
        OrgGroupFilterPipe,
        RIInitialFocusDirective,
        DialogWrapInterpretedContentsComponent,
        InterpretedContentsComponent,
        AgGridDynamicComponent,
        FieldsetDynamicComponent,
        LoadingOverlayComponent,
        SelectDynamicComponent,
        TextAreaDynamicComponent,
        InputDynamicComponent,
        TextDisplayComponent,
        AwsLocationSelectComponent,
        UploadFilesGridComponent,
        ArrayFilterPipe,
        AsyncCellRenderer
    ],
    exports: [
        CustomSearchComponent,
        DialogWrapInterpretedContentsComponent,
        InterpretedContentsComponent,
        AgGridDynamicComponent,
        LoadingOverlayComponent,
        SelectDynamicComponent,
        TextDisplayComponent,
        AwsLocationSelectComponent,
        UploadFilesGridComponent,
        TextAreaDynamicComponent,
    ],
    providers: []
})

export class UtilModule { }
