import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, from, of, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {DialogsService} from '../dialog/dialogs.service';
import { UtilService } from 'src/app/services/util.service';

export const SKIP_ERROR_HANDLING_INTERCEPTOR = 'X-Skip-Error-Handling-Interceptor'

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

  constructor(private dialogService: DialogsService, private utilService: UtilService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>  {
    console.debug('ErrorHandlerInterceptor: intercepting request', request);
    if (!request.headers.has(SKIP_ERROR_HANDLING_INTERCEPTOR) || (next instanceof HttpResponse && next.status === 401)) {
      console.debug('ErrorHandlerInterceptor: handling request', request);
      return next.handle(request).pipe(map((event: HttpEvent<any>) => {
        return event;
      }), catchError(this.errHandler));
    } else {
      console.debug('ErrorHandlerInterceptor: skipping error handling for request', request);
      return next.handle(request);
    }
  }

  private errHandler = (error: HttpErrorResponse): Observable<never> | Observable<any> => {
    if (error.status === 201 && error.statusText === 'Created') {
      // Skip this error because it is successfully processed but failed to parse json body due to the string body.
      return new Observable(subscriber => subscriber.error(error));
    }
    if (error.status >= 300 && error.status < 400) {
      // These are not errors but throwing so original call can handle it
      return throwError(error);
    }

    if (error.status === 401) {
      location.reload();
    }

    if (error.error instanceof ErrorEvent) {
      this.dialogService.error(error.error.message);
      return throwError(error);
    } else if (error.error instanceof Blob) {
      return from(error.error.text()).pipe(map(errorMessage => {
        this.dialogService.error(`${this.utilService.removePeriod(errorMessage)} (status ${error.status}).`);
        return throwError(error)
      }));
    } else if (error.error) {
      this.dialogService.error(`${this.utilService.removePeriod(error.error)} (status ${error.status}).`);
      return throwError(error);
    } else {
      this.dialogService.error(`Something went wrong (status ${error.status}).`);
      return throwError(error);
    }
  }
}
