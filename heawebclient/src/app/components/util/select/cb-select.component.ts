import {CdkVirtualScrollViewport, ScrollingModule} from '@angular/cdk/scrolling';
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnChanges,
  OnDestroy, OnInit,
  Output, Renderer2,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  FormsModule,
  NG_VALUE_ACCESSOR,
  NgControl,
  ReactiveFormsModule,
  UntypedFormControl,
} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatRippleModule} from '@angular/material/core';
import {FloatLabelType, MatFormField, MatFormFieldAppearance, MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatSelect, MatSelectModule} from '@angular/material/select';
import {Subject} from 'rxjs';
import {CommonModule} from '@angular/common';

@Component({
  selector: 'cb-select',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatCheckboxModule,
    MatRippleModule,
    MatChipsModule,
    ScrollingModule,
    CdkVirtualScrollViewport,
  ],
  template: `
<div (focus)="inputFocused($event, select)"
     (click)="inputFocused($event, select)" tabindex="0" >
  <mat-form-field #matFieldAnchor style="display:flex" [appearance]="appearance" >
    <div  style="display: flex; flex-grow: 1" *ngIf="!controlValuePresent()">
      <input #outerInputElement
             class="float-input"
             matInput type="text"
             [(ngModel)]="value"
             (ngModelChange)="onFilterChange($event)">
    </div>
    <mat-select [formControl]="control" [multiple]="multiple"
                (openedChange)="onDropdownStateChange($event)"
                (selectionChange)="selectOptions($event.value)"
                [compareWith]="compareByID" #select>
      <mat-select-trigger >
        <div  class="select-text-container" (click)="$event.stopPropagation()">
          <div *ngIf="!selectOpen"  #chipsContainer class="chips-container">
            <mat-chip class="hci-font chip"
                      *ngFor="let selectedOpt of selectedOptions; trackBy: trackByFn"
                      [removable]="true"
                      (removed)="removeChip(selectedOpt)">
              {{ displayField ? selectedOpt[displayField] : selectedOpt }}
              <mat-icon *ngIf="!control.disabled" matChipRemove>cancel</mat-icon>
            </mat-chip>
          </div>
          <input *ngIf="selectOpen" class="hci-font"  #inputElement
                 [disabled]="!control.enabled"
                 [style.height.px]="chipsContainerHeight"
                 matInput type="text"
                 [(ngModel)]="value"
                 (ngModelChange)="onFilterChange($event)"
                 (keydown.space)="$event.stopPropagation()">
        </div>
      </mat-select-trigger>
      <mat-checkbox  *ngIf="filteredOptions.length > 0 && multiple && allowAllOption" color="primary"
                    style="user-select: none; width:100%; cursor:pointer; padding:5px;" matRipple
                    [checked]="isAllSelected()"
                    (click)="toggleSelectAll()">
        <span>All</span>
      </mat-checkbox>
      <mat-option *ngFor="let opt of filteredOptions; trackBy: trackByFn"
                  [value]="opt" [ngClass]="customOptionClasses">
        {{ displayField ? opt[displayField] : opt }}
      </mat-option>
      <div *ngIf="filteredOptions.length === 0" style="padding:5px 10px;">No results found!</div>
    </mat-select>
  </mat-form-field>
</div>

  `,
  styles: [`
    .cdk-overlay-pane ::ng-deep mat-checkbox:hover{
      background-color: rgba(0, 0, 0, 0.04);
    }
    /* removing floating label */
    .mdc-notched-outline[ng-reflect-open='true'] .mdc-notched-outline__notch {
      display: none;
    }

    ::ng-deep  .mat-mdc-option.mdc-list-item  span {
      white-space: nowrap;
      font-family: hci-font, sans-serif;
      text-overflow: ellipsis;
      max-width: 100%;
    }
    .float-input {
      transform: translateX(-0.5em);
      position: absolute;
      z-index: 10;
    }

    .chips-container {
      display: flex;
      flex-wrap: wrap;
      min-height: 1em;
    }
    .select-text-container {
      max-height: 7.25em;
    }
    .chip {
      margin-bottom: 0.25em;
    }
    .hci-combobox-viewport mat-option .mat-option-text {
      white-space: nowrap;
    }


    ::ng-deep .hci-combobox-viewport .hci-combobox-selected {
      background-color: #dddddd;
    }


    /* fix jittery scroll bug in chrome */
    /*::ng-deep .hci-combobox-viewport {*/
    /*  overflow-anchor: none;*/
    /*}*/
    /*::ng-deep .hci-combobox-viewport .mat-active { .* */
    /*  background-color: #f3f3f3;*/
    /*}*/


  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CbSelectComponent),
      multi: true,
    },
  ],
  host: {
    class: 'hci-combobox-container'
  }

})
export class CbSelectComponent
    implements ControlValueAccessor, OnChanges, OnDestroy, OnInit, AfterViewInit {
  get appearance(){
    return this.__appearance;
  }
  @Input() public set appearance(appearance: MatFormFieldAppearance){
    this.__appearance = appearance;
  }
  @Input()
  public set customOptionClasses(value: string) {
    this.__customOptionClasses = value ? value : '';
  }
  public get customOptionClasses(): string {
    return !!this.__customOptionClasses ? this.__customOptionClasses : '';
  }



  constructor(private injector: Injector, private renderer: Renderer2) {}
  @Input() public customFieldClasses: string;
  @Input() public options: any[] = [];
  @Input() public multiple = false;
  @Input() public forceEmitObject = false;
  @Input() public valueField = 'value';
  @Input() public displayField: string;
  @Input()
  public customViewportClass = '';
  @Input() allowAllOption: boolean;

  @Output() selectionChanged: EventEmitter<any[]> = new EventEmitter<any[]>();
  @ViewChild('inputElement') inputElement!: ElementRef;
  @ViewChild('outerInputElement') outInputElement: ElementRef;
  @ViewChild('matFieldAnchor', { static: false }) fieldAnchor!: MatFormField;
  @ViewChild('chipsContainer', {static: false}) chipsContainer: ElementRef;

  public control = new FormControl<any>([]);
  public filteredOptions: string[] = this.options;
  public selectedOptions: string[] = [];
  public selectOpen = false;
  public value = '';
  public outerControl: AbstractControl = new UntypedFormControl([]);
  public chipsContainerHeight = 0;
  private noNgControl = true;
  private destroy$ = new Subject<void>();
  // tslint:disable-next-line:variable-name
  private __appearance: any;
  private __customOptionClasses = '';
  private onChangeFn: (value: any) => void = () => {};
  private onTouchedFn: () => void = () => {};

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.options) {
      this.filteredOptions = changes.options.currentValue || [];
    }
    setTimeout(() => {
      const currentSelectedOptions = this.control.value || [];
      this.selectOptions(currentSelectedOptions);
    });
  }

  writeValue(value: any[]): void {
    this.options = this.options && Array.isArray(this.options) ? this.options : [];
    this.selectedOptions = this.options.filter((opt: any) => {
      const optValue = this.valueField ? opt[this.valueField] : opt;
      if (value && Array.isArray(value) && typeof value[0] === 'boolean') {
        const booleanOptValue = typeof optValue === 'string' ? optValue === 'true' : Boolean(optValue);
        return value.includes(booleanOptValue);
      }
      return value && Array.isArray(value) ? value.includes(optValue) : false;
    });
    let val =  !this.multiple  && this.selectedOptions.length > 0 ? this.selectedOptions[0]  : this.selectedOptions;
    this.control.setValue(val);
  }


  registerOnChange(fn: any): void {
    this.onChangeFn = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedFn = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled) {
      this.control.disable();
    } else {
      this.control.enable();
    }
  }

  controlValuePresent(): boolean{
    const isArray: boolean = Array.isArray(this.control.value);
    return !!(isArray && this.control.value.length || !isArray && this.control.value);
  }

  toggleSelectAll() {
    if (this.isAllSelected()) {
      this.control.setValue([]);
      this.selectedOptions = [];
    } else {
      this.control.setValue([...this.filteredOptions]);
      this.selectedOptions = [...this.filteredOptions];
    }
    this.onChangeFn(this.selectedOptions.map(opt => this.valueField ? opt[this.valueField] : opt));
    this.selectionChanged.emit(this.selectedOptions);
  }

  isAllSelected(): boolean {
    if (this.control.value && Array.isArray(this.control.value)) {
      return this.control.value.length === this.options.length;
    }else {
      return false;
    }
  }

  public selectOptions(selectedOptions: any[] | any): void {
    const selectedOpts = Array.isArray(selectedOptions) ?  selectedOptions : [selectedOptions];
    const newVal = selectedOpts.map(opt => this.valueField && !this.forceEmitObject ? opt[this.valueField] : opt);
    if (this.noNgControl) {
      this.outerControl.setValue(newVal);
    }
    this.onChangeFn(newVal);
    this.selectedOptions = selectedOpts;
    this.selectionChanged.emit(newVal);
    // this.handleFocus();
  }

  onFilterChange(event: any) {
    const lowercasedEvent = (event || '').toLowerCase();
    this.filteredOptions = this.options.filter(opt => {
      const optDisplay = this.displayField ? opt[this.displayField] : opt;
      return optDisplay.toLowerCase().includes(lowercasedEvent) || this.isOptInOuterValue(opt);
    });
  }

  private isOptInOuterValue(opt: any): boolean {
    const optValue = this.valueField ? opt[this.valueField] : opt;
    if (this.forceEmitObject) {
      return this.outerControl.value.some(outerValOpt =>
          optValue === (this.valueField ? outerValOpt[this.valueField] : outerValOpt));
    }
    return this.outerControl.value.includes(optValue);
  }

  removeChip(opt: string) {
    const updatedOptions = this.selectedOptions.filter(i => i !== opt);
    this.control.setValue(updatedOptions);
    this.selectedOptions = updatedOptions;
    this.selectOptions(this.selectedOptions);
  }

  onDropdownStateChange(isOpened: boolean) {
    this.selectOpen = isOpened;
    if (isOpened) {
      this.chipsContainerHeight = this.chipsContainer?.nativeElement?.offsetHeight;
      this.handleFocus();
    } else {
      this.filteredOptions = this.options;
      this.value = '';
    }
    this.onTouchedFn();
  }

  compareByID = (optOne: any, optTwo: any) => {
    if (optOne && optTwo) {
      return this.valueField ? optOne[this.valueField] === optTwo[this.valueField] : optOne === optTwo;
    }
    return false;
  }

  trackByFn(index: number, opt: any) {
    return opt;
  }
  private handleFocus(): void {
    setTimeout(() => {
      if (this.outInputElement) {
        this.outInputElement.nativeElement.focus();
      } else if (this.inputElement) {
        this.inputElement.nativeElement.focus();
      }
    });

  }

  inputFocused(event: FocusEvent, select: MatSelect) {
    if (!this.selectOpen) {
      select.open(); // Open mat-select
    }
    this.handleFocus();
  }


  ngAfterViewInit(): void {
    const customClasses = this.customFieldClasses.split(' ');
    for (const c of customClasses) {
      this.renderer.addClass(this.fieldAnchor._elementRef.nativeElement, c);
    }

    const ngControl: NgControl = this.injector.get(NgControl, null);
    if (ngControl && ngControl.control) {
      this.outerControl = ngControl.control;
      this.noNgControl = false;
      ngControl.valueAccessor = this;
      setTimeout(() => this.control.setValidators(ngControl.validator));
    } else {
      this.noNgControl = true;
    }
    this.chipsContainerHeight = this.chipsContainer?.nativeElement?.offsetHeight;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
