import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { IHEAObjectContainer } from 'src/app/models/heaobject-container.model';
import { IDesktopObject } from 'src/app/models/heaobject.model';


@Component({
  template: ''
})
export abstract class AbstractCardComponent implements OnInit, OnDestroy{
  busyIcon = 'fas fa-sync fa-spin';
  dataLoading: boolean[] = [false];
  refresh = false;

  @Output() public readyToClose = new EventEmitter<boolean>(false);

  private _dataPath: string[];
  private _subject: IHEAObjectContainer<IDesktopObject>[];
  private subjectBS: BehaviorSubject<IHEAObjectContainer<IDesktopObject>[]> = new BehaviorSubject(null);
  private subjectBSSub: Subscription;

  ngOnInit(): void {
    this.subjectBSSub = this.subjectBS.subscribe(subject => {
      if (subject) {
        this.processSubject();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subjectBSSub) {
      this.subjectBSSub.unsubscribe();
    }
    if (this.subjectBS) {
      this.subjectBS.unsubscribe();
    }
  }

  @Input() set subject(obj: IHEAObjectContainer<IDesktopObject>[]) {
    this._subject = obj;
    if (this.subjectBS) {
      this.subjectBS.next(obj);
    }
  }

  get subject(): IHEAObjectContainer<IDesktopObject>[] {
    return this._subject;
  }

  @Input() set dataPath(path: string[]) {
    this._dataPath = [...path];
  }

  get dataPath(): string[] {
    if (this._dataPath) {
      return [...this._dataPath];
    } else {
      return null;
    }
  }

  close(): void {
    this.readyToClose.emit(this.refresh);
  }

  protected processSubject(): void {}
}

