import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DialogsService} from './dialogs.service';
import {ActionType} from '../../../models/generic-dialog-action.model';
import {MatLegacyDialog as MatDialog, MatLegacyDialogConfig as MatDialogConfig} from '@angular/material/legacy-dialog';

@Component({
    template: ''
})
export class DialogEntryComponent implements OnInit{
    private data: any;
    constructor(public dialog: MatDialog, private router: Router,
                private route: ActivatedRoute, private dialogService: DialogsService) {
        this.route.data.forEach((d: any) => {
            this.data = d;
        });
        this.openDialog(this.data.resolved);
    }
    ngOnInit(){

    }
    openDialog(routeState: any): void {
        const actionConfig: any = {
            actions: [
                {
                    type: ActionType.PRIMARY,
                    name: 'Save',
                    internalAction: 'saveFlowCell'
                },
                {
                    type: ActionType.SECONDARY,
                    name: 'Cancel',
                    internalAction: 'onClose'
                }
            ]
        };
        if (routeState && routeState.dialogClass){
            const config: MatDialogConfig = new MatDialogConfig();
            for (const rsKey in routeState){
                if (rsKey !== 'dialogClass' ){
                    config.data = routeState[rsKey];
                    break;
                }
            }

            const dialogRef = this.dialogService.genericDialogContainer( routeState.dialogClass, null, null, config, actionConfig);
            dialogRef.subscribe(result => {
                this.router.navigate(['../'], { relativeTo: this.route });
            });
        }

    }
}
