import {
  Component,
  ElementRef,
  HostListener,
  Inject,
  OnInit,
  OnDestroy,
  ViewChild, ComponentRef, ChangeDetectorRef
} from '@angular/core';

import {Router} from '@angular/router';
import {BaseGenericContainerDialog} from './base-generic-container-dialog';
import {ActionType, GDAction} from '../../../models/generic-dialog-action.model';
import {LegacyDialogPosition as DialogPosition, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialog as MatDialog, MatLegacyDialogRef as MatDialogRef} from '@angular/material/legacy-dialog';

@Component({
  selector: 'cb-generic-dialog-container',
  templateUrl: './generic-container-dialog.component.html',
  styles: [`

    .no-padding{
      padding:0;
    }
    .no-margin{
      margin-bottom: 0;
      margin-top: 0;
      margin-left: 0;
      margin-right: 0;
    }
    .grabbable {
      cursor: move;
      cursor: grab;
      cursor: -moz-grab;
      cursor: -webkit-grab;
    }
    .grabbed:active {
      cursor: move;
      cursor: grabbing;
      cursor: -moz-grabbing;
      cursor: -webkit-grabbing;
    }
    .exit{
      max-width: 20px;
      cursor: pointer;
    }
    .i-class {
      margin-left:  0.3em;
      margin-right: 0.3em;
    }
    mat-dialog-content {
      max-height: 100% !important;
    }
    .generic-dialog-header-colors{
      background-color: #6da9c7;
      color: #ffffff;
    }
    .generic-dialog-footer-colors{
      background-color: #ffffff;
    }

    ::ng-deep .no-padding .mat-dialog-container {
      padding: 0 !important;
    }
    ::ng-deep .mx-sized-dialog .mat-dialog-container {
      max-width: 100vw !important;
      max-height: 100vh !important;
    }
    .full-height{
      height: 100%;
    }
    .full-width {
      width: 100%;
    }
    .padded {
      padding: .25em;
    }
    /*::ng-deep.mat-dialog-container {*/
    /*    resize: both;*/
    /*    overflow: auto;*/
    /*    background: #fff;*/
    /*}*/

  `]
})
export class GenericContainerDialogComponent implements OnInit, OnDestroy {

  @ViewChild('topmostLeftmost', {static: false}) topmostLeftmost: ElementRef;

  type = ActionType;
  title: string;
  icon: string;
  dialogContentBluePrint: any;
  actions: GDAction[] = [];
  originalXClick = 0;
  originalYClick = 0;
  dialogContent: BaseGenericContainerDialog;

  protected positionX = 0;
  protected positionY = 0;
  movingDialog = false;
  useOwnHeader: boolean = false;
  useSaveFooter: boolean;


  constructor(private dialog: MatDialog,
              private router: Router,
              private changeDetector: ChangeDetectorRef,
              private dialogRef: MatDialogRef<GenericContainerDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data) {
    if (data) {
      this.dialogContentBluePrint = data.dialogContent;
      this.icon = data.icon;
      this.title = data.title;
      this.useOwnHeader = data.useOwnHeader ? data.useOwnHeader : false;
      this.useSaveFooter = data.actionConfig ? data.actionConfig.useSaveFooter : false;
      this.actions = data.actionConfig ? data.actionConfig.actions as GDAction[] : [];
    }
  }

  ngOnInit() {
  }

  createdComponent(event: ComponentRef<any>) {
    this.dialogContent = (event.instance as BaseGenericContainerDialog);
  }

  executeAction(action: GDAction): void {
    if (action.externalAction && !action.internalAction) {
      action.externalAction.function();
    }
    if (action.internalAction) {
      const internalAction: string = action.internalAction;
      if (action.internalAction === 'cancel' || action.internalAction === 'onClose') {
        if (this.dialogContent[internalAction]) {
          this.dialogContent[internalAction](action.externalAction);
        } else {
          this.onClose();
        }
      } else {
        this.dialogContent[internalAction](action.externalAction);
      }
    }
  }

  onMouseDownHeader(event: any): void {
    if (!event) {
      return;
    }

    this.positionX = this.topmostLeftmost.nativeElement.offsetLeft;
    this.positionY = this.topmostLeftmost.nativeElement.offsetTop;

    this.originalXClick = event.screenX;
    this.originalYClick = event.screenY;

    this.movingDialog = true;

    // this.changeDetector.detach();

  }
  isOutOfViewport(elem) {

    // Get element's bounding
    const bounding = elem.getBoundingClientRect();

    // Check if it's out of the viewport on each side
    let out: any = {};
    if (bounding.top < 0){
      out.top = true;
      out.topDiff = bounding.top * -1;
    }else {
      out.top = false;
    }
    if (bounding.left  < 0){
      out.left = true;
      out.leftDiff = bounding.left * -1;
    }else {
      out.left = false;
    }
    if (bounding.bottom > (window.innerHeight || document.documentElement.clientHeight)) {
      out.bottom = true;
      out.bottomDiff = window.innerHeight - bounding.bottom;
    }else {
      out.bottom = false;
    }

    if (bounding.right > (window.innerWidth || document.documentElement.clientWidth)){
      out.right = true;
      out.rightDiff = window.innerWidth - bounding.right;
    }else{
      out.right = false;
    }

    out.anyEdge = out.top || out.left || out.bottom || out.right;

    return out;

  }

  @HostListener('window:mousemove', ['$event'])
  onMouseMove(event: any): void {
    if (!event) {
      return;
    }
    let out = this.isOutOfViewport(this.topmostLeftmost.nativeElement);
    let pos = {left: '', top: ''};
    if (out.anyEdge ){
      if (out.top){
        pos.top = (this.positionY + out.topDiff) + 'px';
      }
      if (out.bottom){
        pos.top = (this.positionY + out.bottomDiff) + 'px';
      }
      if (out.left) {
        pos.left = (this.positionX + out.leftDiff) + 'px';
      }
      if (out.right) {
        pos.left = (this.positionX + out.rightDiff) + 'px';
      }
      pos.left = pos.left !== '' ? pos.left : this.positionX + 'px';
      pos.top = pos.top !== '' ? pos.top : this.positionY + 'px';

      this.dialogRef.updatePosition(pos);
    }

    if (this.movingDialog) {
      this.positionX += event.screenX - this.originalXClick;
      this.positionY += event.screenY - this.originalYClick;

      this.originalXClick = event.screenX;
      this.originalYClick = event.screenY;

      const newDialogPosition: DialogPosition = {
        left:   '' + this.positionX + 'px',
        top:    '' + this.positionY + 'px',
      };

      this.dialogRef.updatePosition(newDialogPosition);
    }
  }


  @HostListener('window:mouseup', ['$event'])
  onMouseUp(): void {
    this.movingDialog = false;
    // this.changeDetector.reattach();
  }



  onClose() {
    this.dialogRef.close();
  }

  ngOnDestroy() {
  }
}
