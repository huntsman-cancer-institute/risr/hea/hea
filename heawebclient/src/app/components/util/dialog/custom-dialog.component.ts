import {UntypedFormGroup} from '@angular/forms';
import {ActionType, GDAction, GDActionConfig} from '../../../models/generic-dialog-action.model';
import {
  ChangeDetectorRef,
  Component,
  ElementRef, HostListener,
  Inject,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {LegacyDialogPosition as DialogPosition, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef} from '@angular/material/legacy-dialog';

@Component({
    selector: 'cb-custom-dialog',
    template: `
        <div class="d-flex full-height full-width flex-column">
            <div *ngIf="this.title"
                 class="full-width generic-dialog-header-colors no-margin no-padding">
                <div #topmostLeftmost mat-dialog-title (mousedown)="onMouseDownHeader($event)"
                     class="force-flex-container-row align-items-center full-width p-2   {{ movingDialog ? 'grabbed' : 'grabbable' }}">
                    <div class="-flex flex-row align-items-center padded">
                        <img *ngIf="icon" class="icon" [src]="this.icon">
                        <div *ngIf="icon && icon.substr(0, 2) === '<i'" class="i-class"
                             [innerHTML]="icon"></div>
                        <h3 style="margin:0;">{{this.title}}</h3>
                    </div>
                    <div class="flex-grow-1"></div>
                    <div class="padded" (click)="onClose()">
                        <i class="exit fas fa-times"></i>
                    </div>
                </div>
            </div>
            <mat-dialog-content class="flex-grow-1 full-width no-margin p-2"
                                style="min-height: 6em;">
                <div #anchor></div>
            </mat-dialog-content>
            <mat-dialog-actions *ngIf="actions && actions.length > 0"
                                class="d-flex flex-row justify-content-end no-margin no-padding generic-dialog-footer-colors">
                <div *ngFor="let action of actions">
                    <cb-save-footer *ngIf="action.type === actionType.PRIMARY;else secondary"
                                    [actionType]="action.type"
                                    [icon]="action.icon"
                                    [disableSave]="primaryDisable"
                                    (saveClicked)="executeAction(action)"
                                    [name]="action.name">
                    </cb-save-footer>
                    <!-- to avoid issue with multiple spinner or dirty notes-->
                    <ng-template #secondary>
                        <cb-save-footer [actionType]="action.type"
                                        (saveClicked)="executeAction(action)"
                                        [icon]="action.icon"
                                        [disableSave]="secondaryDisable"
                                        [name]="action.name">
                        </cb-save-footer>
                    </ng-template>
                </div>
            </mat-dialog-actions>
        </div>
    `,
    styles: [`
        .centered-text { text-align: center; }

        .no-padding{
            padding:0;

        }

        .no-margin{
            margin: 0;
        }

        .grabbable {
            cursor: move;
            cursor: grab;
            cursor: -moz-grab;
            cursor: -webkit-grab;
        }
        .grabbed:active {
            cursor: grabbing;
            cursor: -moz-grabbing;
            cursor: -webkit-grabbing;
        }

        .force-flex-container-row{
            display:flex !important;
        }

        .exit{
            max-width: 20px;
            cursor: pointer;
        }

        .i-class {
            margin-left:  0.3em;
            margin-right: 0.3em;
        }
        mat-dialog-content {
            max-height: 100vh !important;
            max-width: 100vw !important ;
        }
    `]
})

export class CustomDialogComponent implements OnInit {
    @ViewChild('anchor', { read: ViewContainerRef, static : true }) _vcr: ViewContainerRef;
    @ViewChild('topmostLeftmost', {static: false}) topmostLeftmost: ElementRef;
    public title = '';
    public icon = '';

    public actionType: any = ActionType;

    originalXClick = 0;
    originalYClick = 0;
    movingDialog = false;
    protected positionX = 0;
    protected positionY = 0;
    context: any;

    private tempRef: TemplateRef<any>;
    public actions: GDAction[];
    public form: UntypedFormGroup;
    public get primaryDisable(): boolean {
        return this.form && this.form.invalid;
    }
    // todo need make this customizable
    public get secondaryDisable(): boolean {
        return false;
    }

    constructor(public dialogRef: MatDialogRef<CustomDialogComponent>,
                @Inject(MAT_DIALOG_DATA) private data: any,
                private changeDetector: ChangeDetectorRef) {
        this.title = data.title;
        this.tempRef = data.templateRef;
        this.icon = data.icon;
        this.context = data.context;
        const actionConfig: GDActionConfig = data.actionConfig ? data.actionConfig as GDActionConfig : null;
        this.actions = actionConfig ? actionConfig.actions : [];
        this.form = actionConfig && actionConfig.formSource ?  actionConfig.formSource : null;

    }

    ngOnInit() {

        if (!this.context ) { // no matter what we will atleast pass the dialogRef in as context
          this.context = {};
        }
        this.context.dialog = this.dialogRef;
        if (this.form){
            this.context.form = this.form;
        }

        this._vcr.createEmbeddedView<any>(this.tempRef, this.context);

    }

    onClose() {
        this.dialogRef.close();
    }

    onMouseDownHeader(event: any): void {
        if (!event) {
            return;
        }

        this.positionX = this.topmostLeftmost.nativeElement.offsetLeft;
        this.positionY = this.topmostLeftmost.nativeElement.offsetTop;

        this.originalXClick = event.screenX;
        this.originalYClick = event.screenY;

        this.movingDialog = true;
        this.changeDetector.detach();
    }
    @HostListener('window:mousemove', ['$event'])
    onMouseMove(event: any): void {
        if (!event) {
            return;
        }

        if (this.movingDialog) {
            this.positionX += event.screenX - this.originalXClick;
            this.positionY += event.screenY - this.originalYClick;

            this.originalXClick = event.screenX;
            this.originalYClick = event.screenY;

            const newDialogPosition: DialogPosition = {
                left:   '' + this.positionX + 'px',
                top:    '' + this.positionY + 'px',
            };

            this.dialogRef.updatePosition(newDialogPosition);
        }
    }
    @HostListener('window:mouseup', ['$event'])
    onMouseUp(): void {
        this.movingDialog = false;
        this.changeDetector.reattach();
    }

    executeAction(action: GDAction): void {
        if (action.externalAction) {
            if (action.externalAction.function && (this.form  || (action.externalAction.params))){
                action.externalAction.function(this.dialogRef, this.form, action.externalAction.params);
            }else if (action.externalAction.function){
                action.externalAction.function(this.dialogRef);
            }

        }
        if (action.internalAction && action.internalAction === 'onClose') {
            this.onClose();
        }
    }

}
