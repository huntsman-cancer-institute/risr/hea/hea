import {CommonModule} from '@angular/common';
import {AlertDialogComponent} from './alert-dialog.component';
import {SpinnerDialogComponent} from './spinner-dialog.component';
import {CustomDialogComponent} from './custom-dialog.component';
import {GenericContainerDialogComponent} from './generic-container-dialog.component';
import {SaveFooterComponent} from './save-footer.component';
import {MatLegacyDialogModule as MatDialogModule} from '@angular/material/legacy-dialog';
import {MatLegacyButtonModule as MatButtonModule} from '@angular/material/legacy-button';
import {NgModule} from '@angular/core';
import {DialogEntryComponent} from "./dialog-entry.component";
import {MatLegacyProgressSpinnerModule as MatProgressSpinnerModule} from "@angular/material/legacy-progress-spinner";
import {DynamicModule} from "ng-dynamic-component";


@NgModule({
    imports: [
        CommonModule,
        MatDialogModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        DynamicModule,
    ],
    exports: [
        AlertDialogComponent,
        SpinnerDialogComponent,
        CustomDialogComponent,
        DialogEntryComponent
    ],
    declarations: [
        AlertDialogComponent,
        SpinnerDialogComponent,
        CustomDialogComponent,
        GenericContainerDialogComponent,
        SaveFooterComponent,
        DialogEntryComponent
    ]
})
export class DialogsModule {
}

