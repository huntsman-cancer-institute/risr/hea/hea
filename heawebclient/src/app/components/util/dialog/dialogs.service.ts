import {Injectable, TemplateRef} from '@angular/core';

import { Observable } from 'rxjs';

import { AlertDialogComponent } from './alert-dialog.component';
import { SpinnerDialogComponent } from './spinner-dialog.component';
import {CustomDialogComponent} from './custom-dialog.component';
import {GenericContainerDialogComponent} from './generic-container-dialog.component';
import {MatLegacyDialog as MatDialog, MatLegacyDialogConfig as MatDialogConfig, MatLegacyDialogRef as MatDialogRef} from '@angular/material/legacy-dialog';
import {GDActionConfig} from "../../../models/generic-dialog-action.model";

export enum DialogType {
    ERROR = 'Error',
    WARNING = 'Warning',
    ALERT = 'Alert',
    SUCCESS = 'Succeed',
    FAILED = 'Failed',
    CONFIRM = 'Confirm',
    INFO = 'Info',
    VALIDATION = 'Validation Error',
}

@Injectable({providedIn: 'root'})
export class DialogsService {

    private _spinnerDialogIsOpen = false;
    private spinnerWorkItemCount = 0;
    private checkingSpinnerWorkItems = false;

    public spinnerDialogRefs: MatDialogRef<SpinnerDialogComponent>[] = [];

    public get spinnerDialogIsOpen(): boolean {
        return this._spinnerDialogIsOpen;
    }

    constructor(private dialog: MatDialog) { }

    public alert(message: string|string[], title?: string, dialogType?: DialogType, icon?: string,
                 config?: MatDialogConfig): Observable<boolean> {
        return this.openDialog(message, title, dialogType ? dialogType : DialogType.ALERT, icon, config);
    }

    public confirm(message: string|string[], title?: string, icon?: string, config?: MatDialogConfig): Observable<boolean> {
        return this.openDialog(message, title, DialogType.CONFIRM, icon, config);
    }

    public error(message: string|string[], title?: string, icon?: string, config?: MatDialogConfig): Observable<boolean> {
        return this.openDialog(message, title, DialogType.ERROR, icon, config);
    }

    public info(message: string|string[], title?: string, icon?: string, config?: MatDialogConfig): Observable<boolean> {
        return this.openDialog(message, title, DialogType.INFO, icon, config);
    }

    public createCustomDialog(tempRef: TemplateRef<any>, context?: any, title?: string, icon?: string, config?: MatDialogConfig,
                              actionConfig?: GDActionConfig) {
        let configuration: MatDialogConfig = null;
        if (!config) {
            configuration = new MatDialogConfig();
        } else {
            configuration = config;
        }

        configuration.data = configuration.data ? configuration.data : {};
        configuration.data.templateRef = tempRef;
        configuration.data.title = title ? title : '';
        configuration.data.icon = icon ? icon : '';
        configuration.data.context = context;
        if (actionConfig) {
            configuration.data.actionConfig = actionConfig;
        }

        configuration.minWidth = configuration.minWidth ?  configuration.minWidth : '10em';
        configuration.width = configuration.width ? configuration.width : '30em';

        configuration.panelClass = ['mx-sized-dialog', 'no-padding'];
        configuration.disableClose = true;
        configuration.hasBackdrop = true;

        const dialogRef = this.dialog.open(CustomDialogComponent, configuration);

        return dialogRef;
    }

    public startDefaultSpinnerDialog(): MatDialogRef<SpinnerDialogComponent> {
        return this.startSpinnerDialog('Please wait...', 3, 30);
    }

    public startSpinnerDialog(message: string, strokeWidth: number, diameter: number): MatDialogRef<SpinnerDialogComponent> {
        if (this._spinnerDialogIsOpen) {
            return null;
        }

        this._spinnerDialogIsOpen = true;

        const configuration: MatDialogConfig = new MatDialogConfig();
        configuration.data = {
            message,
            strokeWidth,
            diameter
        };
        configuration.width = '13em';
        configuration.disableClose = true;

        const dialogRef: MatDialogRef<SpinnerDialogComponent> = this.dialog.open(SpinnerDialogComponent, configuration);

        dialogRef.afterClosed().subscribe(() => { this._spinnerDialogIsOpen = false; });

        this.spinnerDialogRefs.push(dialogRef);

        return dialogRef;
    }

    // Let there be an alternative, global way to stop all active spinner dialogs.
    public stopAllSpinnerDialogs(): void {
        this.spinnerWorkItemCount = 0;
        for (const dialogRef of this.spinnerDialogRefs) {
            setTimeout(() => {
                dialogRef.close();
            });
        }
        this.spinnerDialogRefs = [];
    }

    public addSpinnerWorkItem(): void {
        this.spinnerWorkItemCount++;
        if (!this.checkingSpinnerWorkItems) {
            this.checkingSpinnerWorkItems = true;
            setTimeout(() => {
                this.checkSpinnerWorkItems();
            });
        }
    }

    public removeSpinnerWorkItem(): void {
        this.spinnerWorkItemCount--;
        if (this.spinnerWorkItemCount < 0) {
            this.spinnerWorkItemCount = 0;
        }
        if (!this.checkingSpinnerWorkItems) {
            this.checkingSpinnerWorkItems = true;
            setTimeout(() => {
                this.checkSpinnerWorkItems();
            });
        }
    }

    private checkSpinnerWorkItems(): void {
        this.checkingSpinnerWorkItems = false;
        if (this.spinnerWorkItemCount) {
            if (!this._spinnerDialogIsOpen) {
                this.startDefaultSpinnerDialog();
            }
        } else if (this._spinnerDialogIsOpen) {
            this.stopAllSpinnerDialogs();
        }
    }

    public genericDialogContainer(dialogContent: any, title: string, icon?: string,
                                  config?: MatDialogConfig, actionConfig?: GDActionConfig): Observable<any> {
        let configuration: MatDialogConfig = null;


        if (!config) {
            configuration = new MatDialogConfig();
        } else {
            configuration = config;
        }
        configuration.data = configuration.data ? configuration.data : {};
        configuration.data.dialogContent = dialogContent;
        configuration.data.title = title;
        if (icon) {
            configuration.data.icon = icon;
        }
        if (actionConfig) {
            configuration.data.actionConfig = actionConfig;
        }
        configuration.panelClass = ['mx-sized-dialog', 'no-padding'];
        configuration.disableClose = true;
        configuration.hasBackdrop = true;
        const dialogRef = this.dialog.open(GenericContainerDialogComponent, configuration );

        return dialogRef.afterClosed();
    }

    private openDialog(message: string|string[], title?: string, type?: DialogType,
                       icon?: string, config?: MatDialogConfig): Observable<any> {
        let configuration: MatDialogConfig = null;
        if (!config) {
            configuration = new MatDialogConfig();
        } else {
            configuration = config;
        }

        configuration.data = configuration.data ? configuration.data : {};
        configuration.data.message = message;
        configuration.data.title = title ? title : '';
        configuration.data.icon = icon ? icon : '';
        configuration.data.dialogType = type ? type : '';

        configuration.maxWidth = configuration.maxWidth ? configuration.maxWidth : '80vw';
        configuration.maxHeight = configuration.maxHeight ? configuration.maxHeight : '80vh';
        configuration.minWidth = configuration.minWidth ?  configuration.minWidth : '30em';

        configuration.panelClass = 'no-padding';
        configuration.disableClose = true;
        configuration.autoFocus = false;
        configuration.hasBackdrop = false;

        let dialogRef: MatDialogRef<AlertDialogComponent>;

        dialogRef = this.dialog.open(AlertDialogComponent, configuration);

        return dialogRef.afterClosed();
    }


}
