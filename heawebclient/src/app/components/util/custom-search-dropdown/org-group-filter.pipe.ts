import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'orgGroupFilter'
})
export class OrgGroupFilterPipe implements PipeTransform {

  transform(items: any[], filter: string, mode: string, pathToProp?: (obj: any) => any ): any[] {
    if (!items) {
      return [];
    }
    if (mode === 'recent') {
      items = items;
    } else if (mode === 'favorite') {
      items = items;
    }

    if (!filter) {
      return items;
    }
    filter = filter.toLowerCase();
    return items.filter((orgGroup: any) => {
      return pathToProp(orgGroup).display_name.toLowerCase().includes(filter);
    });
  }
}
