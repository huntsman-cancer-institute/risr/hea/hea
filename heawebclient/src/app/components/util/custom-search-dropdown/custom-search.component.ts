import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbDropdown} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {OrgService} from '../../../services/org/org.service';
import {HEAObject} from '../../../models/heaobject.model';

/**
 * A component designed to search a list of data.  This search is internal and expects the list provided to be
 * comprehensive.
 */
@Component({
  selector: 'cb-custom-search',
  template: `
    <div
      style="margin: 0; padding: 0; display: flex; flex-direction: column; width: 350px; height: 300px; overflow-y: hidden;">
      <div style="width: 100%; padding-bottom: 40px; overflow-y: hidden;">
        <div class="study-search-parent">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" id="filterIcon">
                <i class="fas fa-search fa-lg"></i>
              </span>
            </div>
            <input name="search-input" 
                   type="text"
                   class="form-control"
                   aria-describedby="filterIcon"
                   [ngModel]="filter"
                   (keyup)="filterChangeEvent($event)"
                   (click)="stopPropagation($event)"/>
          </div>
        </div>
        <div class="d-flex flex-grow-1" style="overflow-y: auto; height: 100%;">
          <div class="study-list-parent flex-grow-1">
            <div *ngIf="allowNone"
                 (click)="selectOrgGroup(null)">None
              <!--              <div>None</div>-->
            </div>
            <div *ngFor="let data of inputData | orgGroupFilter: filter: mode: getObjFromProp.bind(this)"
                 class="study-list-item"
                 (click)="selectOrgGroup(data)">
              <div>{{ getObjFromProp(data).display_name }}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .input-group {
      height: 2rem;
    }

    .study-list-item .bubble {
      border-radius: 25px;
      padding-right: 5px;
      padding-left: 5px;
      width: auto;
    }
  `]
})
export class CustomSearchComponent implements OnInit, AfterViewInit{

  @Input() dropDown: NgbDropdown;

  mode: string;
  filter: string;
  _inputData: any[] = [];
  get inputData(): any[] {
    return this._inputData;
  }
  @Input() set inputData(val: any[]){
    this._inputData = val;
    setTimeout(() => {
      if ((this.autoSelect && !this._autoSelected) && this._inputData && Array.isArray(this._inputData) && this._inputData.length === 1) {
        this.selectOrgGroup(this._inputData[0]);
        this._autoSelected = true;
      }
    });

  }
  @Input() propertyPath: string;
  @Input() allowNone = true;
  @Input() autoSelect = false;
  @Output() selected = new EventEmitter<HEAObject>();
  private _autoSelected: boolean;


  constructor(private router: Router, private orgService: OrgService) {
  }

  ngOnInit() {
    // if(this.allowNone) {
    //   this.inputData.unshift([]);
    // }
  }

  getObjFromProp(obj: any): any {
    if (this.propertyPath){
      const pathParts: string[] = this.propertyPath.split('.');
      for (const p of pathParts) {
        obj = obj[p];
      }
      return obj;
    }
    return obj;
  }

  selectOrgGroup(orgGroup: any) {
    if (this.dropDown) {
      this.dropDown.close();
    }
    this.orgService.updateSelectedOrg(orgGroup);
    this.selected.emit(orgGroup);

  }

  filterChangeEvent(event: Event) {
    this.filterChange((event.target as HTMLInputElement).value);
  }

  filterChange(filter: string) {
    this.filter = filter;
  }

  stopPropagation(event: Event) {
    event.stopPropagation();
  }
  ngAfterViewInit() {

  }
}
