import {Component} from "@angular/core";
import {ICellRendererAngularComp} from "ag-grid-angular";
import {CellRendererValidation} from "./cell-renderer-validation";
import {ICellRendererParams} from "ag-grid-community";

@Component({
  template: `
    <div class="full-width full-height">
      <div class="t full-width full-height fix-table">
        <div class="tr">
          <div *ngIf="!tooltip" class="td cell-text-container ellipsis">
            {{ display }}
          </div>
          <div *ngIf="tooltip"
               [title]="tooltip"
               class="td cell-text-container text-has-tooltip ellipsis">
            {{ display }}
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .t { display: table; }
    .tr { display: table-row; }
    .td { display: table-cell; }

    .cell-text-container {
      vertical-align: middle;
      padding-left: 0.3rem;
    }

    .full-width  { width: 100%; }
    .full-height { height: 100%; }

    .fix-table { table-layout: fixed; }

    .ellipsis {
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .error {
      background: linear-gradient(rgba(255, 0, 0, 0.25), rgba(255, 0, 0, 0.25), rgba(255, 0, 0, 0.25));
      border: solid red 2px;
    }
  `]
})
export class TextAndTooltipRenderer extends CellRendererValidation implements ICellRendererAngularComp {
  public value: string;
  public display: string;

  public tooltipField: string = "tooltip";
  public tooltip: any = null;

  agInit2(params: ICellRendererParams): void {
    this.value = "";

    if (this.params) {
      if (this.params.value) {
        this.value = this.params.value;
      }
    }

    this.display = this.value;

    if (this.params && this.params.column && this.params.column.colDef && this.params.column.colDef.cellRendererParams) {
      if (this.params.column.colDef.cellRendererParams.tooltipField) {
        this.tooltipField = this.params.column.colDef.cellRendererParams.tooltipField;
      }

      let tokens = this.tooltipField.split('.');
      let tempObjectOrString: any = this.params.data;
      let goodState: boolean = false;

      for (let token of tokens) {
        if (tempObjectOrString[token]) {
          goodState = true;
          tempObjectOrString = tempObjectOrString[token];
        } else {
          goodState = false;
          break;
        }
      }

      if (goodState) {
        this.tooltip = tempObjectOrString;
      }
    }
  }

  refresh(): boolean {
    return false;
  }
}
