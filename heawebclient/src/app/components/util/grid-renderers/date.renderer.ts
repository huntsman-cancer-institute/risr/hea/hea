import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import {DateParser} from "../parsers/date-parser";

@Component({
  template: `
    <div class="full-width full-height" >
      <div class="t full-width full-height">
        <div class="tr">
          <div class="td vertical-center right-align padded cursor">
            {{ displayString }}
          </div>
          <!--<div class="td vertical-center">-->
          <!--<button class="full-height"><img [src]="'./assets/calendar_date.png'" alt=""/></button>-->
          <!--</div>-->
        </div>
      </div>
    </div>
  `,
  styles: [`

    .full-width  { width:  100% }
    .full-height { height: 100% }

    .t  { display: table; }
    .tr { display: table-row; }
    .td { display: table-cell; }

    .vertical-center { vertical-align: middle;   }
    .right-align     { text-align:     right;    }
    .padded          { padding:        0 0.3rem; }

    .cursor { cursor: pointer; }

    .error {
      background: linear-gradient(rgba(255,0,0,0.25), rgba(255,0,0,0.25), rgba(255,0,0,0.25));
      border: solid red 2px;
    }
  `]
})
export class DateRenderer implements ICellRendererAngularComp{

  public static readonly DEFAULT_RECEIVED_DATE_FORMAT: string = "yyyy-mm-dd";
  public static readonly DEFAULT_DISPLAY_DATE_FORMAT: string = "mm/dd/yyyy";

  value: any;
  displayString: string;
  params: any;

  // constructor(private propertyService: PropertyService) { }
  // this.usesCustomChartfields = this.propertyService.getExactProperty('configurable_billing_accounts').propertyValue;

  agInit(params: any): void {
    this.params = params;
    this.value = this.params.value;
    this.displayString = '';

    if (this.value && this.value !== '') {
      if (this.params && this.params.colDef && this.params.colDef.dateParser) {
        this.displayString = (this.params.colDef.dateParser as DateParser).parseDateToDisplayString(this.value);
      } else {
        this.displayString = this.value;
      }
    }
  }


  refresh(params: any): boolean {
    return false;
  }
}
