import { Component } from '@angular/core';

import { AgRendererComponent } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { HEAObjectContainer } from 'src/app/models/heaobject-container.model';
import { IDesktopObject } from 'src/app/models/heaobject.model';
import { CjService } from 'src/app/services/cj/cj.service';

@Component({
  selector: 'cb-grid-async-component',
  template: `<span>{{ cellValue }}</span>`,
})
export class AsyncCellRenderer implements AgRendererComponent {
  cellValue: string;
  private params: ICellRendererParams;

  constructor(private cjService: CjService) {

  }

  agInit(params: ICellRendererParams): void {
    this.params = params;

    if (params.value) {
      (params.value as Observable<any>).pipe(take(1)).subscribe(resp => {
        this.cellValue = resp;
      });
    } else {
      this.cellValue = params.value;
    }

  }

  // gets called whenever the user gets the cell to refresh
  refresh(params: ICellRendererParams): boolean {
    // set value into cell again
    if (params.value) {
      (params.value as Observable<any>).pipe(take(1)).subscribe(resp => {
        this.cellValue = resp;
      });
    } else {
      this.cellValue = params.value;
    }

    return true;
  }
}

function getRandomDelay() {
   const min = 500;
   const max = 2000;

  return Math.random() * (max - min) + min;
}
