import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatLegacyTooltipModule as MatTooltipModule } from '@angular/material/legacy-tooltip';
import { DateRenderer } from "./date.renderer";
import { TextAndTooltipRenderer } from "./text-and-tooltip.renderer";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
  ],
  declarations: [
    DateRenderer,
    TextAndTooltipRenderer
  ],
  exports: [
    DateRenderer,
    TextAndTooltipRenderer
  ]
})
export class AgGridRendererModule { }
