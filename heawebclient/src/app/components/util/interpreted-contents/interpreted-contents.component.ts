import {
  Component,
  ComponentRef,
  EventEmitter,
  Input,
  OnDestroy, OnInit,
  Output
} from '@angular/core';
import {EMPTY, Observable, Subscription, of} from 'rxjs';
import {TextAreaDynamicComponent} from './dynamic-components/text-area-dynamic.component';
import {UntypedFormBuilder, UntypedFormGroup} from '@angular/forms';
import {HttpParams, HttpResponse} from '@angular/common/http';
import {ApiService} from '../../../services/api/api.service';
import {InputDynamicComponent} from './dynamic-components/input-dynamic.component';
import {DynamicComponent} from '../../../models/dynamic-component.model';
import {FieldsetDynamicComponent} from './dynamic-components/fieldset.component';
import {DynamicComponentManager} from '../../../models/dynamic-component-manager.model';
import {
  IComponentSpec,
  IComponentSpecContainer,
  ISectionComponentSpecs
} from '../../../models/interpreted-contents.model';
import {ICJData, ICJOption, ICJOptionsFromURL, ICJWriteObject} from '../../../models/cj.model';
import { SelectDynamicComponent } from './dynamic-components/select-dynamic.component';
import { CjService } from 'src/app/services/cj/cj.service';
import { IHEAObjectContainer } from 'src/app/models/heaobject-container.model';
import { IDesktopObject } from 'src/app/models/heaobject.model';
import { catchError } from 'rxjs/operators';

@Component ({
  selector: 'interpreted-contents',
  template: `
    <div class="d-flex flex-column container">
      <ng-container *ngFor="let sectionAndComponentSpec of componentSpecsBySection">
        <ng-container *ngIf="sectionAndComponentSpec.section">
          <ndc-dynamic
                       [ndcDynamicComponent]="sectionAndComponentSpec.componentSpecs[0].componentType"
                       [ndcDynamicInputs]="sectionAndComponentSpec.componentSpecs[0].inputs"
                       [ndcDynamicOutputs]="sectionAndComponentSpec.componentSpecs[0].outputs"
                       (ndcDynamicCreated)="sectionAndComponentSpec.componentSpecs[0].componentCreated($event)">
          </ndc-dynamic>
        </ng-container>
        <ng-container *ngIf="!sectionAndComponentSpec.section">
          <ng-container *ngFor="let cspec of sectionAndComponentSpec.componentSpecs">
            <ndc-dynamic
                         [ndcDynamicComponent]="cspec.componentType"
                         [ndcDynamicInputs]="cspec.inputs"
                         [ndcDynamicOutputs]="cspec.outputs"
                         (ndcDynamicCreated)="cspec.componentCreated($event)">
            </ndc-dynamic>
          </ng-container>
        </ng-container>
      </ng-container>
    </div>
  `,
  styles: [`

    .min-width-50 {
      min-width: 50vw;
    }

    .vertical-center { vertical-align: middle; }


    .no-margin  { margin:  0; }
    .no-padding { padding: 0; }

    .can-be-grabbed {
      cursor: move;
      cursor: grab;
      cursor: -moz-grab;
      cursor: -webkit-grab;
    }
    .grabbed {
      cursor: move;
      cursor: grabbing;
      cursor: -moz-grabbing;
      cursor: -webkit-grabbing;
    }

  `]
})
export class InterpretedContentsComponent implements OnDestroy, OnInit {

  @Output() onReady = new EventEmitter<InterpretedContentsComponent>();
  @Output() valid = new EventEmitter<boolean>();
  title = '';
  icon = '';
  componentSpecs: IComponentSpec[] = [];
  componentSpecsBySection: ISectionComponentSpecs[] = [];

  private templateData: ICJData[];
  private formGroup: UntypedFormGroup;
  private componentManager = new DynamicComponentManager();
  private formGroupChangesSub: Subscription;


  constructor(private api: ApiService,
              private formBuilder: UntypedFormBuilder,
              private cjService: CjService) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({});
    this.valid.emit(this.formGroup.valid);
    this.formGroupChangesSub = this.formGroup.statusChanges.subscribe(res => {
      console.debug('emitting status change in interpreted-contents', this.formGroup.status);
      this.valid.emit(this.formGroup.valid);
    });
  }
  ngOnDestroy(): void {
    if (this.formGroupChangesSub) {
      this.formGroupChangesSub.unsubscribe();
    }
  }

  @Input('template') set template(value: ICJData[]) {
    this.templateData = value;
    this.setupComponents();
  }

  @Input('formData') public set formData(formData: Map<string, any>) {
    if (formData) {
      for (const sectionAndComponentSpec of this.componentSpecsBySection) {
        if (!sectionAndComponentSpec.section) {
          for (const cspec of sectionAndComponentSpec.componentSpecs) {
            formData.forEach((value: any, key: string) => {
              if (cspec.inputs.name === key) {
                cspec.inputs.data = value;
              }
            });
            cspec.inputs = cspec.inputs;
          }
        }
      }
    }
  }

  public update(method: string, href: string,
                options?: {params?: HttpParams | {[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>}}): Observable<HttpResponse<null | string> | null> {
    const writeObject: ICJWriteObject = {
      template: {
        data: []
      }
    };
    for (const component of this.componentManager.getAll()) {
      addToParamsToSave(component);
    }

    if (!this.formGroup.valid) {
      console.debug('Form is invalid');
      return of(null);
    }

    console.debug('Saving', href, writeObject, method);

    switch (method) {
      case 'PUT':
        return this.api.updateCollectionPlusJson(href, writeObject, {params: options?.params});
      case 'POST':
        return this.api.createCollectionPlusJson(href, writeObject, {params: options?.params});
      default:
        return of(null);
    }

    function addToParamsToSave(component: DynamicComponent) {
      const d: ICJData = {
        name: component.name,
        value: component.data
      };
      if (method === 'PUT' && d.name === 'modified') {
        d.value = new Date().toISOString();
      }
      if (method === 'POST' && d.name === 'created') {
        d.value = new Date().toISOString();
      }
      if (!!component.index || component.index === 0) {
        d.index = component.index;
      }
      if (!!component.section) {
        d.section = component.section;
      }
      writeObject.template.data.push(d);
    }
  }

  private async setupComponents() {
    this.componentSpecs.length = 0;
    this.componentManager.clear();
    this.formGroup = this.formBuilder.group({});

    for (const icjData of (this.templateData || [])) {
      if (icjData.type === 'textarea') {
        this.componentSpecs.push(this.makeTextAreaComponent(icjData));
      } else if (icjData.type === 'select') {
        this.componentSpecs.push(await this.makeSelectComponent(icjData, !!icjData.cardinality));
      } else {
        this.componentSpecs.push(this.makeInputComponent(icjData));
      }
    }

    this.componentSpecsBySection.length = 0;
    for (const componentSpec of this.componentSpecs) {
      if (!componentSpec.section || this.componentSpecsBySection[this.componentSpecsBySection.length - 1].section !== componentSpec.section) {
        this.componentSpecsBySection.push({section: componentSpec.section, componentSpecs: [componentSpec]});
      } else {
        this.componentSpecsBySection[this.componentSpecsBySection.length - 1].componentSpecs.push(componentSpec);
      }
    }
    for (const componentSpecSection of this.componentSpecsBySection) {
      if (componentSpecSection.section) {
        componentSpecSection.componentSpecs = [this.makeFieldsetComponent(componentSpecSection.componentSpecs)];
      }
    }

    this.onReady.emit(this);
  }

  private async makeSelectComponent(cjData: ICJData, multiple?: boolean): Promise<IComponentSpec> {
    class MoreInputs {
      options: ICJOption[];
      multiple: boolean;
    }
    const moreInputs = new MoreInputs();
    moreInputs.multiple = multiple;
    if (cjData.options) {
      const cjOptionsFromUrl = (cjData.options as ICJOptionsFromURL);
      if (cjOptionsFromUrl.href) {
        if ((cjData.display == undefined || cjData.display) && (cjData.readOnly == undefined || !cjData.readOnly)) {
          const resp = await this.cjService.getResource(cjOptionsFromUrl.href).toPromise();
          moreInputs.options = resp.map((value: IHEAObjectContainer<IDesktopObject>,
                                        index: number,
                                        array: IHEAObjectContainer<IDesktopObject>[]) => {
            return {'text': value.heaObject[cjOptionsFromUrl.text],
                    'value': value.heaObject[cjOptionsFromUrl.value]}
          });
        } else {
          const options: ICJOption[] = [];
          if (cjData.value) {
            const ids = Array.isArray(cjData.value) ? cjData.value : [cjData.value];
            for (const id_ of ids) {
              const theOptions = await this.cjService.getResource(cjOptionsFromUrl.href.split(/[?#]/)[0] + '/' + id_, {handleErrorsManually: true})
              .pipe(catchError(err => {
                if (err.status >= 500) {
                  console.error('Error getting recent project:', err);
                }
                return EMPTY;
              })).toPromise();
              if (theOptions === undefined) {
                options.push({'text': id_, 'value': id_});
              } else {
                for (const obj of theOptions) {
                  options.push({'text': obj.heaObject[cjOptionsFromUrl.text],
                                'value': obj.heaObject[cjOptionsFromUrl.value]})
                }
              }
            }
          }
          moreInputs.options = options;
        }
      } else if ((cjData.options as ICJOption[]).length) {
        moreInputs.options = (cjData.options as ICJOption[]);
      }
    }
    return this.componentManager.build(cjData, SelectDynamicComponent, moreInputs, this.formGroup);
  }

  private makeTextAreaComponent(component: ICJData): IComponentSpec {
    return this.componentManager.build(component, TextAreaDynamicComponent, null, this.formGroup);
  }

  private makeInputComponent(cjData: ICJData): IComponentSpec {
    class MoreInputs {
      type: string;
      objectUrlTargetTypesInclude: string[] | null;
    }
    const moreInputs = new MoreInputs();
    moreInputs.objectUrlTargetTypesInclude = cjData.objectUrlTargetTypesInclude;
    return this.componentManager.build(cjData, InputDynamicComponent, moreInputs, this.formGroup);
  }

  private makeFieldsetComponent(componentSpecs: IComponentSpec[]): IComponentSpec {
    const temp: IComponentSpecContainer = {
      disabled: false,
      componentType: FieldsetDynamicComponent,
      section: componentSpecs[0].section,
      sectionPrompt: componentSpecs ? componentSpecs[0].sectionPrompt : '',
      inputs: {
        containerFormGroup: this.formGroup,
        subComponentSpecs: componentSpecs,
        paramName: DynamicComponentManager.componentSpecMapKey(componentSpecs ? componentSpecs[0].section : ''),
        dynamicComponentManager: this.componentManager,
        readOnly: componentSpecs.every(res => res.inputs.readOnly)
      },
      componentCreated: (event: ComponentRef<DynamicComponent>) => {
      },
      outputs: {}
    };
    return temp;
  }
}



