import {Component, ElementRef, HostListener, Inject, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {LegacyDialogPosition as DialogPosition, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef} from '@angular/material/legacy-dialog';
import {InterpretedContentsComponent} from './interpreted-contents.component';
import { Subscription } from 'rxjs';
import { HEAObjectContainer, IHEAObjectContainer } from 'src/app/models/heaobject-container.model';
import { DesktopObject, IDesktopObject } from 'src/app/models/heaobject.model';
import { IconService } from 'src/app/services/icon/icon.service';
import { IconOption } from 'src/app/models/icon.model';
import { take } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { ApiService } from 'src/app/services/api/api.service';
import { ICJData } from 'src/app/models/cj.model';


@Component({
  selector: 'dialog-wrap-interpreted-contents',
  template: `
    <div #topmostLeftmost class="cb-card cb-card-no-margin d-flex flex-column">
      <loading-overlay [busy]="dataLoading" [icon]="busyIcon"></loading-overlay>
      <div class="cb-card-header d-flex flex-row" (mousedown)="onMouseDownHeader($event)">
        <div class="" [innerHTML]="icon"></div>
        <div class="flex-grow-1 padded-left-right">
        {{ title ? title : defaultTitle }}
        </div>
        <div class="me-1" (click)="onClose()" title="Close">
          <i class="bi bi-x-lg" style="font-size: 1.1rem">
          </i>
        </div>
      </div>
      <div class="d-flex flex-column cb-card-body inner-container-scroll overflow-auto">
        <div class="flex-grow-1">
          <interpreted-contents [template]="template" (onReady)="onReady($event)" [formData]="formData" (valid)="onValidityChange($event)">
          </interpreted-contents>
        </div>
      </div>
      <div class="d-flex flex-row padded-right">
        <div class="flex-grow-1">
        </div>
        <div class="padded-top-left-bottom">
          <button mat-raised-button class="grey-1g" (click)="closeDialog()">
            {{actionButtonName ? 'CANCEL' : 'CLOSE'}}
          </button>
        </div>
        <div *ngIf="actionButtonName" class="padded-top-left-bottom">
          <button mat-raised-button class="secondary-1g" (click)="update($event)">
            {{actionButtonName}}
          </button>
        </div>
      </div>
    </div>
  `,
  styles: [`
  .cb-card {
    margin: 0.5em;
    background-color: #EAF9FF;
    box-shadow: 0 4px 8px #92969e;
    border: 0.1px solid #707070;
    border-radius: 10px;
    color: #5a6366;
    opacity: 1;
    height: 100%;
    max-height: 75%;
  }

  .cb-card-body {
    padding: 0 0.3em 0 0.3em;
    min-height: 10em;
  }

  .padded-top-left-bottom {
    padding: 0.3em 0 0.3em 0.3em;
  }
  `]
})
export class DialogWrapInterpretedContentsComponent implements OnInit, OnDestroy {

  defaultTitle = 'Unknown Object';
  title = '';
  busyIcon = 'fas fa-sync fa-spin';
  dataLoading = [true];
  icon: string;
  actionButtonName: string;
  private _data: IHEAObjectContainer<IDesktopObject>;
  private _dataPath: string[];
  private _template: ICJData[];
  private _method: 'POST' | 'PUT' | 'GET';

  @Input() set data(value: IHEAObjectContainer<IDesktopObject>) {
    this._data = value;
    if (this.method) {
      this._template = this._data.getTemplate(this.method);
    }
  }

  get data(): IHEAObjectContainer<IDesktopObject> {
    return this._data;
  }

  @Input() set dataPath(path: string[]) {
    this._dataPath = [...path];
  }

  get dataPath(): string[] {
    if (this._dataPath) {
      return [...this._dataPath];
    } else {
      return null;
    }
  }

  get template(): ICJData[] {
    return this._template;
  }
  @Input() set method(value: 'POST' | 'PUT' | 'GET') {
    this._method = value;
    if (this._data) {
      this._template = this._data.getTemplate(this.method);
    }
  }
  get method(): 'POST' | 'PUT' | 'GET' {
    return this._method;
  }
  @Input() href: string;

  @ViewChild('topmostLeftmost', {static: false}) private topmostLeftmost: ElementRef;
  private interpretedContentsRef: InterpretedContentsComponent;
  private updateSub: Subscription;
  private initialOffsetX: any;
  private initialOffsetY: any;
  private movingDialog = false;
  private _valid: boolean;
  private _formData: Map<string, any>;


  constructor(@Inject(MAT_DIALOG_DATA) private inData,
              private dialogRef: MatDialogRef<DialogWrapInterpretedContentsComponent>,
              private iconService: IconService,
              private apiService: ApiService
              ) {
    this.data = inData.data;
    this.method = inData.method;
    this.href = inData.href;
  }

  @Input('formData') set formData(formData: Map<string, any>) {
    this._formData = formData;
  }

  get formData(): Map<string, any> {
    return this._formData;
  }

  get valid(): boolean {
    return this._valid;
  }

  ngOnInit(): void {
    if (this.data) {
      for (const icjData of this.data.getTemplate(this.method)) {
        if (icjData.name === 'display_name') {
          if (Array.isArray(icjData.value)) {
            throw Error('icjData.value must be a string but was an array');
          }
          const templatePrompt = this.data.templatePrompt;
          this.title = templatePrompt ? `${templatePrompt} ${icjData.value}` : icjData.value;
          break;
        }
      }
    }
    this.configureButtons(this.data.href)
    this.icon = this.iconService.getIconForHEAObject(this.data.heaObject, [IconOption.LARGE]);
  }

  ngOnDestroy(): void {
    if (this.updateSub) {
      this.updateSub.unsubscribe();
    }
  }

  public onClose() {
    this.closeDialog();
  }

  public closeDialog(dialogResult?: any) {
    console.debug('dialog result', dialogResult);
    this.dialogRef.close(dialogResult);
  }

  public update(event: MouseEvent) {
    if (!this.valid) {
      return;
    }
    this.dataLoading = [true];
    this.updateSub = this.interpretedContentsRef.update(this.method, this.href).subscribe(response => {
      }, err => {
        this.dataLoading = [false];
        this.closeDialog();
      }, () => {
        this.dataLoading = [false];
        this.closeDialog(true);
      }
    );
  }


  public onReady(event: InterpretedContentsComponent) {
    console.log('onReady');
    this.interpretedContentsRef = event;
    this.dataLoading = [false];
  }

  public onMouseDownHeader(event: MouseEvent) {
    if (!event) {
      return;
    }
    //hack until we upgrade to angular 14 and can directly get the cdk-overlay-pane and query its position.
    let cdkOverlayPane = this.topmostLeftmost.nativeElement.parentNode;
    while (true) {
      if (cdkOverlayPane.className.indexOf('cdk-overlay-pane') >= 0) {
        break;
      } else {
        cdkOverlayPane = cdkOverlayPane.parentNode;
      }
    }
    const clientRect = cdkOverlayPane.getBoundingClientRect();
    this.initialOffsetX = event.clientX - clientRect.left;
    this.initialOffsetY = event.clientY - clientRect.top;

    this.movingDialog = true;
  }

  @HostListener('window:mousemove', ['$event'])
  public onMouseMove(event: MouseEvent) {
    if (!event) {
      return;
    }

    if (this.movingDialog) {
      const positionX = event.clientX - this.initialOffsetX;
      const positionY = event.clientY - this.initialOffsetY;

      const newDialogPosition: DialogPosition = {
        left:   '' + positionX + 'px',
        top:    '' + positionY + 'px',
      };

      this.dialogRef.updatePosition(newDialogPosition);
    }
  }

  @HostListener('window:mouseup', ['$event'])
  public onMouseUp() {
    this.movingDialog = false;
  }

  onValidityChange(event: boolean) {
    setTimeout(() => {this._valid = event}, 0);
  }

  private configureButtons(href: string) {
    if (!this.data.isFormActionForbidden(this.method)) {
      const actionButtonCreator = () => {
        switch (this.method) {
          case 'POST':
            return 'CREATE';
          case 'PUT':
            return 'UPDATE';
          default:
            return 'SAVE';
        }
      }
      this.apiService.isMethodAllowed(href, this.method).pipe(take(1)).subscribe(resp => {
        if (resp) {
          this.actionButtonName = actionButtonCreator();
        }
      }, (error: HttpErrorResponse) => {
        if (error.status === 405) {
          this.actionButtonName = actionButtonCreator();
        }
      });
    }
  }
}
