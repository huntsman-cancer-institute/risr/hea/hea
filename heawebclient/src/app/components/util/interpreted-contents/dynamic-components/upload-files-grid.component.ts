import {Component, EventEmitter, Input, Output, OnInit, ViewChild, ElementRef} from "@angular/core";
import {
  ColDef,
  GetDataPath,
  GetRowNodeIdFunc,
  GridApi,
  ICellRendererComp,
  ICellRendererParams
} from "ag-grid-community";
import {UtilService} from "../../../../services/util.service";


@Component({
  selector: "upload-files-grid",
  template: `
    <div class="h-100 w-100 border-2 background-gradient">
      <div class="h-100 w-100 d-flex flex-column min-height" >
        <div class="d-flex flex-row vertical-center">
          <div class="d-flex flex-column">
            <div class="flex-grow-1">
            </div>
            <div class="padded-left">
              {{ title }}
            </div>
            <div class="flex-grow-1">
            </div>
          </div>
          <div class="flex-grow-1">
          </div>
          <div class="padded-right">
            <input type="file" class="file-input" webkitdirectory multiple (change)="onFileSelected($event)" #folderUpload/>
            <button mat-button class="padded-left-right no-min-width" (click)="folderUpload.click()">
              <i class="fas fa-folder-plus fa-lg green">
              </i>
            </button>
          </div>
          <div class="padded-left-right">
            <input type="file" class="file-input" multiple (change)="onFileSelected($event)" #fileUpload/>
            <button mat-button class="padded-left-right no-min-width" (click)="fileUpload.click()">
              <i class="fas fa-plus fa-lg green">
              </i>
            </button>
          </div>
          <div>
            <button mat-button class="padded-left-right no-min-width" [disabled]="!isRowSelected()" (click)="onClickDelete()">
              <div *ngIf="isRowSelected()">
                <i class="fas fa-minus fa-lg red">
                </i>
              </div>
              <div *ngIf="!isRowSelected()">
                <i class="fas fa-minus fa-lg light-grey">
                </i>
              </div>
            </button>
          </div>
        </div>
        <div class="flex-grow-1 grid-container">
          <ag-grid-angular class="w-100 h-100 ag-theme-balham"
                           (gridReady)="onGridReady($event)">
          </ag-grid-angular>
        </div>
        <div class="text-small">
          <div class="d-flex flex-row">
            <div class="padded-left-right">
              NUMBER OF OBJECT(S):
            </div>
            <div class="red">
              {{ countFiles }}
            </div>
          </div>
          <div class="d-flex flex-row">
            <div class="padded-left-right">
              SIZE:
            </div>
            <div class="red">
              {{ totalSize }}
            </div>
          </div>
        </div>
      </div>
    </div>

  `,
  styles: [`

    .grid-container > .ag-theme-balham > .ag-root-wrapper > .ag-root-wrapper-body > .ag-root > .ag-body-viewport > .ag-center-cols-clipper > .ag-center-cols-viewport {
      overflow-x: hidden !important;
    }

    .file-input {
      display: none;
    }

    .min-height {
      min-height: 14em;
    }

    .vertical-center {
      vertical-align: middle;
    }

    .flex-column {
      display: flex;
      flex-direction: column;
    }

    .border-2 {
      border: solid #d7dcde 1px;
      border-radius: 6px;
    }

    .background-gradient {
      background-image: linear-gradient(#EAF9FF, #FFFFFF);
    }

    .text-small {
      font-size: small;
    }

    .green {
      color: green;
    }

    .red {
      color: red;
    }

    .light-grey {
      color: lightgrey;
    }

  `]
})
export class UploadFilesGridComponent implements OnInit {

  @ViewChild('fileUpload')   fileUpload: ElementRef;
  @ViewChild('folderUpload') folderUpload: ElementRef;

  @Input() title: string = "";
  @Input("rowData") public set rowData(value: any[]) {
    if (value) {
      this._rowData = value;
    } else {
      this._rowData = [];
    }

    if (this.gridApi) {

      this.countFiles = this._rowData.length;

      let sum: number = 0;

      for (let datum of this._rowData) {
        if (datum && datum.size) {
          sum += +datum.size;
        }
      }

      this.totalSize = this.utilService.formatSize('' + sum, null, true);

      this.gridApi.setGridOption("rowData", this._rowData);
      this.gridApi.sizeColumnsToFit();
    }
  };
  @Input("columnDefs") public set columnDefs(value: any[]) {
    if (value) {
      this._columnDefs = value;
    } else {
      this._columnDefs = [];
    }

    if (this.gridApi) {
      this.gridApi.setGridOption("columnDefs", this._columnDefs);
    }
  };

  @Output() onRowSelected = new EventEmitter<any>();


  // public defaultColDef: ColDef = {
  //   flex: 1,
  //   filter: true,
  //   sortable: true,
  //   resizable: true,
  // };
  public autoGroupColumnDef: ColDef = {
    width: 200,
    cellRendererParams: {
      checkbox: false,
      suppressCount: true,
      innerRenderer: getFileCellRenderer(),
    },
  };

  public getDataPath: GetDataPath = (data: any) => {
    return data.filePath;
  };

  public countFiles: number;
  public totalSize: string;
  public context: any;

  private nextIndex: number = 1;  // This is used to pick out files in the grid.  It needs to be > 0 to be truthy.

  private gridApi: GridApi;

  private _rowData: any[] = [];

  private _columnDefs: ColDef[] = [
    {
      headerName: 'TYPE',
      field: 'type',
      width: 100,
      valueFormatter: (params) => {
        return params.value && params.value !== '' ? params.value : 'folders';
      },
    },
    {
      headerName: 'SIZE',
      field: 'size',
      width: 100,
      aggFunc: 'sum',
      valueFormatter: (params) => {
        return params.value ? this.utilService.formatSize(params.value, null, true) : '--';
      },
    },
  ];

  public constructor(private utilService: UtilService) {
    this.context = this;

    this.columnDefs = this._columnDefs;
  }

  public ngOnInit(): void { }


  public onClickDelete(): void {
    if (!this.gridApi) {
      return;
    }

    let selectedRows: any[] = this.gridApi.getSelectedNodes();
    let anyChangeMade: boolean = false;

    for (let selectedRow of selectedRows) {
      if (!(selectedRow && selectedRow.data && selectedRow.data.customIndex)) {
        // Then this is a selected folder - add all of its contents to the list for deletion!

        if (selectedRow.allLeafChildren) {
          for (let leaf of selectedRow.allLeafChildren) {
            selectedRows.push(leaf);
          }
        }
      }
    }

    for (let selectedRow of selectedRows) {
      let temp: number = this._rowData.findIndex((row: any) => {
        if (row && row.customIndex && selectedRow && selectedRow.data && selectedRow.data.customIndex) {
          return row.customIndex === selectedRow.data.customIndex;
        }

        return false;
      });

      if (temp >= 0) {
        this._rowData.splice(temp, 1);
        anyChangeMade = true;
      }
    }

    if (anyChangeMade) {
      this.rowData = this._rowData;
    }
  }


  public isRowSelected(): boolean {
    if (!this.gridApi) {
      return false;
    }

    let selectedRows: any[] = this.gridApi.getSelectedNodes();

    if (!selectedRows) {
      return false;
    }

    return !(Array.isArray(selectedRows) && selectedRows.length === 0);
  }


  public onSelectedRow(event: any): void {
    this.onRowSelected.emit(event);
  }

  public onGridReady(params: any): void {
    this.gridApi = params.api;
    this.gridApi.setGridOption("columnDefs", this._columnDefs);
    this.gridApi.setGridOption("suppressNoRowsOverlay", true);
    this.gridApi.setGridOption("groupDefaultExpanded", -1);
    this.gridApi.setGridOption("getDataPath", this.getDataPath);
    this.gridApi.setGridOption("rowSelection", "multiple");
    this.gridApi.setGridOption("animateRows", true);
    this.gridApi.setGridOption("treeData", true);
    this.gridApi.setGridOption("autoGroupColumnDef", this.autoGroupColumnDef);
    this.rowData = this._rowData;
  }

  public onFileSelected(event: any) {
    if (event && event.target && event.target.files && event.target.files.length > 0) {
      if (!this._rowData) {
        this._rowData = [];
      }

      for (let file of event.target.files) {

        let tokens: string[] = [];

        if (file.webkitRelativePath) {
          tokens = file.webkitRelativePath.split(new RegExp(/\/|\\/));
        } else {
          tokens.push(file.name);
        }

        if (tokens.length > 0 && tokens[tokens.length - 1] !== 'desktop.ini') {
          // "desktop.ini" is a special windows hidden file that would not show up in the file pickers and
          // is not relevant for our purposes in that context.

          this._rowData.push({
            filePath: tokens,
            size: file.size,
            type: file.type,
            file: file,
            customIndex: this.nextIndex++
          });
        }
      }

      this.rowData = this._rowData;

      this.fileUpload.nativeElement.value = '';
      this.folderUpload.nativeElement.value = '';
    }
  }
}

function getFileCellRenderer() {
  class FileCellRenderer implements ICellRendererComp {
    eGui: any;
    init(params: ICellRendererParams) {
      var tempDiv = document.createElement('div');
      var value = params.value;
      var icon = getFileIcon(params);

      if (!icon) {
        tempDiv.innerHTML = value;
      } else {
        tempDiv.innerHTML = '<span ><i class="' + icon + '"></i>' + '<span class="padded-left">' + value + '</span></span>';
      }
      this.eGui = tempDiv.firstChild;
    }
    getGui() {
      return this.eGui;
    }
    refresh() {
      return false;
    }
  }
  return FileCellRenderer;
}

function getFileIcon(params: ICellRendererParams): string {
  if (params && params.node) {
    if ( params.node.allChildrenCount
      && params.node.allChildrenCount > 0
      && params.node.allLeafChildren
      && Array.isArray(params.node.allLeafChildren)
      && params.node.allLeafChildren.length > 0) {

      return 'far fa-folder';
    } else if (params.value) {
      let tokens: string[] = ('' + params.value).split('.');

      if (tokens && tokens.length > 0) {
        switch(tokens[tokens.length - 1]) {
          case '.mp3'  :
          case '.wav'  : return 'far fa-file-audio';
          case '.xls'  :
          case '.xlsx' : return 'far fa-file-excel';
          case '.pdf'  : return 'far fa-file-pdf';
          default : return 'far fa-file';
        }
      }
    }
  }

  return '';
}
