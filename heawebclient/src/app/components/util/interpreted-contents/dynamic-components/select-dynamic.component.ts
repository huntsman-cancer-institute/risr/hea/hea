import {
  Component,
  ElementRef,
  EventEmitter, forwardRef,
  Input, OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {NG_VALUE_ACCESSOR, Validators} from "@angular/forms";
import { ICJOption } from "src/app/models/cj.model";
import { DynamicComponent } from "src/app/models/dynamic-component.model";
import {MatFormFieldAppearance} from "@angular/material/form-field";


@Component({
  selector: "select-dynamic",
  template: `
    <div *ngIf="display" class="d-flex flex-row form-row" [formGroup]="containerFormGroup">
      <label #titleBox *ngIf="title" [for]="paramName" >
        {{ required && !readOnly ? '*' + title : title }}:
      </label>
        <cb-select
          [formControlName]="paramName"
          [options]="options"
          customFieldClasses="hci-font"
          [appearance]="appearance"
          [required]="required"
          [valueField]="valueField"
          [multiple]="multiple"
          [displayField]="displayField"
          (selectionChanged)="onSelectionsChanged($event)">
        </cb-select>
    </div>
  `,
  styles: [`
    textarea:read-only {
      border: none;
      box-sizing: content-box;
      color: var(--black-darkest);
      background: transparent;
      resize: none;
    }
    .hci-combobox-container{
      padding: 0;
    }

    select {
      height: auto;
    }
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectDynamicComponent),
      multi: true
    }
  ]
})
export class SelectDynamicComponent extends DynamicComponent implements OnInit {

  @Input() options: ICJOption[] = [];
  @Input() valueField: string = "value";
  @Input() displayField: string = "text";
  @Input() multiple: boolean = false;
  @Output() optionSelected = new EventEmitter<string | string[]>();
  public showLabel: boolean = false;
  public tooltip: string;
  public appearance: MatFormFieldAppearance = 'outline';
  // public constructor() { }

  public ngOnInit(): void {
    super.ngOnInit();
    console.debug('is required?', this.required);
    if (this.required) {
      this.containerFormGroup.get(this.paramName).validator = Validators.required;
    }
    if (this.readOnly) {
      this.containerFormGroup.get(this.paramName).disable();
    }
    this.tooltip = this.multiple ? `${this.modifierKeyCapitalized}-click to select and deselect more than one option` : 'Select an option';
  }


  public setValue(value: string | string[]) {
    console.log('setting value', value);
    let val = null;
    if (value !== undefined && value !== null) {
      val = Array.isArray(value) ? value : [value] ;
    }else{
      val = [];
    }

    super.setValue(val);
  }

  onSelectionsChanged(event: string[]): void {
    console.debug('selections changed', event);
    const formControl = this.containerFormGroup.get(this.paramName);
    const newVals = this.multiple ? formControl.value : formControl.value[0];
    this.data = newVals !== '' ? newVals : null;
    this.optionSelected.emit(newVals);
  }


  /**
   * Returns the number of selected rows.
   *
   * @returns the number of selected rows.
   */
  size(): number {
    let size: number;
    if (this.readOnly) {
      size = (this.containerFormGroup.get(this.paramName)?.value?.split('\n') as string[])?.length
    } else {
      size = (this.containerFormGroup.get(this.paramName)?.value as string[])?.length;
    }
    return size ? size : 0;
  }

  private getTextForVal(val: string | string[] | null): string | null {
    const text: string[] = [];
    if (Array.isArray(val)) {
      for (const val_ of val) {
        for (const option of this.options) {
          if (option.value === val_) {
            text.push(option.text);
          }
        }
      }
    } else {
      for (const option of this.options) {
        if (option.value === val) {
          text.push(option.text);
        }
      }
    }

    if (text.length > 0) {
      return text.join('\n');
    } else if (Array.isArray(val)) {
      return val.join('\n');
    } else {
      val;
    }
  }

  private getOptVal(opt: any): string | string[] | null {
    let result: any;
    if (opt && typeof opt === 'object') {
      if (this.valueField) {
        result = this.valueField in opt ? opt[this.valueField] : null;
      } else {
        result = 'id' in opt ? opt.id.toString() : opt;
      }
    } else {
      result = opt;
    }
    if (Array.isArray(result)) {
      return result.map(element => {return this.getOptValSupport(element)});
    } else {
      return [this.getOptValSupport(result)];
    }
  }

  private getOptValSupport(opt: any): string {
    if (opt === null || opt === undefined) {
      return null;
    } else {
      return opt.toString();
  }
  }

}
