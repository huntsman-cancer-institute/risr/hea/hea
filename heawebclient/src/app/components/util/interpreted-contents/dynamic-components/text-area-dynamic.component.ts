import {Component} from '@angular/core';
import {DynamicComponent} from '../../../../models/dynamic-component.model';
import { Validators } from '@angular/forms';


@Component({
  selector: 'text-area-dynamic',
  template: `
    <div *ngIf="display" class="d-flex flex-row form-row" [formGroup]="containerFormGroup">
      <label #titleBox *ngIf="title">
        {{ required && !readOnly ? '*' + title : title }}:
      </label>
        <textarea
          [placeholder]="title"
          [formControlName]="paramName"
          [readonly]="readOnly"
          [required]="required"
          (change)="onChange($event)"
          cdkTextareaAutosize
          cdkAutosizeMinRows="3"
          cdkAutosizeMaxRows="15">
        </textarea>
          <mat-error *ngIf="hasError()">Required - cannot be empty</mat-error>
    </div>
  `,
  styles: [`
    textarea:read-only {
      border: none;
      box-sizing: content-box;
      color: var(--black-darkest);
      background: transparent;
    }
  `]
})
export class TextAreaDynamicComponent extends DynamicComponent {
  ngOnInit(): void {
    super.ngOnInit();
    if (this.required) {
      this.containerFormGroup.get(this.paramName).validator = Validators.required;
    }
  }

  hasError(): boolean {
    if (this.containerFormGroup) {
      return this.containerFormGroup.get(this.paramName).hasError('required');
    } else {
      return false;
    }
  }

  onChange($event) {
    this.data = this.containerFormGroup.get(this.paramName).value;
  }
}
