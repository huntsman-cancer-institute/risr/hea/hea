import {OnInit, Component, ElementRef, Input, ViewChild, OnDestroy} from '@angular/core';
import {UtilService} from '../../../../services/util.service';
import {DynamicComponent} from '../../../../models/dynamic-component.model';
import { CjService } from 'src/app/services/cj/cj.service';
import { map, mergeMap } from 'rxjs/operators';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { AbstractControl, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';


@Component({
  selector: 'input-dynamic',
  template: `
    <div *ngIf="display" class="d-flex flex-row form-row" [formGroup]="containerFormGroup">
      <label #titleBox *ngIf="title"  [for]="paramName">
        {{ required && !readOnly ? '*' + title : title }}:
      </label>

        <input  [id]="paramName" [name]="paramName"
          [placeholder]="!readOnly ? title : ''"
          [formControlName]="paramName"
          [type]="(readOnly && type === 'datetime-local' && !data) || (type === 'password' && passwordFieldHidden === false) ? 'text' : type"
          (change)="onChange($event)"
          [readonly]="readOnly"
          [required]="required"
          [step]="step" >
        <span *ngIf="type === 'password' && passwordFieldHidden" (click)="makePasswordVisible()"><i class="fa fa-eye" id="makePasswordVisible"></i></span>
        <span *ngIf="type === 'password' && !passwordFieldHidden" (click)="makePasswordInvisible()"><i class="fa fa-eye-slash" id="makePasswordInvisible"></i></span>
        <mat-error *ngIf="errorRequired">Required - cannot be empty</mat-error>
        <mat-error *ngIf="errorForbiddenType">Wrong type</mat-error>
    </div>
  `,
  styles: [`
    input:read-only {
      border: none;
      background-color: transparent;
    }

    #makePasswordVisible {
      margin-left: -30px;
      cursor: pointer;
    }
    #makePasswordInvisible {
      margin-left: -30px;
      cursor: pointer;
    }
  `]
})

export class InputDynamicComponent extends DynamicComponent implements OnDestroy {

  @ViewChild('titleBox') titleBox: ElementRef;
  errorRequired: boolean;
  errorForbiddenType: boolean;
  passwordFieldHidden: boolean;
  objectUrlType: string | null;
  @Input() objectUrlTargetTypesInclude: string[] | null;
  step = 1;

  private _data: any;
  private dataChanges: BehaviorSubject<string>;
  private dataChangesObx: Observable<string>;
  private formControlValueChangesSubscription: Subscription;
  private dataChangesSubscription: Subscription;


  public constructor(private utilService: UtilService, private cjService: CjService) {
    super();
    this.dataChanges = new BehaviorSubject<string>(null);
    this.dataChangesObx = this.dataChanges.asObservable();
    this.passwordFieldHidden = true;
  }

  public ngOnInit(): void {
    super.ngOnInit();
    const formControl = this.containerFormGroup.get(this.paramName);
    if (this.type === 'datetime-local') {
      formControl.setValue(this._data ? this.utilService.toDatetimeLocal(new Date(this._data)) : null);
      if (!this.readOnly) {
        this.formControlValueChangesSubscription = formControl.valueChanges.subscribe(resp => {
          this.errorRequired = formControl.hasError('required');
          this._data = formControl.value;
        });
      }
    } else if (this.type === 'object-url') {
      formControl.setValidators([objectUrlTargetTypesIncludeValidator(this)]);
      formControl.updateValueAndValidity();
      this.readOnly = true;
      this.dataChangesSubscription = this.dataChangesObx.pipe(mergeMap(res => {
        if (this._data) {
          return this.cjService.getResource(this._data);
        } else {
          return of(null);
        }

      })).subscribe(res => {
        if (res) {
          this.objectUrlType = res[0].heaObject.type;
          formControl.setValue(res[0].heaObject.display_name);
          formControl.markAsDirty();
          this.errorForbiddenType = formControl.hasError('forbiddenType');
          this.errorRequired = formControl.hasError('required');
        }
      });
    } else {
      formControl.setValue(this._data);
      if (!this.readOnly) {
        this.formControlValueChangesSubscription = formControl.valueChanges.subscribe(resp => {
          this.errorRequired = formControl.hasError('required');
          this._data = formControl.value;
        });
      }
    }
  }

  ngOnDestroy() {
    this.dataChanges.unsubscribe();
    if (this.dataChangesSubscription) {
      this.dataChangesSubscription.unsubscribe();
    }
    if (this.formControlValueChangesSubscription) {
      this.formControlValueChangesSubscription.unsubscribe();
    }
  }

  @Input()
  public set data(data: any) {
    this._data = data;
    if (this.type === 'object-url') {
      this.dataChanges.next(data);
    }
  }

  public get data(): any {
    return this._data;
  }

  public onChange(event: Event) {
    this._data = this.containerFormGroup.get(this.paramName).value;
  }

  public makePasswordVisible() {
    this.passwordFieldHidden = false;
  }

  public makePasswordInvisible() {
    this.passwordFieldHidden = true;
  }

}

function objectUrlTargetTypesIncludeValidator(controller: InputDynamicComponent): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!controller.objectUrlTargetTypesInclude || controller.objectUrlTargetTypesInclude?.indexOf(controller.objectUrlType) >= 0) {
      return null;
    } else {
      return {
        forbiddenType: {value: controller.objectUrlType}
      }
    }
  }
}
