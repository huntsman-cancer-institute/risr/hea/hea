import {Component, Input, OnInit} from "@angular/core";


@Component({
  selector: "text-display",
  template: `
    <div class="w-100">
      <div *ngIf="!isList">
        <div *ngFor="let line of description" class="full-width"
             [innerHTML]="line" style="white-space: pre-line">
        </div>
      </div>
      <div *ngIf="isList">
        <ul>
          <li *ngFor="let line of description" class="full-width"
              [innerHTML]="line" style="white-space: pre-line">
          </li>
        </ul>
      </div>
    </div>
  `,
  styles: []
})
export class TextDisplayComponent implements OnInit {

  @Input() public description: string|string[] = null;
  @Input() public isList: boolean = false;

  public constructor() { }
  ngOnInit(): void {
    this.description = Array.isArray(this.description) ? this.description : [this.description];
    for (let i = 0; i < this.description.length; i++) {
      this.description[i] = this.description[i].replace(/\n/g, "<br>");
  }
    }
}
