import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';
import {GridApi, GridReadyEvent, ColDef, GridOptions, IRowNode} from 'ag-grid-community';
import {DynamicComponent} from '../../../../models/dynamic-component.model';


@Component({
  selector: 'ag-grid-dynamic',
  template: `
    <div class="h-100 w-100 mh-100 d-flex flex-column" >
      <div class="d-flex flex-row vertical-center">
        <div class="d-flex flex-column">
          <div class="flex-grow-1">
          </div>
          <div>
            {{ title }}
          </div>
          <div class="flex-grow-1">
          </div>
        </div>
        <div class="flex-grow-1">
        </div>
        <div *ngIf="showAddRemove">
          <button *ngIf="mode === 'normal'" mat-button class="padded-left-right no-min-width" (click)="onClickAdd()">
            <i class="fas fa-plus fa-lg ">
            </i>
          </button>
          <button *ngIf="mode === 'file'" mat-button class="padded-left-right no-min-width" (click)="onClickAdd()">
            ADD
          </button>
        </div>
        <div *ngIf="showAddRemove">
          <button *ngIf="mode === 'normal'" mat-button class="padded-left-right no-min-width" [disabled]="!isRowSelected()" (click)="onClickDelete()">
            <i class="fas fa-minus fa-lg ">
            </i>
          </button>
          <button *ngIf="mode === 'file'" mat-button class="padded-left-right no-min-width" (click)="onClickAdd()">
            REMOVE
          </button>
        </div>
        <div *ngIf="infoTooltip" [matTooltip]="infoTooltip">
          <i  class="fa fa-info-circle" aria-hidden="true"></i>
        </div>
      </div>
      <div class="flex-grow-1">
        <ag-grid-angular class="w-100 h-100 min-height ag-theme-balham"
                         (gridReady)="onGridReady($event)"
                         (rowSelected)="this.onSelectedRow($event)">
        </ag-grid-angular>
      </div>
    </div>
  `,
  styles: [`

    .no-min-width {
      min-width: 0;
    }

    .min-height {
      min-height: 14em;
    }

    .vertical-center {
      vertical-align: middle;
    }


    .flex-column {
      display: flex;
      flex-direction: column;
    }

  `]
})
export class AgGridDynamicComponent extends DynamicComponent implements OnInit {
  private _paramName2: string;
  private _readOnly2: boolean;
  private gridApi: GridApi;
  private _data: any[] = [];
  private _columnDefs: ColDef[] = [];
  private _rowSelection:  'single' | 'multiple' = 'single';
  @Input() showAddRemove = true;
  @Input() mode = 'normal';
  @Input() infoTooltip: string;
  public context: any;

  public constructor() {
    super();
    this.context = this;
  }

  /**
   * Overrides the superclass' paramName property to pass the component's unique name to the grid.
   *
   * @param paramName the component's unique name string.
   */
  @Input()
  public set paramName(paramName: string) {
    this._paramName2 = paramName;
    this.updateColumnDefs();
  }

  /**
   * Overrides the default value of the grid's rowSelection property.
   *
   * @param rowSelection the selection type for the grid to use.
   */
  @Input()
  public set rowSelection(rowSelection: 'single' | 'multiple') {
    this._rowSelection = rowSelection;
  }

  public get rowSelection():  'single' | 'multiple' {
    return this._rowSelection;
  }

  /**
   * Overrides the superclass' paramName property to retrieve the name stored by the setter.
   *
   * @return the component's unique name string.
   */
  public get paramName() {
    return this._paramName2;
  }

  /**
   * Overrides the superclass' readOnly property to configure the grid as read-only or not.
   *
   * @param readOnly true if the component should be read-only, false otherwise.
   */
  @Input()
  public set readOnly(readOnly: boolean) {
    this._readOnly2 = readOnly;
    this.updateColumnDefs();
  }

  /**
   * Gets whether the component is read-only.
   *
   * @return true if the component is read-only, false otherwise.
   */
  public get readOnly() {
    return this._readOnly2;
  }

  /**
   * Sets the data to display in the grid. It expects an array of values or HEAObject ids. If you override this setter
   * in a subclass without calling super, you'll also need to override the getter as well as addEmptyRow and
   * removeSelectedRow.
   *
   * @param data the array of values or HEAObject ids.
   */
  @Input()
  public set data(data: any) {
    console.debug('setting data', data);
    this._data.length = 0;
    this._data.push(...(data || []));
    this.updateColumnDefs();
    if (this.gridApi) {
      this.setGridRowData();
    }
  }

  /**
   * Gets the data in the grid. The data is in the format of an array of values or HEAObject ids. If you override this
   * getter in a subclass without calling super, you'll also need to override the setter as well as addEmptyRow and
   * removeSelectedRow.
   *
   * @return an array of values or HEAObject ids.
   */
  public get data(): any {
    if (!this.gridApi) {
      return [...this._data];
    } else {
      this._data.length = 0;
      this.gridApi.forEachNode((rowNode, index) => {
        this._data.push(rowNode.data[this.paramName]);
      });
      return [...this._data];
    }
  }

  /**
   * Adds an empty row to the grid at the top. If you override this method without calling super, you'll also need to
   * override the data property as well as removeSelectedRow.
   */
  public addEmptyRow() {
    if (!this.gridApi) {
      return;
    }
    this.gridApi.applyTransaction({add: [{}]});
  }

  /**
   * Removes the currently selected row from the grid. If you override this method without calling super, you'll also
   * need to override the data property as well as addEmptyRow.
   */
  public removeSelectedRow() {
    if (!this.gridApi) {
      return;
    }

    this.gridApi.applyTransaction({remove: this.gridApi.getSelectedNodes().map(x => x.data)});
  }

  public get selectedNodes(): IRowNode[] {
    if (this.gridApi) {
      return this.gridApi.getSelectedNodes();
    }

    return [];
  }

  @Input('columnDefs') public set columnDefs(value: ColDef[]) {
    console.debug('Setting column defs', value);
    if (value) {
      this._columnDefs = value;
    } else {
      this._columnDefs = [];
    }

    if (this.gridApi) {
      this.gridApi.setGridOption("columnDefs", this._columnDefs);
    }
  }

  @Output() rowSelected = new EventEmitter<any>();


  public ngOnInit(): void { }


  public onClickAdd(): void {
    this.addEmptyRow();
  }

  public onClickDelete(): void {
    this.removeSelectedRow();
  }


  public isRowSelected(): boolean {
    if (!this.gridApi) {
      return false;
    }

    const selectedRows: any[] = this.gridApi.getSelectedNodes();

    return !(!selectedRows || (Array.isArray(selectedRows) && selectedRows.length === 0));
  }

  public onSelectedRow(event: any): void {
    this.rowSelected.emit(event);
  }

  public onGridReady(params: GridReadyEvent): void {
    this.gridApi = params.api;
    this.updateColumnDefs();
    this.setGridRowData();
    this.gridApi.sizeColumnsToFit();
    this.gridApi.setGridOption("context", this.context);
    this.gridApi.setGridOption("rowSelection", this.rowSelection);
  }

  public clear() {
    if (this.gridApi) {
      this.gridApi.setGridOption("rowData", []);
    }
  }

  private updateColumnDefs() {
    console.debug('updateColumnDefs');
    if (this.paramName && (!this._columnDefs || this._columnDefs.length === 0)) {
      console.debug('default column');
      this._columnDefs = [{
        headerName: this.title,
        field: this.paramName,
        editable: !this.readOnly
      }];
    }
    this.columnDefs = this._columnDefs;
  }

  private setGridRowData() {
    const theData = this._data.map(elt => typeof elt === 'object' ? elt : {[this.paramName]: elt});
    console.debug('theData', theData, this._columnDefs);
    this.gridApi.setGridOption("rowData", theData);

    if (this.gridApi) {
      this.gridApi.sizeColumnsToFit();
    }
  }
}

