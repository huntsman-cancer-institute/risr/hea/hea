import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  ViewChild
} from '@angular/core';
import {UtilService} from '../../../../services/util.service';
import {DynamicComponent} from '../../../../models/dynamic-component.model';
import {DynamicComponentManager} from '../../../../models/dynamic-component-manager.model';
import {IComponentSpec} from '../../../../models/interpreted-contents.model';


@Component({
  selector: 'fieldset-dynamic',
  template: `
    <fieldset [style]="fieldsetDisplay">
      <legend>{{subComponentSpecs[0].sectionPrompt ? subComponentSpecs[0].sectionPrompt : subComponentSpecs[0].section}}</legend>
      <button mat-button class="padded-left-right no-min-width" [ngClass]="{'read-only': readOnly}" (click)="onClickAdd()">
        <i class="fas fa-plus fa-lg "></i>
      </button>
      <ng-container *ngFor="let cspec of subComponentSpecs | arrayFilter:'inputs.index':'>=':0; index as i">
        <button *ngIf="i % templateSize === 0" mat-button class="padded-left-right no-min-width" [ngClass]="{'read-only': readOnly}" (click)="onClickRemove(i + templateSize)">
          <i class="fas fa-minus fa-lg "></i>
        </button>
        <ndc-dynamic class="h-100 w-100"
                   [ndcDynamicComponent]="cspec.componentType"
                   [ndcDynamicInputs]="cspec.inputs"
                   [ndcDynamicOutputs]="cspec.outputs"
                   (ndcDynamicCreated)="cspec.componentCreated($event)">
        </ndc-dynamic>
      </ng-container>
    </fieldset>
  `,
  styles: [`
    .read-only {
      pointer-events: none
    }
  `]
})

/**
 * Displays a fieldset containing components for editing an array of HEA objects. The fieldset has add and remove
 * buttons to add and remove the components corresponding to an HEA object from the array.
 */
export class FieldsetDynamicComponent extends DynamicComponent {

  @ViewChild('titleBox') titleBox: ElementRef;

  private _subComponentSpecs: IComponentSpec[];
  private _templateComponentSpecs: IComponentSpec[] = [];
  templateSize: number;
  private _dynamicComponentManager: DynamicComponentManager;
  fieldsetDisplay = "";

  /**
   * Sets the components to display inside the fieldset. The array argument is passed by reference, so changes made
   * by an instance of this class are reflected outside the class, and vice versa. The components correspond to the
   * properties of zero or more HEA objects. The components array is expected to have at least one set of components
   * representing a HEA object "template" with default values for its properties.
   *
   * @param subComponentSpecs an array of component specs representing HEA objects. The provided component specs are
   * expected to contain section and index values. The section values should all be the same. The index numbers
   * correspond to the default HEA object template (index == -1), followed by zero or more HEA objects with index
   * numbers 1, 2, 3, etc.
   */
  @Input()
  public set subComponentSpecs(subComponentSpecs: IComponentSpec[]) {
    this.display = false;
    this.fieldsetDisplay = "display: none";
    this._subComponentSpecs = subComponentSpecs;
    this._templateComponentSpecs.length = 0;
    for (const subComponent of subComponentSpecs) {
      if (subComponent.inputs.index < 0) {
        this._templateComponentSpecs.push(subComponent);
      }
      if (subComponent.inputs.display) {
        this.display = true;
        this.fieldsetDisplay = "";
      }
    }
    this.templateSize = this._templateComponentSpecs.length;
  }

  /**
   * Gets the components displayed inside the fieldset.
   *
   * @return a reference to the component spec array.
   */
  public get subComponentSpecs(): IComponentSpec[] {
    return this._subComponentSpecs;
  }

  /**
   * The components of the default HEA Object template (with index -1).
   *
   * @return an array of component specs. Never null.
   */
  public get templateComponentSpecs(): IComponentSpec[] {
    return this._templateComponentSpecs;
  }

  @Input()
  public set data(data: any) {
  }

  public get data(): any {
    return null;
  }

  @Input()
  public set dynamicComponentManager(componentManager: DynamicComponentManager) {
    this._dynamicComponentManager = componentManager;
  }

  public get dynamicComponentManager(): DynamicComponentManager {
    return this._dynamicComponentManager;
  }

  public onClickAdd() {
    this.addComponents();
  }

  public onClickRemove(startingIndex: number) {
    if (startingIndex >= this.templateSize) {
      this.removeComponents(startingIndex, startingIndex + this.templateSize);
    }
  }

  private addComponents() {
    const index = (this.subComponentSpecs.length / this.templateSize) + 1;
    for (const subComponentSpec of this.templateComponentSpecs) {
      const copy = this.dynamicComponentManager.clone(subComponentSpec, index);
      copy.inputs.paramName = DynamicComponentManager.componentSpecMapKey(copy.name, copy.section, copy.inputs.index);
      this.subComponentSpecs.push(copy);
    }
    this.subComponentSpecs = [...this.subComponentSpecs]
  }

  private removeComponents(begin: number, end: number) {
    for (let i = begin; i < end; i++) {
      const subComponentSpec = this.subComponentSpecs[i];
      this.containerFormGroup.removeControl(subComponentSpec.inputs.paramName);
      this.dynamicComponentManager.remove(subComponentSpec.name, subComponentSpec.section, subComponentSpec.inputs.index);
    }
    this.subComponentSpecs.splice(begin, end - begin);
    this.subComponentSpecs = [...this.subComponentSpecs];
  }
}
