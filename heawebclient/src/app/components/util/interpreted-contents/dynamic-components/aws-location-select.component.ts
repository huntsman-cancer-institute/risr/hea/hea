import {Component, ElementRef, Input, ViewChild} from "@angular/core";


@Component({
  selector: 'aws-location-select',
  template: `

    <div class="w-100 padded-top">
      <div #mySpoofedInput class="w-100 border-2 d-flex flex-row background-color-white spoofed-input">
        <div class="flex-grow-1 spoofed-text">
          <div style="min-height: 1.5rem;">
            {{ value }}
          </div>
        </div>
        <!--<div class="d-flex flex-column">-->
          <!--<div class="flex-grow-1">-->
          <!--</div>-->
          <!--<div class="padded blue">-->
            <!--<i class="fas fa-search fa-lg ">-->
            <!--</i>-->
          <!--</div>-->
          <!--<div class="flex-grow-1">-->
          <!--</div>-->
        <!--</div>-->
      </div>
      <div #myLabel *ngIf="showLabel" class="d-flex flex-row align-center invisible-background" [ngStyle]="labelStyle" (click)="onClick($event)">
        <div class="label invisible-background">
          <label for="mySpoofedInput" class="invisible-background">DESTINATION</label>
        </div>
      </div>
    </div>

  `,
  styles: [`

    .align-center {
      align-items: center;
    }

    .invisible-background {
      background-color: rgba(255, 255, 255, 0);
    }

    .background-color-white {
      background-color: white;
    }

    .border-2 {
      border: solid #d7dcde 1px;
      border-radius: 6px;
    }

    .blue {
      color: #005580;
    }

    .spoofed-input {
      background-color: #FFFFFF;
    }

    .spoofed-text {
      padding: 0.9em 0.7em 0.5em 0.5em;
      white-space: nowrap;
      overflow: hidden;
    }

    .label {
      width: 100%;
      height: fit-content;
      padding: 0 0.7em 0 0.7em;
      position: relative;
      z-index: 500;
    }

    .spoofed-input:focus {
      background-color: white;

      transition: all .3s;
    }

    .spoofed-input {
      z-index:999;
      background-repeat:no-repeat;
      background-position:3px 19px;
      background-size:16px 16px;

      transition: all .3s;
    }

    .spoofed-input+div {
      font-size: 8pt;
      top: -3.5em;
      left: 0.5em;
      z-index: 500;
      position: relative;
      align-items: flex-start;
      background-color: white;
      transition: all .3s;
    }

    .spoofed-input+div>div>label {
      transition: all .3s;
    }

    .spoofed-input~div {
      top: -2.5em;
      left: 1.0em;
      z-index: 5;
      font-size: 8pt;
      position: relative;
      background-color: rgba(255,255,255,0);
      align-items: flex-start;

      transition: all .3s !important;
    }

  `]
})
export class AwsLocationSelectComponent {

  @ViewChild("mySpoofedInput") mySpoofedInput: ElementRef;
  @ViewChild("myLabel") myLabel: ElementRef;

  @Input() public value: string = "";

  public get labelStyle(): any {
    let style: any = {
      'position'       : 'relative',
      'z-index'        : '500',
      'height'         : '0',
      'width'          : this.labelWidth,
      'top'            : this.labelTop,
      'left'           : this.labelLeft,
      'pointer-events' : 'none',
      'transition'     : 'all 0.3s'
    };

    return style;
  }

  private labelWidth:  string = 'fit-content';
  private labelTop:    string = '0';
  private labelLeft:   string = '1px';

  public showLabel: boolean = false;


  public constructor() { }


  ngAfterViewInit(): void {
    this.labelWidth  = '' + (this.mySpoofedInput.nativeElement.offsetWidth - 2)  + 'px';
    this.labelTop    = '' + (-this.mySpoofedInput.nativeElement.offsetHeight + 1) + 'px';

    setTimeout(() => {
      this.showLabel = true;
    });
  }


  public onClick(event?: any): void {
    console.log('Clicked');
  }

  public onChange(event: any): void {

  }
}
