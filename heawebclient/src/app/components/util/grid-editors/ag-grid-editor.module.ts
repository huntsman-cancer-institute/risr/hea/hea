import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {AgGridModule} from "ag-grid-angular";

import {AgGridRendererModule} from "../grid-renderers/ag-grid-renderer.module";
import {DateEditor} from './date.editor'
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatLegacyInputModule as MatInputModule} from "@angular/material/legacy-input";



@NgModule({
    imports: [
        AgGridModule,
        AgGridRendererModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MatInputModule
    ],
    declarations: [
        DateEditor
    ],
    exports: [
        DateEditor
    ]
})
export class AgGridEditorModule {
}
