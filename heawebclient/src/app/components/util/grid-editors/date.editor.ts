import {AfterViewInit, Component, ElementRef, ViewChild} from "@angular/core";

import { ICellEditorAngularComp } from "ag-grid-angular";
import {DateParser} from "../parsers/date-parser";
import {DialogsService} from "../dialog/dialogs.service";
import {MatDatepicker} from "@angular/material/datepicker";
import moment from 'moment';
import {Moment} from "moment/moment";

@Component({
  template: `
    <div class="full-width full-height flex-row-container">
      <div class="full-width full-height flex-stretch">
        <div class="t full-width full-height" (click)="onClick()">
          <div class="tr">
            <div  class="td vertical-center">
              <div class="invisible">
                <mat-form-field>
                  <input matInput [matDatepicker]="picker" (dateChange)="stopEditing()"  [(ngModel)]="date">
                  <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
                </mat-form-field>
              </div>
              <div class="full-width right-align">
                {{ display }}
              </div>
              <mat-datepicker #picker></mat-datepicker>
            </div>
          </div>
        </div>
      </div>
      <div *ngIf="showFillButton" class="full-height button-container">
        <button class="full-height" (click)="onFillButtonClicked()">Fill</button>
      </div>
    </div>
  `,
  styles: [`
    .t  { display: table;      }
    .tr { display: table-row;  }
    .td { display: table-cell; }

    .full-width  { width:  100%; }
    .full-height { height: 100%; }

    .vertical-center { vertical-align: middle; }

    .invisible {
      visibility: hidden;
      width: 0;
      height: 0;
    }

    .right-align { text-align: right; }

    .flex-row-container {
      display: flex;
      flex-direction: row;
    }
    .flex-stretch {
      flex: 1;
    }
  `]
})
export class DateEditor implements AfterViewInit, ICellEditorAngularComp {

  @ViewChild('picker',{static: false}) picker: MatDatepicker<Date>;

  params: any;
  value: string;
  _date: Moment;
  display: string;
  dateParser: DateParser;

  private gridFieldName: string = '';

  showFillButton: boolean;		// This represents whether the editor should show the "Fill" button,
  // which is used to copy the value of this cell to other cells in this column in the grid
  fillGroupAttribute: string;		// This attribute is used to specify which "Group" a particular
  // row belongs to, which is used when the fill button is active.
  // When clicked, the fill button will copy the data in that cell
  // to the corresponding cells in rows of the same group.

  get date(): Moment {
    return this._date;
  }

  set date(date: Moment) {
    this._date = date;

    this.display = this.dateParser.parseDateToDisplayString(this._date);
    this.value = this.dateParser.parseDateToModelString(this._date);
  }

  constructor(private dialog: DialogsService) {}

  agInit(params: any): void {
    this.params = params;
    this.value = "";
    this.display = "";


    if (this.params && this.params.value && this.params.value != "") {
      this.value = this.params.value;

      if (this.params && this.params.colDef && this.params.colDef.dateParser) {
        this.dateParser = this.params.colDef.dateParser;
      }

      this._date = this.dateParser.getModel(this.value);
      this.display = this.dateParser.parseDateToDisplayString(this.value);
    }

    if (this.params && this.params.column && this.params.column.colDef) {
      this.gridFieldName = this.params.column.colDef.field;
      this.fillGroupAttribute = this.params.column.colDef.fillGroupAttribute;

      this.showFillButton = this.params.column.colDef.showFillButton && ("" + this.params.column.colDef.showFillButton).toLowerCase() !== "false";
    }

    if (this.showFillButton && (!this.fillGroupAttribute || this.fillGroupAttribute === '')) {
      throw new Error('Invalid state, cannot use fill button without specifying the fillGroupAttribute.');
    }

  }

  ngAfterViewInit(): void {
    setTimeout(() => { this.picker.open();});
  }

  onChange(event: any): void {
    // toLocaleDateString works in this case because we used new Date before, so that it has the
    // user's timezone, and will not produce date change errors.
    // this.value = DateRenderer.parseDateString(this._date.toLocaleDateString(), 'm/d/yyyy', DateRenderer.DEFAULT_RECEIVED_DATE_FORMAT);
  }

  onClick(): void {
    this.picker.open();
  }

  stopEditing():void{
    if(this.params && this.params.column && this.params.column.gridApi){
      this.params.column.gridApi.stopEditing();
    }
  }

  private static getDisplayStringFromDate(date: Moment): string {
    if (!date) {
      return "";
    }

    return date.format();
  }

  getValue(): any {
    this.display = this.dateParser.parseDateToDisplayString(this._date);
    this.value = this.dateParser.parseDateToModelString(this._date);
    return this.value;
  }

  isPopup(): boolean {
    return false;
  }

  onFillButtonClicked(): void {
    if (!this.fillGroupAttribute || this.fillGroupAttribute === '') {
      throw new Error('No column attribute "fillGroupAttribute" specified. This is required to use the Fill functionality.');
    }

    if (this.params && this.params.column && this.params.column.gridApi && this.params.node && this.fillGroupAttribute && this.fillGroupAttribute !== '') {
      let thisRowNode = this.params.node;
      this.dialog.startDefaultSpinnerDialog();

      this.params.column.gridApi.forEachNode((rowNode, index) => {
        if (rowNode && rowNode.data && thisRowNode && thisRowNode.data
          && rowNode.data[this.fillGroupAttribute] === thisRowNode.data[this.fillGroupAttribute]) {
          rowNode.setDataValue(this.gridFieldName, this.value);
        }
      });

      this.params.column.gridApi.refreshCells();
      setTimeout(() => {
        this.dialog.stopAllSpinnerDialogs();
      });
    }
  }
}
