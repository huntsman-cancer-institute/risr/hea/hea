import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HeaobjectExplorerComponent } from './heaobject-explorer.component';

describe('HomeComponent', () => {
  let component: HeaobjectExplorerComponent;
  let fixture: ComponentFixture<HeaobjectExplorerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaobjectExplorerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaobjectExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
