import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {OrgService} from '../../../services/org/org.service';
import {map, mergeMap, take} from 'rxjs/operators';
import {ApiService} from '../../../services/api/api.service';
import {Item, Organization, Person} from '../../../models/heaobject.model';
import {HEAObjectContainer} from '../../../models/heaobject-container.model';
import {CjService} from '../../../services/cj/cj.service';
import { DialogOpenerService } from 'src/app/services/dialog-opener/dialog-opener.service';

@Component({
  selector: 'cb-users-card',
  template: `
    <div class="d-flex flex-row h-100 w-100 padded">
      <div class="cb-card cb-card-no-margin d-flex full-height flex-column flex-grow-1" >
        <loading-overlay [busy]="dataLoading" [icon]="busyIcon"></loading-overlay>
        <div class="cb-card-header d-flex justify-content-between">
          <div class="d-flex align-items-center">
            <div class="mx-1"> <i class="bi bi-people-fill" style="font-size: 1.2rem"  aria-hidden="true"></i> </div>
            <div>{{this.selectedOrg?.heaObject.display_name}} Members</div>
          </div>
          <div (click)="onClickCog()" title="Edit Members" class="cursor">
            <i class="bi bi-info-circle-fill" style="font-size: 1.1rem"></i>
          </div>
        </div>
        <div class="d-flex flex-column cb-card-body inner-container-scroll overflow-auto" >
          <div class="d-flex flex-row flex-wrap" *ngFor="let r of rows; let i = index" >
            <div  [title]="usr.heaObject.display_name + (usr.heaObject.email ? '\\n' + usr.heaObject.email : '')"
                  *ngFor="let usr of userList.slice(i*4, ((i* 4) + 4))"  style="min-width: 10em;" class="d-flex  flex-column p-1 gap-1" >
              <div class="cursor d-flex flex-row center" (click)="onClickUser(usr)">
                <div class="me-1"> <i class="bi bi-person-square" style="font-size: 1.75rem; vertical-align: text-top;"> </i></div>
                <div class="d-flex flex-column font-sm">
                  <button class="link-button left-align"><div class="message inline-block font-weight-bold">{{usr.heaObject.display_name}}</div></button>
                  <label>{{usr.heaObject.title}}\n{{getMembershipLevel(usr.heaObject.id).join(', ')}}</label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`

    div.cb-header-detail {
      background-color: #9d9a9a;
      color: #fff;
    }

    .enable {
      color: white;
    }

    .disable {
      color: var(--greywarm-medlight);
    }

    .inline-block { display: inline-block; }

    .left-align { text-align: left; }

    button.link-button {
      background: none;
      background-color: inherit;
      border: none;
      padding: 0;
      text-decoration: underline;
      cursor: pointer;
    }

    button.link-button:focus {
      outline: none;
    }

    .message {
      text-decoration: underline;
      color: blue;
    }

    ::ng-deep .mat-tooltip-test {
      white-space: pre-line !important;
    }


  `]
})
export class UserCardComponent implements OnInit, OnDestroy {

  // public sortedUserList: any[] = [];

  public set userList( val: HEAObjectContainer<Person>[]) {
    this._userList = val;
    if (this._userList){
      this.rows =  [...Array( Math.ceil( this._userList.length / 4 ) ).keys()];
      // this.sortUserList('name');
    }else {
      this._userList = [];
    }
  }
  public get userList( ): HEAObjectContainer<Person>[] {
    return this._userList;
  }

  private _userList: HEAObjectContainer<Person>[] = [];
  selectedOrg: HEAObjectContainer<Item>;
  rows: number[] = [];
  private sub: any;
  dataLoading = [false];
  busyIcon = 'fas fa-sync fa-spin';



  constructor(private router: Router,
              private orgService: OrgService,
              private apiService: ApiService,
              private cjService: CjService,
              private dialogOpener: DialogOpenerService) {

  }

  ngOnInit() {
    this.sub = this.orgService.selectedOrgValueNoCache.pipe(
      mergeMap(orgResp => {
        if (orgResp) {
          this.dataLoading = [true];
          return this.apiService.getCollectionPlusJson(this.cjService.findLink(orgResp.links, 'hearesource-organizations-memberseditor').href);
        } else {
          return [];
        }
      })
    )
    .pipe(map(resp => this.cjService.asHEAObjectContainers<Person>(resp)[0]))
    .pipe(
      mergeMap(orgResp => {
        this.selectedOrg = orgResp;
        if (orgResp){
          return this.apiService.getCollectionPlusJson(this.cjService.findLink(orgResp.links, 'application/x.person').href);
        }
      }))
      .pipe(map(resp => this.cjService.asHEAObjectContainers<Person>(resp)))
      .subscribe(userList => {
        this.userList = userList
        this.dataLoading = [false];
    }, error => {
      this.dataLoading = [false];
    });
    if (this.orgService.selectedOrgValue && this.selectedOrg !== this.orgService.selectedOrgValue ){
      this.orgService.updateSelectedOrg(this.orgService.selectedOrgValue);
    }
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  onClickUser(user: HEAObjectContainer<Person>) {
    if (user) {
      this.cjService.getActualObjectOrSelf(user).pipe(take(1)).subscribe(resp => {
        const href = resp.href;
        this.dialogOpener.openModal(href, 'Edit', n => {
          if (n) {
            this.orgService.refresh()
          }
        });
      });
    }
  }

  onClickCog() {
    if (this.selectedOrg) {
      this.dialogOpener.openModal(this.selectedOrg.href, 'Edit', (n) => {
        if (n) {
          this.setUserList()
        }
      });
    }
  }

  disableCog(): string {
    let temp = !(this.selectedOrg) ? 'disable' : 'enable';
    return temp;
  }

  getMembershipLevel(user_id: string): string[] {
    const membership = [];
    if (this.selectedOrg) {
      const org = this.selectedOrg.heaObject as Organization;

      if (org && user_id) {
        if (org.principal_investigator_id === user_id) {
          membership.push('PI');
        }
        if (org.admin_ids && org.admin_ids.indexOf(user_id) >= 0) {
          membership.push('Admin');
        }
        if (org.manager_ids && org.manager_ids.indexOf(user_id) >= 0) {
          membership.push('Manager');
        }
        if (org.member_ids && org.member_ids.indexOf(user_id) >= 0) {
          membership.push('Member');
        }
        if (org.collaborator_ids && org.collaborator_ids.indexOf(user_id) >= 0) {
          membership.push('Collaborator');
        }
      }
    }
    return membership;

  }

  private setUserList() {
    // Needs to refresh the org somehow, but the org service doesn't seem designed to allow that.
    this.orgService.refresh();
  }
}
