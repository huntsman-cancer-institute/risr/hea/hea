import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {ColDef, GridApi, RowNode} from 'ag-grid-community';
import {ChartOptions, ChartType} from 'chart.js';
import {Label, SingleDataSet} from 'ng2-charts';
import * as pluginLabels from 'chartjs-plugin-labels';
import {MatButtonToggleChange} from '@angular/material/button-toggle';
import {BehaviorSubject} from 'rxjs';
import {DialogsService} from '../../util/dialog/dialogs.service';
import {UtilService} from '../../../services/util.service';
import {AccountTreeNavService} from '../../../services/account-tree-nav.service';
import {StorageClass} from '../../../models/aws-s3-object.model';
import {AwsDataProcessing} from '../../util/parsers/aws-data-processing';
import {CjService} from '../../../services/cj/cj.service';
import { AWSS3Storage, AccountView } from 'src/app/models/heaobject.model';
import { take } from 'rxjs/operators';
import { IHEAObjectContainer } from 'src/app/models/heaobject-container.model';
import { IStorageSummary, IUsageSummary } from 'src/app/models/storage.model';


@Component({
  selector: 'cb-usage-card',
  template: `
    <div class="d-flex flex-row h-100 w-100 padded">
      <div class="cb-card cb-card-no-margin d-flex flex-column h-100 w-100">
        <loading-overlay [busy]="dataLoading" [icon]="this.utilService.BUSY_ICON"></loading-overlay>
        <div class="cb-card-header d-flex flex-row justify-content-between">
          <div class="d-flex align-items-center">
            <div class="mx-1"><i class="bi bi-speedometer" style="font-size: 1.2rem"></i></div>
            <div> Usage Summary</div>
          </div>
          <div class="d-flex flex-row align-items-center">
            <div class="me-2">
              <mat-button-toggle-group
              aria-labelledby="unit-toggle-group-label"
              class="unit-toggle-group"
              (change)="dataSizeUnitChange($event)"
              [(ngModel)]="selectedUnit">
              <mat-button-toggle class="unit-toggle-button" *ngFor=" let dataSizeUnit of dataSizeUnits"
                                 [value]="dataSizeUnit"
                                 [ngClass]="{'active' : selectedUnit===dataSizeUnit}">
                {{dataSizeUnit}}
              </mat-button-toggle>
            </mat-button-toggle-group>
            </div>
            <div class="cursor ms-1" (click)="onCalculatorClicked($event)">
              <i class="bi bi-calculator-fill" style="font-size: 1.2rem"></i>
            </div>
            <!--<div class="padded-left-right">
              <i class="fas fa-cog"></i>
            </div>-->
          </div>
        </div>
        <div class="d-flex flex-column flex-grow-1 no-padding cb-card-body">
          <div class="d-flex flex-grow-1 align-items-center flex-wrap justify-content-center" style="min-height: 8rem">
            <canvas baseChart
                    [data]="doughnutChartData"
                    [labels]="doughnutChartLabels"
                    [colors]="this.hasUsageData ? doughnutChartColors : doughnutChartColorsInit"
                    [chartType]="doughnutChartType"
                    [plugins]="doughnutChartPlugins"
                    [options]="doughnutOptions">
            </canvas>
          </div>
          <div *ngIf="hasUsageData" class="d-flex flex-grow-1 padded-left" style="font-weight: bolder">
            AS OF: {{' ' + this.calDateTime }}
          </div>
          <div class="d-flex flex-grow-1 align-items-center" style="text-align: center">
            <span class="padded-left-right" *ngIf="hasUsageData">TOTAL ACCOUNT SIZE: {{totalAccountSizeString}}</span>
            <span class="padded-left-right" *ngIf="hasUsageData">OBJECT COUNT: {{sumData[0].accObjCount?.toLocaleString()}}</span>
            <span class="padded-left-right" *ngIf="hasUsageData">AVG OBJECT SIZE: {{totalAvgObjectSizeString}}</span>
          </div>
          <div class="d-flex flex-grow-1 align-items-center flex-wrap" style="min-height: 10em">
            <ag-grid-angular class="h-100 flex-grow-1 ag-theme-balham"
                             (gridReady)="onGridReady($event)">
            </ag-grid-angular>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
.unit-toggle-group {
      background: none !important;
      border-radius: 0.2em;
      align-items: center;
      display: flex;
      flex-direction: row;
      height: 23px;
    }
    .unit-toggle-button {
      font-family: hci-font;
      background: linear-gradient(0deg,var(--greywarm-darker) 30%,var(--bluewarm-lightest) 100%);
      color: var(--greycool-lighter) !important;
      border-right: var(--bluewarm-medlight) 0.5px solid !important;
      font-weight: 300;
      line-height: 1rem !important;
      padding: 0.4em;
    }

    .unit-toggle-button.active {
      background: var(--pure-yellow) !important;
      color: var(--white-lightest) !important;
      font-weight: 600;
    }
    .risr-material .mat-button-toggle-appearance-standard .mat-button-toggle-label-content {
      font-family: 'hci-font';
      line-height: 1.3rem !important;
      padding: 0 0.5rem;
    }
    .mat-button-toggle-appearance-standard .mat-button-toggle-focus-overlay {
      background-color: var(--white-lightest) !important;
    }

    .cal-padded {
      padding: 0 1.5rem 0 1.5rem;
    }
  `]
})
export class UsageCardComponent implements OnInit, AfterViewInit, OnDestroy {

  STANDARD = 'STANDARD';
  STANDARD_ALIAS = 'S3';

  selectedUnit: string;
  dataSizeUnits: string[] = ['KB', 'MB', 'GB', 'TB'];
  defaultUnit = 'GB';

  busy = false;
  dataLoading: boolean[] = [false];
  gridApi: GridApi;

  context: any;
  calDateTime: string;
  sumData: IUsageSummary[] = [];

  defaultColDef: any = {
    filter: true,
    sortable: true,
    resizable: true
  };
  colDefs: ColDef[] | any[] = [
    { headerName: 'STORAGE CLASS', field: 'storageClass', width: 200,
    },
    { headerName: 'TOTAL SIZE', field: 'storageBytes', width: 100,
      valueFormatter: ((params) => {
        const val = this.utilService.formatSize(params.value, this.selectedUnit);
        return this.utilService.formatNumber(val) + ' ' + this.selectedUnit;
      }).bind(this),
      comparator: (valueA, valueB, nodeA: RowNode, nodeB: RowNode, isInverted) => {
        return this.utilService.sortNumber(valueA, valueB);
      }
    },
    { headerName: 'MIN DURATION', field: 'minDurationDays', width: 100,
      valueFormatter: (params) => (params.value || params.value === 0) ? (params.value + ' Days') : null,
      comparator: (valueA, valueB, nodeA: RowNode, nodeB: RowNode, isInverted) => {
        return this.utilService.sortNumber(valueA, valueB);
      }
    },
    { headerName: 'MAX DURATION', field: 'maxDurationDays', width: 100,
      valueFormatter: (params) => (params.value || params.value === 0) ? (params.value + ' Days') : null,
      comparator: (valueA, valueB, nodeA: RowNode, nodeB: RowNode, isInverted) => {
        return this.utilService.sortNumber(valueA, valueB);
      }
    },
    { headerName: 'AVG DURATION', field: 'avgDurationDays', width: 100,
      valueFormatter: (params) => (params.value || params.value === 0) ? (params.value + ' Days') : null,
      comparator: (valueA, valueB, nodeA: RowNode, nodeB: RowNode, isInverted) => {
        return this.utilService.sortNumber(valueA, valueB);
      }
    }
  ];

  doughnutChartColors: any[] = [{
    backgroundColor: ['#FF0000', '#A8A100', '#00B300', '#8B24F0', '#BF7014', '#00AD90', '#E62EE6', '#1273B3'],
  }];
  doughnutChartColorsInit: any[] = [{backgroundColor: ['#E7E7E7']}];
  doughnutChartLabels: Label[];
  doughnutChartData: SingleDataSet;
   doughnutChartType: ChartType = 'doughnut';
  doughnutOptions: ChartOptions;
  doughnutChartPlugins = [];
  totalAccountSizeString: string;
  totalObjectCountString: string;
  totalAvgObjectSizeString: string;

  private selectedAccount: IHEAObjectContainer<AccountView>;
  private preSelectedAccount: any;
  private usageList: StorageClass[] = [];
  private accUsageData = new BehaviorSubject<IStorageSummary>(undefined);

  get hasUsageData(): boolean {
    return this.sumData && this.sumData.length > 0 && this.sumData[0].accTotalSize > 0 ? true : false;
  }

  private createOptions(): ChartOptions {
    return {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        position: 'right',
        align: 'start'},
      tooltips: {
        enabled: true,
        mode: 'label',
        callbacks: {
          label: (tooltipItem: Chart.ChartTooltipItem, data: Chart.ChartData): string | string[] => {
            const size = data.datasets[0].data[tooltipItem.index] as number;
            return size > 0 ? data.labels[tooltipItem.index] + ': ' +
              this.utilService.formatNumber( this.utilService.formatSize('' + size,
                this.selectedUnit)) + ' ' + ( this.selectedUnit ? this.selectedUnit : this.defaultUnit ) : 'No Data to show';
          }
        }
      },
      plugins: {
        labels: [
          {
            render: 'percentage',
            fontColor: '#FFF',
            overlap: false,
            precision: 2
          }
        ]
      },
    };
  }

  constructor(private router: Router,
              private dialogService: DialogsService,
              public utilService: UtilService,
              private treeNavService: AccountTreeNavService,
              private datePipe: DatePipe,
              private cjService: CjService) {
    this.context = this;
  }

  ngOnInit() {

    this.initUsageData();

    this.doughnutOptions = this.createOptions();
    this.doughnutChartPlugins = [pluginLabels];
    this.selectedUnit = this.defaultUnit;

    this.treeNavService.selectedAwsAccount.subscribe(data => {
      this.selectedAccount = data;
      if (!this.preSelectedAccount || this.preSelectedAccount.id !== data.id) {
        this.preSelectedAccount = data;
        this.initUsageData();
        this.accUsageData.next(null);
        this.calDateTime = null;
        this.treeNavService.getStorageObservable().subscribe((accUsage: IStorageSummary[]) => {
          if (accUsage && accUsage.length > 0) {
            for (const acc of accUsage) {
              if (acc.accountId === this.selectedAccount.heaObject.id) {
                if (acc.storageData && acc.storageData.length > 0) {
                  this.usageList = AwsDataProcessing.processStorageClassData(acc.storageData);
                  this.calDateTime = acc.calDateTime;
                  this.sumData = acc.accUsageSum;
                }
                this.refreshGrids();
                this.createDoughnutChartData();
                this.updateTotals();
              }
            }
          }
        });
      }
    });
  }

  initUsageData() {
    this.usageList = [];
    this.sumData = [];
    this.usageList = AwsDataProcessing.processStorageClassData([]);
  }

  ngAfterViewInit(): void {
    this.createDoughnutChartData();
  }

  createDoughnutChartData() {
    const doughnutChartLabels = [];
    const doughnutChartData = [];
    for (const usg of this.usageList) {
      let size = 0;
      if (usg.storageBytes) {
        size = usg.storageBytes;
      }
      doughnutChartData.push(size);
      doughnutChartLabels.push(usg.storageClass);
    }
    console.debug('labels', doughnutChartLabels)
    setTimeout(() => {
      this.doughnutChartLabels = doughnutChartLabels;
      this.doughnutChartData = doughnutChartData;
    }, 0);
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridApi.setGridOption("columnDefs", this.colDefs);
    this.gridApi.setGridOption("context", this.context);
    this.gridApi.setGridOption("rowSelection", "single");
    this.gridApi.setGridOption("defaultColDef", this.defaultColDef);
    this.refreshGrids();
  }

  refreshGrids() {
    // Refresh the storage details grid data
    if (this.gridApi) {
      if (this.hasUsageData) {
        this.gridApi.setGridOption("rowData", this.usageList);
      } else {
        this.gridApi.setGridOption("rowData", []);
      }
      this.gridApi.sizeColumnsToFit();
    }
  }

  dataSizeUnitChange(event: MatButtonToggleChange) {
    this.createDoughnutChartData();
    if (this.gridApi) {
      this.gridApi.redrawRows();
      this.gridApi.sizeColumnsToFit();
    }
    this.updateTotals();
  }

  onCalculatorClicked(event: PointerEvent) {
    if (!this.selectedAccount || this.selectedAccount.heaObject.type !== 'heaobject.account.AccountView') {
      alert('Please select an account from the tree sidebar to see its usage summary');
      return;
    } else {
      this.dataLoading = [true];
      this.cjService.getActualObjectOrSelf(this.selectedAccount).pipe(take(1)).subscribe(actual => {
        const storageLink = this.cjService.findLink(actual.links, UtilService.REL_OPENER_CHOICES).href;
        const rel = UtilService.REL_AWS_OPENER + ' ' + UtilService.MIME_TYPE_AWS_STORAGE;
        this.cjService.getOpenerAndGo<AWSS3Storage>(storageLink, rel).subscribe(storageResp => {
          if (storageResp && storageResp.length > 0) {
            const storageSummaries: AWSS3Storage[] = [];
            let totalSize = 0;
            let objCount = 0;
            let avgObjSize = 0;
            for (const heaStorage of storageResp) {
              storageSummaries.push(heaStorage.heaObject);

              // Calculate the usage summary of the account
              totalSize += (heaStorage.heaObject.size ? heaStorage.heaObject.size : 0);
              objCount += (heaStorage.heaObject.object_count ? heaStorage.heaObject.object_count : 0);
            }
            const accUsageSum = {
              accTotalSize: totalSize,
              accObjCount: objCount,
              accAvgObjSize: objCount ? +(totalSize / objCount).toFixed(2) : 0
            };

            const accStorage = {
              accountId: this.selectedAccount.heaObject.id,
              calDateTime: new Date().toLocaleString(),
              accUsageSum: [accUsageSum],
              storageData: storageSummaries
            };
            this.treeNavService.emitStorageSummary(accStorage);

          }
          this.dataLoading = [false];
        }, err => {
          console.error('Error!', err.status, err.error);
          this.dataLoading = [false];
        });
      });
    }
  }

  ngOnDestroy() {
  }

  private updateTotals() {
    const nbsp = String.fromCharCode(160);  // non-breaking space
    this.totalAccountSizeString = this.utilService.formatNumber(this.utilService.formatSize(this.sumData[0].accTotalSize, this.selectedUnit)) + nbsp + this.selectedUnit;
    this.totalAvgObjectSizeString = this.utilService.formatNumber(this.utilService.formatSize(this.sumData[0].accAvgObjSize, this.selectedUnit)) + nbsp + this.selectedUnit;
  }
}

