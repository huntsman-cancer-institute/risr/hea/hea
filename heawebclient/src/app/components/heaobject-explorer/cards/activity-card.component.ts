import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DialogsService} from '../../util/dialog/dialogs.service';
import {ColDef, GridApi, GridReadyEvent, IServerSideGetRowsParams, RowDoubleClickedEvent, RowNode, RowSelectedEvent} from 'ag-grid-community';
import {UtilService} from '../../../services/util.service';
import {map, take} from 'rxjs/operators';
import {Activity, IDesktopObject} from '../../../models/heaobject.model';
import {Subscription, of} from 'rxjs';
import {CjService} from '../../../services/cj/cj.service';
import { HEAObjectContainer } from 'src/app/models/heaobject-container.model';
import { Observable } from 'rxjs-compat';
import { ApiService } from 'src/app/services/api/api.service';
import { HttpParams } from '@angular/common/http';
import { AsyncCellRenderer } from '../../util/grid-renderers/async-cell-renderer.component';
import { ICJOptionsFromURL } from 'src/app/models/cj.model';

@Component({
  selector: 'cb-activity-card',
  template: `
    <div class="d-flex flex-row h-100 padded">
      <div class="cb-card cb-card-no-margin d-flex flex-column full-height flex-grow-1">
        <div class="cb-card-header d-flex flex-row">
            <div class="mx-1"><i class="bi bi-person-workspace" style="font-size: 1.2rem"></i></div>
            <div>Activity</div>
          <!--<div>
            <i class="fas fa-cog"></i>
          </div>-->
        </div>
        <ag-grid-angular class="d-flex flex-column flex-grow-1 no-padding ag-theme-balham"
                          (gridReady)="onGridReady($event)"
                          rowSelection="single"
                          (rowDoubleClicked)="onRowDoubleClicked($event)"
                          (rowSelected)="this.onSelectedRow($event)"
                          rowModelType="serverSide"
                          cacheBlockSize="10"
                          maxBlocksInCache="100"
                          maxConcurrentDatasourceRequests="2"
                          [getRowId]="getRowId"
                          [components]="frameworkComponents">
          <!-- [stopEditingWhenGridLosesFocus]="true" -->
        </ag-grid-angular>
      </div>
    </div>
  `,
  styles: [`

    div.cb-header-detail {
      background-color: #9d9a9a;
      color: #fff;
    }

  `]
})
export class ActivityCardComponent implements OnInit, OnDestroy {
  public gridApi: GridApi;
  public defaultColDef: any = {
    sortable: true,
    resizable: true
  };
  colDefs: ColDef[] | any[] = [
    {
      headerName: 'Date',
      field: 'date',
      width: 150,
      cellRenderer: params => {
        if (params.value === null || params.value === undefined) {
          return `<i class="${this.utilService.BUSY_ICON}"></i>`;
        } else {
          return new Date(params.value).toLocaleString()
        }
      },
      comparator: (valueA, valueB, nodeA: RowNode, nodeB: RowNode, isInverted) => {
        const valueADate = new Date(valueA);
        const valueBDate = new Date(valueB);
        if (valueADate === valueBDate) return 0;
        return (valueADate > valueBDate) ? 1 : -1;
      },
      suppressSizeToFit: true
    },
    {
      headerName: 'Request',
      field: 'request',
      width: 400,
      editable: false,
      sortable: false,
      suppressMenu: true
    },
    { headerName: 'Status',
      field: 'status',
      width: 100,
      enableCellChangeFlash: true,
      sortable: false,
      suppressMenu: true,
      suppressSizeToFit: true
    },
    { headerName: 'Submitted By',
      field: 'submittedBy',
      width: 150,
      sortable: false,
      suppressMenu: true,
      cellRenderer: 'asyncCellRenderer',
      suppressSizeToFit: true
    }
  ];
  context: any;
  private dataSourceSub: Subscription;
  private socket: WebSocket;
  private wsSub: Subscription;
  dataSource = {
    getRows: (params: IServerSideGetRowsParams) => {
      const request = params.request;
      console.debug('asking for', request.startRow, 'to', request.endRow);
      let sortOrder: string;
      if (request.sortModel.length > 0) {
        sortOrder = request.sortModel[0].sort;
      } else {
        sortOrder = 'desc'
      }

      const queryParams = new HttpParams().set('begin', request.startRow.toString())
        .set('end', request.endRow.toString())
        .set('sort', sortOrder)
        .set('excludecode', 'hea-get');
      this.dataSourceSub = this.api.getCollectionPlusJson('desktopobjectactions', {params: queryParams}).subscribe(resp => {
        const containers = this.cjService.asHEAObjectContainers<Activity>(resp);
        if (containers.length > 0) {
          this.resolveTemplateValue(containers[0], 'user_id').pipe(take(1)).subscribe(resp => {
            const activities = [];
            for (const objContainer of containers) {
              const activity = {
                id: objContainer.heaObject.id,
                date: new Date(objContainer.heaObject.status_updated),
                request: objContainer.heaObject.description,
                status: objContainer.heaObject.status,
                submittedBy: this.resolveTemplateValue(objContainer, 'user_id')
              };
              activities.push(activity);
            }
            console.debug('activities updated', request.startRow, request.endRow, activities, params);
            params.success({rowData: activities});
          });
        } else {
          params.success({rowData: []});
        }
      });
    }
  };
  private gridStale = false;
  private gridRefreshTimerId: NodeJS.Timeout;
  private socketTimeout: NodeJS.Timeout;
  private pingTimeout: NodeJS.Timeout;
  private webSocketClosed = false;
  frameworkComponents: any;
  private asyncCache: Map<string, HEAObjectContainer<IDesktopObject>[]>;

  constructor(private router: Router,
              private dialogService: DialogsService,
              private utilService: UtilService,
              private cjService: CjService,
              private api: ApiService) {
    this.context = this;
    this.frameworkComponents = {asyncCellRenderer: AsyncCellRenderer};
    this.asyncCache = new Map();
  }

  ngOnInit(): void {
    this.gridRefreshTimerId = setInterval(() => {
      if (this.gridStale) {
        console.debug('Refreshing grid');
        this.gridApi.refreshServerSide();
        this.gridStale = false;
      }
    }, 5000);
  }

  ngOnDestroy(): void {
    this.webSocketClose();
    if (this.dataSourceSub) {
      this.dataSourceSub.unsubscribe();
    }
    if (this.gridRefreshTimerId) {
      clearInterval(this.gridRefreshTimerId);
    }
    window.removeEventListener('resize', this.sizeColumnsToFit);
  }

  onGridReady(params: GridReadyEvent) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
    this.gridApi.updateGridOptions({
      "context": this.context,
      "defaultColDef": this.defaultColDef,
      "serverSideDatasource": this.dataSource,
      "columnDefs": this.colDefs
    })
    this.webSocketInit();

    window.addEventListener('resize', this.sizeColumnsToFit);
  }

  sizeColumnsToFit = () => {this.gridApi.sizeColumnsToFit()}

  onClickRequestLink(params: any) {
    console.debug(params);
  }

  onRowDoubleClicked(event: RowDoubleClickedEvent) {
  }

  onSelectedRow(event: RowSelectedEvent) {
  }

  onSocketOpen(event: Event) {
    console.debug('Activity card web socket connection opened');
    this.pingTimeout = setInterval(function(that) {
      if (that.socket) {
        if (that.socket.readyState === that.socket.OPEN) {
          console.debug('Sending ping');
          that.socket.send('__ping__');
          that.socketTimeout = setTimeout(function(that) {
            console.debug('Activity card web socket timed out');
            that.webSocketClose();
            console.debug('Activity card getting another web socket');
            that.webSocketInit();
          }, 5000, that);
          console.debug('Set timeout waiting for pong', that.socketTimeout);
        }
    }}, 30000, this);
  }

  onSocketMessage(event: MessageEvent<Blob | string>) {
    console.debug('received message', event);
    if (event && event.data) {
      if (event.data === '__pong__') {
        console.debug('Clearing socket timeout', this.socketTimeout);
        if (this.socketTimeout) {
          clearTimeout(this.socketTimeout);
          this.socketTimeout = null;
        }
        return;
      }
      this.gridStale = true;
    }
  }

  getRowId(params) {
    return params.data.id;
  };

  onSocketClose(event: CloseEvent) {
    console.debug('Activity card web socket connection closed');
    if (this.wsSub) {
      this.wsSub.unsubscribe();
    }
    if (!this.webSocketClosed) {
      this.webSocketInit();
    }
  }

  onSocketError(event: Event) {
    console.error('Activity card web socket error:', event);
  }

  private webSocketInit() {
    if (!this.wsSub || this.wsSub.closed) {
      this.wsSub = this.api.getWebSocket('desktopobjectactionslistener?excludecode=hea-get').subscribe(ws => {
        this.socket = ws;
        this.socket.onopen = this.onSocketOpen.bind(this);
        this.socket.onmessage = this.onSocketMessage.bind(this);
        this.socket.onclose = this.onSocketClose.bind(this);
        this.socket.onerror = this.onSocketError.bind(this);
      });
    }
  }

  private webSocketClose() {
    if (this.pingTimeout) {
      clearTimeout(this.pingTimeout);
    }
    if (this.socket) {
      this.socket.close();
    }
    if (this.wsSub) {
      this.wsSub.unsubscribe();
    }
    this.webSocketClosed = true;
  }

  private resolveTemplateValue(objContainer: HEAObjectContainer<IDesktopObject>, field: string): Observable<any> {
    const cjData = objContainer.template?.find(elt => elt.name === field);
    if (!cjData) {
      return of(null);
    } else {
      if (cjData.type === 'select') {
        const cjOptionsFromUrl = (cjData.options as ICJOptionsFromURL);
        if (cjOptionsFromUrl?.href) {
          const cachedValues = this.asyncCache.get(cjOptionsFromUrl.href);
          console.debug('Using cached template values', cachedValues);
          if (cachedValues) {
            for (const c of cachedValues) {
              if (c.heaObject[cjOptionsFromUrl.value] === objContainer.heaObject[field]) {
                return of(c.heaObject[cjOptionsFromUrl.text])
              }
            }
            return of(null);
          } else {
            console.log('Going to the server for template values', cjOptionsFromUrl.href);
            return this.cjService.getResource(cjOptionsFromUrl.href).pipe(map(resp => {
              this.asyncCache.set(cjOptionsFromUrl.href, resp);
              for (const c of resp) {
                if (c.heaObject[cjOptionsFromUrl.value] === objContainer.heaObject[field]) {
                  return c.heaObject[cjOptionsFromUrl.text]
                }
              }
              return null;
            }));
          }
        }
      }
    }
  }
}
