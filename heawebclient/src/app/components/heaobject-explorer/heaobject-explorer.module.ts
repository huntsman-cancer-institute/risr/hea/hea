import {HeaobjectExplorerComponent} from './heaobject-explorer.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NavigationModule} from '@huntsman-cancer-institute/navigation';
import {NgModule} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {DateModule, InlineModule} from '@huntsman-cancer-institute/input';
import {MatLegacySelectModule as MatSelectModule} from '@angular/material/legacy-select';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatLegacyListModule as MatListModule} from '@angular/material/legacy-list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatLegacyMenuModule as MatMenuModule} from '@angular/material/legacy-menu';
import {MatLegacyDialogModule as MatDialogModule} from '@angular/material/legacy-dialog';
import {MatLegacyInputModule as MatInputModule} from '@angular/material/legacy-input';
import {MatLegacyButtonModule as MatButtonModule} from '@angular/material/legacy-button';
import {MatLegacyCardModule as MatCardModule} from '@angular/material/legacy-card';
import {DialogsModule} from '../util/dialog/dialogs.module';
import {ManageProjectDialogComponent} from './recent-projects/manage-project-dialog.component';
import {MatLegacyTabsModule as MatTabsModule} from '@angular/material/legacy-tabs';
import {TooltipExtractorModule} from '../../directives/tooltip-extractor.module';
import {UserCardComponent} from './cards/user-card.component';
import {AgGridModule} from 'ag-grid-angular';
import {ActivityCardComponent} from './cards/activity-card.component';
import {AgGridRendererModule} from '../util/grid-renderers/ag-grid-renderer.module';
import {UsageCardComponent} from './cards/usage-card.component';
import {ChartsModule} from 'ng2-charts';
import {MatLegacyRadioModule as MatRadioModule} from '@angular/material/legacy-radio';
import {MatLegacySlideToggleModule as MatSlideToggleModule} from '@angular/material/legacy-slide-toggle';
import {AccountTreeNavComponent} from './account-tree-nav/account-tree-nav.component';
import {RecentProjectsComponent} from './recent-projects/recent-projects.component';
import {ExplorerHomeComponent} from './explorer-home/explorer-home.component';
import {AuthGuard} from '../../services/auth/auth.guard';
import {MiscModule} from '@huntsman-cancer-institute/misc';
import {MatLegacyProgressSpinnerModule as MatProgressSpinnerModule} from '@angular/material/legacy-progress-spinner';
import {DatePipe} from '@angular/common';
import {UtilModule} from "../util/util.module";
import { TreeModule } from '@circlon/angular-tree-component';
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import { IntroComponent } from './intro/intro';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '', component: HeaobjectExplorerComponent, canActivate: [AuthGuard],
                children: [
                    {path: '', redirectTo: 'explorer-home', pathMatch: 'full'},
                    {path: 'explorer-home', component: ExplorerHomeComponent, canActivate: [AuthGuard]},
                ]
            }
        ]),
        CommonModule,
        FormsModule,
        NgbModule,
        DateModule,
        InlineModule,
        MatListModule,
        MatIconModule,
        MatSelectModule,
        MatToolbarModule,
        MatGridListModule,
        MatMenuModule,
        MatDialogModule,
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        NavigationModule,
        DialogsModule,
        MatTabsModule,
        TooltipExtractorModule,
        AgGridModule,
        AgGridRendererModule,
        ChartsModule,
        MatSlideToggleModule,
        MatRadioModule,
        MiscModule,
        MatProgressSpinnerModule,
        UtilModule,
        TreeModule,
        MatButtonToggleModule
    ],
  providers: [
    AuthService,
    DatePipe
  ],
  declarations: [
    ExplorerHomeComponent,
    RecentProjectsComponent,
    HeaobjectExplorerComponent,
    AccountTreeNavComponent,
    ManageProjectDialogComponent,
    UserCardComponent,
    ActivityCardComponent,
    UsageCardComponent,
    IntroComponent
  ],
})
export class HeaobjectExplorerModule {}
