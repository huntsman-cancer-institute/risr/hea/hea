import {Component} from "@angular/core";

@Component({
  selector: "cb-intro",
  template: `
    <div class="intro-container">
      <div class="mb-3" style="color: #505659">
        <h3 class="welcome-statement d-flex" style="justify-content: center">Welcome to CORE Browser</h3>
            CORE Browser (Comprehensive Oncology Research Environment Browser), developed and maintained by RISR, is a
            web-based research data desktop. This application’s robust design creates a hassle-free central hub of
            files located across multiple platforms, including cloud-based storage. Users can view, manage, store,
            share, and organize those files without the need of using other applications. With CORE Browser’s support
            of Amazon Simple Storage Service (AWS S3), power users have access to AWS’s Command line interface (CLI).
            We plan to expand cloud-based storage support in future releases. Partnered with HCI’s Cancer Bioinformatics
            (CBI), researchers can easily manage their HCI High-Throughput Genomics (HTG) genomic experimentation and
            bioinformatic analysis files. For details, see the <a style="color: #0000ff;" target="_blank"
            href="https://risr.hci.utah.edu/appinfo/clinical-research-apps/core-browser.html">CORE Browser documentation</a>
            on the RISR website.
      </div>
      <div class="mb-3" style="color: #505659">
        <p>More Information</p>
        <ul>
          <li>If you are looking for application support, submit a request to the
            <a style="color: #0000ff;" target="_blank" href="https://ri-jira.hci.utah.edu/servicedesk/customer/portal/9/create/439">RISR Help Center</a>.</li>
          <li>Watch for CORE Browser information sessions and training offered by RISR and CBI, coming soon!</li>
        </ul>
      </div>

      <div class="d-flex" style="justify-content: space-between; color:#000000;">
        <div style="width: 100%; padding-left: 5%; padding-right: 5%;">
          <div style="font-size:14px; color:#006699">Interfaces</div>
          <div style="font-size:12px; padding-left: 1em;">
            In the future, CORE Browser will interface with the CORE and GNomEx applications for seamless access to
            sequencing and specimen data.
          </div>
        </div>

        <div style="width: 100%; padding-left: 5%; padding-right: 5%; font-size: 12px">
          <div style="font-size:14px; color:#006699;">Requirements</div>
          <div style="width: 100%; padding-left: 1em;">
            <div>Google Chrome (latest)</div>
            <div>Microsoft Edge (latest)</div>
            <div>Safari (latest)</div>
          </div>
          <br>
          <div style="width: 100%;">
            <div style="font-size:14px; color:#006699">Services Available</div>
            <div style="padding-left: 1em;">Training</div>
            <div style="padding-left: 1em;">Technical Support</div>
            <div style="padding-left: 1em;">Application Configuration</div>
            <div style="padding-left: 1em;">Application Development</div>
            <!--<div style="padding-left: 1em;">Query and Report Generation</div>-->
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .intro-container {
      width: 50%;
      margin-left: 25%;
      padding: 1.5rem;
      background-image: linear-gradient(#e8f8ff, #ffffff);
      border: solid #000000 1px; margin-top: 5%;
      border-radius: 5px;
      box-shadow: 0px 0px 10px #505659;
    }
    .welcome-statement {
      padding-top: 1rem;
      padding-bottom: 1rem;
    }
  `]
})
export class IntroComponent {

}
