import {
  ChangeDetectorRef,
  Component, ComponentFactoryResolver,
  ElementRef,
  HostBinding,
  OnDestroy,
  OnInit,
  Renderer2,
} from '@angular/core';
import {AuthService} from 'src/app/services/auth/auth.service';
import {
  NavComponent,
  NavigationGlobalService,
  NavigationService,
} from '@huntsman-cancer-institute/navigation';
import { Subscription } from 'rxjs';
import { OrgService } from 'src/app/services/org/org.service';

@Component({
  selector: 'cb-heaobject-explorer',
  template: `
      <div *ngIf="auth.userProfile | async as profile" class="outlet-row">
          <div class="d-flex flex-row h-100 w-100">
              <cb-account-tree-nav *ngIf="orgService.selectedOrgValue"></cb-account-tree-nav>
              <router-outlet></router-outlet>
          </div>
      </div>
  `,
  styles: [`
      ::ng-deep .theme-core-default .sidebar-splitter {
          background-color: #dee2e6;
      }

      ::ng-deep .hci-sidebar .bottom-of-sidebar {
          background-color: transparent;
      }
  `],
  providers: [
    NavigationService,
    NavigationGlobalService
  ],
})
export class HeaobjectExplorerComponent extends NavComponent implements OnInit, OnDestroy {

  @HostBinding('class') classList: string = 'outlet-column';

  isSelected: boolean = false;
  selectedId: string;
  private sub: Subscription;

  constructor(elementRef: ElementRef,
              renderer: Renderer2,
              resolver: ComponentFactoryResolver,
              navigationService: NavigationService,
              changeDetectorRef: ChangeDetectorRef,
              public auth: AuthService,
              public orgService: OrgService) {
    super(elementRef, renderer, resolver, navigationService, changeDetectorRef);
  }

  ngOnInit() {
    this.sub = this.auth.userProfileSubject.subscribe((user) => {
      console.debug('authenticated');
    });
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

}
