import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {
  SearchTreeComponent, SearchTreeControllerComponent,
  SidebarComponent
} from '@huntsman-cancer-institute/navigation';
import {ITreeOptions, TreeComponent, TreeModel, TreeNode} from '@circlon/angular-tree-component';
import {ActivatedRoute, Router} from '@angular/router';
import {OrgService} from '../../../services/org/org.service';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {HEAObjectContainer, IHEAObjectContainer} from '../../../models/heaobject-container.model';
import {AccountTreeNavService} from '../../../services/account-tree-nav.service';
import {UtilService} from '../../../services/util.service';
import {ITreeNode} from '@circlon/angular-tree-component/lib/defs/api';
import {DesktopObject, IDesktopObject} from '../../../models/heaobject.model';
import {CjService} from '../../../services/cj/cj.service';
import { IconService } from 'src/app/services/icon/icon.service';
@Component({
  selector: 'cb-account-tree-nav',
  templateUrl: './account-tree-nav.component.html',
  styles: [`
    :host {
      min-height: 0px;
      display: flex;
      flex-direction: column;
      flex-shrink: 1;
    }

    .treeBox {
      min-height: 0px;
      display: flex;
      flex-direction: column;
      flex-grow: 1;
      flex-shrink: 1;
    }

    .searchTree {
      height: 100%;
    }
    .scrollable {
      overflow-y: auto;
      flex-shrink: 1;
      flex-direction: column;
      min-height: 7rem;
    }

    .single-line {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

  ::ng-deep #search-title div div {
      margin-left: auto !important;
    }

  `]
})
export class AccountTreeNavComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('sidebar', { static: true } ) private sidebar: SidebarComponent;
  @ViewChild('itemTemplate', { read: TemplateRef, static: true }) private itemTemplate: any;
  @ViewChild('searchInfo', { read: TemplateRef, static: true } ) private searchInfoTemplate: TemplateRef<any>;

  private sidebarSize = 325;
  private treeOptions: ITreeOptions;
  private treeNavSub: Subscription = new Subscription();

  constructor( private orgService: OrgService,
               private treeNavService: AccountTreeNavService,
               private route: ActivatedRoute,
               private router: Router,
               private cjService: CjService,
               public iconService: IconService) { }

  ngOnInit() {
    this.setup();

    this.treeNavSub = this.treeNavService.invokeTreeDataSource().subscribe(roots => {
      console.debug('nav tree roots', roots);
      if (roots) {
        this.treeNavService.treeData.next(this.treeNavService.toTreeNodes(roots, true));
        this.treeNavService.treeDataLoading.next(false);
      } else {
        this.treeNavService.treeDataLoading.next(false);
      }
    }, err => {
      this.treeNavService.treeDataLoading.next(false);
    });
  }

  onUpdateData(event: any, tree: TreeComponent) {
    const treeModel: TreeModel = event.treeModel;
    const roots: TreeNode[] = treeModel.roots;
    for (const r of roots){
      r.expand();
    }
  }

  getAllExpandedNodeIDs(node: TreeNode) {
    let arraywithIDs: number[] = [];
    if (node.hasChildren) {
      node.children.forEach((node1: TreeNode) => {
        arraywithIDs.push(node1.id);
        console.debug(node1);
        if (node1.isExpanded){
          this.treeNavService.expandedTreeNodeIDs.add(node1.data.id);
          arraywithIDs = arraywithIDs.concat(this.getAllExpandedNodeIDs(node1));
        }
      });
    }
    return arraywithIDs;
  }
  refreshTree(event: PointerEvent) {
    console.debug('refreshing nav tree');
    this.orgService.updateSelectedOrg(this.orgService.selectedOrgValue);
  }

  getChildren(node: ITreeNode): Promise<ITreeNode[]> {
    /* Used to lazy load nodes in concert with the hasChildren flag being set to true on the node.data */
    const obj = node.data as IHEAObjectContainer<IDesktopObject>;
    if (node.children) {
      return Promise.resolve(node.children);
    }
    return this.cjService.getActualObjectOrSelf(obj, {data: false}).pipe(mergeMap(actual => {
      return this.cjService.getDefaultOpenerAndGo<DesktopObject>(actual, UtilService.REL_AWS_CONTEXT)
        .pipe(catchError(this.treeNavService.reportError<HEAObjectContainer<DesktopObject>>))
        .pipe(map(folders => {
          return this.treeNavService.toTreeNodes(folders, node.level <= this.treeNavService.maxLevel - 1);
      }))
    })).toPromise();
  }

  advanceToExplorer(event: PointerEvent) {
    this.router.navigate(['/object-explorer', 'collections', 'default']);
  }

  ngAfterViewInit() {
  }

  ngOnDestroy(){
    if (this.treeNavSub) {
      this.treeNavSub.unsubscribe();
    }
  }

  private setup() {
    this.treeOptions = {
      idField: 'id',
      displayField: 'display_name',
      childrenField: 'children',
      useVirtualScroll: true,
      nodeHeight: 22,
      allowDrag: false,
      allowDrop: false,
      getChildren: this.getChildren.bind(this)
    };

    this.sidebar?.setConfig({
      sidebarSize: this.sidebarSize,
      sidebarSplitterWidth: 4,
      id: 'project-group-list-sidebar',
      showCollapse: false,
      children: [
        {
          id: 'search-tree-controller',
          title: 'Objects',
          type: SearchTreeControllerComponent,
          fields: [{field: 'display_name', display: 'display_name'}],
          filterByAllowedFields: ['display_name'],
          sortByAllowedFields: ['display_name'],
          buttonTemplate: this.searchInfoTemplate
        }, {
          type: SearchTreeComponent,
          id: 'account-tree',
          title: 'Accounts',
          itemTemplate: this.itemTemplate,
          dataSubject: this.treeNavService.treeData,
          pageSize: 0,
          onClick: (cmpt: SearchTreeComponent, tree: TreeComponent, event: any) => {
            this.onNodeClick(tree, event);
          },
          onNodeMoved: (cmpt: SearchTreeComponent, tree: TreeComponent, event: any) => {
            this.onNodeMoved(tree, event);
          },
          onDataUpdate: (cmpt: SearchTreeComponent, tree: TreeComponent, event: any) => {
            this.onUpdateData(event, tree);
          },
          loadingSubjects: [this.treeNavService.treeDataLoading],
          busyIcon: 'fas fa-sync fa-spin',
          searchTreeOptions: this.treeOptions,
          expandTreeNodeIDs: []
        }

      ]
    });
  }

  private onNodeClick(tree: TreeComponent, event: any) {
    const node: TreeNode =  event.node;
    // If account node level, set selected account node and refresh the detail page.
    if (node?.level <= 2) {
      node.setIsSelected(true);
      this.treeNavService.selectedAwsAccount.next(node.data);
      this.router.navigate(['./explorer-home'],
        {relativeTo: this.route, queryParamsHandling: 'merge' });
    }else{
      if (this.sidebar.children && this.sidebar.children.length > 1) {
        const treeModel: TreeModel = (this.sidebar.children[1] as SearchTreeComponent).treeComponent.treeModel;
        this.getAllExpandedNodeIDs(treeModel.getFirstRoot().parent);
      }
      const path: string[] = []
      let _node = node;
      while (_node && _node.data.uniqueId) {
        path.push(_node.data.uniqueId);
        _node = _node.parent;
      }
      path.reverse();
      console.log('path', path);
      this.router.navigate(['/object-explorer', 'collections', 'default'], {queryParams: {'initialExpansion': path}});
    }

  }

  private onNodeMoved(tree: TreeComponent, event: any) {
    console.debug('I was moved', tree);
  }
}
