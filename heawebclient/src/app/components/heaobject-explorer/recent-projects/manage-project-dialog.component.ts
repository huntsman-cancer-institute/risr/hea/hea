import {Component, HostBinding, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BaseGenericContainerDialog} from '../../util/dialog/base-generic-container-dialog';

@Component({
  selector: 'cb-manage-project',
  template: `
    <div class="d-flex flex-row h-100 w-100">

      <mat-tab-group #tabs
                     class="h-100 max-height w-100 ">
        <mat-tab class="h-100 w-100" label="General">
           <div> have a gander at this sweet component </div>
        </mat-tab>
        <mat-tab class="h-100 w-100" label="Possible Matches">
          <div> then look at  this sweet component </div>
        </mat-tab>
      </mat-tab-group>

    </div>


  `,
  styles: [`
    .mat-tab-group-border {
      border: 1px solid #9b9a9a;
    }
    .mat-tab-group.flex-style mat-tab-header .mat-tab-label-container .mat-tab-list .mat-tab-labels .mat-tab-label {
      min-width: 48px;
      flex: 1;
    }
    .mat-tab-body-wrapper {
      height: 100%;
    }
  `]
})
export class ManageProjectDialogComponent extends BaseGenericContainerDialog implements OnInit{

  @HostBinding('class') classList = 'outlet-column';
  public projectList: any[] = [];
  public sortedProjectList: any[] = [];

  constructor(private router: Router) {
    super();
  }

  ngOnInit() {
    this.projectList = [{path: 's3://crosslinking/1l7304R', lastModified: '10/19/2021', description: 'something pcr testing virus'},
      {path: 's3://crosslinking/17506R', lastModified: '09/19/2021', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://crosslinking/17506R', lastModified: '09/19/2021', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://crosslinking/18624R', lastModified: '10/19/2021', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://crosslinking/11568R', lastModified: '09/19/2021', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://crosslinking/11998R', lastModified: '09/19/2021', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://novoalignments/15955R', lastModified: '06/19/2022', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://novoalignments/16196R', lastModified: '12/31/2020', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://novoalignments/16196R', lastModified: '12/31/2020', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://novoalignments/16196R', lastModified: '12/31/2020', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://novoalignments/16196R', lastModified: '12/31/2020', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://novoalignments/16196R', lastModified: '12/31/2020', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://novoalignments/16196R', lastModified: '12/31/2020', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
      {path: 's3://novoalignments/16196R', lastModified: '01/01/2021', description: 'something pcr testing virus and some ' +
          'more interesting details about copy number variant. and some more text to fill up space'},
    ];





  }


}
