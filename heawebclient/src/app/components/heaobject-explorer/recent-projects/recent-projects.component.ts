import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import { CjService } from 'src/app/services/cj/cj.service';
import { Organization, RecentlyAccessedView } from 'src/app/models/heaobject.model';
import { take, map, toArray, mergeAll } from 'rxjs/operators';
import { IconService } from 'src/app/services/icon/icon.service';
import { IconOption } from 'src/app/models/icon.model';
import { DialogOpenerService } from 'src/app/services/dialog-opener/dialog-opener.service';
import { HEAObjectContainer } from 'src/app/models/heaobject-container.model';
import { HttpParams } from '@angular/common/http';


@Component({
  selector: 'cb-recent-projects',
  template: `
    <div class="cb-card cb-card-no-margin d-flex flex-column h-100">
      <loading-overlay [busy]="dataLoading" [icon]="busyIcon"></loading-overlay>
      <div class="cb-card-header d-flex">
        <div class="ms-1"><i class="bi bi-clock-history"  style="font-size: 1.2rem"></i></div>
        <div class="flex-grow-1 ms-1">Recent Projects</div>
        <div class="d-flex gap-1 me-1">
          <div (click)="refresh()" title="Refresh" style="cursor: pointer">
            <i class="bi bi-repeat" aria-hidden="true" style="font-size: 1.2rem"></i>
          </div>
          <div (click)="sortProjectList('desc')" [ngClass]="{cursor: projectList && projectList.length > 0, disabled: !(projectList && projectList.length > 0), activated: order === 'desc'}" title="Sort by date down">
            <i class="bi bi-sort-down-alt "style="font-size: 1.2rem"></i>
          </div>
          <div (click)="sortProjectList('asc')" [ngClass]="{cursor: projectList && projectList.length > 0, disabled: !(projectList && projectList.length > 0), activated: order === 'asc'}" title="Sort by date up">
            <i class="bi bi-sort-up" style="font-size: 1.2rem"></i>
          </div>
        </div>
      </div>
      <div class="cb-card-sub no-padding overflow-auto">
        <div *ngFor="let project of sortedProjectList" class="cb-card d-flex flex-column">
          <div class="cb-card-subheader d-flex justify-content-between flex-wrap">
            <div class="cursor" [innerHTML]="project.icon" title="Open in Object Explorer" (click)="goToRecentProject($event, project.view)"></div>
            <div class="flex-grow-1 ms-1 cursor" title="Open in Object Explorer" (click)="goToRecentProject($event, project.view)">{{project.displayName}}</div>
            <div class="padded-left-right" title="Accessed {{project.modified.toLocaleString()}}">{{ project.modified ? 'Accessed ' + project.modified.toLocaleDateString() : ''}}</div>
            <div (click)="launchManageProjectDialog($event, project.view)" class="cursor" title="Properties">
              <i class="bi bi-info-circle-fill"  style="font-size: 1rem"></i>
            </div>
          </div>
          <div class="cb-card-body font-sm">
            <pre>{{project.description ? project.description.substring(0, 100) : 'No description found.'}}</pre>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .activated {
      color: var(--pure-yellow);
    }

    .activated .icon-color {
      color: (--pure-yellow);
    }
  `]
})
export class RecentProjectsComponent implements OnInit, OnDestroy {
  // Giving the top element of the card the "display: flex" property makes them unable to have a max height, which makes them inteeract... oddly with scrollbars and ag-grids.
  // @HostBinding('class') classList = 'outlet-column';

  projectList: any[] = [];
  sortedProjectList: any[] = [];
  dataLoading = [true];
  busyIcon = 'fas fa-sync fa-spin';
  order = 'desc';
  @Input() organization: HEAObjectContainer<Organization>;

  constructor(private cjService: CjService,
              private iconService: IconService,
              private dialogOpener: DialogOpenerService,
              private router: Router) {
  }

  async ngOnInit(): Promise<void> {
    const count = 10;
    let begin = 0;
    let end = count;
    this.projectList.length = 0;
    while (this.projectList.length < count) {
        await this.cjService.getResource<RecentlyAccessedView>(`organizations/${this.organization.heaObject.id}/recentlyaccessed/bytype/heaobject.project.AWSS3Project?begin=${begin}&end=${end}`).pipe(mergeAll()).pipe(map((view, i) => {
          let params = new HttpParams();
          for (const pathPart of view.heaObject.context_dependent_object_path || []) {
            params = params.append('path', pathPart);
          }
          const displayName = view.heaObject.display_name;
          const modifiedStr = view.heaObject.accessed;
          const modified = modifiedStr ? new Date(modifiedStr) : null;
          const description = view.heaObject.description;
          const projectItem = {
            icon: this.iconService.getIconForHEAObject(view.heaObject, [IconOption.LARGE]),
            displayName: displayName,
            modified: modified,
            description: description ? description : 'No description found.',
            view: view
          }
          this.projectList.push(projectItem);
          return view;
          })).pipe(toArray()).pipe(take(1)).subscribe(tuples => {
              this.sortProjectList();
              this.dataLoading = [false];
            }, error => {
            this.dataLoading = [false];
            });

        if (this.projectList.length === begin) {
          break;
        }
        begin = this.projectList.length;
        end = begin + count;
    };
  }

  ngOnDestroy(): void {
  }

  launchManageProjectDialog(event: MouseEvent, view: HEAObjectContainer<RecentlyAccessedView>): void {
    let params = new HttpParams();
    for (const pathPart of view.heaObject.context_dependent_object_path || []) {
      params = params.append('path', pathPart);
    }
    this.cjService.getActualObjectOrSelf(view, {params: params}).pipe(take(1)).subscribe(resp => {
      this.dialogOpener.openModal(resp.href, 'Edit', n => {
        if (n) {
          this.refresh();
        }
      }, {params: params});
    });
  }

  goToRecentProject(event: Event, container: HEAObjectContainer<RecentlyAccessedView>) {
    console.debug('going to', container);
    const path: string[] = container.heaObject.context_dependent_object_path;
    if (path) {
      path.push(container.uniqueId);
      this.router.navigate(['/object-explorer', 'collections', 'default'], {queryParams: {'initialExpansion': path}});
    }
  }

  refresh(): void {
    this.dataLoading = [true];
    this.ngOnInit().then();
  }

  sortProjectList(order: string = 'desc', limit: number = 10){
    console.debug('sorting', order, this.projectList);
    this.projectList.sort((obj1: any, obj2: any) => {
      let obj1Time = obj1.modified ? obj1.modified.getTime() : null;
      let obj2Time = obj2.modified ? obj2.modified.getTime() : null;
      if (!obj1Time || !obj2Time) {
        obj1Time = obj1.displayName || '';
        obj2Time = obj2.displayName || '';
        if (order === 'desc') {
          return obj2Time.localeCompare(obj1Time);
        } else {
          return obj1Time.localeCompare(obj2Time);
        }
      } else {
        if (order === 'desc') {
          return obj2Time - obj1Time;
        } else {
          return obj1Time - obj2Time;
        }
      }

    });
    const rangeLimit: number[] =  [...Array(this.projectList.length > limit ? limit : this.projectList.length).keys()];
    this.sortedProjectList = [];
    for (const i of rangeLimit){
      this.sortedProjectList.push(this.projectList[i]);
    }
    this.order = order;
  }
}
