import {Component, HostBinding, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { OrgService } from 'src/app/services/org/org.service';

@Component({
  selector: 'cb-explorer-home',
  template: `
    <div *ngIf="orgService.selectedOrgValue" class="d-flex flex-row h-100 w-100 padded background overflow-auto">
      <div class="flex-grow-1 padded">
        <div class="d-inline-block h-100 mh-100 w-100 overflow-auto">
          <cb-recent-projects [organization]="orgService.selectedOrgValue"></cb-recent-projects>
        </div>
      </div>
      <div class=" h-100 d-flex flex-column w-50 foreground">
        <div class="overflow-auto d-flex flex-column flex-grow-1">
          <div class="h-25">
            <cb-users-card></cb-users-card>
          </div>
          <div class="h-25">
            <cb-activity-card></cb-activity-card>
          </div>
          <div class="h-50">
            <cb-usage-card></cb-usage-card>
          </div>
        </div>
      </div>
    </div>
    <div class="padded" *ngIf="!orgService.selectedOrgValue"><cb-intro></cb-intro></div>
  `,
  styles: [`
    div.cb-header-detail {
      background-color: #9d9a9a;
      color: #fff;
    }

    .background {
      background-color: var(--white-darkest);
    }
  `]
})
export class ExplorerHomeComponent implements OnInit {
  @HostBinding('class') classList: string = 'outlet-column';

  constructor(private router: Router, public orgService: OrgService) {
  }

  ngOnInit(): void {
  }

}
