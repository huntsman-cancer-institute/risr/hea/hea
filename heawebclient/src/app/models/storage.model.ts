import { AWSS3Storage } from "./heaobject.model";

export interface IUsageSummary {
  accTotalSize: number;
  accObjCount: number;
  accAvgObjSize: number;
}

export interface IStorageSummary {
  accountId: string;
  calDateTime: string;
  accUsageSum: IUsageSummary[];
  storageData: AWSS3Storage[];
}
