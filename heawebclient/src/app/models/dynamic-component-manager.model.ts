import {DynamicComponent} from './dynamic-component.model';
import {IComponentSpec} from './interpreted-contents.model';
import {BehaviorSubject} from 'rxjs';
import {ComponentRef} from '@angular/core';
import {UntypedFormGroup} from '@angular/forms';
import * as _ from 'lodash-es';
import {ICJData} from './cj.model';

export class DynamicComponentManager {

  private componentMap = {};
  private componentSpecsInOrder: IComponentSpec[] = [];
  private componentsInOrder: DynamicComponent[] = [];
  private sections: string[] = [];

  constructor() {}

  static componentSpecMapKey(name: string, section?: string, index?: number): string {
    return `${section || ''}|${(index || index === 0) ? index : ''}|${name || ''}`;
  }

  clone(componentSpec: IComponentSpec, index: number): IComponentSpec {
    const temp: IComponentSpec = {
        disabled: false,
        componentType: componentSpec.componentType,
        name: componentSpec.name,
        section: componentSpec.section,
        sectionPrompt: componentSpec.sectionPrompt,
        inputs: _.clone(componentSpec.inputs),
        outputs: {},
        componentCreated: (event: ComponentRef<DynamicComponent>) => {
          this.componentCreated(event, temp);
        }
      };
      temp.inputs.paramName = DynamicComponentManager.componentSpecMapKey(componentSpec.name,
        componentSpec.inputs.section, componentSpec.inputs.index);
      temp.inputs.index = index;
    let inSection = false;
    let i = 0;
    for (; i < this.componentSpecsInOrder.length; i++) {
      const c = this.componentSpecsInOrder[i];
      if (c.section === temp.section) {
        inSection = true;
      } else if (inSection) {
        break;
      }
    }
    this.componentSpecsInOrder.splice(i, 0, temp);
    return temp;
  }

  build(cjData: ICJData,
        componentType: new (...args: any[]) => DynamicComponent,
        moreInputs?: any,
        formGroup?: UntypedFormGroup): IComponentSpec {
    let temp: IComponentSpec;
    if (cjData) {
      temp = {
        disabled: false,
        componentType: componentType,
        name: cjData.name,
        section: cjData.section,
        sectionPrompt: cjData.sectionPrompt,
        inputs: {
          title: cjData.prompt,
          data: cjData.value,
          containerFormGroup: formGroup,
          readOnly: cjData.readOnly,
          required: cjData.required,
          name: cjData.name,
          index: cjData.index,
          section: cjData.section,
          paramName: DynamicComponentManager.componentSpecMapKey(cjData.name, cjData.section, cjData.index),
          display: cjData.display !== null && typeof cjData.display !== 'undefined' ? cjData.display: true,
          type: cjData.type
        },
        outputs: {},
        componentCreated: (event: ComponentRef<DynamicComponent>) => {
          this.componentCreated(event, temp);
        }
      };
      _.extend(temp.inputs, moreInputs);
    }
    this.componentSpecsInOrder.push(temp);
    return temp;
  }

  get(name: string, section?: string, index?: number): DynamicComponent {
    return this.componentMap[DynamicComponentManager.componentSpecMapKey(name, section, index)];
  }

  getAll(): DynamicComponent[] {
    return this.componentsInOrder;
  }

  remove(name: string, section?: string, index?: number) {
    delete this.componentMap[DynamicComponentManager.componentSpecMapKey(name, section, index)];
    let hasSection = false;
    for (let i = 0; i < this.componentsInOrder.length; i++) {
      const c = this.componentsInOrder[i];
      if (c.name === name && (section ? c.section === section : true) && ((index || index === 0) ? c.index === index : true)) {
        this.componentsInOrder.splice(i, 1);
        i++;
      } else if (section) {
        hasSection = true;
      }
    }
    if (!hasSection) {
      this.sections.splice(this.sections.indexOf(section), 1);
    }
  }

  removeAll() {
    this.clear();
  }

  clear() {
    Object.keys(this.componentMap).forEach(key => delete this.componentMap[key]);
    this.componentSpecsInOrder.length = 0;
    this.componentsInOrder.length = 0;
    this.sections.length = 0;
  }

  private componentCreated(event: ComponentRef<DynamicComponent>, temp: IComponentSpec) {
    this.componentMap[temp.inputs.paramName] = event.instance;  /* Always in order of creation, so won't work for insertions */
    if (temp.section) {
      const sectionNames = new Set<string>();
      for (const c of this.componentSpecsInOrder) {
        if (event.instance.section === c.section && event.instance.name === c.name) {
          break;
        }
        sectionNames.add(JSON.stringify({section: c.section, name: c.name}));
      }
      const index = this.sections.indexOf(temp.section);
      let i = 0;
      if (index >= 0) {
        let inSection = false;
        for (; i < this.componentsInOrder.length; i++) {
          const c = this.componentsInOrder[i];
          if (c.section === temp.section) {
            inSection = true;
            if (index < c.index) {
              break;
            }
          } else if (inSection) {
            break;
          }
        }
      } else {
        for (; i < this.componentsInOrder.length; i++) {
          const c = this.componentsInOrder[i];
          if (!sectionNames.has(JSON.stringify({section: c.section, name: c.name}))) {
            break;
          }
        }
        this.sections.push(temp.section);
      }
      this.componentsInOrder.splice(i, 0, event.instance);
    } else {
      this.componentsInOrder.push(event.instance);
    }
  }
}
