import {UntypedFormGroup} from '@angular/forms';
import {BehaviorSubject} from 'rxjs';
import {DynamicComponent} from './dynamic-component.model';
import {ComponentRef} from '@angular/core';
import {DynamicComponentManager} from './dynamic-component-manager.model';
import {HEAObjectContainer} from './heaobject-container.model';

export interface IComponentSpecInputs {
  title?: string;
  data?: any;
  containerFormGroup: UntypedFormGroup;
  readOnly?: boolean;
  required?: boolean;
  name?: string;
  index?: number;
  section?: string;
  paramName: string;
  display?: boolean;
  type?: string;
}

export interface IComponentSpec {
  disabled: boolean;
  componentType: new (...args: any[]) => DynamicComponent;
  name?: string;
  section?: string;
  sectionPrompt?: string;
  inputs: IComponentSpecInputs;
  outputs: {};
  componentCreated: (event: ComponentRef<DynamicComponent>) => void;
}

export interface IComponentSpecContainerInputs extends IComponentSpecInputs {
  subComponentSpecs: IComponentSpec[];
  dynamicComponentManager: DynamicComponentManager;
}

export interface IComponentSpecContainer extends IComponentSpec {
  inputs: IComponentSpecContainerInputs;
}

export interface ISectionComponentSpecs {
  section: string;
  componentSpecs: IComponentSpec[];
}

export interface IInterpretedContentsData {
  inHEAObjectContainer: HEAObjectContainer<any>;
}
