export interface ICJDataExtensions {
  cardinality?: string;

}

export interface ICJOption {
  value: string,
  text: string
}

export interface ICJOptionsFromURL {
  href: string,
  value: string,
  text: string
}

export interface ICJData {
  section?: string;
  sectionPrompt?: string;
  name: string;
  value: string | string[];
  prompt?: string;
  display?: boolean;
  type?: string;
  index?: number;
  readOnly?: boolean;
  required?: boolean;
  cardinality?: string;
  options?: ICJOption[] | ICJOptionsFromURL;
  objectUrl?: string;
  objectUrlTargetTypesInclude?: string[];
}

export interface ICJLink {
  display: string;
  prompt: string;
  rel: string;
  href: string;
}

export interface ICJItem {
  href?: string;
  data: ICJData[];
  links: ICJLink[];
}

export interface ICJTemplate {
  data: ICJData[];
  prompt?: string;
}

export interface ICJQuery {
  href: string;
  data: ICJData[];
  prompt?: string;
}

export interface ICJCollection {
  href: string;
  permissions: string[][];
  links: ICJLink[];
  items: ICJItem[];
  template?: ICJTemplate;
  queries?: ICJQuery[];
}

export interface ICJReadObject {
  collection: ICJCollection;
}

export interface ICJWriteObject {
  template: ICJTemplate;
}

