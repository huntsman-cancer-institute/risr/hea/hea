import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {UntypedFormControl, UntypedFormGroup} from '@angular/forms';

/**
 * Abstract base class for dynamic components for the interpreted contents screens. The interpreted contents
 * screens are controlled by the InterpretedContentsComponent. They are dynamically generated forms using the template
 * in a Collection+JSON document. Each component renders a form field or data entry grid.
 */
@Component({
  template: ''
})
export abstract class DynamicComponent implements OnInit {

  public readonly isMac = navigator.userAgent.toUpperCase().includes('MAC');
  public readonly modifierKey = this.isMac ? 'command' : 'control';
  public readonly modifierKeyCapitalized = this.modifierKey.charAt(0).toUpperCase() + this.modifierKey.slice(1);

  @ViewChild('titleBox') titleBox: ElementRef;

  private _title = '';
  private _required = false;  // whether the required validator is currently attached.
  private _containerFormGroup: UntypedFormGroup;
  private _paramName: string;
  private _editable = true;  // whether the field is editable presently
  private __data: any
  private _display = true;
  private _type = 'text';
  @Input()
  public width: number;
  @Input()
  public name: string;
  @Input()
  public index: number;
  @Input()
  public section: string;

  public constructor() {
  }

  public ngOnInit(): void {
    const formControl = new UntypedFormControl();
    this._containerFormGroup.addControl(this.paramName, formControl);
    this.setValue(this.data);
  }

  public setValue(value: any) {
    this._containerFormGroup.get(this.paramName).setValue(value);
  }

  @Input()
  public set type(type: string) {
    this._type = '' + (type === 'datetime' ? 'datetime-local' : type);
  }

  public get type(): string {
    return this._type;
  }

  /**
   * Sets the data to display in the component.
   *
   * @param data the data to display. The type is dependent on the component.
   */
  @Input() public set data(data: any) {
    this.__data = data;
  }

  /**
   * Gets the data displayed in the component.
   *
   * @return the data in the component. The type is dependent on the component.
   */
  public get data(): any {
    return this.__data;
  }

  /**
   * Sets the component's label.
   *
   * @param title the label, a string.
   */
  @Input() public set title(title: string) {
    this._title = title;
  }

  /**
   * Gets the component's label.
   *
   * @return the label, a string.
   */
  public get title(): string {
    return this._title;
  }

  /**
   * Sets whether the component is read-only.
   *
   * @param readOnly true if read-only, false otherwise.
   */
  @Input() public set readOnly(readOnly: boolean) {
    this._editable = !readOnly;
  }

  /**
   * Gets whether the component is read-only.
   *
   * @return true if read-only, false otherwise.
   */
  public get readOnly(): boolean {
    return !this._editable;
  }

  /**
   * Sets whether the component is a required part of the form.
   *
   * @param required true if the user must enter data into the component, false otherwise.
   */
  @Input() public set required(required: boolean) {
    this._required = required;
  }

  /**
   * Gets whether the component is a required part of the form.
   *
   * @return true if the user must enter data into the component, false otherwise.
   */
  public get required(): boolean {
    return this._required;
  }

  @Input() public set containerFormGroup(formGroup: UntypedFormGroup) {
    this._containerFormGroup = formGroup;
  }

  public get containerFormGroup(): UntypedFormGroup {
    return this._containerFormGroup;
  }

  /**
   * Sets the unique name of the component in the form.
   *
   * @param paramName the component's unique name string.
   */
  @Input() public set paramName(paramName: string) {
    this._paramName = paramName;
  }

  /**
   * Gets the component's unique name in the form.
   *
   * @return the component's unique name string.
   */
  public get paramName(): string {
    return this._paramName;
  }
  @Input()
  public set display(display: boolean) {
    this._display = display;
  }

  public get display(): boolean {
    return this._display;
  }
}
