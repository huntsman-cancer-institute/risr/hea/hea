export const registeredClasses = {};
export const containers = {};

export class PermissionAssignment {
  user: string;
  permissions: string[];
}

export class Invite extends PermissionAssignment {
  accepted: boolean;
}

export class Share extends PermissionAssignment{
  invite: Invite;
}

export interface IHEAObject {
  type: string;
}

export class HEAObject implements IHEAObject {
  type: string;
}

export interface IDesktopObject extends IHEAObject {
  id?: string;
  source?: string;
  version?: string;
  name?: string;
  // tslint:disable-next-line:variable-name
  display_name: string;
  description?: string;
  owner: string;
  created?: string;
  modified?: string;
  // tslint:disable-next-line:variable-name
  derived_by?: string;
  // tslint:disable-next-line:variable-name
  derived_from: string[];
  invites: Invite[];
  shares: Share[];
}

export class DesktopObject extends HEAObject implements IDesktopObject {
  id?: string;
  source?: string;
  version?: string;
  name?: string;
  // tslint:disable-next-line:variable-name
  display_name: string;
  description?: string;
  owner: string;
  created?: string;
  modified?: string;
  // tslint:disable-next-line:variable-name
  derived_by?: string;
  // tslint:disable-next-line:variable-name
  derived_from: string[];
  invites: Invite[];
  shares: Share[];
}

export interface IView extends IDesktopObject{
  // tslint:disable-next-line:variable-name
  actual_object_type_name?: string;
  // tslint:disable-next-line:variable-name
  actual_object_id?: string;
  // tslint:disable-next-line:variable-name
  actual_object_uri?: string;
}

export class Item extends DesktopObject implements IView {

  // tslint:disable-next-line:variable-name
  folder_id?: string;
  // tslint:disable-next-line:variable-name
  volume_id?: string;
  size?: number;
  // tslint:disable-next-line:variable-name
  human_readable_size?: string;
  // tslint:disable-next-line:variable-name
  actual_object_type_name?: string;
  // tslint:disable-next-line:variable-name
  actual_object_id?: string;
  // tslint:disable-next-line:variable-name
  actual_object_uri?: string;
  // tslint:disable-next-line:variable-name
  mime_type?: string;
}
registeredClasses['heaobject.folder.Item'] = Item;

export class AWSS3Item extends Item {
  // tslint:disable-next-line:variable-name
  s3_uri?: string;
  // tslint:disable-next-line:variable-name
  bucket_id?: string;
}
registeredClasses['heaobject.folder.AWSS3Item'] = AWSS3Item;

export class AWSS3BucketItem extends AWSS3Item {}
registeredClasses['heaobject.folder.AWSS3BucketItem'] = AWSS3BucketItem;

export class AWSS3ItemInFolder extends AWSS3Item {
  key?: string;
}
export class AWSS3SearchItemInFolder extends AWSS3ItemInFolder {
  // tslint:disable-next-line:variable-name
  is_delete_marker: boolean;
  // tslint:disable-next-line:variable-name
  event_name: string;
  // tslint:disable-next-line:variable-name
  version_id: string;
  // tslint:disable-next-line:variable-name
  account_id: string;
}
registeredClasses['heaobject.folder.AWSS3SearchItemInFolder'] = AWSS3SearchItemInFolder;

registeredClasses['heaobject.folder.AWSS3ItemInFolder'] = AWSS3ItemInFolder;

export class AccountView extends DesktopObject implements IView {
}
registeredClasses['heaobject.account.AccountView'] = AccountView;

export class Activity extends DesktopObject {
  // tslint:disable-next-line:variable-name
  mime_type: string;
  // tslint:disable-next-line:variable-name
  application_id?: string;
  // tslint:disable-next-line:variable-name
  user_id?: string;
  // tslint:disable-next-line:variable-name
  requested?: Date;
  // tslint:disable-next-line:variable-name
  began?: Date;
  // tslint:disable-next-line:variable-name
  ended?: Date;
  // tslint:disable-next-line:variable-name
  status_updated: Date;
  // tslint:disable-next-line:variable-name
  duration: number;
  // tslint:disable-next-line:variable-name
  human_readable_duration: string;
  // tslint:disable-next-line:variable-name
  action?: string;
  // tslint:disable-next-line:variable-name
  status?: string;
}
registeredClasses['heaobject.activity.Activity'] = Activity;

export class Action extends Activity {
  // tslint:disable-next-line:variable-name
  status: string;
  // tslint:disable-next-line:variable-name
  code?: string;
}
registeredClasses['heaobject.activity.Action'] = Action;

export class DesktopObjectAction extends Activity {
  // tslint:disable-next-line:variable-name
  old_object_uri?: string;
  // tslint:disable-next-line:variable-name
  new_object_uri?: string;
}
registeredClasses['heaobject.activity.DesktopObjectAction'] = DesktopObjectAction;

export class RecentlyAccessedView extends DesktopObject implements IView {
  // tslint:disable-next-line:variable-name
  actual_object_type_name?: string;
  // tslint:disable-next-line:variable-name
  actual_object_id?: string;
  // tslint:disable-next-line:variable-name
  actual_object_uri?: string;
  // tslint:disable-next-line:variable-name
  accessed?: string;
  // tslint:disable-next-line:variable-name
  context_dependent_object_path?: string[];
}
registeredClasses['heaobject.activity.RecentlyAccessedView'] = RecentlyAccessedView;

export class Person extends DesktopObject {
  // tslint:disable-next-line:variable-name
  preferred_name?: string;
  // tslint:disable-next-line:variable-name
  first_name?: string;
  // tslint:disable-next-line:variable-name
  last_name?: string;
  title?: string;
  email?: string;
  // tslint:disable-next-line:variable-name
  phone_number?: string;
}
registeredClasses['heaobject.person.Person'] = Person;

export class Role extends DesktopObject {
  // tslint:disable-next-line:variable-name
  role: string;
}
registeredClasses['heaobject.person.Role'] = Role;

export class Group extends DesktopObject {
  // tslint:disable-next-line:variable-name
  group: string;
}
registeredClasses['heaobject.person.Group'] = Group;

export class DataObject extends DesktopObject {
  // tslint:disable-next-line:variable-name
  mime_type: string;
}

export class Account extends DataObject {}

export class AWSAccount extends Account {
  // tslint:disable-next-line:variable-name
  full_name?: string;
  // tslint:disable-next-line:variable-name
  phone_number?: string;
  // tslint:disable-next-line:variable-name
  alternate_contact_name?: string;
  // tslint:disable-next-line:variable-name
  alternate_contact_address?: string;
  // tslint:disable-next-line:variable-name
  alternate_phone_number?: string;
  // tslint:disable-next-line:variable-name
  email_address?: string;
}
registeredClasses['heaobject.account.AWSAccount'] = AWSAccount;

export class Organization extends DataObject {
  // tslint:disable-next-line:variable-name
  aws_account_ids: string[];
  // tslint:disable-next-line:variable-name
  principal_investigator_id?: string;
  // tslint:disable-next-line:variable-name
  admin_ids: string[];
  // tslint:disable-next-line:variable-name
  manager_ids: string[];
  // tslint:disable-next-line:variable-name
  member_ids: string[];
  // tslint:disable-next-line:variable-name
  collaborator_ids: string[];
}
registeredClasses['heaobject.organization.Organization'] = Organization;

export class Tag {
  key: string;
  value: string;
}

export interface S3Object extends IDesktopObject {
  s3_uri?: string;
  presigned_url?: string;
}

export interface S3StorageClassMixin {
  storage_class: string;
}

export class AWSBucket extends DesktopObject implements S3Object {
  // tslint:disable-next-line:variable-name
  s3_uri?: string;
  // tslint:disable-next-line:variable-name
  presigned_url?: string;
  arn?: string;
  encrypted?: boolean;
  versioned?: boolean;
  locked?: boolean;
  region?: string;
  size?: number;
  // tslint:disable-next-line:variable-name
  object_count?: number;
  tags: Tag[];
  // tslint:disable-next-line:variable-name
  permission_policy?: string;
}
registeredClasses['heaobject.bucket.AWSBucket'] = AWSBucket;

export class Folder extends DataObject {}
registeredClasses['heaobject.folder.Folder'] = Folder;

export class AWSS3Folder extends Folder implements S3Object {
  // tslint:disable-next-line:variable-name
  s3_uri?: string;
  // tslint:disable-next-line:variable-name
  presigned_url?: string;
}
registeredClasses['heaobject.folder.AWSS3Folder'] = AWSS3Folder;

export class Project extends DataObject {}
registeredClasses['heaobject.project.Project'] = Project;

export class AWSS3Project extends Project implements S3Object {
  // tslint:disable-next-line:variable-name
  s3_uri?: string;
  // tslint:disable-next-line:variable-name
  presigned_url?: string;
}
registeredClasses['heaobject.project.AWSS3Project'] = AWSS3Project;

export class DataFile extends DataObject {
  size?: number;
  // tslint:disable-next-line:variable-name
  human_readable_size?: string;
}
registeredClasses['heaobject.data.DataFile'] = DataFile;

export class AWSS3FileObject extends DataFile implements S3Object, S3StorageClassMixin {
  // tslint:disable-next-line:variable-name
  s3_uri?: string;
  // tslint:disable-next-line:variable-name
  storage_class: string;
}
registeredClasses['heaobject.data.AWSS3FileObject'] = AWSS3FileObject;

export class Storage extends DataObject {}
registeredClasses['heaobject.storage.Storage'] = Storage;

export class AWSS3Storage extends Storage {
  arn?: string;
  // tslint:disable-next-line:variable-name
  size?: number;
  // tslint:disable-next-line:variable-name
  with_storage_location?: number;
  // tslint:disable-next-line:variable-name
  object_count?: number;
  // tslint:disable-next-line:variable-name
  object_earliest_created?: Date;
  // tslint:disable-next-line:variable-name
  object_last_modified?: Date;
  // tslint:disable-next-line:variable-name
  object_min_duration?: number;
  // tslint:disable-next-line:variable-name
  object_max_duration?: number;
  // tslint:disable-next-line:variable-name
  object_average_duration?: number;
  // tslint:disable-next-line:variable-name
  human_readable_max_duration: string;
  // tslint:disable-next-line:variable-name
  human_readable_min_duration: string;
  // tslint:disable-next-line:variable-name
  human_readable_average_duration: number;
  // tslint:disable-next-line:variable-name
  object_average_size: number;
}
registeredClasses['heaobject.storage.AWSS3Storage'] = AWSS3Storage;

export class AWSS3FolderFileTrashItem extends DesktopObject {
  // tslint:disable-next-line:variable-name
  s3_uri: string;
  // tslint:disable-next-line:variable-name
  key: string;
  // tslint:disable-next-line:variable-name
  bucket_id: string;
  // tslint:disable-next-line:variable-name
  actual_object_id: string;
  // tslint:disable-next-line:variable-name
  actual_object_type_name: string
  // tslint:disable-next-line:variable-name
  original_location: string;
  // tslint:disable-next-line:variable-name
  human_readable_original_location: string;
}
registeredClasses['heaobject.trash.AWSS3FolderFileTrashItem'] = AWSS3FolderFileTrashItem;

export class S3Version extends DesktopObject {

}
registeredClasses['heaobject.aws.S3Version'] = S3Version;

export class SettingsObject extends DesktopObject implements IView {
  // tslint:disable-next-line:variable-name
  actual_object_type_name?: string;
  // tslint:disable-next-line:variable-name
  actual_object_id?: string;
  // tslint:disable-next-line:variable-name
  actual_object_uri?: string;
}
registeredClasses['heaobject.settings.SettingsObject'] = SettingsObject;

export class Collection extends DesktopObject {

}
registeredClasses['heaobject.registry.Collection'] = Collection;

export class Component extends DesktopObject {

}
registeredClasses['heaobject.registry.Component'] = Component;

export class Credentials extends DesktopObject {

}
registeredClasses['heaobject.keychain.Credentials'] = Credentials;

export class AWSCredentials extends Credentials {

}
registeredClasses['heaobject.keychain.AWSCredentials'] = AWSCredentials;

export class CredentialsView extends DesktopObject {

}
registeredClasses['heaobject.keychain.CredentialsView'] = CredentialsView;


export class Volume extends DesktopObject {

}
registeredClasses['heaobject.volume.Volume'] = Volume;


export const CONTAINER_TYPES = [
  'heaobject.folder.AWSS3Folder',
  'heaobject.bucket.AWSBucket',
  'heaobject.project.AWSS3Project',
  'heaobject.registry.Collection',
  'heaobject.account.AWSAccount',
  'heaobject.volume.Volume'
];
