import {MatLegacyDialogRef as MatDialogRef} from '@angular/material/legacy-dialog';
import {UntypedFormGroup} from '@angular/forms';

export enum ActionType {
    PRIMARY = 'primary',
    SECONDARY = 'accent'
}
export interface GDActionConfig {
    formSource?: UntypedFormGroup;
    actions: GDAction[];
}
export interface GDAction {
    type: ActionType;
    internalAction?: string;
    externalAction?: GDExternalAction;
    icon?: string;
    name: string;

}
export interface GDExternalAction {
    function: (dialogRef?: MatDialogRef<any>, formGroup?: UntypedFormGroup, params?: any) => void;
    params?: any;
}
