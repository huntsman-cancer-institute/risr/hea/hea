export class StorageClass {
  id: string;
  name: string;
  displayName: string;
  description: string;
  arn: string;
  created: string;
  modified: string;
  derivedBy: string;
  derivedFrom: string;
  invites: string;
  mimeType: string;
  minStorageDuration: string;
  objectCount: number;
  objectInitModified: string;
  objectLastModified: string;
  owner: string;
  s3Uri: string;
  shares: Share[] = [];
  source: string;
  storageBytes: number;
  storageClass: string;
  version: string;
  volumeId: string;
  maxDurationDays: number;
  minDurationDays: number;
  avgDurationDays: number;
  constructor() {
  }
}

export class Share {
  type: string;
  user: string;
  permissions: any[];
  constructor() {}
}
