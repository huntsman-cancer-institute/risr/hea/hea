import { ICJLink } from "./cj.model";

export interface Action {
  actionName(): string;
}export const DELETE_ACTION = {
  actionName() { return 'DELETE'; }
};
export const UPLOAD_ACTION = {
  actionName() { return 'UPLOAD'; }
};
export const DOWNLOAD_ACTION = {
  actionName() { return 'DOWNLOAD'; }
};
export const PROPERTIES_ACTION = {
  actionName() { return 'PROPERTIES'; }
};
export const REFRESH_SELECTED_ACTION = {
  actionName() { return 'REFRESH_SELECTED'; }
};
export const REFRESH_SELECTED_PURGE_ACTION = {
  actionName() { return 'REFRESH_SELECTED_PURGE'; }
};
export const CREATE_ACTION = {
  actionName() { return 'ADD'; }
};
export const PREVIEW_ACTION = {
  actionName() { return 'PREVIEW'; }
};
export interface IParameterizedAction extends Action {
  param: object;
}
export abstract class CJLinkAction {
  private _param: ICJLink;

  constructor(link: ICJLink) {
    this._param = link;
  }

  get param(): ICJLink {
    return this._param;
  }
}

export class DynamicStandardActionWithCJLink extends CJLinkAction implements IParameterizedAction {

  actionName(): string {
    return 'DYNAMIC_STANDARD';
  }
}

export class DynamicClipboardActionWithCJLink extends CJLinkAction implements IParameterizedAction {

  actionName(): string {
    return 'DYNAMIC_CLIPBOARD';
  }
}

export class TrashActionWithCJLink extends CJLinkAction implements IParameterizedAction {

  actionName(): string {
    return 'TRASH';
  }
}

export class VersionsActionWithCJLink extends CJLinkAction implements IParameterizedAction {

  actionName(): string {
    return 'VERSIONS';
  }
}

export class OpenActionWithCJLink extends CJLinkAction implements IParameterizedAction {

  actionName(): string {
    return 'OPEN';
  }
}

export interface ActionStates {
  highlight: boolean;
  disabled: boolean;
  activated: boolean;
}
export enum Card {
  DELETE = 'DELETE',
  UPLOAD = 'UPLOAD',
  DOWNLOAD = 'DOWNLOAD',
  PROPERTIES = 'PROPERTIES',
  REFRESH_SELECTED_PURGE = 'REFRESH_SELECTED_PURGE',
  REFRESH_SELECTED = 'REFRESH_SELECTED',
  ADD = 'ADD',
  OPEN = 'OPEN',
  DYNAMIC_CLIPBOARD = 'DYNAMIC_CLIPBOARD',
  DYNAMIC_STANDARD = 'DYNAMIC_STANDARD',
  TRASH = 'TRASH',
  VERSIONS = 'VERSIONS',
  PREVIEW = 'PREVIEW',

}

