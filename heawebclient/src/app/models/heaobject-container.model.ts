import {CONTAINER_TYPES, IDesktopObject, IView} from './heaobject.model';
import {ICJData, ICJLink, ICJQuery} from './cj.model';
import * as _ from 'lodash-es';

export const UPDATER_PERMS = ['EDITOR', 'COOWNER'];
export const CREATOR_PERMS = ['CREATOR', 'COOWNER'];
export const DELETER_PERMS = ['DELETER', 'COOWNER'];


export interface IHEAObjectContainer<H extends IDesktopObject> {
  heaObject: H;
  template?: ICJData[];
  templatePrompt?: string;
  queries?: ICJQuery[];
  links: ICJLink[];
  parent: string;
  href: string;
  uniqueId: string;
  children?: IHEAObjectContainer<IDesktopObject>[];
  path?: string[];
  permissions?: string[];

  getTemplate(method: 'POST' | 'PUT' | 'GET'): ICJData[];
  isDeleteForbidden(): boolean;
  isPostForbidden(): boolean;
  isPutForbidden(): boolean;
  isFormActionForbidden(method: 'POST' | 'PUT' | 'GET'): boolean;
}

export class HEAObjectContainer<H extends IDesktopObject> implements IHEAObjectContainer<H> {
  heaObject: H;
  template?: ICJData[];
  templatePrompt?: string;
  queries?: ICJQuery[];
  links: ICJLink[];
  parent: string;
  href: string;
  uniqueId: string;
  children?: IHEAObjectContainer<IDesktopObject>[];
  path?: string[];
  permissions?: string[];

  getTemplate(method: 'POST' | 'PUT' | 'GET'): ICJData[] {
    const readOnly = this.isFormActionForbidden(method);
    console.debug('is readOnly', readOnly, method, this.permissions);
    if (!this.template) {
      return [];
    } else if (readOnly) {
      return this.template.map(e => {const e_ = _.cloneDeep(e); e_.readOnly = true; return e_;})
    } else {
      return _.cloneDeep(this.template);
    }
  }

  isDeleteForbidden(): boolean {
    return !this.permissions.some(perm => DELETER_PERMS.includes(perm));
  }

  isPostForbidden(): boolean {
    return this.isFormActionForbidden('POST');
  }

  isPutForbidden(): boolean {
    return this.isFormActionForbidden('PUT');
  }

  /**
   * Checks whether an action attempted using a form is forbbiden.
   *
   * @param method the HTTP method to check.
   * @returns
   */
  isFormActionForbidden(method: 'POST' | 'PUT' | 'GET'): boolean {
    if (this.template && this.template.length > 0 && this.template.every(val => val.readOnly)) {
      return true;
    }
    if (!this.permissions) {
      return false;
    }
    switch (method) {
      case 'POST':
        return false;
      case 'PUT':
        return !this.permissions.some(perm => UPDATER_PERMS.includes(perm));
      case 'GET':
        return false;
      default:
        throw Error(`Invalid method ${method}`);
    }
  }
}

export function isContainerType(obj: IHEAObjectContainer<IDesktopObject>): boolean {
  return CONTAINER_TYPES.includes(obj.heaObject.type)
}

export function isActualObjectContainerType(obj: IHEAObjectContainer<IView>): boolean {
  return CONTAINER_TYPES.includes(obj.heaObject.actual_object_type_name);
}

/**
 * Converts an HEA object's URL to the unique id of an object container on the HEA desktop.
 *
 * @param url the HEA object's URL.
 * @returns its unique id.
 */
export function urlToUniqueId(url: string | URL): string {
  return btoa(url.toString());
}

/**
 * Converts an HEA object's URL to the unique id of an object container on the HEA desktop, or if the argument is an
 * IHEAObjectContainer, returns its unique id.
 * @param url
 * @returns
 */
export function toUniqueId(url: string | URL | IHEAObjectContainer<IDesktopObject>): string {
  if (typeof url === 'string' || url instanceof URL) {
    return urlToUniqueId(url);
  } else {
    return url.uniqueId;
  }
}

/**
 * Converts the unique id of an object container on the HEA desktop to its URL.
 *
 * @param uniqueId the object's unique id.
 * @returnsa a URL.
 */
export function uniqueIdToURL(uniqueId: string): URL {
  return new URL(atob(uniqueId));
}
