import {Component, HostBinding, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { AuthService } from './services/auth/auth.service';
import { Subscription } from 'rxjs';
import { AppHeaderComponent, LiDdNavComponent, LiNavComponent, TemplateComponent, UlNavComponent
} from '@huntsman-cancer-institute/navigation';
import {OrgService} from './services/org/org.service';
import {DesktopObject, HEAObject, IDesktopObject, IHEAObject, Item} from './models/heaobject.model';
import {HEAObjectContainer, IHEAObjectContainer, urlToUniqueId} from './models/heaobject-container.model';
import {MatSidenav} from '@angular/material/sidenav';
import {Router} from '@angular/router';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { map, mergeAll, mergeMap, take } from 'rxjs/operators';
import { CjService } from './services/cj/cj.service';
import { DialogOpenerService } from './services/dialog-opener/dialog-opener.service';
import { UsersService } from './services/users/users.service';
import { IconService } from './services/icon/icon.service';
import {environment} from "../environments/environment";
import packageJson from '../../package.json';  // Be careful not to change prod mode such that the entire package.json is served to the browser!!!
import { ICJLink } from './models/cj.model';


@Component({
  selector: 'cb-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  @HostBinding('class') classList = 'app-root risr-material';

  @ViewChild('header', {static: true}) header: AppHeaderComponent;
  @ViewChild('sidenavHeader', {static: true}) sidenavHeader: AppHeaderComponent;

  @ViewChild('notificationRef', { read: TemplateRef, static: true }) notificationRef: any;
  @ViewChild('helpRef', { read: TemplateRef, static: true }) helpRef: any;
  @ViewChild('userTemplate', { read: TemplateRef, static: true }) userTemplate: any;
  @ViewChild('orgDropDownRef', { read: TemplateRef, static: true }) private orgDropDownRef: any;
  @ViewChild('orgPropertiesRef', { read: TemplateRef, static: true}) private orgPropertiesRef: any;
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('versionRef', { read: TemplateRef, static: true }) private versionRef: any;
  @ViewChild('versionDescriptionRef', { read: TemplateRef, static: true }) private versionDescriptionRef: any;

  opened = false;

  _loginUser = '';

  selectedOrg: any;
  selectedOrgDisplay = 'Select an Organization';
  isLoadingData = false;

  orgDataList: any[] = [];

  reason = '';
  expandState = false;
  systemMenuItems: ICJLink[];
  userMenuItems: ICJLink[];

  private _helpUrl = environment["helpUrl"] ? environment["helpUrl"] : '';
  private _versionDescription = environment["versionDescription"] ? environment["versionDescription"] : '';
  private _versionDescriptionCSS = environment["versionDescriptionCSS"] ? environment["versionDescriptionCSS"] : '';
  private systemMenuItemSub = new Subscription();
  private userMenuItemSub = new Subscription();
  private authenticationSubscription = new Subscription();
  private authSubscription = new Subscription();
  private orgListSub = new Subscription();
  private selectedOrgSub = new Subscription();
  private authorizedOrgSubs = new Subscription();

  readonly VERSION = packageJson.version;

  private get helpUrl(): string {
    return this._helpUrl;
  }

  public get versionDescription(): string {
    return this._versionDescription;
  }

  public get versionDescriptionCSS(): string {
    return this._versionDescriptionCSS;
  }

  set loginUser(value: string) {
    this._loginUser = value;
  }
  get loginUser(): string {
    return this._loginUser;
  }

  constructor(public auth: AuthService,
              private router: Router,
              private orgService: OrgService,
              private dialog: MatDialog,
              private cjService: CjService,
              private dialogOpener: DialogOpenerService,
              private users: UsersService,
              private icons: IconService) {
              }

  ngOnInit() {
    this.setupSidenavHeader();
    this.authenticationSubscription = this.auth.isAuthenticated().subscribe((authenticated: boolean) => {
      if (authenticated) {
        this.authSubscription?.unsubscribe();
        this.authSubscription = this.auth.userProfile.subscribe(userProfileSubject => {
          if (userProfileSubject) {
            const firstName = userProfileSubject.firstName;
            const lastName = userProfileSubject.lastName;
            this._loginUser = (firstName ? firstName : '') + (lastName ? (' ' + lastName) : '');
            this.header.getNavigationService().setUserRoles([]);
            this.header.refreshChildren();
            const systemMenuItems: ICJLink[] = [];
            this.systemMenuItemSub?.unsubscribe();
            this.systemMenuItemSub = this.users.getCurrentUser()
              .pipe(map(resp => this.cjService.findAllLinks(resp.links, 'hea-system-menu-item')))
              .subscribe(resp => {
                  for (const c of resp) {
                    systemMenuItems.push(c);
                  }
                  systemMenuItems.sort((a, b) => a.prompt.localeCompare(b.prompt));
                  this.systemMenuItems = systemMenuItems;
              });
            const userMenuItems: ICJLink[] = [];
            this.userMenuItemSub?.unsubscribe();
            this.userMenuItemSub = this.users.getCurrentUser()
            .pipe(map(resp => this.cjService.findAllLinks(resp.links, 'hea-user-menu-item')))
            .subscribe(resp => {
                for (const c of resp) {
                  userMenuItems.push(c);
                }
                userMenuItems.sort((a, b) => a.prompt.localeCompare(b.prompt));
                this.userMenuItems = userMenuItems;
                this.setupHeaderComponent();
            });
          } else {
            this._loginUser = '';
            this.header.getNavigationService().setUserRoles(null);
          }

        });

        // Get organizations from back end.
        this.isLoadingData = true;
        this.orgListSub?.unsubscribe();
        this.orgListSub = this.orgService.getAll().subscribe((orgList) => {
          console.debug('orgList', orgList);
          if (orgList && orgList.length > 0) {
            const orgDataList: any[] = [];
            for (const org of orgList) {
              if (org.heaObject) {
                orgDataList.push(org);
              }
            }
            // making deep copy
            this.orgDataList = JSON.parse(JSON.stringify(orgDataList));
            this.orgService.setAuthorizedOrganizations(JSON.parse(JSON.stringify(orgDataList)));
          }
        },
        error => {
          this.isLoadingData = false;
          console.error(`Error getting directory items ${error}`);
        },
        () => {
          this.isLoadingData = false;
          console.debug(`Completed refresh`);
        });
      } else {
        this.orgDataList = [];
        this.orgService.setAuthorizedOrganizations([]);
        this._loginUser = '';
        this.header.getNavigationService().setUserRoles(null);
      }
      this.header.refreshChildren();
    });
    this.selectedOrgSub?.unsubscribe();
    this.selectedOrgSub = this.orgService.getSelectedOrg().subscribe(org => {
      if (!org) {
        this.selectedOrgDisplay = 'Select an Organization';
      } else {
        this.selectedOrg = org;
        this.selectedOrgDisplay = org.heaObject.display_name;
      }
    });
    this.authorizedOrgSubs?.unsubscribe();
    this.authorizedOrgSubs = this.orgService.getAuthorizedOrganizations().subscribe((orgObjList: any[]) => {
      if (orgObjList) {
        this.orgDataList = orgObjList;
      }
    });

  }

  getIconForLink(link: ICJLink) {
    return this.icons.getIconForLink(link);
  }

  private userMenuDynamicItem(link: ICJLink) {
    const icon = this.icons.getFontIconForLink(link);
    return {type: LiNavComponent, title: link.prompt, aClass: 'dropdown-item nav-link', iClass: `${icon} mr-3`,
      route: this.routeFromHref(link.href)
    }
  }

  private userMenuDynamicItems() {
    return this.userMenuItems?.map(link => this.userMenuDynamicItem(link))
  }

  setupHeaderComponent() {
    let rightSideChildElements: any[] = [];

    if (this.versionDescription) {
      rightSideChildElements.push({type: TemplateComponent, template: this.versionDescriptionRef, id: 'version-description-dd'});
    }
    rightSideChildElements.push({type: TemplateComponent, template: this.versionRef});
    if (this.helpUrl) {
      rightSideChildElements.push({type: TemplateComponent, template: this.helpRef, id: 'help-dd'});
    }
    rightSideChildElements.push({type: TemplateComponent, template: this.notificationRef, id: 'notification-dd'});
    rightSideChildElements.push({type: LiDdNavComponent, id: 'user-profile-dd', roleName: 'AUTHC', iClass: 'bi bi-person-circle fa-2x mr-3',
      aClass: 'dropdown-toggle no-caret nav-link', ulClass: 'dropdown-menu dropdown-menu-right',
      route: '/home',
      children: [
        {type: TemplateComponent, template: this.userTemplate},
        ...this.userMenuDynamicItems() || [],
        {type: LiNavComponent, title: 'Logout', aClass: 'dropdown-item', iClass: 'bi bi-escape fa-1x mr-3',
          liClick: () => {
            this.auth.logout();
          }
        }
      ]
    });

    const config = {
      id: 'header',
      children: [
        {
          type: UlNavComponent, roleName: 'AUTHC', ulClass: 'nav-container',
          children: [
            {type: LiDdNavComponent, id: 'current-module', liClass: 'nav-container', roleName: 'AUTHC',
              iClass: 'bi bi-list fa-lg', aClass: 'nav-link no-caret', ulClass: 'dropdown-menu',
              liClick: () => { this.sidenav.open(); }
            },
            {type: LiNavComponent, imgSrc: 'assets/core-browser-logo-07-min.svg', iClass: 'logo', aClass: 'nav-link', route: '/home'},
            {type: TemplateComponent, template: this.orgDropDownRef},
            {type: TemplateComponent, template: this.orgPropertiesRef}
          ]
        },
        {
          type: UlNavComponent, id: 'header-right-container', container: 'rightContainer', ulClass: 'navbar-nav',
          children: rightSideChildElements
        }
      ]
    };

    this.header.setConfig(config);

  }
  setupSidenavHeader() {
    const config = {
      id: 'sidenavHeader',
      children: [
        {
          type: UlNavComponent, ulClass: 'nav-container h-100',
          children: [
            {type: LiNavComponent, iClass: "bi bi-x-lg", aClass: 'nav-link',
              liClick: () => { this.sidenav.close(); }},
            {type: LiNavComponent, imgSrc: 'assets/core-browser-logo-07-min.svg', iClass: 'logo', aClass: 'nav-link',
              route: '/home', liClick: () => { this.sidenav.close(); }}
          ]
        }
      ]
    };
    this.sidenavHeader.setConfig(config);
  }

  onClickOrgProperties() {
    if (this.selectedOrg) {
      this.cjService.getActualObjectOrSelf(this.selectedOrg).pipe(take(1)).subscribe(resp => {
        const href = resp.href;
        this.dialogOpener.openModal(href, 'Edit', n => {if (n) {this.orgService.refresh()}});
      });
    }
  }

  onClickNotification() {
    console.log('Call this.onClickNotification()');
  }

  onClickUserProfile(event: any) {
    console.log('Call this.onClickUserProfile()');
  }

  showHelp() {
    // console.log('Call this.showHelp()');
    if (this.helpUrl) {
      window.open(this.helpUrl, "_blank");
    }
  }

  closeSidenav(reason: string) {
    this.reason = reason;
    this.sidenav.close();
  }

  onRouteOrganizationScreens(route: string) {
    console.debug('routing from organization to', route);
    switch (route) {
      case 'home':
        this.router.navigateByUrl('home/explorer-home');
        break;
      case 'object-explorer':
        this.router.navigateByUrl('object-explorer/collections/default');
        break;
    }
    this.sidenav.close();
  }

  onRouteCollectionSidenav(collectionLink: ICJLink) {
    console.debug('routing from collection to', collectionLink);
    this.onRouteCollectionHref(collectionLink.href);
    this.sidenav.close();
  }

  ngOnDestroy() {
    this.authenticationSubscription.unsubscribe();
    this.systemMenuItemSub?.unsubscribe();
    this.userMenuItemSub?.unsubscribe();
    this.orgListSub?.unsubscribe();
    this.selectedOrgSub?.unsubscribe();
    this.authorizedOrgSubs?.unsubscribe();
  }

  private onRouteCollectionHref(href: string) {
    this.router.navigate(this.routeCommandsFromHref(href));
  }

  private routeCommandsFromHref(href: string) {
    return ['object-explorer', 'collections', urlToUniqueId(href)];
  }

  private routeFromHref(href: string) {
    return `/object-explorer/collections/${urlToUniqueId(href)}`;
  }
}
