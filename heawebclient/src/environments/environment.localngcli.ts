// This heaserver-folders can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of heaserver-folders replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiURL: 'http://localhost:4200/api',
  wsUrl: 'ws://localhost:4200/ws',
  clientId: 'mod-oauth2',
  domain: 'hci.utah.edu',
  keycloakURL: 'https://localhost:8444/auth',
  keycloakRealm: 'hea',
  keycloakClientId: 'core-browser'
};



/*
 * For easier debugging in development mode, you can import the following heaserver-folders
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
