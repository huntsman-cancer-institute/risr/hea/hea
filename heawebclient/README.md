# CORE Browser Web Client
[Research Informatics Shared Resource](https://risr.hci.utah.edu), [Huntsman Cancer Institute](https://hci.utah.edu), 
Salt Lake City, UT

The CORE Browser Web Client is the web browser-based user interface to the CORE Browser research data desktop.

## Version 1.2.1
* After deleting an object, check whether ancestor objects still exist and remove them from the object explorer if they 
  do not.
* Whitelist supported operating systems and architectures. The package-lock.json can now have platform-specific 
  options.

## Version 1.2.0
* Preview featured added so user can now quickly view their data in the object explorer tree.
* Bug fix in upload card making it more reliable.

## Version 1.1.22
* Increased reliability with which clicking on a project in the Recent Projects card navigates to the project in the
  Object Explorer.
* The object explorer no longer allows trying to open an archived AWS S3 object.

## Version 1.1.21
* Bug fix for typeahead not showing in all cases for the dropdown.
* Tabbing on form will now invoke dropdown menu, click no longer required.
* The recent projects card now only displays recently accessed projects from accounts in the currently selected
  organization.
* Increased reliability with which clicking on a project in the Recent Projects card navigates to the project in the
  Object Explorer.

## Version 1.1.20
* New intro page with app information and important links.
* Overhauled the usage card, fixing the average size and duration calculations.

## Version 1.1.19
* The recently accessed card now supports clicking on the object name to open it in the object explorer.
* Projects no longer end up with a new accessed date from being listed in the recently accessed card.
* Recently accessed card performance is vastly improved.

## Version 1.1.18
* Sped up opening accounts, buckets, folders, and projects in the nav tree and object explorer.
* Fixed regression where changing the object type in the New card fails to change the form.

## Version 1.1.17
* Check for async operation completion more frequently.
* Avoid redundant refreshes.
* Bug fix for Managed Credentials not deleting expired credentials properly.
* New UI design for multi and single select

## Version 1.1.16
* When the refresh button is pressed while a non-container object is selected, refresh the parent.

## Version 1.1.15
* Updated database to ensure the settings and credentials collections continue to be visible in the user interface.
* Fixed bug affecting properties card rendering when the Collection+JSON template has sections.
* Added a Volume icon (only seen if your account has access to the volumes collection).
* Reloads the page when the user tries interacting with a timed-out session (there remain some situations where this 
does not work).
* The Open menu item in the icon corral now attempts to open a selected file in a new tab or window in the browser, 
otherwise it downloads the file.
* Double clicking an object in the object explorer triggers Open action.
* Disable the icon corral when a row is unselected in the tree view.
* Clear dynamic standard card when the object is unselected or object selection has changed.
* Update dynamic standard card buttons when object selection has changed.
* Improved dynamic standard and dynamic clipboard card title display.
* Improved versions card title display.
* Improved create card title display.

## Version 1.1.14
* Don't display ERR in the activities card when empty.
* Label collaborator users as Collaborator in the members card.
* Handle situation where a user doesn't have access to a select item without crashing.

## Version 1.1.13
* Fixed object explorer refresh issues.
* Show "Loading" while loading root nodes of the tree grid.

## Version 1.1.12
* Addressed card bug where save/update/etc. was hidden for some user permissions.

## Version 1.1.11
* Introduces Managed Credentials, credentials that can last multiple days and automatically deletes after specified time.

## Version 1.1.10
* Fixed dynamic card and dialog alignment.
* Improved display of read-only select form fields in dynamic cards and dialogs.
* Improved display of textareas in dynamic cards and dialogs.
* Added tooltips to select form fields in dynamic cards and dialogs.
* More than one collection can now be displayed in the system menu.
* Minor display improvements for the delete and download dialogs.
* Improved performance of rendering read-only select form fields in dynamic cards and dialogs.

## Version 1.1.9
* Unarchive card now requires Restore Duration option avoiding a potential bug.
* Text of button on majority cards is now accurate to what it does.

## Version 1.1.8
* Eliminated "Please enter a valid value..." tooltip on valid timestamps in properties cards.

## Version 1.1.7
* Trash card - added "DELETED" column with the creation date of the AWS delete marker (versioned buckets only)

## Version 1.1.6
* The Object Explorer's TYPE column is now populated completely.
* The SIZE column is now right justified.
* Added multiselect functionality to trash popup, with support for restore and delete

## Version 1.1.5
* Uploads to the root of an S3 bucket are now supported.
* Cards, modal dialogs, and the icon corral now dynamically detect the user's permissions and hide functionality 
appropriately (editing, saving, etc.).
* The activities card now only displays actions that involve modifiying an object.
* The recent projects card now omits the accessing timestamp because it was useless, and it sorts projects in reverse 
alphabetical order.

## Version 1.1.4
* Upload card, now support selection of storage class type other than Standard 
* Fixed issue where download button could be incorrectly disabled when a file was selected in the Object Explorer
* Cachebusting code added to prevent older versions of Core Browser from being stored

## Version 1.1.3
* Added icon corral to the Settings screen.
* Users can select a credentials file and use the icon corral to generate an .aws/credentials file.
* Fixed issue where scrolling a folder's items might infinitely loop.
* For users with multiple organizations and before the user has selected an organization, the Home screen no longer 
partially displays the organization dashboard.
* New Home and Object Explorer links in the sidebar when an organization is selected.
* Icon updates.
* New Collections section in the sidebar, currently containing a link to Settings.

## Version 1.1.2
* Styling enhancements.
* Fix for the object explorer only partially updating after a copy or move in some situations.
* For users with multiple organizations, the default login behavior changed to wait for organization selection to load.
* Updated keycloak plugin.

## Version 1.1.1
* Downloading S3 objects now triggers the web browser's downloading notification.
* Added configurable version description to main header.

## Version 1.1.0
* Angular 15.
* Removed Font Awesome Pro dependency.
* Removed internal NPM repository dependency. No need to maintain a custom .npmrc anymore.
* Updated styling and icons.
* Made trash window bigger.

## Version 1.0.2
* Correctly display error messages from downloads.

## Version 1.0.1
* Various bug fixes.

## Version 1
Initial release.

## Runtime requirements
* Major web browsers, including Firefox, Safari, Chrome, Edge, and Internet Explorer version 10 or greater.
* Docker 19.03.5 or greater, available from https://www.docker.com/.

## Configuration
The web client supports three environment files that allow you to configure it for different runtime scenarios:
* development: src/environments/environment.ts (for development)
* ngcli: src/environments/environment.ngcli.ts (for `npm start`, see below)
* production: src/environments/environment.prod.ts (for production)

Because the content of these files will differ depending on your environment, we cannot provide them to you. However, the format is as follows:
```
export const environment = {
  production: <true or false>,
  apiURL: 'http://<hostname>:<port>/api',
  clientId: '<the client id from your OpenID Connect identity provider>',
  domain: '<the domain of your OpenID Connect identity provider>',
  helpUrl: '<A link for the help button to open>',
  keycloakURL: '<URL for the authentication server>',
  keycloakRealm: '<Name of the realm in the authentication server>',
  keycloakClientId: '<Name of the client in the realm on the authentication server to use for login>',
  wsUrl: '<Websocket URL, required for activity service>'
  versionDescription: '<A string that will appear at the top-right of the header (blank by default)>',
  versionDescriptionCSS: '<a string containing CSS that will be applied to the banner message (blank by default)>',
};
```
You will have to provide these three files before building heawebclient.


## Build requirements
To build the heawebclient docker container, all you need is docker as described in the README.md in this project's parent directory. If you want to use the
Angular CLI to recompile heawebclient automatically as you do software development on it, you need the following:
* Node.js 14.21.3: available from https://nodejs.org/en/download/ or your package manager. You can either select the
option to install dependencies automatically with Chocolatey, or install Visual Studio Community and Python yourself as
described below.
* Angular CLI 15.2.10: from your command line, run `npm install -g @angular/cli@15.2.10`. On Mac and Linux, if your node installation is not in your home directory, you probably need to prefix this command with `sudo`, like `sudo npm install -g @angular/cli@15.2.10`.
* Any development environment that knows about `.editorconfig` files is fine.
* On Windows, you also will need:
    * Visual Studio Community, found at https://visualstudio.microsoft.com/vs/community/. Select the Desktop Development with C++ workload.
    * git, found at https://git-scm.com/download/win.
    * ssh, found in the Windows OpenSSH optional feature, among other sources.
    * Python, installable using winget (`winget install --id Python.Python.3.11 --source winget`)
* On Mac, Xcode or the command line developer tools is required, found in the Apple Store app. You just need this for the command line tools. 

Next, in this directory, run `npm install` to install necessary node packages.

Then, launch Angular's built-in server with `npm start`. It uses the `environment.ngcli.ts` file for hostname, port and other configuration. The intent is for 
the ng server to connect to an instance of the HEA server docker containers on localhost, which you can run as described in the README.md in this project's
parent directory. The `environment.ngcli.ts` file must at least have the following:
```
export const environment = {
  production: false,
  apiURL: 'http://localhost:4200/api', /* The URL for accessing HEA APIs */
  ...
};
```
