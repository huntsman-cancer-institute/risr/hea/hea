# Releasing the HEA Web Client

## Versioning scheme
Use semantic versioning as described at https://www.youtube.com/watch?v=VCIQ2W04GB4.

## Bumping the version number
* Select the new version number.
* Add a header for the new version to the versions section of the README.md.
* List the high-level changes that will be released in the new version.
* Update the version number in the package.json.
* Run npm install in the heawebclient directory so that the package-lock.json also contains the new version number.
* Commit those changes to version control.

## Release process
Angular single page applications must be rebuilt to incorporate environment-specific configuration differences, so we
do not package heawebclient and upload it to a package repository like NPM. It must be rebuilt for each environment.
The steps are as follows:

* For the stage environment, ensure the changes are in the dev branch. For prod, ensure you changes are in the master
  branch.
* Shutdown the webclient.
* Check out the correct branch.
* Pull your changes.
* Run `docker compose build -f docker-compose-release.yml heawebclient` to build a new heawebclient docker image.
* Start the new docker image. 
