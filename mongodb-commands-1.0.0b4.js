// Convert many:many relationship between Organization and Account to [one, none]:many
// We specifically allow multiple organizations to have no accounts to allow for new Organizations to be setup.
db.organizations.createIndex({aws_account_ids: 1},{name: "idx_organization_aws_account_id", unique: true, partialFilterExpression: {$and: [{aws_account_ids: {$exists: true}}, {aws_account_ids: {$type: "array"}} ]} });
