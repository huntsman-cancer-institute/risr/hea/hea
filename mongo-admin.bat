@echo off
SET workdir=%~dp0

setlocal enabledelayedexpansion
for /f "usebackq tokens=1,* delims==" %%i in ("%workdir%.env") do (
    if "%%i"=="MONGO_ROOT_USERNAME" set mongo_hea_username=%%j
    if "%%i"=="MONGO_ROOT_PASSWORD" set mongo_hea_password=%%j
    if "%%i"=="MONGO_HOSTNAME" set mongo_hostname=%%j
)

set "mongo_hea_password=%mongo_hea_password:'=%"
set "mongo_hea_username=%mongo_hea_username:'=%"

if defined mongo_hostname (
    set "mongo_hostname=%mongo_hostname:'=%"
    mongosh "%mongo_hea_database%" --host "%mongo_hostname%" -u "%mongo_hea_username%" -p "%mongo_hea_password%" --eval "disableTelemetry()" --shell
) else (
    docker exec -it mongo mongosh "%mongo_hea_database%" -u "%mongo_hea_username%" -p "%mongo_hea_password%" --eval "disableTelemetry()" --shell
)