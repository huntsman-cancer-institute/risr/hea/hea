// Remove duplicate index.
db.awss3foldersmetadata.dropIndex("idx_awss3foldersmetadata_get_by_bucket_id_and_encoded_key")

// Add index for querying metadata by bucket and folder.
db.awss3foldersmetadata.createIndex({'bucket_id': 1, 'parent_encoded_key': 1}, {name: "idx_awss3foldersmetadata_bucket_and_parent_folder"});

// Fix parent_encoded_key data corruption.
function fixParentEncodedKey(key) {const result = atob(key).split('/').slice(0, -2).join('/'); if (result.length > 0) {return btoa(result + '/');} else {return 'root';}}; db.awss3foldersmetadata.find().forEach(function (doc) {db.awss3foldersmetadata.updateOne({_id: doc._id},{$set: {parent_encoded_key: fixParentEncodedKey(doc.encoded_key)}});});