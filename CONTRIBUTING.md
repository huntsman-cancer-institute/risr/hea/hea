# Contributing to HEA

## Project structure
* `hea` (this project): The main project; it contains a docker-compose.yml file for starting and stopping HEA, and it 
  contains Dockerfiles and other configuration files for all the Docker containers that comprise HEA. In addition, it 
  contains the source code for the HEA web client.
* `heaclient`: Python HTTP client library for calling HEA REST API services.
* `heaobject`: Python objects that HEA clients POST, PUT, GET, and DELETE to/from HEA REST services.
* `heaserver`: Python HEA REST API service library.
* `heawebclient`: Angular web client for HEA.
* `heaserver-registry`: Python microservice for managing mappings of HEA Object types to the microservices for storing 
and retrieving them.
* `heaserver-folders`: Python microservice for managing the folders of the HEA desktop.
* `heaserver-data-adapters`: Python microservice for accessing data files and databases.
* `heaserver-volumes`: Python microservice for managing volumes, which are different storage filesystems for HEA objects.

## Developing the heawebclient

The heawebclient is an Angular CLI project. It comes with a `ngcli` configuration for testing using `ng serve`. Using
`ng serve` requires some setup. First, create an environment file, `heawebclient/src/environments/environment.ngcli.ts`, 
as described above. The `apiURL` property should be `http://localhost:4200/api`. Next, configure your Open ID Connect
provider to allow `http://localhost:4200` as a callback URL and a logout URL. Then, run 
`ng serve --configuration=ngcli`, or simply run `npm start`.

Any environment.*.ts files that do not begin with environment.local are ignored by git, so they will not end up in 
version control.

The server side just requires one configuration change to work with Angular CLI: set the following environment
variable in your `.env`: `HEASERVER_API_URL=http://localhost:4200`. Then start the server side with
`docker-compose` as described above.

### Switching between configurations
If you want to create multiple configurations, you can use the switch-configurations script (currently Windows-only) to
achieve this. Put your `.env` file content in a `.env.<config>` file instead, replacing `<config>` with the name of your
configuration (no spaces allowed). Similarly, create a custom-mongodb-commands.js.<config> file instead of a
custom-mongodb-commands.js file; and `environment.<config>.ts` and `environment.<config>ngcli.ts` files instead of
`environment.ts` and `environment.ngcli.ts` files. Do this for as many configurations as you want.

Default configuration files are provided for a "local" configuration in which HEA and Keycloak both run on localhost.

Then run .\switch-configurations <config> to switch to a configuration. **Note, your MongoDB database will be reset when
you do this!** The reset may be eliminated from a future release. To use the local configuration, create a `.env.local`
file and optionally a `custom-mongodb-commands.js.local` file, and run `.\switch-configurations local` to use it.

## Developing backend HEA code in Python
* *Python version*: for existing projects, look at each project's README.md for the version that is supported. For new
  projects, you will want to use the same version that is supported by any core HEA dependencies that your project
  needs.
* *Pip configuration*: we use the [PyPI](https://pypi.org) repository to host all HEA Python packages. No special 
  configuration is required to download packages from there.
* Other dependencies
  * *Docker*: only needed to run integration tests, in addition to running HEA as described above. Any recent version 
  should do.

### Python project structure
HEA projects implemented in Python should have the following structure:
```
projectname
    README.md
    setup.py
    .gitignore
    .editorconfig
    LICENSE
    MANIFEST.in
    mypy.ini
    pytest.ini
    requirements_dev.txt
    src
        module.py
        package/
            __init__.py
            module.py
    tests
        unittestpkg
            unittest.py
```

Server-side projects all should have the following customization of this structure:
```
projectname
    README.md
    setup.py
    .gitignore
    .editorconfig
    LICENSE
    MANIFEST.in
    mypy.ini
    pytest.ini
    requirements_dev.txt
    src/
        heaserver/
            package/
                __init__.py
                module.py
    tests/
        heaserver/
            packagetest/
              unittest.py
    integrationtests/
        heaserver/
            packageintegrationtest/
              integrationtest.py
```
Note the `heaserver` namespace, with only a package within it (no `__init__py` file!) that is unique to the project. 
The `heaserver` namespace is reserved for core HEA microservices and related core software libraries such as those on
the project's [Gitlab organization](https://gitlab.com/huntsman-cancer-institute/risr/hea).

### Working on an existing HEA project
* Clone the project from [Gitlab](https://gitlab.com/huntsman-cancer-institute/risr/hea).
* Create a virtual environment in your project's root directory by running `python -m venv venv` in
  your terminal. Activate the environment with `source ./venv/bin/activate` on Linux and Mac, and 
  `.\venv\Scripts\activate` on Windows.
* Run `pip install wheel` (not necessary if the project uses pyproject-build to build wheels).
* Run `pip install -r requirements_dev.txt` to install dependencies. **Do NOT run `python setup.py install` because it
  will break your development environment.**
* Run `pytest` and `pytest integrationtests` to see if everything worked!

### New project templates
We use [Cookiecutter](https://cookiecutter.readthedocs.io) for project templates (currently just the microservice
template below). Install Cookiecutter version 1.7 into your system Python (version 3.5 or higher required) with 
`pip install -U cookiecutter~=1.7.3`. Follow any prompts to add the installation directory to your path. 

The templates name things in a specific way to give core HEA projects consistency.

### Creating a new microservice
The [hea-cookiecutter-microservice-template](https://gitlab.com/huntsman-cancer-institute/risr/hea/hea-cookiecutter-microservice-template) 
project provides a template for you to create new microservices rapidly. It will build a project with the above 
structure. To use it:
* In your terminal, run `cookiecutter https://gitlab.com/huntsman-cancer-institute/risr/hea/hea-cookiecutter-template`
* Respond to the prompts as follows:
  * *email*: contact email address for the project.
  * *project_name*: replace "Something" in the default with a capitalized name for your new microservice.
  * *project_slug*: the project slug in your version control system. It should start with `heaserver-`. The remainder
    of the slug should be all lowercase, and use dashes between words. If the name refers to a HEA object that the
    microservice will manage, 
  * *project_short_description*: a brief description, one phrase, first letter not capitalized, no period.
  * *heaserver_package_name*: the package name in the heaserver namespace, singular and lowercase, that is unique to your 
    microservice. The package name, like all package names in Python, should be all lowercase and singular, with no 
    dashes or underscores.
  * *heaobject_module_name*: replace with the name of the module in the heaobject project that will contain classes 
    managed by your new microservice. The module name, like all module names in python, should be all lowercase and 
    singular, with no dashes or underscores.
  * *heaobject_class_name*: the name of the primary HEAObject class managed by your new microservice. The class name,
  like all class names in Python, should be in CamelCase.
* Create a virtual environment for your project, in the project's root directory, with `python -m venv venv`. Activate
  the environment with `source ./venv/bin/activate` on Linux and Mac, and `.\venv\Scripts\activate` on Windows.
* Run `pip install wheel` (not necessary if the project uses pyproject-build to build wheels).
* Run `pip install -r requirements_dev.txt` to install dependencies. Do NOT run `python setup.py install` - the project
  will not work, and you might have to blow away your virtual environment to fix it.
* Run `pytest` and `pytest integrationtests` to see if everything worked (you'll need to create your module in
  the heaobject project first). The integration tests require Docker as a dependency.

### Python style guide
Follow the PEP-8 style guide, available from https://www.python.org/dev/peps/pep-0008/, and use
spaces for indentation, 4 spaces per indentation level. All HEA projects have a `.editorconfig` file to enforce these 
and other settings. If your editor does not support `.editorconfig` files, make sure that you configure it similarly.