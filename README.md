# Health Enterprise Analytics (HEA)
[Research Informatics Shared Resource](https://risr.hci.utah.edu), [Huntsman Cancer Institute](https://hci.utah.edu), 
Salt Lake City, UT

HEA is a research and analytics desktop in your web browser, designed for academic medical centers that strive for
integrated research, discovery, and clinical missions. Its goals are to:

* Present a simple desktop metaphor for securely interacting with data and tools for analyzing data.
* Integrate locally developed and third-party data sources and analysis tools into a consistent and easy-to-use data 
capture, retrieval, and analysis environment.
* Make electronic health record data accessible for research and analytics, and bring research and analytics findings
to the point of care.
* Support automated chart abstraction at scale (millions of patient records) using business rules, natural language 
processing, and machine learning classification techniques.
* Support development and training of machine learning algorithms for prediction and classification.

##  Runtime Requirements
* Up-to-date Safari, Chrome, or Edge web browser
* Docker Engine 20.10.6 or greater, available from https://www.docker.com/. Note:  Desktop versions of Docker have a
  different versioning scheme and you will need to check which Docker engine version is packaged within them
* Python 3 to run configuration scripts. The run-first-time.py script requires python 3.10, 3.11, or 3.12.
* A recent version of KeyCloak (HEA has tested with versions 15, 19 & 22). You can use our customized KeyCloak docker
  image, available from https://gitlab.com/huntsman-cancer-institute/risr/hea/hea-keycloak.
* MongoDB database version 6 with TLS enabled

## Running HEA
You can run HEA on any Intel operating system that Docker supports. All network connections to HEA go through a reverse 
proxy Docker container that listens on ports `8080` and `8443` by default. The port numbers can be configured as described 
below. Normally, another web server will sit between the network and the reverse proxy container, listening on 
privileged ports like `80` and `443`.

### Configuring HEA 
Create a properties file named `.env` in your `hea` directory and populate it with the following:
```
MONGO_ROOT_USERNAME=<your_username> (required)
MONGO_ROOT_PASSWORD=<your password> (required)
MONGO_HEA_DATABASE=<your_database> (will default to hea if omitted)
MONGO_HEA_USERNAME=<your username> (required)
MONGO_HEA_PASSWORD=<your password> (required)
MONGO_PORT=<the port that MongoDB listens on -- if omitted, MongoDB listens on port 27017>
HEASERVER_API_ORIGIN=<the scheme, hostname, and port for making API calls, in format http(s)://hostname:port> # will default to https://localhost:8443 if omitted
HEASERVER_API_PATH=<path to API calls, with a leading slash and without a trailing slash> # will default to /api if omitted
HEA_WEBCLIENT_PATH=<base path for the web client, with a leading slash and without a trailing slash> # will default to '' if omitted
HEA_WEBCLIENT_CONFIG=<the angular configuration to use>  # set to prod; if omitted, you will get a development build of the web client; for prod (production) builds, there must be an environment.prod.ts file in the ./heawebclient/src/environments directory, with the production property set to true (see Web client front end configuration files below)
HEA_REVERSE_PROXY_PORT=<port #> (unencrypted port that the reverse proxy container listens on; defaults to 8080 if omitted)
HEA_REVERSE_PROXY_SSL_PORT=<port #> (encrypted port that the reverse proxy listens on; defaults to 8443 if omitted)
HEA_REVERSE_PROXY_SERVER_ADMIN=<email name> (will default to webmaster@localhost if omitted)
HEA_REVERSE_PROXY_HOST_NAME=<host name> (will default to localhost if omitted)
HEA_REVERSE_PROXY_SCHEME=<http or https> (the public user-facing scheme for URLs hitting the reverse proxy; should be the same as the HEASERVER_API_URL's scheme; will default to https if omitted)
HEA_REVERSEPROXY_CERT_DIR=<CA certs> (the path on the host to where the root ca cert, intermediate, and key are located)
OIDC_CRYPTO_PASSPHRASE=<secret> (required; used internally by the HEA backend's OAuth 2 client)
OIDC_PROVIDER_METADATA_URL=<Open ID Connect provider metadata url (will default to https://host.docker.internal:8444/auth/realms/hea/.well-known/openid-configuration if omitted)
OIDC_CLIENT_ID=<Open ID Connect client ID (will default to hea if omitted); this is used by the reverse proxy and the heaserver-people service
OIDC_SCOPE=<Open ID Connect scope> (will default to "openid email" if omitted)
OIDC_REDIRECT_URI=<Open ID Connect redirect URI> (will default to https://localhost:8443/api/redirect if omitted)
OIDC_REMOTE_USER_CLAIM=<Open ID Connect remote user claim> (will default to email if omitted)
OIDC_DEFAULT_LOGGED_OUT_URL=<Open ID Connect default logged out URL> (will default to https://host.docker.internal:8444/ if omitted)
OIDC_OAUTH_VERIFY_JWKS_URI=<Open ID Connect OAuth verify JWKS URI> (will default to https://host.docker.internal:8444/auth/realms/hea/protocol/openid-connect/certs if omitted)
OIDC_OAUTH_SSL_VALIDATE_SERVER=<Whether or not to validate SSL certificates> (will default to Off -- turn On in production!)
OIDC_LOG_LEVEL=<Log level for API access requests through the reverse proxy> (see https://httpd.apache.org/docs/2.4/mod/core.html#loglevel for allowed values; the default is debug)
AWS_REGION=<the Amazon Web Services region that you will use the most -- if omitted, defaults to us-west-2>
RABBITMQ_DEFAULT_USER=<the username for the management console -- if omitted, defaults to guest; services also connect to RabbitMQ with this account from within the docker network>
RABBITMQ_DEFAULT_PASS=<the password for the management console -- if omitted, defaults to guest; services also connect to RabbitMQ with this account from within the docker network>
KEYCLOAK_REALM: The Keycloak realm for HEA (will default to hea)
KEYCLOAK_HOST: The Keycloak base URL (will default to https://host.docker.internal:8444). This can be an internal URL not accessible to the web.
KEYCLOAK_ALT_HOST: The alternate Keycloak base URL, accessible to the web. It is required for some features of the heaserver-keychain service, otherwise it is optional.
KEYCLOAK_VERIFY_SSL: Whether services should verify Keycloak's SSL certificate (will default to true; never set to false in production settings)
KEYCLOAK_ADMIN_SECRET: The secret that the People Microservice should use to connect to Keycloak. See the Keycloak admin configuration setup section below for details.
KEYCLOAK_ADMIN_SECRET_FILE: A file containing the secret the People Microservice should use to connect to Keycloak (not used by default; handy when using Docker Compose's secrets functionality)
KEYCLOAK_COMPATIBILITY: Either 15 or 19, meaning the version 15 web services APIs (with /auth/) and the APIs from version 19 onward (without /auth/). Will default to 15 if omitted.
KEYCLOAK_ADMIN_CLIENT_ID: the admin client id to use. The default is admin-cli and rarely needs to change.
OPENSEARCH_HOSTNAME=<host name for opensearch service> (Defaults to localhost)
OPENSEARCH_PORT=<port for the opensearch service> (Defaults to 9200)
OPENSEARCH_USE_SSL=<flag to instruct opensearch client use ssl> (Defaults to false) 
OPENSEARCH_VERIFY_CERTS=<flag to instruct opensearch client to verify ssl cert > (Defaults to false)
OPENSEARCH_INDEX=<name of the opensearch index to search within for the client> (required)
OPENSEARCH_INITIAL_ADMIN_PASSWORD=<password for admin opensearch won't startup without it> (required)
```

### Web Client Front End Configuration Files
Next, create the following environment files to configure the web client front end for different runtime scenarios:
* Default: `heawebclient/src/environments/environment.ts` (it doesn't matter what is in this file; it will be replaced anyway).
* Production build: Create a `heawebclient/src/environments/environment.prod.ts` file based on the environment.local.ts file example.

The file format is as follows:
```
export const environment = {
  production: <true or false>,
  apiURL: 'http://<hostname>:<port>/api',
  clientId: '<the client id from your OpenID Connect identity provider>',
  domain: '<the domain of your OpenID Connect identity provider>',
  helpUrl: '<A link for the help button to open>',
  keycloakURL: '<URL for the authentication server>',
  keycloakRealm: '<Name of the realm in the authentication server>',
  keycloakClientId: '<Name of the client in the realm on the authentication server to use for login>',
  wsUrl: '<Websocket URL, required for activity service>'
  versionDescription: '<A string that will appear at the top-right of the header (blank by default)>',
  versionDescriptionCSS: '<A string containing CSS that will be applied to the banner message (blank by default)>',
};
```
The `apiURL` should use the hostname and port that are exposed to the network.

Finally, configure your Open ID Connect provider.

###  Custom MongoDB Commands File (Optional)
This file will contain extra MongoDB commands that run and tailor your environment database. Follow the directions in  [hea-keycloak/README.md](https://gitlab.com/huntsman-cancer-institute/risr/hea/hea-keycloak/-/blob/dev/README.md?ref_type=heads)

Note: Create and name this file as `custom-mongodb-commands.js` in the `hea` directory

###  Starting KeyCloak and HEA
1. Start KeyCloak
   1. Follow the directions in  [hea-keycloak/README.md](https://gitlab.com/huntsman-cancer-institute/risr/hea/hea-keycloak/-/blob/dev/README.md?ref_type=heads)
   2. Note: To Access KeyCloak Admin Console
      1. Go to https://localhost:8444/keycloak/admin 
      2. The default Username and Password are `admin`
2. Start HEA
   1. From the `hea` directory in your terminal, run
   ```
   docker compose up --build
   ```
   2. Note: First Time Running HEA: HEA's MongoDB database must be initialized. After `docker compose up --build` is finished, run `python run-first-time.py` (Python 3 only)
   3. Note: Stopping HEA: From the `hea` directory in another terminal, run
   ```
   docker compose down
   ```
3.  Log Into Application
    1. Go to  https://localhost:8443/ or substitute `8443` with the port you specified in the `.env` file
    2. Enter the `Username` & `Password` for the user you created and then done
4. MongoDB Instance is listening on port` 27017` or the port you specified in the `.env` file

### KeyCloak Admin Configuration Setup
In order for the HEA People Microservice to function, you must activate the credentials flow for generating a token. This token will have admin level permission, so please follow these instructions carefully for the version of KeyCloak you are using:

#### Keycloak 15-18
1. Login to Keycloak Admin console and ensure that `core-browser` realm is selected
2. On the left-side menu, select `Clients`
3. Select `admin-cli` in the `Client ID` column
4. In the `Settings` tab, ensure `Access Type` is set to `confidential`
   1. Ensure the following toggle settings is `On`: `Direct Access Grants Enabled` and `Service Accounts Enabled`
5. Select the `Credentials` tab, ensure `Client Authenticator` has `Client Id and Secret`, and click the `Generate Secret` or `Regenerate Secret` button that is listed with `Secret`
6. Copy the `Secret` key and paste it as your `KEYCLOAK_ADMIN_SECRET=` property in the`.env` file
   1. Alternatively, you can create a `.secret` file (with the pasted `Secret` key inside)  in the`hea` directory and set the path to that file as the `KEYCLOAK_ADMIN_SECRET_FILE=` property in the`.env` file
   2. Do not commit this file to version control! It is ignored by git by default.
7. Select the `Service Account Roles` tab and click the `Client Roles` dropdown. Select `realm-management`
   1. Ensure that `create-client`, `manage-clients`, `manage-users`, `query-clients`, `query-groups`, `query-realms`, `query-users`, `view-clients`, `view-realm`, and `view-users` roles are added
####  Keycloak 19 or later
1. Follow the directions in [hea-keycloak/README.md](https://gitlab.com/huntsman-cancer-institute/risr/hea/hea-keycloak/-/blob/dev/README.md?ref_type=heads)

###  View / Manage MongoDB (Optional)
To view and manage HEA's MongoDB database, we recommend using MongoDB Compass. [Click Here]( https://www.mongodb.com/products/tools/compass) to download and install the application

#### Configuring MongoDB Compass
1. In MongoDB Compass, click on the `New connection` button
2. Paste the following in the `URI` field:
```
mongodb://<MONGO_HEA_USERNAME>:<MONGO_HEA_PASSWORD>@localhost:27017/admin?authMechanism=DEFAULT&authSource=hea
```
   1. Note: `MONGO_HEA_USERNAME` and `MONGO_HEA_PASSWORD` are the values you specified in the `.env` file. You will need to substitute `27017` to the specified port if you changed the default `MONGO_PORT` value
3. Click the `Save & Connect` button


###  Running HEA on Windows

Note that recent versions of Docker Desktop combined
with recent versions of Windows 10 may not start Docker. Instead, you'll get an Access Denied error with a message to
add yourself to the `docker-users` group. Fixing this on Windows 10 Home Edition requires editing the registry, and we
strongly recommend using another edition of Windows. On other editions of Windows, you can fix the problem with the 
Computer Management application. As an administrator, open Computer Management, right-click on the docker-users group, 
and type in your domain\username. You will have to enter the user's password. Then restart and log back in as yourself.

The first time the mongo container starts, Windows 10 may ask you to confirm that you want to share your filesystem for 
the mongo database. It may then prompt you for a username and password. The prompt dialog may be hidden behind other 
windows, and mongo will appear to fail to start.

### Firewall Considerations

* The `docker-compose build` process requires access to https://pypi.org on port `443` (TCP) to download Python packages.
* Running HEA requires access to https://gitlab.com on port `443` (TCP) to download JSON schemas.

### Troubleshooting While Running "docker-compose build" Command

If you get an error like "generating es5 bundles for differential loading... call retries were exceeded", this is
probably due to the limit size of node runtime memory or docker memory (if you are using an old version of Docker 
Desktop which is  not working on the WSL 2 based engine) . 
* To increase the node runtime memory, simply add "ENV NODE_OPTIONS --max-old-space-size=<size>(eg. 8192)" to the 
  Dockerfile before "RUN npm install" command line.
* To set Docker Memory limit size, go to Docker Desktop Settings --> Resource --> Advanced, drag to expand the sizes of 
  Memory (8 GB suggested) and Disk Image Size (128 GB suggested).

If need to install some packages from your private artifactory instead of the public npm registry, you will need to 
create your own .npmrc file including your credentials to access your private artifactory and put it where the 
package.json file is.
