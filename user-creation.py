import argparse
import re
import sys
from enum import Enum
import json
import logging
from typing import Optional, Tuple, List, Union
import aiohttp
from string import whitespace
from datetime import datetime
import asyncio

from aiohttp.web_exceptions import HTTPBadRequest
from aiohttp.web_response import Response
from motor import motor_asyncio
import urllib.parse
from yarl import URL
from aiohttp import ClientResponseError, web, hdrs, ClientResponse, ClientSession

ROLE_NAMES = ["OrganizationAccountAccessRole", "aws-Manager.Role", "aws-Member.Role"]


class KeycloakCompatibility(Enum):
    FIFTEEN = "15"  # APIs prior to version 19. We have only tested with 15.
    NINETEEN = "19"  # APIs for version 19 and later.


DEFAULT_KEYCLOAK_COMPATIBILITY = KeycloakCompatibility.FIFTEEN  # Possible values are '15' and '19'


class KeycloakAdmin:

    def __init__(self, group_name: str, session: ClientSession, args: dict, base: str, token: str, usr_rep: dict):
        self.token = token
        self.base = base
        self.group_name = group_name
        self.session = session
        self.args = args
        self.usr_rep = usr_rep
        self.usr_id = args.get('usr_id')

    @classmethod
    async def create(cls, session: ClientSession, args: dict):

        root = 'auth' if args.get('KEYCLOAK_COMPATIBILITY') == KeycloakCompatibility.FIFTEEN.value else ''
        host = args.get('KEYCLOAK_HOST')
        base = f"{host}{root}" if host[-1] == '/' else f"{host}/{root}"
        token = await get_token(args=args, session=session, base=base)

        usr_info_list = args.get('usr_info').split(",") if args.get('usr_info') else []
        usr_keys = ["username", "firstName", "lastName"]
        usr_rep = {usr_keys[i]: item for i, item in enumerate(usr_info_list)}

        group_name = args.get('group_name')
        self = cls(group_name=group_name, session=session, args=args, token=token, base=base,
                   usr_rep=usr_rep if usr_rep else None)
        return self

    async def get_user(self, url: Optional[str] = None):
        """
        Gets the user from Keycloak with the given id using the '/auth/admin/realms/{realm}/users/{id}' REST API call.

        :param url: the url to get user resource
        :param access_token: the access token to use (required).
        :param session: the client session (required).
        :param id_: the user id (required).
        :return: a Person object.
        :raises ClientResponseError if an error occurred or the person was not found.
        """
        headers = {'Authorization': f'Bearer {self.token}', 'cache-control': 'no-cache'}

        try:
            if url:
                user_url = url
            elif self.usr_rep.get('username'):
                user_url = URL(self.base) / 'admin' / 'realms' / self.args.get \
                    ('KEYCLOAK_REALM') / 'users'
            else:
                print('User exists,provide user id instead')
                return None

            return await get(session=self.session, url=user_url, headers=headers, params={'username': self.usr_rep.get('username')})


        except ClientResponseError as e:
            return None
        return user_json

    async def post_user(self) -> Union[web.Response, None]:
        """
        Creates the user

        :return: an aiohttp Response
        """
        try:
            if self.usr_rep:
                realm_name = self.args.get('KEYCLOAK_REALM', None)
                url = URL(self.base) / 'admin/realms' / realm_name / 'users'
                headers = {hdrs.AUTHORIZATION: f'Bearer {self.token}',
                           hdrs.CONTENT_TYPE: 'application/json'}
                self.usr_rep['enabled'] = True

                return await post_json(session=self.session, url=url, headers=headers, json=self.usr_rep)
            else:
                return None
        except ClientResponseError as ce:
            if ce.status == 409:
                print("User already exists")
                return None

    async def post_client_role(self) -> web.Response:
        """
        Posts the provided keycloak-admin.
        :param request: the HTTP request.
        :return: a Response object with a status of Created and the object's URI in the
        """

        client_resp, client_uuid = await self._get_client_uuid()
        realm_name = self.args.get('KEYCLOAK_REALM', None)
        role: str = self.args.get('role')
        org_name = self.args.get('org_name')

        if not realm_name or not client_uuid:
            return web.HTTPBadRequest()
        try:
            data = {"name": role, "description": org_name}
            data = data if len(data) > 0 else None

            url = URL(self.base) / 'admin/realms' / realm_name / 'clients' / client_uuid / 'roles'
            headers = {hdrs.AUTHORIZATION: f'Bearer {self.token}',
                       hdrs.CONTENT_TYPE: 'application/json'}

            return await post(session=self.session, url=url, headers=headers, data=json.dumps(data).encode('utf-8'))
        except ClientResponseError as e:
            return None

    async def post_group_children(self, location: str, name: str):
        """
        Post realm group children
        :param location this is the url to access the previously created group
        :param name this is the group name of the child being created
        :return: a Response object
        """
        try:
            headers = {hdrs.AUTHORIZATION: f'Bearer {self.token}',
                       hdrs.CONTENT_TYPE: 'application/json'}
            data = {"name": name}
            url = f"{location}/children"
            resp = await post_json(session=self.session, url=url, headers=headers, json=data)

        except ClientResponseError as ce:
            if ce.status == 409:
                return None
            raise ValueError(f"Cannot create group an error occurred:\n{str(ce)}")
        return resp

    async def post_group(self) -> Optional[web.Response]:
        """
        Post realm group
        :return: a Response object
        """
        try:

            realm_name = self.args.get('KEYCLOAK_REALM', None)
            data = {"name": f"{self.args.get('org_name')}"}
            url = URL(self.base) / 'admin' / 'realms' / realm_name / 'groups'
            headers = {hdrs.AUTHORIZATION: f'Bearer {self.token}',
                       hdrs.CONTENT_TYPE: 'application/json'}
            resp = await post_json(session=self.session, url=url, headers=headers, json=data)

        except ClientResponseError as ce:
            if ce.status == 409:
                return None
            raise ValueError(f"Cannot create group an error occured:\n{str(ce)}")
        return resp

    async def post_group_role(self, url: str) -> web.Response:
        """
        Assigns group's client role
        :param url: the url of the group with its id
        :return: a Response object with a status of Created and the object's URI in the

        """
        client_resp, client_uuid = await self._get_client_uuid()
        realm_name = self.args.get('KEYCLOAK_REALM', None)

        data = {}
        data['name'] = self.args.get('role')
        role_resp, role_id = await self._get_client_role_id(client_uuid=client_uuid, role_obj=data)
        if not realm_name or not client_uuid or not url or not role_id:
            return web.HTTPBadRequest()
        data['id'] = role_id
        data['composite'] = False
        data['clientRole'] = True
        try:
            group_role_url = f"{url}/role-mappings/clients/{client_uuid}"
            headers = {hdrs.AUTHORIZATION: f'Bearer {self.token}',
                       hdrs.CONTENT_TYPE: 'application/json'}
            # return await post_json(session=self.session, url=group_role_url, headers=headers, json=data)
            return await post(session=self.session, url=group_role_url, headers=headers,
                              data=json.dumps([data]).encode('utf-8'))
        except ClientResponseError as ce:
            if ce.status == 409:
                return None
            raise ValueError(f"Assign role {data['name']} to group, an error occurred:\n{str(ce)}")

    async def post_user_role(self) -> web.Response:
        """
        Posts user's role
        :param request: the HTTP request.
        :return: a Response object with a status of Created and the object's URI in the

        """
        client_resp, client_uuid = await self._get_client_uuid()
        realm_name = self.args.get('KEYCLOAK_REALM', None)
        data = {"name": self.args.get('role')}
        # data = { "name": f"arn:aws:iam::{self.args.get('account_number')}:role/{self.args.get('role')}" }
        data = data if len(data) > 0 else None
        # this is the SUBJECT claim for oidc
        user_id = self.usr_id
        if not realm_name or not client_uuid or not data or not user_id:
            return web.HTTPBadRequest()

        role_resp, role_id = await self._get_client_role_id(client_uuid=client_uuid, role_obj=data)
        data['id'] = role_id
        data['composite'] = False
        data['clientRole'] = True
        try:
            url = URL(self.args.get('KEYCLOAK_HOST')) \
                  / self.base / 'admin' / 'realms' / realm_name / 'users' / user_id / 'role-mappings/clients' / client_uuid
            headers = {hdrs.AUTHORIZATION: f'Bearer {self.token}',
                       hdrs.CONTENT_TYPE: 'application/json'}
            return await post(session=self.session, url=url, headers=headers, data=json.dumps([data]).encode('utf-8'))
        except ClientResponseError as e:
            return None

    async def _get_client_uuid(self):
        """
        Coroutine for getting the client uuid .
        :param request: the aiohttp request (required).
        :param token: the token needed to authorize the request (required).
        :returns Response with HTTP OK and the uuid of the client retrieved from the GET
        """
        _logger = logging.getLogger(__name__)
        realm_name = self.args.get("KEYCLOAK_REALM", None)
        client_name = self.args.get("keycloak_client", None)
        if not self.token or not client_name or not realm_name:
            return web.HTTPBadRequest()

        url = URL(self.base) / 'admin/realms' / realm_name / 'clients'
        params = {'clientId': client_name}
        headers = {hdrs.AUTHORIZATION: f'Bearer {self.token}'}
        resp, result = await get(session=self.session, url=url, params=params, headers=headers)
        return resp, result.get('id') if result.get('id') else None

    async def _get_client_role_id(self, client_uuid: str, role_obj: dict):
        """
        Coroutine for getting the client role id.
        :param request: the aiohttp request (required).
        :param token: the token needed to authorize the request (required).
        :param client_uuid: the client_uuid need to identify the client (required).
        :returns Response with HTTP OK and the id of role retrieved from the GET
        """
        _logger = logging.getLogger(__name__)
        realm_name = self.args.get("KEYCLOAK_REALM", None)
        # putting role name in post body to avoid encoding issues in the url
        role_name = role_obj['name'] if len(role_obj) > 0 and role_obj['name'] else None
        safe_role_name = urllib.parse.quote_plus(role_name)

        if not self.token or not client_uuid or not realm_name or not role_name:
            return web.HTTPBadRequest()

        # url = str(URL(self.args.get('KEYCLOAK_HOST')) / self.base / 'admin' / 'realms' / realm_name / 'clients' / client_uuid / 'roles' / role_name)
        #This URL will fail if it has a // after the domain, which is possible depending on the Keycloak version
        url = f"{self.base if self.base[-1] == '/' else self.base + '/'}admin/realms/{realm_name}/clients/{client_uuid}/roles/{safe_role_name}"
        headers = {hdrs.AUTHORIZATION: f'Bearer {self.token}'}
        resp, result = await get(session=self.session, url=url, headers=headers)
        return resp, result.get('id') if result.get('id') else None

    async def get_group(self, url: str, sub_group: Optional = None) -> Union[str, None]:
        """
        Get group by name
        :param url: url to group
        :param sub_group to query for and return constructed location to (optional)
        :return: location to group or subgroup
        """
        realm_name = self.args.get("KEYCLOAK_REALM", None)
        #This  URL will fail if it has a // after the domain, which is possible depending on the Keycloak version
        u = f"{self.base if self.base[-1] == '/' else self.base + '/'}admin/realms/{realm_name}/groups"
        # putting role name in post body to avoid encoding issues in the url

        if not realm_name or not url:
            raise ValueError('Missing parameters needed to request group by name')
        try:
            headers = {hdrs.AUTHORIZATION: f'Bearer {self.token}'}
            resp, result = await get(session=self.session, url=url, headers=headers)
            found_group = list((filter(lambda x:  sub_group == x['name'], result.get('subGroups'))))
            sub_group_id = found_group[0].get('id') if sub_group else result.get('id')
            return f"{u}/{sub_group_id}" if sub_group_id else None

        except ClientResponseError as e:
            return None

    async def get_group_by_name(self, name: str) -> Union[str, None]:
        """
        Get group by name
        :param name: the group name to find (required)

        :return: The aiohttp Web Response
        """
        realm_name = self.args.get("KEYCLOAK_REALM", None)
        # putting role name in post body to avoid encoding issues in the url

        if not realm_name or not name:
            raise ValueError('Missing parameters needed to request group by name')
        try:
            #This particular URL will fail if it has a // after the domain, which is possible depending on the Keycloak version
            url = f"{self.base if self.base[-1] == '/' else self.base + '/'}admin/realms/{realm_name}/groups"
            headers = {hdrs.AUTHORIZATION: f'Bearer {self.token}'}
            resp, result = await get(session=self.session, url=url, headers=headers, params={"search": name})

            return f"{url}/{result.get('id')}" if result.get('id') else None
        except ClientResponseError as e:
            return None


    async def put_user_in_group(self, url: str):
        client_resp, client_uuid = await self._get_client_uuid()
        realm_name = self.args.get('KEYCLOAK_REALM', None)
        data = {}
        # this is the SUBJECT claim for oidc
        user_id = self.usr_id
        if not realm_name or not client_uuid or not user_id:
            raise web.HTTPBadRequest()

        try:
            url = URL(self.base) / 'admin' / 'realms' / realm_name / 'users' / user_id / url[url.index('groups'):]
            headers = {hdrs.AUTHORIZATION: f'Bearer {self.token}',
                       hdrs.CONTENT_TYPE: 'application/json'}
            return await put_json(session=self.session, url=url, headers=headers, json=data)
        except ClientResponseError as e:
            return None


def parse_args():
    parser = argparse.ArgumentParser(description="This script allows you programmatically create aws accounts")
    parser.add_argument("-r", "--role", help="Role to be assigned to the user in keycloak", required=True)
    parser.add_argument("-u", "--usr_info", help="Provide the user info to create user or if user exists,"
                                                  " User ID. This parameter needs to be in format (username, first name, last name) seperated by commas.",
                        required=False)
    parser.add_argument("-o", "--org_name", help="Name of the organization", required=True)
    parser.add_argument('-c', '--keycloak_client',
                        help="This is the name of the client for which the token is generated, default is core-browser",
                        nargs='?', default="core-browser", type=str)
    parser.add_argument("-g", "--group_name",
                        help="group name or sub-group. note prefix name with * if admin group. Also if not admin a"
                             " sub-group will be created under the group named after the org name", required=True)

    return vars(parser.parse_args())


def get_env_data_as_dict(path: str) -> dict:
    with open(path, 'r') as f:
        return dict((k, v.strip("'\" ")) for k, v in (tuple(line.strip().split('=')) for line in f.readlines()
                                                      if not line.startswith('#') and not line.startswith(
            tuple(w for w in whitespace))))


class MongoUser:

    def __init__(self, args: dict):
        self.db: motor_asyncio.AsyncIOMotorDatabase = None
        self.args = args

    async def init_database(self):
        try:
            mongo_uri = f"mongodb://{self.args.get('MONGO_HEA_USERNAME')}:{self.args.get('MONGO_HEA_PASSWORD')}@{self.args.get('MONGO_HOSTNAME')}:{self.args.get('MONGO_PORT')}/?authMechanism=SCRAM-SHA-1&authSource={self.args['MONGO_HEA_DATABASE']}&tls={self.args.get('MONGO_USE_TLS', 'false')}"
            client = motor_asyncio.AsyncIOMotorClient(mongo_uri)
            db = client[self.args.get('MONGO_HEA_AUTH_SOURCE')]
        except  Exception as e:
            print(f"Failed to connect to mongo with this string {mongo_uri}")
            raise e
        self.db = db

    async def error_if_not_unique_org_exists(self):
        try:
            org_name = self.args.get('org_name') if self.args.get('org_name') else None
            coll_org = self.db['organizations']
            orgs = [o async for o in coll_org.find({"display_name": org_name})]

            if len(orgs) > 1:
                raise ValueError(
                    f"Error, Multiple Organizations exist with the name provided:  {org_name}. aborting...")
            elif len(orgs) == 0:
                raise ValueError(f"Error, No Organization with the name provided:  {org_name}. aborting...")

            #Organization exists - continue
        except Exception as e:
            #Organization does not exist - percolate error up and stop main
            raise e


    async def add_org_permissions(self, usr_id: str, group_id: str):
        try:
            org_name = self.args.get('org_name') if self.args.get('org_name') else None
            coll_org = self.db['organizations']
            orgs = [o async for o in coll_org.find({"display_name": org_name})]
            print(f"the sub group id to be added to organization group array: {group_id}")

            if not group_id:
                raise ValueError(f"Error, Group Id for the organization cannot be null. aborting...")

            if len(orgs) > 1:
                raise ValueError(
                    f"Error, Multiple Organizations exist with the name provided:  {org_name}. aborting...")
            elif len(orgs) == 0:
                raise ValueError(f"Error, No Organization with the name provided:  {org_name}. aborting...")
            org_id = orgs[0]['_id']
            shares = [s for o in orgs for s in o['shares'] if (s['user'] == 'system|all') and (s['type'] == 'heaobject.root.ShareImpl') and (len(s['permissions']) == 1) and (s['permissions'][0] == 'CHECK_DYNAMIC') ]

            if shares:
                print("skipping adding CHECK_DYNAMIC share because it already exists for this organization")
            else:
                await coll_org.update_one({'_id': org_id},
                                          {'$push': {
                                              'shares': {
                                                  'invite': None,
                                                  'permissions': ["CHECK_DYNAMIC"],
                                                  'type': "heaobject.root.ShareImpl",
                                                  'user': 'system|all'
                                              }
                                          }
                                          }
                                          )
            # We now need to look through the admin_ids, manager_ids, and member_ids arrays and make sure this user is
            # added to the appropriate ones (unless they are the owner/PI)
            group_name = self.args.get('group_name') if self.args.get('group_name') else None

            pi_id = orgs[0]['principal_investigator_id']
            owner_id = orgs[0]['owner']

            # PI/owner case (ignores adding the user to other groups)
            # The PI/owner should already be listed due to the organization creation script.
            if pi_id == usr_id or owner_id == usr_id:
                # Do nothing.
                print(f"skipping adding group entry for user because they are the organization's {'owner' if owner_id == usr_id else 'pi'}")

            # admin case
            elif group_name == 'Admins' or group_name[0] == '*':
                existing_admin_id = [id for o in orgs for id in o['admin_ids'] if (id == usr_id) ]
                existing_admin_group_id = [g_id for o in orgs for g_id in o['admin_group_ids'] if (g_id == group_id)]

                if existing_admin_id:
                    print("skipping adding admin_ids entry for user because it already exists on this organization")
                else:
                    await coll_org.update_one({'_id': org_id}, {'$push': { 'admin_ids': usr_id }})

                if existing_admin_group_id:
                    print("skipping adding admin_group_ids entry for the group because it already exists on this organization")
                else :
                    await coll_org.update_one({'_id': org_id}, {'$push': { 'admin_group_ids': group_id }})

            # manager case
            elif group_name == 'Managers':
                existing_manager_id = [id for o in orgs for id in o['manager_ids'] if (id == usr_id) ]
                existing_manager_group_id = [g_id for o in orgs for g_id in o['manager_group_ids'] if (g_id == group_id)]

                if existing_manager_id:
                    print("skipping adding manager_ids entry for user because it already exists on this organization")
                else:
                    await coll_org.update_one({'_id': org_id}, {'$push': { 'manager_ids': usr_id }})

                if existing_manager_group_id:
                    print("skipping adding manager_group_ids entry for the group because it already exists on this organization")
                else:
                    await coll_org.update_one({'_id': org_id}, {'$push': { 'manager_group_ids': group_id }})

            # member case
            elif group_name == 'Members':
                existing_member_id = [id for o in orgs for id in o['member_ids'] if (id == usr_id) ]
                existing_member_group_id = [g_id for o in orgs for g_id in o['member_group_ids'] if (g_id == group_id)]

                if existing_member_id:
                    print("skipping adding member_ids entry for user because it already exists on this organization")
                else:
                    await coll_org.update_one({'_id': org_id}, {'$push': { 'member_ids': usr_id }})

                if existing_member_group_id:
                    print("skipping adding member_group_ids entry for the group because it already exists on this organization")
                else:
                    await coll_org.update_one({'_id': org_id}, {'$push': { 'member_group_ids': group_id }})

            else:
                raise ValueError(f"Error, Group (-g argument) {group_name} did not match *.*, Managers nor Members, and user is not PI/Owner.")

        except Exception as e:
            print(str(e))

    async def create_user_credentials(self, name: str, usr_id: str):
        try:
            pattern = r'arn:aws:iam::(\d+):role.*'
            match = re.search(pattern, self.args.get('role') if self.args.get('role') else None)
            account_id = match.group(1) if match else ''
            org_name = self.args.get('org_name') if self.args.get('org_name') else None
            name_id = f"{name}_{account_id}"
            display_name = f"{account_id} - {org_name}"
            coll_vol = self.db['volumes']
            coll_cred = self.db['credentials']
            col = await coll_cred.find_one({"name": name_id, "owner": usr_id})
            if col:
                raise ValueError(f"User {usr_id} already exists with this name {name_id}")

            new_cred = await coll_cred.insert_one(
                {
                    "created": datetime.now(),
                    "derived_by": None,
                    "derived_from": [],
                    "description": None,
                    "display_name": display_name,
                    "invited": [],
                    "modified": datetime.now(),
                    "name": name_id,
                    "owner": usr_id,
                    "where": "us-east-1",
                    "expiration": "2022-10-26T12:34:41Z",
                    "session_token": "",
                    "role_arn": self.args.get('role'),
                    "shares": [],
                    "source": None,
                    "type": "heaobject.keychain.AWSCredentials",
                    "version": None,
                    "account": "",
                    "password": "",
                    "temporary": True
                })
            new_vol = await coll_vol.insert_one(
                {'created': datetime.now(),
                 'derived_by': None,
                 'derived_from': [],
                 'description': None,
                 'display_name': display_name,
                 'invited': [],
                 'modified': datetime.now(),
                 "name": name_id,
                 "owner": usr_id,
                 "shares": [], "source": None, "type": "heaobject.volume.Volume",
                 "version": None,
                 "file_system_name": "DEFAULT_FILE_SYSTEM",
                 "file_system_type": "heaobject.volume.AWSFileSystem",
                 "credential_id": str(new_cred.inserted_id),
                 "account": {
                     "actual_object_id": str(account_id),
                     "actual_object_type_name": "heaobject.account.AWSAccount",
                     "type": "heaobject.account.AccountAssociation"
                   }
                 })


        except Exception as e:
            print(str(e))


async def main(usr_id=None):
    args = parse_args()
    if not args.get('usr_info'):
        print("Invalid please provide either a user id or the user info parameter")
        sys.exit()

    envs = get_env_data_as_dict(".env")
    for k in envs:
        args[k] = envs[k]
    try:

        mu = MongoUser(args=args)
        await mu.init_database()

        await mu.error_if_not_unique_org_exists()

        async with aiohttp.ClientSession() as session:

            kc = await KeycloakAdmin.create(args=args, session=session)
            resp = await kc.post_user()
            location = resp.headers.get('Location') if resp else None

            resp, usr = await kc.get_user(url=location)
            kc.usr_id = usr['id']
            if usr is None:
                raise ValueError("User cannot be found")
            client_role = await kc.post_client_role()
            if client_role is None:
                print(f"{args['role']} cannot be created on the client possibly already exists?")
            # await kc.post_user_role()
            role_coros = []
            # if "*" in args['group_name']:
            #     l = await kc.get_group_by_name(name=args['group_name'])
            #     role_coros.append(kc.post_group_role(url=l))
            #     role_coros.append(kc.put_user_in_group(url=l))
            #
            # else:
            group = await kc.post_group()
            location = group.headers.get('Location') if group else None
            if group is None:
                print(f"{args['role']} cannot be created Group possibly already exists?")
                location = await kc.get_group_by_name(name=args['org_name'])

            #role_coros.append(kc.put_user_in_group(url=location))
            sub_group = await kc.post_group_children(location=location, name=args.get('group_name'))
            l = sub_group.headers.get('Location') if sub_group else None
            if sub_group is None:
                print(f"{args['role']} cannot be created Sub Group possibly already exists?")
                l = await kc.get_group(location, args.get('group_name'))
            sub_group_id = l.split("/")[-1] if l and len(l.split("/")) > 0 else None
            role_coros.append(kc.post_group_role(url=l))
            role_coros.append(kc.put_user_in_group(url=l))

            resp = await asyncio.gather(*role_coros, return_exceptions=True)

            await mu.add_org_permissions(kc.usr_id, sub_group_id)
            # await mu.create_user_credentials(name=f"{usr['firstName']}_{usr['lastName']}", usr_id=kc.usr_id)
    except Exception as e:
        print(e)


async def get_token(args: dict, session: ClientSession, base: str) -> str:
    """
    Request an access token from Keycloak.

    :param args: script args (required).
    :param session: aiohttp session (required)
    :param base: the base url for requests to keycloak
    :return: the access token or None if not found.
    """
    logger = logging.getLogger(__name__)

    token_url = URL(base) / 'realms' / args[
        'KEYCLOAK_REALM'] / 'protocol' / 'openid-connect' / 'token'

    logger.debug('Requesting new access token using credentials')

    if args['KEYCLOAK_ADMIN_SECRET']:
        secret = args['KEYCLOAK_ADMIN_SECRET']
        logger.debug('Read secret from config')
    else:
        raise ValueError('No secret defined')

    token_body = {
        'client_secret': secret,
        'client_id': 'admin-cli',
        'grant_type': 'client_credentials'
    }
    # logger.debug('Going to verify ssl? %r', self.verify_ssl)
    async with session.post(token_url, data=token_body, verify_ssl=True) as response_:
        content = await response_.json()
        logging.getLogger(__name__).debug(f'content {content}')
        access_token = content['access_token']
    return access_token


def handle_get(data) -> web.Response:
    """
    Coroutine for sending an HTTP GET Status in the Response.
    :param resp: the aiohttp resp (required).
    :param data: the body of the post (optional) .
    :returns a web Response with the HTTP OK Status or HTTP Not Found if item wasn't found
    """
    if not data:
        return web.HTTPNotFound()
    return web.HTTPOk(body=json.dumps(data), headers={hdrs.CONTENT_TYPE: 'application/json'})


async def get(session: ClientSession, url: str, params: Optional[dict] = None, headers: Optional[dict] = None) -> Tuple[
    web.Response, dict]:
    """
   Coroutine that performs an HTTP GET.
   :param request: the aiohttp request (required).
   :param url: the URL (str or URL) of the resource (required).
   :param headers: optional dict of headers.
   :returns a web Response and the object
   """
    _logger = logging.getLogger(__name__)
    _logger.debug('Getting content at %s with headers %s', url, headers)
    async with session.get(url, params=params, headers=headers, raise_for_status=False) as response_:
        response_.raise_for_status()
        result = await response_.json()
        result = [result] if type(result) == dict else result
        _logger.debug('Client returning %s', result)

        if result:
            return handle_get(data=result[0]) if len(result) == 1 else result, result[0]
        else:
            raise ValueError(f'Result from {url} missing')


def handle_post(resp: ClientResponse, data: Optional[dict] = None) -> web.Response:
    """
    Coroutine sending an HTTP Post Status in the Response.
    :param resp: the aiohttp resp (required).
    :param data: the body of the post (optional) .
    :returns a web Response with the HTTP Created Status
    """
    if data:
        return web.HTTPCreated(
            headers={hdrs.LOCATION: resp.headers[hdrs.LOCATION]} if resp.headers.get(hdrs.LOCATION) else None, body=data
        )
    return web.HTTPCreated(
        headers={hdrs.LOCATION: resp.headers[hdrs.LOCATION]} if resp.headers.get(hdrs.LOCATION) else None)


def handle_put(resp: ClientResponse, data: Optional[dict] = None) -> web.Response:
    """
    Coroutine sending an HTTP PUT Status in the Response.
    :param resp: the aiohttp resp (required).
    :param data: the body of the put (optional) .
    :returns a web Response with the HTTP Created Status
    """
    if data:
        return web.HTTPCreated(
            headers={hdrs.LOCATION: resp.headers[hdrs.LOCATION]} if resp.headers.get(hdrs.LOCATION) else None, body=data
        )
    return web.HTTPNoContent(
        headers={hdrs.LOCATION: resp.headers[hdrs.LOCATION]} if resp.headers.get(hdrs.LOCATION) else None)


async def post_with_body(session: ClientSession, url: str, data, headers: Optional[dict] = None):
    """
    Coroutine that performs an HTTP Post and returns result in POST body.
    :param request: the aiohttp request (required).
    :param url: the URL (str or URL) of the resource (required).
    :param data: the body of the post (required) .
    :param headers: optional dict of headers.
    :returns a web Response with the HTTP Created Status and the json result
    """
    async with session.post(url=url, data=data, headers=headers, raise_for_status=False) as response_:
        response_.raise_for_status()
        result = await response_.json()
        return handle_post(response_), result['access_token'] if result else None


async def post_json(session: ClientSession, url: str, json: dict, headers: Optional[dict] = None):
    """
   Coroutine that performs an HTTP Post w and returns .
   :param request: the aiohttp request (required).
   :param url: the URL (str or URL) of the resource (required).
   :param json: the json body (dict) in request (required) .
   :param headers: optional dict of headers.
   :returns a web Response with the HTTP Created Status
   """
    async with session.post(url=url, json=json, headers=headers, raise_for_status=True) as response_:
        return handle_post(response_)


async def post(session: ClientSession, url: str, data, headers: Optional[dict] = None) -> web.Response:
    """
    Coroutine that performs an HTTP Post and returns result in POST body.
    :param request: the aiohttp request (required).
    :param url: the URL (str or URL) of the resource (required).
    :param data: the body of the post (required) .
    :param headers: optional dict of headers.
    :returns a web Response with the HTTP Created Status
    """
    async with session.post(url=url, data=data, headers=headers, raise_for_status=False) as response_:
        response_.raise_for_status()
        return handle_post(response_)


async def put_json(session: ClientSession, url: str, json: dict, headers: Optional[dict] = None):
    """
   Coroutine that performs an HTTP PUT and returns .
   :param url: the URL (str or URL) of the resource (required).
   :param json: the json body (dict) in request (required) .
   :param headers: optional dict of headers.
   :returns a web Response with the HTTP Created Status
   """
    async with session.put(url=url, json=json, headers=headers, raise_for_status=True) as response_:
        return handle_put(response_)


async def delete(session, url, headers=None) -> web.Response:
    """
    Coroutine that deletes a HEAObject.
    :param request: the aiohttp request (required).
    :param url: the URL (str or URL) of the resource (required).
    :param headers: optional dict of headers.
    """
    _logger = logging.getLogger(__name__)
    _logger.debug('Deleting %s', str(url))
    async with session.delete(url, headers=headers, raise_for_status=False) as response_:
        response_.raise_for_status()
        return web.HTTPNoContent()


if __name__ == "__main__":
    asyncio.run(main())
