db.components.deleteOne({name: 'heaserver-storage'})
db.components.updateOne({name: 'heaserver-folders-aws-s3'}, {$push: {resources: {file_system_name: 'DEFAULT_FILE_SYSTEM', resource_collection_type_display_name: 'AWS S3 Storage Summaries', type: 'heaobject.registry.Resource', type_display_name: 'Resource', base_path: 'volumes', resource_type_name: 'heaobject.storage.AWSS3Storage', file_system_type: 'heaobject.volume.AWSFileSystem'}}});
db.components.updateOne({name: 'heaserver-folders-aws-s3'}, {$set: {'resources.$[e1].resource_collection_type_display_name': 'AWS S3 Files'}}, {arrayFilters: [{'e1.resource_type_name': 'heaobject.data.AWSS3FileObject'}]});
db.credentials.updateMany({}, [{$set: {lifespan_class: '$lifespan'}}]);
db.credentials.updateMany({}, [{$unset: 'lifespan'}]);
db.credentials.updateMany({managed: true, display_name: /24hr$/}, {$set: {lifespan: 24*60*60}});
db.credentials.updateMany({managed: true, display_name: /48hr$/}, {$set: {lifespan: 48*60*60}});
db.credentials.updateMany({managed: true, display_name: /72hr$/}, {$set: {lifespan: 72*60*60}});
db.credentials.updateMany({managed: true, display_name: /168hr$/}, {$set: {lifespan: 168*60*60}});
db.credentials.updateMany({temporary: true}, {$set: {lifespan: 12*60*60}});