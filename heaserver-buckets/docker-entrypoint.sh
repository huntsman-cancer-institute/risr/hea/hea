#!/bin/sh
set -e

cat > .hea-config.cfg <<EOF
[DEFAULT]
Registry=${HEASERVER_REGISTRY_URL:-http://heaserver-registry:8080}
MessageBrokerEnabled=${HEA_MESSAGE_BROKER_ENABLED:-true}

[MessageBroker]
Hostname = ${RABBITMQ_HOSTNAME:-rabbitmq}
Port = 5672
Username = ${RABBITMQ_USERNAME:-guest}
Password = ${RABBITMQ_PASSWORD:-guest}
EOF

cat > .hea-logging-config.cfg <<EOF
[loggers]
keys=root

[handlers]
keys=defaulthand

[formatters]
keys=defaultform

[logger_root]
level=INFO
handlers=defaulthand

[handler_defaulthand]
class=StreamHandler
level=NOTSET
formatter=defaultform
args=(sys.stdout,)

[formatter_defaultform]
format=%(levelname)s:%(name)s:%(message)s
EOF

exec heaserver-buckets -f .hea-config.cfg -b ${HEASERVER_BUCKETS_URL:-http://localhost:8080} -l .hea-logging-config.cfg


