#!/bin/sh
set -e

cat > .hea-config.cfg <<EOF
[DEFAULT]
Registry=${HEASERVER_REGISTRY_URL:-http://heaserver-registry:8080}

[MongoDB]
ConnectionString=mongodb://${MONGO_HEA_USERNAME}:${MONGO_HEA_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT:-27017}/${MONGO_HEA_DATABASE}?authMechanism=DEFAULT&authSource=${MONGO_HEA_AUTH_SOURCE:-admin}&tls=${MONGO_USE_TLS:-false}&appname=heaserver-organizations&compressors=zlib

[MessageBroker]
Hostname = ${RABBITMQ_HOSTNAME:-rabbitmq}
Port = 5672
Username = ${RABBITMQ_USERNAME:-guest}
Password = ${RABBITMQ_PASSWORD:-guest}
EOF

cat > .hea-logging-config.cfg <<EOF
[loggers]
keys=root

[handlers]
keys=defaulthand

[formatters]
keys=defaultform

[logger_root]
level=INFO
handlers=defaulthand

[handler_defaulthand]
class=StreamHandler
level=NOTSET
formatter=defaultform
args=(sys.stdout,)

[formatter_defaultform]
format=%(levelname)s:%(name)s:%(message)s
EOF

exec heaserver-organizations -f .hea-config.cfg -b ${HEASERVER_ORGANIZATIONS_URL:-http://localhost:8087} -l .hea-logging-config.cfg


