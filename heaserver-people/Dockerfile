FROM registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea-python-base:3.12.9 as intermediate
ARG PROJECT_VERSION
WORKDIR /home/app
RUN pip install heaserver-people==$PROJECT_VERSION
RUN pip download --no-deps --no-binary :all: heaserver-people==$PROJECT_VERSION
RUN mkdir sdist
RUN tar xvzf heaserver_people-$PROJECT_VERSION.tar.gz --directory sdist --strip-components 1
WORKDIR /home/app/sdist
RUN pip install "pytest~=8.3.3" "pytest-xdist[psutil]~=3.6.1" "mypy==1.11.1"
RUN pytest -n auto
RUN mypy

FROM registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea-python-base:3.12.9
RUN useradd --create-home app
WORKDIR /home/app
COPY docker-entrypoint.sh .
RUN dos2unix ./docker-entrypoint.sh
ENV HEASERVER_PEOPLE_URL=https://localhost:8443/api
COPY --from=intermediate /usr/local/lib/python3.12/site-packages /usr/local/lib/python3.12/site-packages
COPY --from=intermediate /usr/local/bin/heaserver-people /usr/local/bin
USER app
ENTRYPOINT [ "sh", "docker-entrypoint.sh" ]
EXPOSE 8080