@echo off

set usage=Usage: build-and-push-docker-image.bat project-name version
set helptext=build-and-push-docker-image.bat - Builds and pushes a docker image to gitlab.com.
set usageoptions=Options:
set usageoptionsprojnm=    project-name: the package name.
set usageoptionsprojvn=    version: the project version.

if "%~1"=="-h" goto HELP
if "%~1"=="--help" goto HELP
if "%~1"=="" goto ERR
if "%~2"=="" goto ERR
goto MAIN

:ERR
echo %usage%
exit /b 1

:HELP
echo %helptext%
echo.
echo %usage%
echo %usageoptions%
echo %usageoptionsprojnm%
echo %usageoptionsprojvn%
exit

:MAIN
echo Working...
setlocal enabledelayedexpansion

echo Building and pushing docker image for %1 version %2 and setting it to latest...

set olddir = %~dp0
cd %1 >nul
docker build --no-cache --build-arg PROJECT_VERSION="%2" -t "registry.gitlab.com/huntsman-cancer-institute/risr/hea/%1:%2" -t "registry.gitlab.com/huntsman-cancer-institute/risr/hea/%1:latest" .
if not "%errorlevel%"=="0" (set retval=1) else set retval=0
if "%retval%"=="0" (docker push "registry.gitlab.com/huntsman-cancer-institute/risr/hea/%1:%2") else echo Building docker image failed && goto CLEANUP
if not "%errorlevel%"=="0" (set retval=1) else set retval=0
if "%retval%"=="0" (docker push "registry.gitlab.com/huntsman-cancer-institute/risr/hea/%1:latest") else echo Pushing docker image failed && goto CLEANUP
if not "%errorlevel%"=="0" (set retval=1) else set retval=0
if "%retval%"=="0" (echo Done) else echo Pushing docker image failed && goto CLEANUP

:CLEANUP
cd %olddir% >nul
exit /b %retval%
