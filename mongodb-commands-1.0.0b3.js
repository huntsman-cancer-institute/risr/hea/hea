// HEA_ACTIVITY INDEX
db.desktopobjectactions.dropIndex("idx_desktop_object_actions_status_updated");
db.desktopobjectactions.createIndex({owner: 1, "shares.user": 1, status_updated: -1}, {name: "idx_desktopobjectactions_get_all_desktopobjectactions"});
db.desktopobjectactions.createIndex({owner: 1, "shares.user": 1, new_object_type_name: 1, status: 1, status_updated: -1}, {name: "idx_desktopobjectactions_recently_accessed_views"});
